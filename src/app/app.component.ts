import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
/**
 * Services Import
 */
import { SqliteService } from './services/sqlite/sqlite.service';
import { JwtService } from './services/jwt/jwt.service';
import { ToastService } from './services/toast/toast.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  // Props of side menu content
    public test = 'autres';
    public storage = 'test';
    public appPages = [
    {
      title: 'Profil',
      url: '/profil',
      icon: 'contact'
    },
    {
      title: 'Visite',
      url: '/visit',
      icon: 'briefcase'
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private sql: SqliteService,
    private jwt: JwtService,
    private toast: ToastService
  ) {
    this.initializeApp();
    this.allowStatusBar();
    this.initDB()
    .then(() => {
      this.jwt.getToken()
          .then((response) => {
              if (response === undefined) {
                  this.logout();
              } else {
                  this.jwt.checkValidity(response[0].token)
                      .then((resp) => {
                          if (resp === true) {
                              this.jwt.storageClientId()
                                  .then(() => {
                                      this.login();
                                  });
                          } else {
                              this.logout();
                          }
                      })
                      .catch(() => {
                          this.toast.logMessage('Une erreur s\'est produite', 'warning')
                              .then(() => {
                                  this.logout();
                              });
                      });
              }
          })
          .catch(() => {
            this.toast.logMessage('Une erreur s\'est produite', 'warning')
                .then(() => {
                    this.logout();
                });
          });
    });
  }
  // Method to init
  initializeApp() {
    this.platform.ready()
        .then(() => {
          this.statusBar.styleDefault();
          this.splashScreen.hide();
        });
  }
  allowStatusBar() {
// set status bar to white
      this.statusBar.backgroundColorByHexString('#69aec4');
      this.statusBar.overlaysWebView(false);
  }
  // Method to redirect to login page if user is not logged
  logout() { // Redirect to login page
      this.router.navigateByUrl('/login');
  }
  // Method to redirect to home page if user is logged
  login() { // Redirect to profile page
    this.router.navigateByUrl('/profil');
  }
  initDB() { // Init BDD on app launch
    return this.sql.createDB().then(() => {
      this.sql.databaseTables.map((table) => {
        this.sql.createTable(table)
            .then((resp) => {
                console.log(resp);
            });
      });
    });
  }
  logoutJwt() { // logout and erase JWT
    this.jwt.logOut();
  }
}
