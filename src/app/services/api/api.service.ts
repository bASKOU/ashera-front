import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { JwtService } from '../jwt/jwt.service';
import { SqliteService } from '../sqlite/sqlite.service';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  private apiUrl: string;
  private httpOptions: any;

  constructor(public httpClient: HttpClient, private jwt: JwtService, private sqlite: SqliteService) {
    this.apiUrl = 'http://90.113.154.212/ashera/index.php/';
    this.httpOptions =  {
      'Access-Control-Allow-Headers' : '*',
      'Access-Control-Allow-Credentials' : 'true'
    };
  }
  getHttpOptions(options) {
    // Initialize our headers for our http request
    const header = this.httpOptions;
    // Merge options passed in argument with our base header
    // tslint:disable-next-line:forin
    for (const prop in options) {
        header[prop] = options[prop];
    }
    /**
     *  Merge the httpOptions.headers <Object> with the arguments options <Object>
     */
    return {
        headers: new HttpHeaders(header)
    };
  }
  /**
   * Service used for post methods
   * @param data // The data you need to send
   * @param uri // The method used by the Api to manage your data
   * @param options // Headers & http options
   * @param dev // Use final api url or custom one in this service properties
   */
   apiPostRequest(data: any, uri: string, options: any) {
      const httpOptions = this.getHttpOptions(options);
      /**
       * Create a promise for the return of our service
       */
      return new Promise((resolve, reject) => {
          const url = this.apiUrl + uri;
          this.httpClient.post(url, data, httpOptions)
              .subscribe((response) => {
                resolve(response);
              }, (error) => {
                reject(error);
              }
              );
      });
   }

  apiGetRequest(url, query) {
    return this.jwt.makeJwtOption().then((result: any) => {
      if (result) {
        const httpOptions = this.getHttpOptions(result);
        return new Promise((resolve, reject) => {
          this.httpClient.get(this.apiUrl + url + query, httpOptions)
              .subscribe((response) => {
                resolve(response);
              }, (error) => {
                reject(error);
          });
      });
      } else {
        throw Error('Can\'t access to the token');
      }
    });
  }

  async asyncForEach(array: any[], callback){
    for (let index = 0; index < array.length; index++) {
      await callback(array[index], index, array);
    }
  }
  async syncUtilityTables() {
    const tables = ['visite_cas', 'visite_status', 'visite_note'];
    let result = {};
    await this.asyncForEach(tables, async (table: any) => {
      await this.fetchUtilityTable(table)
      .then((utilityTable: any) => {
        result[table] = utilityTable;
        this.dropUtilityTable(table)
        .then(() => {
          this.createUtilityTable(table)
          .then(() => {
            result[table].forEach((dataObj: any) => {
              this.insertFromObj(table, dataObj)
              .then(() => {
              })
              .catch(() => {
              });
            });
          });
        });
      });
    });

  }
  fetchUtilityTable(tableName: any) {
    const uriQuery = tableName.split('_')[1];
    return this.apiGetRequest('get/', uriQuery);
  }

  dropUtilityTable(tableName: any) {
    return this.sqlite.dropTable(tableName);
  }
  createUtilityTable(tableName: any) {
    const databaseObjArray = [
      'visite_cas',
      'visite_note',
      'visite_status',
    ];
    const index = databaseObjArray.indexOf(tableName);
    return this.sqlite.createTable(this.sqlite.databaseTables[index]);
  }

  insertFromObj(tableName: any, dataObj: any) {
    return this.constructInsertQuery(dataObj).then((queryArray: any) => {
      return this.sqlite.insertRow({table: tableName, column: queryArray[0], value: queryArray[1]});
    });
  }

  constructInsertQuery(entity: any) {
    const keys = Object.keys(entity);
    let columns = '';
    let values = keys.length > 1 ? '(' : '';

    return new Promise((resolve, reject) => {
      if (keys.length < 1) {
        reject(new Error('Parameter is not an array/object or it\'s an empty one'));
      } else {
        keys.forEach((value: any, index: number) => {
          columns += ` "${value}"`;
          values += isNaN(entity[value]) ? ` "${entity[value]}"` : ` ${entity[value]}`;
          if (keys.length - 1 === index) {
            values += keys.length > 1 ? ')' : '';
            columns += '';
            resolve([columns, values]);
          } else {
            columns += ',';
            values += ',';
          }
        });
      }
    });
  }
}
