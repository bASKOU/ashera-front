import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toastController: ToastController) { }

  async logMessage(msg: any, color: any) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000,
      color: color,
      buttons: [
        {
          side: 'end',
          role: 'cancel',
          text: 'Fermer',
          handler: () => {
          }
        }
      ]
    });
    toast.present();
  }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      header: 'Toast header',
      message: 'Click to Close',
      position: 'top',
      buttons: [
        {
          side: 'start',
          icon: 'star',
          text: 'Favorite',
          handler: () => {
          }
        }, {
          text: 'Done',
          role: 'cancel',
          handler: () => {
          }
        }
      ]
    });
    toast.present();
  }
}
