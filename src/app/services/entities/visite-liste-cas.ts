import { VisitPhoto } from './visit-photo';

export class VisiteListeCas {
    public id_visite_liste_cas: number; // After insertion
    public id_visite_cas: number; // FK
    public id_visite_partie_ouvrage: number; // FK
    public id_visite_note: number; // FK _NULL
    public id_photo: string; // FK _NULL
    public commentaire: string; // _NULL
    public status: boolean; // DEFAULT 0
    public tableName: string;

    constructor(idCas: number, idPO: number) {
        this.tableName = 'visite_liste_cas';
        this.id_visite_cas = idCas;
        this.id_visite_partie_ouvrage = idPO;
    }

}
