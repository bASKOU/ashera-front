import { VisitEquipement } from './visit-equipement';
import { VisiteListeCas } from './visite-liste-cas';

export class VisitPartieOuvrage {

    public id_visite_partie_ouvrage: number;
    public id_visite: number;
    public id_partie_ouvrage: number;
    public nom_partie_ouvrage: string;
    public status: string;
    public static count: number = 1;
    public tableName: string = 'visite_partie_ouvrage';
    /**
     * Construct for each VistPO, linked to Visit Object
     * @param idVisite 
     * @param idPO 
     * @param namePO 
     */
    constructor(idVisite: number, idPO: number, namePO: string) {
        this.id_visite = idVisite; 
        this.id_partie_ouvrage = idPO; 
        this.nom_partie_ouvrage = namePO; 
    }

    
}
