/**
 * Utilitary table
 */
export class VisitCas {
    
    public id_visite_cas: number;
    public cas: string;
    public notable: boolean;
    public tableName: string;
    private allCases: any[];

    constructor() {
        this.tableName = "visite_cas";
        /* 
            Array fetched with GSO database
        */
        let allCasesFromService = [
            // Id of each object will be it's index + 1
            {cas: 'État du patrimoine', notable: 0},
            {cas: 'Sécurité du site et abords extérieurs', notable: 0},
            {cas: 'Entretien des installations', notable: 0},
            {cas: 'Renouvellement', notable: 0},
            {cas: 'État général du site', notable: 1},
            {cas: 'Propreté du sité', notable: 1},
            {cas: 'État du génie civil', notable: 1},
            {cas: 'Clôture', notable: 1},
            {cas: 'Portail', notable: 1},
            {cas: 'Espaces verts, Haies', notable: 1},
            {cas: 'Peinture, enduits, apsects extérieurs', notable: 1},
            {cas: 'Huisseries', notable: 1},
            {cas: 'Armoire éléctrique', notable: 1},
            {cas: 'Équipements électromécaniques', notable: 1},
            {cas: 'Accessoires hydrauliques', notable: 1},
            {cas: 'Compteurs', notable: 1}
        ];
        this.allCases = allCasesFromService;
    };

    public getCas(idCas: number) {
        return this.allCases[idCas];
    }
    
    public fetchCases(){
        return new Promise((resolve,reject)=>{
            setTimeout(()=>{
                let cases = this.allCases;
                resolve(cases);
            },500)
        });
    }
}
