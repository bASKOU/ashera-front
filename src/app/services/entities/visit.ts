export class Visit {

    public id_visite: number;
    public id_visite_status: number;
    public id_ouvrage: number;
    public id_client: number;
    public nom_client: string;
    public nom_ouvrage: string;
    public commentaire: string;
    public date: string;
    public static count: number = 1;
    public tableName: string = 'visite';

    /**
     * Construct for object Visit
     * @param idOuvrage the one used for the visit
     * @param idAgent can be get with proper service
     */
    constructor(idOuvrage: number, idAgent: number, nameOuvrage: string) {
        this.id_ouvrage = idOuvrage;
        this.id_client = idAgent;
        this.nom_ouvrage = nameOuvrage;
    }
}
