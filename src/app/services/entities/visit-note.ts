/**
 * Utilitary table
 */
export class VisitNote {

    public id_visite_note: number;
    public note: string; // Text to represent the note 
    public value: number; // Absolute number to represent the note ; from 0 to X (in opposition of id that are only positive !)
    public tableName: string;
    private allNotes: any[];

    constructor() {
        this.tableName = 'visite_note';
        /* 
            Array fetched with GSO database
        */
        let allNotesFromService = [ 
            {note: "Absent", value: 0},
            {note: "Mauvais", value: 1},
            {note: "Moyen", value: 2},
            {note: "Bon", value: 3},
            {note: "Très Bien", value: 4},
            {note: "Excellent", value: 5}
        ]; 
        this.allNotes = allNotesFromService
    }

    getNote(idNote: number){
        return this.allNotes[idNote];
    }
}
