export class VisitPhoto {

    public id_visite_photo: number; // After insertion
    public url: string; // path in device to access the phone ; != from photo's name
    public name: string; 
    public commentaire: string; // _NULL
    public favori: boolean; // DEFAULT = 0
    public geoloc_x: number; // _NULL
    public geoloc_y: number; // _NULL
    public tableName: string;

    constructor(path: string, filename: string) {
        this.tableName = 'visite_photo'
        this.url = path;
        this.name = filename;
    }

}
