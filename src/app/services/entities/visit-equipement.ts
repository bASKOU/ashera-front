import { VisitPhoto } from './visit-photo';

export class VisitEquipement {

    public id_visite_equipement: number;
    public id_visite_partie_ouvrage: number;
    public id_photo: string;
    public id_visite_note: number;
    public id_equipement: number;
    public nom_equipement: string;
    public commentaire: string;
    public status: string;
    public static count: number = 1;
    public tableName: string;

    /**
     * Construct for each Equipement, link to VisitPartieOuvrage Object
     * @param idVisitePO
     * @param idEquipement 
     * @param nomEquipement 
     */
    constructor(
        idVisitePO: number,
        idEquipement: number,
        nomEquipement: string
    ) {
        this.tableName = 'visite_equipement';
        this.id_visite_partie_ouvrage = idVisitePO;
        this.id_equipement = idEquipement;
        this.nom_equipement = nomEquipement;
    }

}
