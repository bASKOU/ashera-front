import { Injectable } from '@angular/core';
import { SqliteService } from '../sqlite/sqlite.service';
import { ApiService } from '../api/api.service';

/* All entity class */
import { Visit } from '../entities/visit';
import { VisitPartieOuvrage } from '../entities/visit-partie-ouvrage';
import { VisitEquipement } from '../entities/visit-equipement';

/* Extra services to manage error and output data to the view */
import { ToastService } from '../toast/toast.service';
import { VisitCas } from '../entities/visit-cas';
import { JwtService } from '../jwt/jwt.service';
import { throwError } from 'rxjs';
import { VisiteListeCas } from '../entities/visite-liste-cas';
import { Storage } from '@ionic/storage';
import { VisitPhoto } from '../entities/visit-photo';
import { promise } from 'protractor';


@Injectable({
  providedIn: 'root'
})
export class VisitService {

  private apiUrl: string;
  private tokenAgent: any;
  public syncStructure: any;
  constructor(private sqlite: SqliteService, private api: ApiService, private toast: ToastService, private jwt: JwtService, private storage: Storage) {
    this.tokenAgent = {};
    this.getAgentId()
    .then((idAgent: number) => { 
      console.log('Trying to get clientId', idAgent);
      this.tokenAgent.id = idAgent;
      console.log(this.tokenAgent.id);
    });

    this.syncStructure = {
      'visite_partie_ouvrage': {
        'visite_liste_cas' : {
            'visite_photo' : ''
        },
        'visite_equipement': {
            'visite_photo': ''
        }
      }
    };
  }


  /* Here are all methods used only for sync with GS'O database ! */

  async recursiveObj(obj, structure, id, parentName) {
    //console.log('In recursivity, \n - starting obj :', obj);
    //console.log(' - structure : ', structure);
    //console.log(' - parent : ' + parentName + ' with id : ' + id);
    /* Get the object of previous call or instanciate a new one */
    let result = obj || {};
    let promises = [];
    for(let prop in structure) {
      /* Like our structure, try to append a new prop in our previous object filled with result of a custom function */
      
      let conditionStmt;
      if(prop === 'visite_photo') {
        conditionStmt =  result['id_photo'] ? 'WHERE id_visite_'+ 'photo' + ' IN ('+ result['id_photo']+')' : 'WHERE id_visite_photo = 0';
        //console.log('Parent prop : photo')
        delete result['id_'+'photo'];
        delete result['id_'+ parentName];
      }else{
        conditionStmt = 'WHERE id_' + parentName + ' = ' + id;
        //console.log('Parent prop : ', parentName)
        delete result['id_'+parentName];
      }
      //console.log('prop : ', prop);
      //console.log('condition STMT : ', conditionStmt);
      promises.push(this.sqlite.getRows(prop, '*', conditionStmt)
        .then((res : any) => {
          //console.log('result of fillArray for '+ prop + ' with parent '+ parentName + 'and id ' + id, res);
          result[prop] = res || [];
          //console.log(prop+' array = ',result[prop]);
          if(typeof structure[prop] === 'object'){
            //console.log('Need to recall this recusrive function for ' + prop + ' prop');
            //console.log('For Prop type object \'' + prop + '\'', parentName)
            if(result[prop]){
              result[prop].map(async (value, index, array) => {
                delete result[prop][index]['id_'+parentName];
                let idParent = value['id_'+prop];
                delete result[prop][index]['id_'+prop];
                let nestedArray = await this.recursiveObj(result[prop][index], structure[prop], idParent, prop);
                //console.log('Nested Array for '+ prop + 'and id : '+idParent,nestedArray);
                return nestedArray;
              });
              //console.log('Result : ', result);
              return result[prop];
            }else{
              return result[prop] = [];
            }
          }else{
            //console.log('Not type object ; ', result[prop]);
            //console.log('Result : ', result);
            return result[prop].map(async (value, index) => {
              delete result[prop][index]['id_'+prop];
            });
          }
        })
      )
    }
    return Promise.all(promises).then((res) => {
      //console.log('Result after all promises resolvation : ', res);
      //console.log('Result var = ', result);
      return result;
    })
  }


  fillArray(id, parentName, childName){
    return this.sqlite.getRows(childName, '*', 'WHERE id_'+parentName+' = '+id); 
  }

  syncVisit(idVisit) {
    console.log('Sync launched with id_visite = '+idVisit);
    return this.sqlite.getRows('visite', '*', 'WHERE id_visite = ' + idVisit)
    .then((visit: any) => {
      console.log('Visite from table \'visite\' : ',visit);
      return this.recursiveObj(visit[0], this.syncStructure, idVisit, 'visite');
    })
    .then((res: any) => {
      console.log('Final json to send to the back ! →', res);
      return res;
    })
    .catch((err) => {
      console.error('Error occured while trying to build json for sync ...', err);
      throw Error('Error occured, check log for more informations !');
    })
  }

  syncVisitMethod(idVisit) {
    return new Promise(async (resolve, reject) => {
      let columns = 'commentaire, date, id_client, id_ouvrage, id_visite_status, nom_client, nom_ouvrage';
      const visit = await this.sqlite.getRows('visite', columns, 'WHERE id_visite = '+idVisit);
      console.log('Visit: ', visit);
      this.syncPo(idVisit).then((visitPoArr) => {
        resolve({...visit[0], 'visite_partie_ouvrage':visitPoArr});
      })
    })
  }

  syncPo(idVisit) {
    return new Promise(async (resolve, reject) => {
      let column ='id_partie_ouvrage, nom_partie_ouvrage, status, id_visite_partie_ouvrage';
      const visitPo = await this.sqlite.getRows('visite_partie_ouvrage', column, 'WHERE id_visite = '+ idVisit);
      console.log('VisitPo: ', visitPo);
      let listeCas = visitPo ? visitPo.map((val, index) => {
        let idPo = visitPo[index]['id_visite_partie_ouvrage'];
        return this.syncVisitCas(idPo)
        .then((res) => {
          console.log('Trying to build liste cas array : ', res);
          return {...visitPo[index], 'visite_liste_cas': res};
        })
      }) : [] ;
      Promise.all(listeCas)
      .then((visitPoArray) => {
        let equip = [];
        for(let i = 0; i < visitPoArray.length ; i++){
          let idPO = visitPoArray[i]['id_visite_partie_ouvrage'];
          
          delete visitPoArray[i]['id_visite_partie_ouvrage'];
          equip.push(this.syncVisitEquip(idPO).then((res) => {
            console.log('Trying to build equip array : ', res);
            if(res) {
              return {...visitPoArray[i], 'visite_equipement': res}
            }
          }))
        }

        Promise.all(equip).then((visitPoArr) => { resolve(visitPoArr); });
      });

    })
  }


  syncVisitEquip(idPo) {
    return new Promise(async (resolve, reject) => {
      let columns = 'commentaire, id_equipement, id_photo, id_visite_note, nom_equipement, status'
      const visitEquip = await this.sqlite.getRows('visite_equipement', columns, 'WHERE id_visite_partie_ouvrage = '+ idPo);
      let picture = visitEquip ? visitEquip.map((val, index) => {
        let idPhoto = visitEquip[index]['id_photo'];
        delete visitEquip[index]['id_photo'];
        let photo_columns = 'commentaire, favoris, url, x, y';
        return this.sqlite.getRows('visite_photo', photo_columns, 'WHERE id_visite_photo IN ('+idPhoto+')')
        .then((res) => {
          console.log('Response from sqlite service : ', res);
          let photo = res || [];
          return {...visitEquip[index], 'visite_photo': photo};
        }) 
      }) : [];
      Promise.all(picture).then((resArray) => {
        console.log('Resolve of all promises (pictures) for equip : ', resArray);
        resolve(resArray);
      })
    })
  }


  syncVisitCas(idPo) {
    return new Promise(async (resolve, reject) => {
      let columns = 'commentaire, id_photo, id_visite_cas, id_visite_note, status';
      const visitCas = await this.sqlite.getRows('visite_liste_cas', columns, 'WHERE id_visite_partie_ouvrage = '+ idPo) || [];
      let picture = visitCas ? visitCas.map((val, index) => {
        let idPhoto = visitCas[index]['id_photo'];
        delete visitCas[index]['id_photo'];
        let photo_columns = 'commentaire, favoris, url, x, y';
        return this.sqlite.getRows('visite_photo', photo_columns, 'WHERE id_visite_photo IN ('+idPhoto+')')
        .then((res) => {
          console.log('Response from sqlite service : ', res);
          let photo = res || [];
          return {...visitCas[index], 'visite_photo': photo};
        })
      }) : [];

      Promise.all(picture).then((resArray) => {
        console.log('Resolve of all promises (pictures) for liste cas : ', resArray);
        resolve(resArray);
      })
    })
  }

  syncVisitRaw(visitId) {
    let result = {};
    return this.sqlite.getRows('visite', '*', 'WHERE id_visite = ' + visitId)
    .then((visit: any) => {
      result = visit[0];
      return this.sqlite.getRows('visite_partie_ouvrage', '*', 'WHERE id_visite = ' + result['id_visite']);
    })
    .then((visitePo: any) => {
      result['visite_partie_ouvrage'] = visitePo || [];
      const promisesEquip = result['visite_partie_ouvrage'].map((val, index) => {
        return this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = '+ val['id_visite_partie_ouvrage'])
        .then((equipArray) => {
          let promisePhoto = equipArray.map((val) => {
            return this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN (' + val['id_photo']+ ')')
          });
          return Promise.all(promisePhoto).then((arrayPhotoEquip) => {
            equipArray.forEach((val, index) => {
              equipArray[index]['visite_photo'] = arrayPhotoEquip[index];
            })
          })
        });
      });
      const promisesListeCas = result['visite_partie_ouvrage'].map((val, index) => {
        return this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = '+ val['id_visite_partie_ouvrage']);
      });
      return [promisesEquip, promisesListeCas];
    })
    .then((arrayPromises: any) => {
      return [
        Promise.all(arrayPromises[0]).then((arrayEquip) => {
          //result['visite_partie_ouvrage']['visite_equipement'] = arrayEquip;
          const promisesPictureEquip = arrayEquip.map((val, index) => {
            return this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN ('+ val['id_photo']+')');
          })
          return promisesPictureEquip;
        }),
        Promise.all(arrayPromises[1]).then((arrayListeCas) => {
          result['visite_partie_ouvrage']['visite_liste_cas'] = arrayListeCas;
          const promisesPictureListeCas = arrayListeCas.map((val, index) => {
            return this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN ('+ val['id_photo']+')');
          })
          return promisesPictureListeCas;
        })
      ]
    })
    .then((arrayAllPromises) => {
      arrayAllPromises[0]
    })
  }

  asyncForEach(array, callback) {
    return new Promise((resolve) => {
      let result;
      for(let i = 0; i < array.length; i++){
        if(i === array.length - 1 ){
          resolve(array)
        }
      }
    })  
  }
  async syncVisitAsync(idVisit) {
    const visit = await this.sqlite.getRows('visite', '*', 'WHERE id_visite = '+idVisit);
    let result = visit[0];
    result[0]['visite_partie_ouvrage'] = await this.sqlite.getRows('visite_partie_ouvrage', '*', 'WHERE id_visite = '+ idVisit);
    let indexEquip, indexListCas = 0;
    for await(let val of result[0]['visite_partie_ouvrage']){
      let visiteEquip = await this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = ' + val['id_visite_partie_ouvrage']) || [];
      visiteEquip.forEach(async (value, index) => {
        let photo = await this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN ('+value['id_photo']+')');
        visiteEquip[index]['visite_photo'] = photo || [];
      });
      let listeCas = await this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = ' + val['id_visite_partie_ouvrage']) || [];
      listeCas.forEach(async (value, index) => {
        let photo = await this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN ('+value['id_photo']+')');
        listeCas[index]['visite_photo'] = photo || [];
      });
      result[0]['visite_partie_ouvrage'][indexEquip++]['visite_equipement'] = visiteEquip; 
      result[0]['visite_partie_ouvrage'][indexListCas++]['visite_liste_cas'] = listeCas;
    }
    /* result[0]['visite_partie_ouvrage'].forEach(async (val, index) => {
      let visiteEquip = await this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = ' + val['id_visite_partie_ouvrage']) || [];
      visiteEquip.forEach(async (value, index) => {
        let photo = await this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN ('+value['id_photo']+')');
        visiteEquip[index]['visite_photo'] = photo || [];
      })
      let listeCas = await this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = ' + val['id_visite_partie_ouvrage']) || [];
      listeCas.forEach(async (value, index) => {
        let photo = await this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN ('+value['id_photo']+')');
        listeCas[index]['visite_photo'] = photo || [];
      })
      result[0]['visite_partie_ouvrage'][index]['visite_equipement'] = visiteEquip; 
      result[0]['visite_partie_ouvrage'][index]['visite_liste_cas'] = listeCas;
    }) */

    return result[0];

  }
  /* -- END :: SYNC METHODS */

  /**
   * Fake Service -- Will be replaced by storage access
   */
  getAgentId() {
    return this.storage.get('clientId');
  }
  
  async getPOData(idPo: number) {
    const equip = await this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = '+idPo);
    const listCas = await this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = '+idPo);
    return {
      equipement: equip, 
      listeCas: listCas
    };
  }

  async getUtilitaryTables() {
    const cases = await this.sqlite.getRows('visite_cas');
    const rates = await this.sqlite.getRows('visite_note');
    return {
      cas: cases,
      note: rates
    };
  }

  /* Offlines features */
  updateVisiteData(value, table, column, conditionColumn, conditionValue) {
    // "UPDATE table SET column1 = value1 WHERE id = 1"
    return this.sqlite.updateOneColumn({table, column, columnValue: value, conditionColumn, conditionValue});
  }
  fetchVisitCas() {
    let visitCas = new VisitCas();
    visitCas.fetchCases()
    .then((casesArray: any) => {
      casesArray.forEach((value: any) => {
        this.constructQueryMultiple(value).then((queryArray: any) => {
          let columnQuery = queryArray[0];
          let valueQuery = queryArray[1];
          this.sqlite.insertRow({table: visitCas.tableName, column: columnQuery, value: valueQuery}).then((res: any)=>{
            console.log("After insertion -> ", res);
          });
        });
      });
    });
  }

  constructVisitListeCas(idVisitPo: number = 4) {
    this.sqlite.getRows('visite_cas').then((casesArray: any)=>{
      console.log(casesArray);
      casesArray.forEach((visitCas: VisitCas) => {
        let visitListCas = new VisiteListeCas(visitCas.id_visite_cas, idVisitPo);
        this.constructInsertReplaceQuery(visitListCas)
        .then((queryArray: any) => {
          console.log('queryArray', queryArray);
          this.sqlite.insertOrReplace({table: visitListCas.tableName, column: queryArray[0], value: queryArray[1]})
          .then((res: any)=>{
            console.log('sql res', res);
          });
        })
      }) /* END :: FOREACH */
    }) /* END :: PROMISE */
  }

  /* /!\ Should be place in sqlite service ! */
  constructInsertReplaceQuery(entity: any){
    let columns = [];
    let values = [];
    let keys = Object.keys(entity);

    return new Promise((resolve, reject) => {
      keys.forEach((value: any, index) => {
        if (value !== 'tableName') { //Prop in each entity to know where to insert in sqliteDatabase
          columns.push(value);
          values.push(entity[value]); //To respect integer syntax
          if (keys.length - 1 === index) {
            resolve([columns, values]); // End of our promise
          }
        }
      });
    });
  }

  /**
   * /!\ Problem list : 
   * - Know when a chaining promise IN a foreach are ALL dones (and so for nested promise)
   * - Set an execution time_out that display a toast 'Execution max_time exceeded'
   * - 
   */

  getVisitPoData(idPo: number = 4) {
    // Select all From equip, visite_liste_cas WHERE id_visite_partie_ouvrage = idPO
    // Then : => equip = {id, name, status, id_note} ; liste_cas = {id, id_cas, id_note}
    /**
      SELECT e.* l.* p.*
      FROM visite_partie_ouvrage p
      INNER JOIN visite_equipement e ON e.id_visite_partie_ouvrage = p.id_visite_partie_ouvrage 
      INNER JOIN visite_liste_cas l ON l.id_visite_partie_ouvrage = p.id_visite_partie_ouvrage
      WHERE p.id_visite_partie_ouvrage = idPo
    */
    /* 
    let query1 = 'SELECT * FROM visite_equipement WHERE id_visite_partie_ouvrage = ' + idPo;
    let query2 = 'SELECT * FROM visite_liste_cas WHERE id_visite_partie_ouvrage = '+ idPo;
    let promises = [];
    promises.push(this.sqlite.getRows('visite_liste_cas','*', 'WHERE id_visite_partie_ouvrage = ' + idPo));
    promises.push(this.sqlite.getRows('visite_equipement','*', 'WHERE id_visite_partie_ouvrage = ' + idPo));
    return Promise.all(promises); */
  
    let result = [];
    const conditionPo = 'SELECT * FROM visite_liste_cas WHERE id_visite_partie_ouvrage = ' + idPo;
    const conditionEquipement = 'SELECT * FROM visite_equipement WHERE id_visite_partie_ouvrage = ' + idPo;
    return new Promise ((resolve, reject) => {
      this.sqlite.customQuery(conditionPo)
          .then((res) => {
            console.log('res', res);
            result['cas'] = [];
            res.map((cas, i) => {
              console.log(cas);
              const conditionNotable = 'SELECT * FROM visite_cas WHERE id_visite_cas = (' + cas.id_visite_cas + ')';
              this.sqlite.customQuery(conditionNotable)
                  .then((response) => {
                    console.log(response);
                    cas.notable = response[0].notable;
                    cas.name = response[0].cas;
                    result['cas'][i] = cas;
                    this.sqlite.customQuery(conditionEquipement)
                        .then((resp) => {
                          result['equipement'] = resp;
                          resolve(result);
                        })
                        .catch((error) => {
                          reject(error);
                        });
                  });
            });
          })
          .catch((err) => {
            reject(err);
          });
    });
  }

  getClosedVisit(){
    return this.sqlite.getRows('visite','*','WHERE id_visite_status = 3');
  }
  
  
  getVisitData(idVisit: number = 1, status: number = 2) {
    let condition = 'WHERE id_visite IN (SELECT id_visite FROM visite WHERE id_visite = ' + idVisit + ' AND id_visite_status = ' + status + ')';
    return this.sqlite.getRows('visite_partie_ouvrage', '*', condition);
  }

  initVisit(idVisit: number, data = {nameAgent: 'NameAgent', date: '2019-11-19', comment: ''}) {
    // Update with sqlite service the visit with previous status ++
    // Specifiy in select two clause, id_visit = idVisit AND id_status = visit.status
    let setStatement = 'nom_client = \'' + data.nameAgent + '\',' +
        ' date = \'' + data.date + '\',' + ' commentaire = \'' + data.comment + '\'';
    return this.sqlite.updateRequest({table: 'visite', setStmt: setStatement, conditionStmt: 'WHERE id_visite = ' +
          idVisit + ' AND id_visite_status = 1'})
    .then((res: any) => {
      if(res.hasOwnProperty('rowsAffected')) {
        if(res.rowsAffected > 0) {
          console.log('someRowsWereAffected');
          console.log('update visite with id = ' + idVisit, res);
          return this.sqlite.updateIncrement({table: 'visite',
            column: 'id_visite_status',
            incrementValue: 1,
            condition: 'WHERE id_visite = ' + idVisit + ' AND id_visite_status = 1'})
          .then((res: any) => {
            console.log('changed status visite with id = ' + idVisit, res);
            return res;
          })
          .catch((err) => {
            console.error(err);
            throw Error('Error occured : ' + JSON.stringify(err));
          });
        } else {
          throw Error('Can\'t update visite table before updating status ; Init abort');
        }
      } else {
        throw Error('Failing while trying to update visite table at id '+ idVisit + '; No modification were made !');
      }
    }).catch((error) => {
      console.error(error);
      throw Error("Error occured : " + JSON.stringify(error));
    });
    /* return this.sqlite.updateRequest({table: 'visite', setStmt: 'nom_agent = \'' + data.nameAgent +'\', date = \''+ data.date + '\', commentaire = \'' + data.comment, conditionStmt:'\' WHERE id_visite = '+idVisit+' AND id_visite_status = 1'})
    .then((res) => {
      console.log(res);
      if(res.hasOwnProperty('rowsAffected')){
        if(res.rowsAffected != 0){
          console.log('some row are affected');
        }
      }
    }).catch((err) => {
      console.error(err);
    }) */

  }

  fetchVisit(type: string) {
    // id_visite_status : 1 = créer , 2 = initialiser, 3 = clôturer
    let condition = type === 'init' ? 1 : type === 'resume' ? 2 : null;
    return this.sqlite.getRows('visite', '*', 'WHERE id_visite_status = ' + condition);
  }

  /* Connection required features */
  visitPrepareOption(name: string, id: number) {
    const result = [];
    const uri = `preparation/${name}/`;
    // return this.api.apiGetRequest(uri,id);
    const nameRegex = RegExp('nom', 'i');
    const idRegex = RegExp('id', 'i');
    return this.api.apiGetRequest(uri, id)
    .then((apiResponse: any) => {
      for (const object in apiResponse) {
        const select = {
          name: '',
          id: ''
        };
        for (const prop in apiResponse[object]) {
        if (nameRegex.test(prop)) {
          select.name = apiResponse[object][prop];
        } else if (idRegex.test(prop)) {
          select.id = apiResponse[object][prop];
        }
      }

        result.push(select);
    }
      return result;
    }).then((toSort: any) => {
      toSort.sort((a: any, b: any) => {
        return a.name > b.name ? 1 : -1;
      });
      return toSort;
    })
    .catch((apiError) => {
      console.error(apiError);
    });
  }

  fetchVisitData(idOuvrage: number = 2925) {
    /* Call to Api after form submit, passing id Ouvrage */
    return this.api.apiGetRequest('visite/visite/', idOuvrage)
    .then((jsonData: any) => {
      console.log(jsonData);
      this.storage.get('clientId').then((response) => {
        let idAgent = this.tokenAgent.id || response
        const visit: Visit = new Visit(jsonData.id_ouvrage, idAgent, jsonData.nom_ouvrage);
        console.log('Cette visite object = ', visit);
        const partieOuvrageData = jsonData.hasOwnProperty('visite_partie_ouvrage') ? jsonData.visite_partie_ouvrage : false;
        
        // Will call the sqlite service and continue to pass our object
        this.constructVisit(visit, partieOuvrageData)
        .then((data: any) => {
          /* Here the visit is created, now create the next table 'visite_partie_ouvrage' */
          console.log(data[0]);
          const visit: Visit = data[0];
          const partieOuvrageArray = data[1];
          partieOuvrageArray.forEach((value: any) => {
            /* Here all 'visite_partie_ouvrage' rows are inserted, and will create the next table 'visite_equipement' */
            // To change for 'nom_partie_ouvrage'
            const partieOuvrage = new VisitPartieOuvrage(visit.id_visite, value.id_visite_partie_ouvrage, value.nom_ouvrage);
            const equipementData = value.hasOwnProperty('visite_equipement') ? value.visite_equipement : false;
            this.constructVisitPO(partieOuvrage, equipementData)
            .then((data: any) => {
              console.log(data[0]);
              const partieOuvrage: VisitPartieOuvrage = data[0];
              const visitEquipementArray = data[1];

              this.constructVisitListeCas(partieOuvrage.id_visite_partie_ouvrage);

              if (visitEquipementArray !== false) {
                visitEquipementArray.forEach((value: any) => {
                  /* Finally all the equipement */
                  const equipement = new VisitEquipement(partieOuvrage.id_visite_partie_ouvrage,
                      value.id_visite_equipement,
                      value.nom_equipement);
                  this.constructVisitEquipement(equipement)
                  .then((data: any) => {
                    console.log(data);
                  });
                });
              }else{
                //return visit.id_visite;
              }
            });
          });
        });
      })
      
    })
    .catch((error: any) => {
      console.error('', error);
    });
  }

  constructQueryMultiple(entity: any) {
    let columns = '';
    let values = '(';
    const keys = Object.keys(entity);

    return new Promise((resolve, reject) => {
      keys.forEach((value: any, index) => {
        if (value !== 'tableName') { // Prop in each entity to know where to insert in sqliteDatabase
          columns += ' "' + value + '"';
          values += isNaN(entity[value]) ? ' "' + entity[value] + '"' : ' ' + entity[value]; // To respect integer syntax
          if (keys.length - 1 === index) {
            columns += '';
            values += ')';
            resolve([columns, values]); // End of our promise
          } else {
            columns += ',';
            values += ',';
          }
        }
      });
    });
  }

  constructVisit(visitData: Visit, partieOuvrageData: any) {
    visitData.id_visite_status = 1;
    return new Promise((resolve, reject) => {
      this.constructQueryMultiple(visitData)
      .then((data : any)=>{
        let queryColumn = data[0];
        let queryValues = data[1];
        //alert('To insert \ncolumns : '+queryColumn+' ; values : '+queryValues);
        this.sqlite.insertRow({table: visitData.tableName, column: queryColumn, value: queryValues})
        .then((response: any) => {
          console.log(response);
          if (response.hasOwnProperty('rowsAffected')) {
            // alert("Insert successful ! Res -> "+JSON.stringify(response["insertId"]));
            this.toast.logMessage('Insertion effectuée avec succès !', 'primary');
            visitData.id_visite = response.insertId;
            resolve([visitData, partieOuvrageData]);
          } else {
            throw ['Error occured while inserting in database', ' Code : ' + response.code + ' , ' + response.message];
          }
        });
      })
      .catch((errorArray: any) => {
        // alert("Error : "+error);
        console.log(errorArray[1]);
        this.toast.logMessage(errorArray[0], 'danger');
      });
    });
    /* alert(this.sqlite.insertRow({table: visitData.tableName, })) */
    /* SQL query to make table visit and get back id visite */
    /* return new Promise((resolve, reject)=>{
      setTimeout(()=>{
        visitData.id_visite = Visit.count++;
        resolve([visitData, partieOuvrageData]);
        // Reject will be about the sqlite service
      },500);
    }) */
  }

  vacuumPoData(){

  }

  vacuumPhoto(entity: any){
    if(entity.hasOwnProperty('id_photo')){
      return this.sqlite.getRows('visite_photo','*', 'WHERE id_visite_photo IN ('+ entity.id_photo + ')')
      .then((photoArray: any) => {
          return photoArray;
      });
    }else{
      return [];
    }
  }

  endVisit2(idVisit: number = 2) {
    let promisesEquip = [];
    let promisesListeCas = [];
    let promiseVisite = new Promise((resolve, reject) => {
      /* Get the visit */
      this.sqlite.getRows('visite', '*', 'WHERE id_visite = '+idVisit+'')
      .then((result: any) => {
        console.log('visite query : ', result);
          resolve(result[0]);
      })
    })
    .then(async (visit: any) => {
      let json = visit;
      const partieOuvrage = await this.sqlite.getRows('visite_partie_ouvrage', '*', 'WHERE id_visite = '+idVisit)
      .then((result: any) => {
        console.log('partie ouvrage query :',result);
        console.log('... and visit param : ',visit);
        return result;
      });
      return {
        ...json,
        'visite_partie_ouvrage': partieOuvrage
      };
    })
    .then((result: any) => {
      console.log('pass json object after visite_partie_ouvrage added :', result);
      let json = result;
      json['visite_partie_ouvrage'] = result['visite_partie_ouvrage'].map(async (partieOuvrage: VisitPartieOuvrage, i) => {
        const equip = await this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = '+partieOuvrage.id_visite_partie_ouvrage)
        .then((res: any) => {
          if(res){
            res.map(async (equip: VisitEquipement)=>{
              if(equip){

              }else{

              }
            })
          }else{
            return []
          }
        });

        const liste_cas = await this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = '+partieOuvrage.id_visite_partie_ouvrage)
        .then((res: any) => {
          return res;
        });

        return {
          ...partieOuvrage,
          'visite_equipement': equip,
          'visite_liste_cas': liste_cas 
        }
      });
      return json;
    })
    .then((result: any)=>{
      console.log('after added equip and liste_cas',result);
    })
  }
  /* 
    return this.sqlite.getRows('visite', '*', 'WHERE id_visite = '+idVisit+'')
    .then((visitArray: Visit[]) => {
      console.log('Get visit, Step 1 / ? ', visitArray);
      const visit: Visit = visitArray[0];
      return {
        ...visit,
        'visite_partie_ouvrage': 
          this.sqlite.getRows('visite_partie_ouvrage', '*', 'WHERE id_visite = '+visit.id_visite)
          .then((partieOuvrageArray: VisitPartieOuvrage[]) => {
          console.log('Get POs, Step 2 / ? ', partieOuvrageArray);
          if(partieOuvrageArray){
            return partieOuvrageArray.map((partieOuvrage, index) => {
              return {
                ...partieOuvrage, 
                'visite_equipement': 
                  this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = '+partieOuvrage.id_visite_partie_ouvrage)
                  .then((equipementArray: VisitEquipement[]) => {
                  console.log('Get Equips, Step 2.1.'+ (index+1) +' / ?', equipementArray);
                  if(equipementArray){
                    return equipementArray.map((equipement, equipIndex) => {
                      if(equipement.id_photo){
                        this.sqlite.getRows('visite_photo','*', 'WHERE id_visite_photo IN ('+ equipement.id_photo + ')')
                        .then((photoArray: VisitPhoto[]) => {
                          console.log('Get Photos, Step 3.1.'+(equipIndex+1)+'.'+(index+1)+' / ?', photoArray);
                          return {...equipement, 'visite_photo': photoArray };
                        })
                      }else{
                        return {...equipement, 'visite_photo': ['empty photo_array']};
                      }
                    });
                  }else{
                    return ['empty equip_array'];
                  }
                }),
                'visite_liste_cas': 
                this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = '+partieOuvrage.id_visite_partie_ouvrage)
                .then((listeCasArray: VisiteListeCas[]) => {
                  console.log('Get Cases, Step 2.2.'+ (index+1) +' / ?', listeCasArray);
                  if(listeCasArray){
                    return listeCasArray.map((listeCas, casIndex) => {
                      if(listeCas.id_photo){
                        this.sqlite.getRows('visite_photo','*', 'WHERE id_visite_photo IN ('+ listeCas.id_photo + ')')
                        .then((photoArray: VisitPhoto[]) => {
                          console.log('Get Photos, Step 3.2.'+(casIndex+1)+'.'+(index+1)+' / ?', photoArray);
                          return {...listeCas, 'visite_photo': photoArray};
                        })
                      }else{
                        return {...listeCas, 'visite_photo': ['empty photo_array']};
                      }
                    })
                  }else{
                    return ['empty liste_cas_array'];
                  }
                })
              };
            })
          }else{
            return ['empty partie_ouvrage_array'];
          }
        })
      };
    }) */


  iterateThroughLevel() {

  }
  megaRecursive(idVisit: number, dataToSend = {}) {
    const tableToSend = 
    {
      'visite_partie_ouvrage': {
        'visite_equipement': {
          'visite_photo': {}
        },
        'visite_liste_cas': {
          'visite_photo': {}
        }
      }
      
    };

    if(Object.keys(dataToSend).length > 0){
      for(let prop in tableToSend){

      }
    }else{
      this.sqlite.getRows('visite','*','WHERE id_visite = '+idVisit)
      .then((res: any) => {
        this.megaRecursive(idVisit, res[0]);
      });
    }
  }
    


  endVisit(idVisit: number) {
    const tableToSend = 
    {
      'visite': {
        'visite_partie_ouvrage': {
          'visite_equipement': {
            'visite_photo': {}
          },
          'visite_liste_cas': {
            'visite_photo': {}
          }
        }
      }
    };

    const visit: Visit = new Visit(1,1,'NAEM');
    
    return this.sqlite.getRows('visite', '*', 'WHERE id_visite = '+idVisit+'')
    .then((visitArray: Visit[]) => {
      console.log('Get visit, Step 1 / ? ', visitArray);
      const visit: Visit = visitArray[0];
      return {
        ...visit,
        'visite_partie_ouvrage': 
          this.sqlite.getRows('visite_partie_ouvrage', '*', 'WHERE id_visite = '+visit.id_visite)
          .then((partieOuvrageArray: VisitPartieOuvrage[]) => {
          console.log('Get POs, Step 2 / ? ', partieOuvrageArray);
          if(partieOuvrageArray){
            return partieOuvrageArray.map((partieOuvrage, index) => {
              return {
                ...partieOuvrage, 
                'visite_equipement': 
                  /* Equip for PO */
                  this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = '+partieOuvrage.id_visite_partie_ouvrage)
                  .then((equipementArray: VisitEquipement[]) => {
                  console.log('Get Equips, Step 2.1.'+ (index+1) +' / ?', equipementArray);
                  if(equipementArray){
                    return equipementArray.map((equipement, equipIndex) => {
                      if(equipement.id_photo){
                        this.sqlite.getRows('visite_photo','*', 'WHERE id_visite_photo IN ('+ equipement.id_photo + ')')
                        .then((photoArray: VisitPhoto[]) => {
                          console.log('Get Photos, Step 3.1.'+(equipIndex+1)+'.'+(index+1)+' / ?', photoArray);
                          return {...equipement, 'visite_photo': photoArray };
                        })
                      }else{
                        return {...equipement, 'visite_photo': ['empty photo_array']};
                      }
                    });
                  }else{
                    return ['empty equip_array'];
                  }
                }),
                'visite_liste_cas': 
                /* Liste Cas for PO */
                this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = '+partieOuvrage.id_visite_partie_ouvrage)
                .then((listeCasArray: VisiteListeCas[]) => {
                  console.log('Get Cases, Step 2.2.'+ (index+1) +' / ?', listeCasArray);
                  if(listeCasArray){
                    return listeCasArray.map((listeCas, casIndex) => {
                      if(listeCas.id_photo){
                        this.sqlite.getRows('visite_photo','*', 'WHERE id_visite_photo IN ('+ listeCas.id_photo + ')')
                        .then((photoArray: VisitPhoto[]) => {
                          console.log('Get Photos, Step 3.2.'+(casIndex+1)+'.'+(index+1)+' / ?', photoArray);
                          return {...listeCas, 'visite_photo': photoArray};
                        })
                      }else{
                        return {...listeCas, 'visite_photo': ['empty photo_array']};
                      }
                    })
                  }else{
                    return ['empty liste_cas_array'];
                  }
                })
              };
            })
          }else{
            return ['empty partie_ouvrage_array'];
          }
        })
      };
    })

    //return 
    /* const visite_partie_ouvrage: VisitPartieOuvrage[] = await this.sqlite.getRows('visite_partie_ouvrage','*','WHERE id_visite = '+visit.id_visite); */
    /* let visite_equipement: VisitEquipement[] = visite_partie_ouvrage.map(async (value, index) => { 
      return this.sqlite.getRows('visite_equipement','*', 'WHERE id_visite_partie_ouvrage = '+value.id_visite_partie_ouvrage);
    }) */
    /* const fetchPoData = new Promise((resolve, reject)=>{
      visite_partie_ouvrage.forEach(async (valPo, index) => { */
        /* Object.defineProperties(visite_partie_ouvrage[index], {
          'visite_equipement': {
            valPo: await this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = '+valPo.id_visite_partie_ouvrage),
            writable: true
          },
          'visite_liste_cas': {
            valPo: await this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = '+valPo.id_visite_partie_ouvrage),
            writable: true 
          }
        }); */
        /* visite_partie_ouvrage[index].setEquipement(
          await this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = '+valPo.id_visite_partie_ouvrage)
        );
        visite_partie_ouvrage[index].setListeCas(
          await this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = '+valPo.id_visite_partie_ouvrage)
        )
        if(index === visite_partie_ouvrage.length - 1){
          resolve('All data for each partie ouvrage get binded !');
        }
      });
    }); */
/* 
    const fetchPhotoPO = new Promise((resolve) => {
      visite_partie_ouvrage.forEach((valPo, index) => {
        visite_partie_ouvrage[index].getEquipement().map(async (valEquip: VisitEquipement, idx: number) => {
          visite_partie_ouvrage[index].getEquipement()[idx].setPhoto(
            await this.sqlite.getRows('visite_photo','*','WHERE id_visite_photo IN (' + valEquip.id_photo + ')')
          );
          if(idx === visite_partie_ouvrage[index].getEquipement().length - 1){
            resolve(true);
          }
        });
      });
    }); */

    /* const fetchPhotoEquip = new Promise((resolve) => {
      visite_partie_ouvrage.forEach((valPo, index) => {
        visite_partie_ouvrage[index]['visite_liste_cas'].map(async (valCas: VisiteListeCas, indx: number) => {
          visite_partie_ouvrage[index]['visite_liste_cas'][indx]['visite_photo'] = await this.sqlite.getRows('visite_photo','*','WHERE id_visite_photo IN (' + valCasvalEquip.id_photo + ')');

          if(indx === visite_partie_ouvrage[index]['visite_liste_cas'].length - 1){
            resolve(true);
          }
        });
      })
    }); */
      /* 
      if(resolvePO === true && resolveEquip === true){
        resolve('All photos for each partie ouvrage equip & list cas were binded succesfuly !');
      }else{
        if((resolvePO !== true) && (resolveEquip !== true)){
          throw Error('Error occured while trying to bind, photos with equipment AND listCas !');
        }else if((resolvePO !== true) || (resolveEquip !== true)){
          let customError = resolvePO !== true ? 'listCas' : 'equipment';
          throw Error('Error occured while trying to bind photos with ' + customError + ' ... Aborted !');
        }
      } 
      */

    /* return fetchPoData
    .then((res: any) => {
      console.log('Equip & List Cas binded ! Log : ', res);
      fetchPhotoEquip
      .then((res: any)=>{
        console.log('Photos binded ! Log : ', res);
        fetchPhotoPO.then((res: any)=>{
          console.log('Photos binded ! Log : ', res);
          const result = {...visit, visite_partie_ouvrage: visite_partie_ouvrage};
          Object.defineProperty(visit, 'visite_partie_ouvrage',{
            value: visite_partie_ouvrage,
            writable: false
          });
          console.log(result);
          return result
        });
        
      })
    }) */
  }

  uploadVisit() {

  }

  destroyVisit() {

  }

  constructVisitPO(partieOuvrage: VisitPartieOuvrage, equipementData: any) {
    return new Promise((resolve, reject) => {
      this.constructQueryMultiple(partieOuvrage)
      .then((data : any)=>{
        console.log('query made : ',data);
        let queryColumn = data[0];
        let queryValues = data[1];
        //alert('To insert \ncolumns : '+queryColumn+' ; values : '+queryValues);
        this.sqlite.insertRow({table: partieOuvrage.tableName, column: queryColumn, value: queryValues})
        .then((response: any) => {
          console.log(response);
          if (response.hasOwnProperty('rowsAffected')) {
            // alert("Insert successful ! Res -> "+JSON.stringify(response["insertId"]));
            this.toast.logMessage('Insertion effectuée avec succès !', 'primary');
            partieOuvrage.id_visite_partie_ouvrage = response.insertId;
            resolve([partieOuvrage, equipementData]);
          } else {
            throw ['Error occured while inserting in database', ' Code : ' + response.code + ' , ' + response.message];
          }
        })
        .catch((errorArray: any) => {
          // alert("Error : "+error);
          console.log(errorArray[1]);
          this.toast.logMessage(errorArray[0], 'danger');
        });
      });
    });
    /* SQL query to make table visite_partie_ouvrage and get back id or row inserted */
    /* return new Promise((resolve, reject)=>{
      setTimeout(()=>{
        partieOuvrage.id_visite_partie_ouvrage = VisitPartieOuvrage.count++;
        resolve([partieOuvrage, equipementData]); // Can't pass two argument to the resolve callback
      },500);
    }) */
  }

  constructVisitEquipement(equipement: VisitEquipement) {
    return new Promise((resolve, reject) => {
      this.constructQueryMultiple(equipement)
      .then((data : any)=>{
        let queryColumn = data[0];
        let queryValues = data[1];
        //alert('To insert \ncolumns : '+queryColumn+' ; values : '+queryValues);
        this.sqlite.insertRow({table: equipement.tableName, column: queryColumn, value: queryValues})
        .then((response: any) => {
          console.log(response);
          if (response.hasOwnProperty('rowsAffected')) {
            // alert("Insert successful ! Res -> "+JSON.stringify(response["insertId"]));
            this.toast.logMessage('Insertion effectuée avec succès !', 'primary');
            equipement.id_visite_equipement = response.insertId;
            resolve(equipement);
          } else {
            throw ['Error occured while inserting in database', ' Code : ' + response.code + ' , ' + response.message];
          }
        })
        .catch((errorArray: any) => {
          // alert("Error : "+error);
          console.log(errorArray[1]);
          this.toast.logMessage(errorArray[0], 'danger');
        });
      });
    });

    /* return new Promise((resolve, reject)=>{
      setTimeout(()=>{
        equipement.id_visite_equipement = VisitEquipement.count++;
        resolve(equipement);
      },500);
    }) */
  }

  /**
   *
   * @param idOuvrage // 'Ouvrage' to make the visit on
   */
  createVisit(idOuvrage: number) {
    this.sqlite.insertRow();
  }
}
