import { Injectable, OnInit } from '@angular/core';
import { Camera, CameraOptions} from '@ionic-native/camera/ngx';
import {ActionSheetController, Platform} from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { File, FileEntry } from '@ionic-native/File/ngx';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { FilePath } from '@ionic-native/file-path/ngx';
import { ToastService } from '../toast/toast.service';
import { SqliteService } from '../sqlite/sqlite.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {ApiService} from '../api/api.service';
import {JwtService} from '../jwt/jwt.service';

@Injectable({
  providedIn: 'root'
})
export class PhotoService implements OnInit {
  public images = [];
  public image: any;
  public favorite: number;
  private tabLength;
  constructor(private camera: Camera,
              private file: File,
              private webview: WebView,
              private storage: Storage,
              private filePath: FilePath,
              private plt: Platform,
              private sql: SqliteService,
              private toast: ToastService,
              private geo: Geolocation,
              private api: ApiService,
              private jwt: JwtService,
              private action: ActionSheetController,
              private platform: Platform) {}
  ngOnInit() {
      this.images = [];
  }
  // Load images path and name from Ionic storage
  loadStoredImages(storageKey) {
      this.images = []; // Reset value to avoid duplicate from other pages on service load
      this.storage.get(storageKey) // Get storage of the equipment or "partie d'ouvrage"
        .then(images => {
          if (images) {
            const arr = JSON.parse(images);
            this.images = [];
            for (const img of arr) {
              const filePath = this.file.dataDirectory + img;
              const resPath = this.pathForImage(filePath);
              this.sql.getRows('visite_photo', '*', ' WHERE url = ("' + img + '")')
                  .then((response) => {
                      this.images.push({ name: img, path: resPath, filePath, id: response[0].id_visite_photo });
                      if (response[0].favoris === 1) {
                          this.favorite = response[0].id_visite_photo;
                      }
                  })
                  .catch(() => {
                      this.toast.logMessage('Une erreur  est survenue', 'dark');
                  });
            }
          }
        })
          .catch(() => {
              this.toast.logMessage('Une erreur  est survenue', 'dark');
          });
  }
  // Set path for the image
  pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return this.webview.convertFileSrc(img);
    }
  }
  // Method to take picture
  takePicture(storageKey, ouvrageName, id, type, sourceType) {
    // Camera options
    const options: CameraOptions = {
      quality: 50,
      sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };
    // Start camera or convert picture from library
    this.camera.getPicture(options)
        .then(imagePath => {
            const data = {ouvrage: ouvrageName, type: type + id};
            // convert picture from library (It creates a new image)
            if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
                this.filePath.resolveNativePath(imagePath)
                    .then(filePath => {
                        const correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                        const currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                        this.copyFileToLocalDir(correctPath, currentName, this.createFileName(data), storageKey, type, id);
                    });
            } else { // Picture taken from the camera
                const currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                const correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                this.copyFileToLocalDir(correctPath, currentName, this.createFileName(data), storageKey, type, id);
            }
        });
  }
    // Method to select to use camera or charge picture from gallery
    async selectImage(storageKey, ouvrageName, id, type) {
      console.log(id);
      const actionSheet = await this.action.create({
            header: 'Selection de la source',
            buttons: [{
                text: 'Charger depuis la gallery',
                handler: () => {
                    this.takePicture(storageKey, ouvrageName, id, type, this.camera.PictureSourceType.PHOTOLIBRARY);
                }
            },
                {
                    text: 'Appareil photo',
                    handler: () => {
                        this.takePicture(storageKey, ouvrageName, id, type, this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Annuler',
                    role: 'cancel'
                }
            ]
        });
      await actionSheet.present();
    }
  // Create name of the picture
  createFileName(data) {
    const a = data.ouvrage.split(' ').join('').toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, ''),
        b = data.type.replace(' ', ''),
        c = Date.now(),
        d = Math.floor(Math.random() * 9999);
    return a + b + c + d + '.jpg';
  }
  // Method to copy the file in the device
  copyFileToLocalDir(namePath, currentName, newFileName, storageKey, type, id) {
      const data = {table: 'visite_photo', column: 'url', value: '("' + newFileName + '")'};
      this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName)
        .then(() => {
            this.sql.insertRow(data)
                .then((response) => {
                    this.updateStoredImages(response.insertId, newFileName, storageKey, type, id);
                });
        })
          .catch(() => {
              this.toast.logMessage('Erreur lors de l\'enrgistrememnt de la photo.', 'warning');
          });
  }
  // Method to the pictures in the storage
  updateStoredImages(id, name, storageKey, type, idType) {
      this.storage.get(storageKey)
        .then(images => {
          const arr = JSON.parse(images);
          if (!arr) {
            const newImages = [name];
            this.storage.set(storageKey, JSON.stringify(newImages));
          } else {
            arr.push(name);
            this.storage.set(storageKey, JSON.stringify(arr));
          }
          const filePath = this.file.dataDirectory + name;
          const resPath = this.pathForImage(filePath);
          const newEntry = {
            name,
            path: resPath,
            filePath,
            id
          };
          this.images.push(newEntry);
          this.getGeoloc(id);
          this.insertPhotoList(idType, type);
        });
  }
  // Method to stringify the list of pictures
  pushToTab() {
      const arr = [];
      this.images.map((img) => {
              arr.push(img.id);
          });
      return arr.join();
  }
  // Method to insert the list of pictures on a liste_cas or equipement
  insertPhotoList(id, type) {
      console.log(type);
      console.log(id);
      let table: string;
      let conditionColumn: string;
      if ( type === 'equip') {
          table = 'visite_equipement';
          conditionColumn = 'id_visite_equipement';
      } else if (type === 'cas') {
          table = 'visite_liste_cas';
          conditionColumn = 'id_visite_liste_cas';
      }
      const photoList = this.pushToTab();
      const val = {
          table,
          column: 'id_photo',
          columnValue: photoList ,
          conditionColumn,
          conditionValue: '(' + id + ')'
      };
      console.log(val);
      this.sql.updateOneColumn(val)
          .then((resp) => {
              console.log(resp);
          })
          .catch((error) => {
              console.log(error);
              this.toast.logMessage('Erreur de sauvegarder de la liste des photos', 'dark');
          });
  }
  // Method to delete an image in the storage and device
  deleteImage(imgEntry, position, storageKey, id, type) {
    this.images.splice(position, 1);
    this.storage.get(storageKey).then(images => {
      const arr = JSON.parse(images);
      const filtered = arr.filter(name => name !== imgEntry.name);
      this.storage.set(storageKey, JSON.stringify(filtered));
      const correctPath = imgEntry.filePath.substr(0, imgEntry.filePath.lastIndexOf('/') + 1);
      this.file.removeFile(correctPath, imgEntry.name)
          .then((res) => {
              const arg = {table: 'visite_photo', column: 'url'};
              this.sql.deleteRow('("' + imgEntry.name + '")', arg)
                .then(() => {
                    this.toast.logMessage('Photo effacé.', 'success');
                    this.insertPhotoList(id, type);
                })
                .catch(() => {
                    this.toast.logMessage('Erreur lors de l\'éffacement de la photo.', 'warning');
                });
          })
          .catch((error) =>  {
              this.toast.logMessage('Erreur lors de l\'éffacement de la photo.', 'warning');
          });
    });
  }
  // Method to get localisation of the picture
  getGeoloc(id) {
      this.geo.getCurrentPosition()
          .then((resp) => {
              const x = {table: 'visite_photo', column: 'x',
                  columnValue: resp.coords.longitude,
                  conditionColumn: 'id_visite_photo',
                  conditionValue: id};
              const y = {table: 'visite_photo', column: 'y',
                  columnValue: resp.coords.latitude,
                  conditionColumn: 'id_visite_photo',
                  conditionValue: id};
              this.sql.updateOneColumn(x)
                  .then(() => {
                      this.sql.updateOneColumn(y)
                          .then(() => {
                              this.toast.logMessage('Gélocalisation réussie.', 'success');
                          })
                          .catch((error) => {
                              this.toast.logMessage('Gélocalisation incomplète.', 'dark');
                          });
                  })
                  .catch(() => {
                      this.toast.logMessage('Erreur de géolocalisation, reprenez une photo', 'dark');
                  });
          })
          .catch(() => {
              this.toast.logMessage('Echec de gélocalisation, problème de couverture GPS.', 'dark');
          });
  }
  // Method to insert comment into the sqlite table
  insertComment(comment, table, condition) {
      const data = {table, column: 'commentaire',
          columnValue: comment,
          conditionColumn: condition.column,
          conditionValue: condition.value};
      return this.sql.updateOneColumn(data);
  }
  favoritePicture(table, idPhoto) {
      let arr = [];
      const url = 'url';
      arr = this.images.map((img) => {
          return '"' + img.name + '"';
      });
      const joined = arr.join();
      const data = {table, column: 'favoris',
          columnValue: 0,
          conditionColumn: url,
          conditionValue: '(' + joined + ')'};
      this.sql.updateMultiValueOneColumn(data)
          .then(() => {
              data.columnValue = 1;
              data.conditionColumn = 'id_visite_photo';
              data.conditionValue = idPhoto;
              this.sql.updateOneColumn(data)
                  .then(() => {
                      this.favorite = idPhoto;
                      this.toast.logMessage('Photo favorite sélectionné.', 'success');
                  })
                  .catch((error) => {
                      this.toast.logMessage('Erreur d\'ajout aux favoris', 'dark');
                  });
          })
          .catch(() => {
              this.toast.logMessage('Erreur d\'ajout aux favoris', 'dark');
          });
   }
   getStorages(idVisit) {
      return new Promise((resolve, reject) => {
          this.sql.getRows('visite_storage_key', 'storage_key', ' WHERE id_visite = (' + idVisit + ')')
              .then((result) => {
                  console.log(result);
                  this.tabLength = result.length;
                  this.getImagesFromStorages(result)
                      .then((res) => {
                          this.filterArray(res)
                              .then((resp) => {
                                  console.log(resp);
                                  this.startUpload(resp)
                                      .then(() => {
                                          resolve(resp);
                                      })
                                      .catch((error) => {
                                          this.toast.logMessage('Erreur lors de l\'envoi des photos', 'dark');
                                          reject(error);
                                      });
                              });
                      })
                      .catch((error) => {
                          console.log(error);
                          this.toast.logMessage('Aucune photo trouvé.', 'dark');
                          reject(error);
                      });
              })
              .catch((err) => {
                  console.log(err);
                  this.toast.logMessage('Aucune photo trouvé.', 'dark');
                  reject(err);
              });
      });
   }
   getImagesFromStorages(storages) {
      const result = [];
      return new Promise((resolve) => {
          for (let i = 0; i < storages.length; i++ ) {
              this.storage.get(storages[i].storage_key)
                  .then((images) => {
                      if (images) {
                          const buffer = JSON.parse(images);
                          result.push(buffer);
                      }
                      if (i === storages.length - 1) {
                          resolve(result);
                      }
                  });
          }
      });
   }

   filterArray(tab) {
      return new Promise((resolve) => {
          const finalTab = [];
          for (let j = 0; j < tab.length; j++) {
              if (tab[j]) {
                  // tslint:disable-next-line:prefer-for-of
                  for (let i = 0; i < tab[j].length; i++) {
                      const filePath = this.file.dataDirectory + tab[j][i];
                      const resPath = this.pathForImage(filePath);
                      const temp = { name: tab[j][i], path: resPath, filePath };
                      finalTab.push(temp);
                  }
              }
              if (j === tab.length - 1 ) {
                  resolve(finalTab);
              }
          }
      });
   }
    async startUpload(images) {
      const length = images.length;
      await images.map((img, i) => {
          this.file.resolveLocalFilesystemUrl(img.filePath)
              .then((entry) => {
                  ( entry as FileEntry).file(file => this.readFile(file));
              })
              .catch((err) => {
                  this.toast.logMessage('Erreur lors de la lecture de la photo.', 'warning');
              });
          if (i === length - 1 ) {
              return true;
              this.toast.logMessage('Toutes les photos ont était envoyé', 'success');
          }
      });
    }
    // Method to get picture
    readFile(file: any) {
        const reader = new FileReader();
        reader.onload = () => {
            const formData = new FormData();
            const imgBlob = new Blob([reader.result], {
                type: file.type
            });
            formData.append('file', imgBlob, file.name);
            this.uploadImageData(formData);
        };
        // reader.readAsDataURL(file);
        reader.readAsArrayBuffer(file);
    }
    // Method to upload pictures to the api
    uploadImageData(formData) {
        this.jwt.makeJwtOption()
            .then((res) => {
                this.api.apiPostRequest(formData, 'upload/upload', res)
                    .catch((error) => {
                        this.toast.logMessage('POST: Erreur lors de l\'upload.', 'warning');
                    });
            })
            .catch(() => {
                this.toast.logMessage('JWT: Erreur lors de l\'upload.', 'warning');
            });
    }
    // Method to insert new storage name to BDD
    insertStorageName(storageName, idVisit) {
        const data = {
            table: 'visite_storage_key',
            column: 'id_visite, storage_key',
            value: '(' + idVisit + ', "' + storageName + '")'
        };
        return this.sql.insertRow(data);
    }
    /**
     * Method to check if storage name already exist on the BDD or not
     * If exist return true else return false
     */
    checkStorage(storageName, idVisit) {
        return this.sql.getRows('visite_storage_key', 'storage_key', ' WHERE storage_key = ("' + storageName + '")')
            .then((response) => {
                if (response !== undefined && !response.message) {
                    return false;
                } else {
                    return true;
                }
            })
            .catch(() => {
                this.toast.logMessage('Une erreur est survenue, revenez en arrière.', 'dark');
            });
    }
}
