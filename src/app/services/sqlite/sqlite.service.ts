import {Injectable} from '@angular/core';
// import SQLite packages
import { SQLite, SQLiteObject } from '@ionic-native/sqlite/ngx';

@Injectable({
  providedIn: 'root'
})
export class SqliteService {
  public databaseName: string; // Database name
  public databaseTables: any[]; // Array of tables
  private databaseObj: SQLiteObject; // Database instance object
  constructor(
      private sqlite: SQLite,
  ) {
    this.databaseName = 'ashera.db'; // BDD name
    // Fixed tables
    this.databaseTables = [
        {
            table: 'visite_cas',
            column: '(id_visite_cas INTEGER PRIMARY KEY, cas TEXT, notable INTEGER) '
        },
        {
            table: 'visite_note',
            column: '(id_visite_note INTEGER PRIMARY KEY, note TEXT)'
        },
        {
            table: 'visite_status',
            column: '(id_visite_status INTEGER PRIMARY KEY, status TEXT)'
        },
        {
            table: 'visite_token',
            column: '(id_token INTEGER PRIMARY KEY, token TEXT)'
        },
        {
            table: 'visite',
            column: '(id_visite INTEGER PRIMARY KEY, ' +
                'id_ouvrage INTEGER, id_client INTEGER, ' +
                'nom_client TEXT, ' +
                'nom_ouvrage TEXT, ' +
                'date TEXT,' +
                'commentaire TEXT NULL, ' +
                'id_visite_status INTEGER, FOREIGN KEY (id_visite_status) REFERENCES visite_status (id_visite_status))'
        },
        {
            table: 'visite_partie_ouvrage',
            column: '(id_visite_partie_ouvrage INTEGER PRIMARY KEY, ' +
                'id_visite INTEGER, ' +
                'id_partie_ouvrage INTEGER, ' +
                'nom_partie_ouvrage TEXT, ' +
                'status INTEGER DEFAULT 0, ' +
                'FOREIGN KEY (id_visite) REFERENCES visite (id_visite))'
        },
        {
            table: 'visite_photo',
            column: '(id_visite_photo INTEGER PRIMARY KEY, ' +
                'url VARCHAR(255), ' +
                'commentaire TEXT NULL, ' +
                'favoris INTEGER, ' +
                'x TEXT NULL, ' +
                'y TEXT NULL)'
        },
        {
            table: 'visite_liste_cas',
            column: '(id_visite_liste_cas INTEGER PRIMARY KEY, ' +
                'id_visite_partie_ouvrage INTEGER, ' +
                'id_visite_note INTEGER DEFAULT 1, ' +
                'id_photo TEXT, ' +
                'commentaire TEXT, ' +
                'status INTEGER DEFAULT 0, ' +
                'id_visite_cas INTEGER, ' +
                'FOREIGN KEY (id_visite_partie_ouvrage) REFERENCES visite_partie_ouvrage (id_visite_partie_ouvrage), ' +
                'FOREIGN KEY (id_visite_note) REFERENCES visite_note (id_visite_note), ' +
                'FOREIGN KEY (id_visite_cas) REFERENCES visite_cas (id_visite_cas))'
        },
        {
            table: 'visite_equipement',
            column: '(id_visite_equipement INTEGER PRIMARY KEY, ' +
                'id_visite_partie_ouvrage INTEGER, ' +
                'id_equipement INTEGER, ' +
                'id_photo TEXT, ' +
                'id_visite_note INTEGER DEFAULT 1, ' +
                'commentaire TEXT, ' +
                'nom_equipement TEXT, ' +
                'status INTEGER DEFAULT 0, ' +
                'FOREIGN KEY (id_visite_note) REFERENCES visite_note (id_visite_note), ' +
                'FOREIGN KEY (id_visite_partie_ouvrage) REFERENCES visite_partie_ouvrage (id_visite_partie_ouvrage))'
        },
        {
            table: 'visite_storage_key',
            column: '(id_visite_storage_key INTEGER PRIMARY KEY, ' +
                'id_visite INTEGER, ' +
                'storage_key TEXT, ' +
                'FOREIGN KEY (id_visite) REFERENCES visite (id_visite))'
        }
    ];
  }
  /**
   * Method to create de BDD
   * then name is fixed internally on a prop
   */
  createDB() { // Method to create BDD
      return this.sqlite.create({
        name: 'ashera.db',
        location: 'default'
      })
          .then((db: SQLiteObject) => {
            this.databaseObj = db;
            return db;
          })
          .catch(error => {
              return error;
          });
  }
  /**
   * Method to create tables
   * The tables are fixed on a prop
   * need to think of a way to make it dynamically
   */
  createTable(tableObject) {
     return this.databaseObj.executeSql('CREATE TABLE IF NOT EXISTS ' + tableObject.table + ' ' + tableObject.column, [])
        .then((response) => {
            return response;
        })
        .catch(e => {
            return e;
        });
  }

  insertOrReplace(data = {table: 'visite_liste_cas', column: ['id_visite_partie_ouvrage'], value: ['4']}){
    if (data.column.length !== data.value.length) {
        throw Error('Columns and Values array have a different sizes ! Sql request aborted..');
    } else {
        let toConcatenate: any;
        let whereClause = '';
        const length = data.column.length;
        let valueGroup = '';
        let columnGroup = '';
        let value: any;
        let column: any;
        for (let i = 0; i < data.column.length; i++) {
            value = data.value[i];
            column = data.column[i];
            toConcatenate = isNaN(value) ? `"${value}"` : `${value}`;
            if (i === length - 1) {
                valueGroup += toConcatenate;
                columnGroup += column;
                whereClause += `${column} = ${toConcatenate}`;
            } else {
                valueGroup += `${toConcatenate}, `;
                columnGroup += `${column}, `;
                whereClause += `${column} = ${toConcatenate} AND `;
            }
        }
        const request = 'INSERT INTO ' + data.table + ' (' + columnGroup + ') SELECT ' + valueGroup +
            ' WHERE NOT EXISTS ( SELECT 1 FROM ' + data.table + ' WHERE ' + whereClause + ')';
        return this.databaseObj.executeSql(request, []);
    }
  }
    /**
     * @param data = {table: 'visite', setStmt: 'date = \'15/10/2018\', nom_agent=\'AgentName\'', conditionStmt: 'WHERE id_visite = 5'}
     */
    updateRequest(data: any) {
        const updateQuery = `UPDATE ${data.table} SET ${data.setStmt} ${data.conditionStmt}`;
        return this.databaseObj.executeSql(updateQuery,[]);
    }
    updateIncrement(data = {
        table: 'visite',
        column: 'id_visite_status',
        incrementValue: 1,
        condition: 'WHERE id_visite = 1 AND id_visite_status = 1'}
        ) {
        const updateQuery = `UPDATE ${data.table} SET ${data.column} = ${data.column} + ${data.incrementValue} ${data.condition}`;
        return this.databaseObj.executeSql(updateQuery)
            .then((res) => {
                return res;
            })
            .catch((err) => {
                return err;
            });
    }
  /**
   * Method to insert row in table
   * Object format [table: 'table name', column: 'column(s) name', value: 'value(s) to insert' in respect to SQL syntax
   * @param data // object of data(s) to insert
   */
  insertRow(data = {table: 'visite_note', column: 'note', value: '("mauvais")'}, option = '') {
      return this.databaseObj.executeSql('INSERT INTO ' + data.table + ' (' + data.column + ') VALUES ' + data.value + option, [])
          .then((res) => {
              return res;
          })
          .catch(e => {
              return e;
          });
  }
  customQuery(query) {
    return this.databaseObj.executeSql(query, [])
    .then((res) => {
        if (res.rows.length > 0) {
            const rowData = [];
            for (let i = 0; i < res.rows.length; i++) {
                rowData.push(res.rows.item(i));
            }
            return rowData;
        } else {
            return res;
        }
    })
    .catch(e => {
        return e;
    });
  }
  /**
   * Method to get all rows from a table
   * @param table table name
   * @param columns all columns you want, if multiple join them in parenthesis separeted by commas -> (column1, column2)
   * @param options here goes all sql conditions (WHERE, LIKE etc...)
   */
  getRows(table: string, columns = '*', options = '') {
      return this.databaseObj.executeSql('SELECT ' + columns + ' FROM ' + table + ' ' + options, [])
        .then((res) => {
            if (res.rows.length > 0) {
                const rowData = [];
                for (let i = 0; i < res.rows.length; i++) {
                    rowData.push(res.rows.item(i));
                }
                return rowData;
            }
        })
        .catch(e => {
            return e;
        });
  }
  /**
   * Method to delete a row
   * @param value // Value to search
   * @param arg // Object containing prop table and column
   */
  deleteRow(value, arg = {table: 'nom de la table', column: 'nom de la column' }) {
      return this.databaseObj.executeSql('DELETE FROM ' + arg.table + ' WHERE ' + arg.column + ' = ' + value, []);
  }
  /**
   * Method to DROP a table
   */
  dropTable(table) {
      return this.databaseObj.executeSql('DROP TABLE IF EXISTS ' + table, []);
  }
  /**
   * Method to update the value in a row
   * @param data // {table: 'table name', column: 'name of the column to change', columnValue: 'value to change or update',
   * conditionColumn: 'name of the column of the condition', conditionValue: 'value of the condition'}
   */
  updateOneColumn(data) {
      return this.databaseObj.executeSql('UPDATE ' + data.table + ' SET ' + data.column +
          ' = ("' + data.columnValue + '") WHERE ' + data.conditionColumn + ' = ' + data.conditionValue, []);
  }
  updateMultiValueOneColumn(data) {
      return this.databaseObj.executeSql('UPDATE ' + data.table + ' SET ' + data.column + ' = (' + data.columnValue + ') WHERE ' +
          data.conditionColumn + ' IN ' + data.conditionValue, []);
  }
}
