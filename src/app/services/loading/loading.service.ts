import { Injectable } from '@angular/core';
import { LoadingController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class LoadingService {

  constructor(private loadingController: LoadingController) { }

  createLoading(msg: string = 'Chargement...') {
    this.loadingController.create({
      spinner: 'crescent',
      message: msg
    }).then((res) => {
      res.present();
    });
  }
  closeLoading(id = '') {
    this.loadingController.dismiss(id);
  }
}
