import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { SqliteService } from '../sqlite/sqlite.service';
import { ToastService } from '../toast/toast.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class JwtService {
  private token: any;
  private payload;
  constructor(
      private sql: SqliteService,
      private storage: Storage,
      private toast: ToastService,
      private router: Router
      ) { }
  // Method to get token from internal database
  getToken() {
    return this.sql.getRows('visite_token');
  }
  // Method to check validity of the token
  checkValidity(token) {
    return this.splitToken(token)
        .then((response) => {
          this.payload = JSON.parse(response[1]);
          return this.payload.exp > Date.now();
        })
        .catch((error) => {
          return error;
        });
  }
  // Method to split the token and decode it
  splitToken(token) {
    const tab = [];
    return new Promise((resolve, reject) => {
      const temp: any = token.split('.');
      for (let i = 0; i < temp.length - 1; i ++) {
        tab.push(atob(temp[i]));
      }
      if (tab.length > 0) {
          resolve(tab);
      } else {
          reject('Pas de token.');
      }
    });
  }
  // Method to get client Id from the storage
  storageClientId() {
    return this.storage.set('clientId', this.payload.data.client_id);
  }
    // Method to make header with the token
  async makeJwtOption() {
    try {
      const response = await this.getToken();
      if (response !== false) {
          return { Authorization: 'Bearer' + response[0].token };
        } else {
          throw Error('No token found !');
        }
    } catch (error) {
      return error;
    }
  }

    /**
     * Method to remove the table token the create it again
     * In order to alway keep a clean table
     */
  logOut() {
      this.sql.dropTable('visite_token')
          .then(() => {
              this.toast.logMessage('Vous vous êtes déconnecté!', 'success')
                  .then(() => {
                      this.sql.createTable({table: 'visite_token', column: '(id_token INTEGER PRIMARY KEY, token TEXT)'})
                          .then(() => {
                              this.router.navigateByUrl('/login');
                          });
                  });
          })
          .catch(() => {
              this.toast.logMessage('Erreur', 'black');
          });
  }
}
