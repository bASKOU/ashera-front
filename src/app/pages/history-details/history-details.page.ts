import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, NavigationExtras, Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {ApiService} from '../../services/api/api.service';


@Component({
  selector: 'app-history-details',
  templateUrl: './history-details.page.html',
  styleUrls: ['./history-details.page.scss'],
})
export class HistoryDetailsPage implements OnInit {
  public data: any;
  public viewSegment: any;
  public equipment: any;
  public listeCas: any;
  protected cas: any;
  protected target;
  protected tab: [];
  protected id;


  constructor(  protected storage: Storage,
                protected api: ApiService,
                // tslint:disable-next-line:max-line-length
                private route: ActivatedRoute, private router: Router) {
      this.data = this.route.snapshot.params.id;
      this.viewSegment = 'equipement';
      this.route.queryParams.subscribe(params => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.data = this.router.getCurrentNavigation().extras.state.id;
      }
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
      this.getDataApi();
  }

  getDataApi() {
      this.api.apiGetRequest('history/history/', this.data)
          .then((res) => {
              for (const prop in res) {
                  if (prop === 'liste_equipement') {
                      this.equipment = res[prop];
                  } else if (prop === 'liste_cas') {
                      this.listeCas = res[prop];
                  }
              }
        });
  }

}
