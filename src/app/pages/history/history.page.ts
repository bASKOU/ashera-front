import { Component, OnInit } from '@angular/core';
import {Storage} from '@ionic/storage';
import {ApiService} from '../../services/api/api.service';
import {NavigationExtras, Router} from '@angular/router';


@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {

    protected tabs;
    public equip;
    protected visite;
    protected result;
    protected partieOuvrage: any;
    protected equipement: any;
    protected listeCas: any;

    protected idClient: number;
    protected idPartieOuvrage: any;
    protected idVisite: number;
    protected idListeCas: number;
    protected idEquipement: number;

    constructor(
        protected storage: Storage,
        protected api: ApiService,
        private router: Router
    ) {
    }

    ngOnInit() {
        this.storage.get('clientId')
            .then((res) => {
                console.log('VISITE', res);
                this.idClient = res;
                this.getVisite(this.idClient);
            })
            .catch(
                err => console.log(err)
            );
        this.equip = 'equipment';
    }

    getVisite(id) {
        this.api.apiGetRequest('history/visite/', id)
            .then((res) => {
                console.log(res);
                this.result = res;
                this.idVisite = this.result.id_visite;
            });

    }


    getPartieOuvrage() {
        this.api.apiGetRequest('history/partieouvrage/', this.idVisite)
            .then((res) => {
                console.log('PARTIEOUVRAGE', res);
                this.partieOuvrage = res;
            });

    }


    registerChange() {
        let navigationExtras: NavigationExtras = {
            state: {
                id: this.idPartieOuvrage
            }
        };
        this.router.navigate(['history-details'], navigationExtras)
            .then(res =>  this.idPartieOuvrage);
    }

}
