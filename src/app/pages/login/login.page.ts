/** Core importations */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
/** Services importations */
import { ApiService } from 'src/app/services/api/api.service';
import { SqliteService } from '../../services/sqlite/sqlite.service';
/** Components importations */
import { MenuController } from '@ionic/angular';
import { JwtService } from '../../services/jwt/jwt.service';
import {ToastService} from '../../services/toast/toast.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  private uri: string;
  private options: any;
  public log: any; // Messages & Api output
  public credentials: any; // User informations

  constructor(
    public toast: ToastService,
    private router: Router,
    private apiService: ApiService,
    private menuCtrl: MenuController,
    private sql: SqliteService,
    private jwt: JwtService
  ) {
    this.initDB();
    this.uri = 'login';
    this.options = {'Access-Control-Allow-Headers' : '*', 'Access-Control-Allow-Credentials' : 'true'};
    this.menuCtrl.swipeGesture(false, 'main-content'); // Disable swipe to block menu on login page
  }

  ngOnInit() {
  }

  loginService(data: any) {
    /* *
     * Call our service with form's data and prop from page environment
     */
    this.apiService.apiPostRequest(data.value, this.uri, this.options)
    .then((result: any) => {
      if (result.messages) {
        this.toast.logMessage(result.messages.error, 'danger');
      } else {
        this.insertToken(result.token)
        .then(() => {
          this.jwt.checkValidity(result.token)
              .then((response) => {
                if (response === true) {
                  this.jwt.storageClientId()
                      .then(() => {
                        this.toast.logMessage('Connexion réussie !', 'success');
                        this.router.navigateByUrl('/profil');
                        // Sync utility tables when jwt is storred !
                        this.apiService.syncUtilityTables();
                      });
                } else {
                  this.jwt.logOut();
                }
              });
        })
        .catch(() => {
          this.toast.logMessage('Merci de vous reconnecter', 'dark');
        });
      }
    })
    .catch(() => {
      this.toast.logMessage('Une erreur est survenue !', 'dark');
    });
  }
  // Method to insert JWT to SQLite table
  insertToken(token: any) {
    const data = {table: 'visite_token', column: 'token', value: '("' + token + '")'};
    return this.sql.insertRow(data).then((res) => {
      return res;
    })
    .catch((error) => {
      return error;
    });
  }
  initDB() { // Init BDD on app launch
    return this.sql.createDB().then(() => {
      this.sql.databaseTables.map((table) => {
        this.sql.createTable(table)
            .then((resp) => {
              console.log(resp);
            });
      });
    });
  }
}
