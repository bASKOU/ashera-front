import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { VisitService } from 'src/app/services/visit/visit.service';
import { Storage } from '@ionic/storage';
import { resolve } from 'url';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-preparation',
  templateUrl: './preparation.page.html',
  styleUrls: ['./preparation.page.scss']
})
export class PreparationPage implements OnInit {

  /* All props for the visit preparation */
  public visitForm: FormGroup;
  public formEnded: boolean;
  public visit: any;
  /* formUtilities */
  public orderForm: any[];
  public formTitle: any;
  /* service Properties */
  private idClient: number;
  /* Template & styling purposes */
  private clicked: any;
  public selectCheckbox: boolean;
  public selectIndeterminate: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private visitService: VisitService,
    private storage: Storage
  ) {
    this.selectCheckbox = false;
    this.selectIndeterminate = false;
    this.storage.get('clientId')
        .then((response) => {
          this.idClient = response;
          this.visitService.visitPrepareOption(this.orderForm[0], this.idClient).then((result: any) => {
            this.visit.collectivite = result;
          });
          /* formAttributes */
          this.visitForm = this.formBuilder.group({
            collectivite: ['', Validators.required],
            entiteGestion: ['', Validators.required],
            infrastructure: ['', Validators.required],
            ouvrage: ['', Validators.required]
          });
        })
        .catch(() => {
        });
    this.orderForm = [
      'collectivite',
      'entiteGestion',
      'infrastructure',
      'ouvrage'
    ];
    this.formTitle = {
      collectivite: 'Collectivité',
      entiteGestion: 'Entité de Gestion',
      infrastructure: 'Infrastructure',
      ouvrage: 'Ouvrage'
    };
    this.visit = {
      collectivite: '',
      entiteGestion: '',
      infrastructure: '',
      ouvrage: ''
    };
  }

  ngOnInit() {
  }

  getData(dataName: any, dataId: any) {
    if (dataId) {
      const indexChoice = this.orderForm.indexOf(dataName);

      /* Condition to test if we are at the end of our form */
      if (indexChoice >= 0 && indexChoice < this.orderForm.length - 1) {
        /* Form ended ? False */
        this.formEnded = false;
        /* Preparation of the next input */
        const nextChoice =  this.orderForm[indexChoice + 1];
        /* In case of changing previous input, reinitialise all of them after the one selected */
        for (let i = indexChoice + 1; i < this.orderForm.length; i++) {
          const propName = this.orderForm[i];
          this.visit[propName] = null; // reinitialize select option array
          this.visitForm.controls[propName].reset(); // reinitialize input value;
        }
        /* Call the service to fill the next input */
        this.visitService.visitPrepareOption(nextChoice, dataId).then((result: any) => {
          this.visit[nextChoice] = result;
        });

      } else {
        console.log('Form ended !');
        this.formEnded = true;
      }
    }
  }

  submitForm() {
    console.log('form', this.visitForm.value);
    const idOuvrage = this.visitForm.value.ouvrage;
    if (idOuvrage) {
      if (typeof idOuvrage === 'object') {
        const promises = [];
        idOuvrage.forEach((val, index) => {
          promises.push(new Promise((resolve) => {
            this.visitService.fetchVisitData(val)
            .then(() => {
              console.log('Visite crée avec succès !' + (index + 1) + ' / ' + idOuvrage.length);
              resolve(true);
            });
          }));
          Promise.all(promises).then(() => {
            console.log('Toutes les visites crées avec succès !');
          })
          .catch((err) => {
            console.error('Erreur  survenue lors de la création multiple de visite →', err);
          });
        });
      } else {
        this.visitService.fetchVisitData(idOuvrage)
        .then(() => {
        });
      }
    }
  }
  submitTest() {
    this.visitService.fetchVisitData(2930);
  }
  compare(a: any, b: any) {
    return (a.name < b.name) ? -1 : 1 ;
  }

  testInputEvent2(event: any, input: string) {

    if (event.target === this.clicked) {
      this.getData(input, event.target.value);
    }

  }

  setClicked(event: any) {
    this.clicked = event.target;
  }

  generateInterfaceOptions(header: string) {
    return {
      header,
      translucent: true
    };
  }
}
