import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitEvalPage } from './visit-eval.page';

describe('VisitEvalPage', () => {
  let component: VisitEvalPage;
  let fixture: ComponentFixture<VisitEvalPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitEvalPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitEvalPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
