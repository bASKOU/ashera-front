import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { VisitEvalPage } from './visit-eval.page';
import { ComponentsModule } from 'src/app/components/components.module';

//import { IonicRatingModule } from 'ionic4-rating';
import { StarRatingModule } from 'ionic4-star-rating';
import { IonicRatingModule } from 'ionic-rating';
const routes: Routes = [
  {
    path: '',
    component: VisitEvalPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    IonicRatingModule,
    StarRatingModule,
    RouterModule.forChild(routes)
  ],
  declarations: [VisitEvalPage]
})
export class VisitEvalPageModule {}
