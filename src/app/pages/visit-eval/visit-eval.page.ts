import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { ModalController } from '@ionic/angular';
import { DetailEquipPoComponent } from 'src/app/components/modals/detail-equip-po/detail-equip-po.component';
import { VisitService } from 'src/app/services/visit/visit.service';
import { SqliteService } from '../../services/sqlite/sqlite.service';

@Component({
  selector: 'app-visit-eval',
  templateUrl: './visit-eval.page.html',
  styleUrls: ['./visit-eval.page.scss'],
})
export class VisitEvalPage implements OnInit {

  /* Data imported */
  public ouvrageName: string; // To send to the detail page
  public partieOuvrage;
  public listeCas;
  public equipement;
  public notes;
  public cas;
  public partieOuvrageTitle: string;
  public idVisit: any;
  /* Data made */
  public progressPO: any;
  public progressEquip: any;
  public casNotable: any[];
  public casOther: any[];
  /* View properties */
  public viewSegment: string;
  public rate: any[]; // Array for our ngModel in the front of the rating component (one cell forEach listeCas);
  public searchToggled: boolean;
  constructor(
    private router: ActivatedRoute,
    private loading: LoadingService,
    private modal: ModalController,
    private visitService: VisitService
  ) {
    /* Initializing variables */
    this.viewSegment = 'partieOuvrage';
    console.log(this.router.snapshot.paramMap.get('partieOuvrageId'));
    console.log('ouvrageName : ', this.router.snapshot.paramMap.get('ouvrageName'));
    console.log(this.router.snapshot.paramMap.get('partieOuvrageTitle'));
    this.searchToggled = false;
    this.casNotable = [];
    this.casOther = [];
    this.progressPO = {
      count: 0,
      total: 0
    };
    this.progressEquip = {
      count: 0,
      total: 0
    };
    this.rate = [];
    this.partieOuvrage = this.router.snapshot.paramMap.get('partieOuvrageId');
    this.partieOuvrageTitle = this.router.snapshot.paramMap.get('partieOuvrageTitle');
    this.ouvrageName = this.router.snapshot.paramMap.get('ouvrageName');
    this.idVisit = this.router.snapshot.paramMap.get('idVisit');
  }

  ngOnInit() {
    
  }

  ionViewWillEnter() {
    /* Fetching data with the help of our services */
    this.initEvaluation(this.partieOuvrage);
  }

  initEvaluation(idVisitePo) {
    let promises = [];

    promises.push(this.visitService.getPOData(idVisitePo));
    promises.push(this.visitService.getUtilitaryTables());

    Promise.all(promises)
    .then((res: any) => {
      this.listeCas = res[0]['listeCas'] || [] ;
      this.equipement = res[0]['equipement'] || [] ;
      this.cas = res[1]['cas'] || [] ;
      this.notes = res[1]['note'] || [] ;
      return new Promise((resolve, reject) => {
        this.casNotable = [];
        this.casOther = [];
        this.listeCas.forEach(async (value) => {
          let column = 'id_visite_cas';
          let cas = await this.isInArray(this.cas, value[column], column);
          if (cas['notable'] === 1) {
            this.casNotable.push(value);
            this.rate[value.id_visite_cas] = value.id_visite_note || false;
          } else if (cas['notable'] === 0){
            this.casOther.push(value);
          }
        });
        this.updateProgress();
      });
    });
  }

  isInArray(array, idToFind, propName) {
    return new Promise((resolve, reject) => {
      for (let i = 0; i < array.length; i++) {
        if (array[i][propName] === idToFind) {
          resolve(array[i]);
        }
      }
      reject('No item found !');
    });
  }

  getCas(idCas) {
    /**
     * Trying to optimise the execution time (length - 1) * length loop less
     */
    if (this.cas[(idCas - 1)]['id_visite_cas'] === idCas) {
      return this.cas[(idCas - 1)].cas;
    } else {
      // tslint:disable-next-line:prefer-for-of
      for (let i = 0; i < this.cas.length ; i++) {
        if (idCas === this.cas[i].id_visite_cas) {
          return this.cas[i].cas;
        }
      }
      return false;
    }
  }

  updateProgress(whichOne = '') {
    if (whichOne === 'equipement') {
      this.progressEquip.count = 0;
      this.progressEquip.total = this.equipement.length;
      this.equipement.forEach((value) => {
        if (value.status) {
          this.progressEquip.count ++ ;
        }
      });
    } else if (whichOne === 'liste_cas') {
      this.progressPO.count = 0;
      this.progressPO.total = this.listeCas.length;
      this.listeCas.forEach(async (value) => {
        if (value.status) {
          this.progressPO.count++;
        }
      });
    } else {
      this.updateProgress('liste_cas');
      this.updateProgress('equipement');
    }
  }

  sliceText(arg: any, maxSize = 50) {
    if (arg) {
      return arg.length > maxSize + 1 ?  arg.slice(0, maxSize) + '..' : arg;
    } else {
      return '';
    }
  }
  onRateChange(event: any, type: string, id: any, idType: any) {
    const table = type === 'equip' ? 'visite_equipement' : type === 'cas' ? 'visite_liste_cas' : '' ;
    this.visitService.updateVisiteData(event, table, 'id_visite_note', 'id_' + table, idType)
    .then((res) => {
      // Need to add extra condition to avoid reupdate where status = 1 already !
      return this.visitService.updateVisiteData(1, table, 'status', 'id_' + table, idType);
    })
    .then((res) => {
      if (res.hasOwnProperty('rowsAffected')) {
        if (res.rowsAffected > 0) {
          const prop = type === 'equip' ? 'equipement' : type === 'cas' ? 'casNotable' : '';
          if (this[prop][id]['id_' + table] === idType) {
            this[prop][id]['status'] = 1;
          } else {
            // tslint:disable-next-line:prefer-for-of
            for (let i = 0; i < this[prop].length; i++) {
              if (this[prop][i]['id_' + table] === idType) {
                this[prop][i]['status'] = 1;
              }
            }
          }
          this.updateProgress(prop);
        }
      }
    });
  }
  /**
   * Method used for creating a custom interface of the select option
   * (as defined in the component 'interfaceOptions' attribute for 'interface' attribute)
   */
  createInterfaceOptions(headerCustom: string, subHeaderCustom: string = '', messageCustom: string = '') {
    return {
      header: headerCustom,
      subHeader: subHeaderCustom,
      message: messageCustom,
      translucent: true
    };
  }

  async detailModal(data: any) {
    const modal = await this.modal.create({
      component: DetailEquipPoComponent,
      componentProps: {
        name: data.name,
        id: data.id,
        type: data.type
      }
    });
    return await modal.present();
  }
}
