import { Component, OnInit } from '@angular/core';
import { Visit } from 'src/app/services/entities/visit';
import { VisitService } from 'src/app/services/visit/visit.service';
import {AlertController, LoadingController} from '@ionic/angular';
import { ApiService } from 'src/app/services/api/api.service';
import { PhotoService } from 'src/app/services/photo/photo.service';
import {SqliteService} from "../../services/sqlite/sqlite.service";
import {JwtService} from "../../services/jwt/jwt.service";
import {Storage} from "@ionic/storage";
import {ToastService} from "../../services/toast/toast.service";

@Component({
  selector: 'app-send',
  templateUrl: './send.page.html',
  styleUrls: ['./send.page.scss'],
})
export class SendPage implements OnInit {

  public visitClosed: Visit[];
  protected uri: string;
  protected options: any;

  constructor(private visitService: VisitService,
              private alertController: AlertController,
              private apiService: ApiService,
              private photoService: PhotoService,
              private sql: SqliteService,
              private jwt: JwtService,
              private loading: LoadingController,
              private storage: Storage,
              private toast: ToastService) {
    this.uri = 'cloture/clotureVisite';
    this.options = {'Access-Control-Allow-Headers' : '*', 'Access-Control-Allow-Credentials' : 'true', 'Content-Type' : 'application/json'};
    this.visitClosed = [];
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    this.visitService.getClosedVisit().then((res: any) => {
      this.visitClosed = res;
    });
  }

  convertDate(date: any) {
    const formatedDate = new Date(date);
    return formatedDate.toLocaleDateString('fr-FR');
  }
  /* Create An Alert when trying to send visit ! */
  async displayAlert(idVisit) {
      const alert = await this.alertController.create({
        header: 'Veuillez confirmer',
        subHeader: 'Conseil : Assurez-vous d\'avoir assez de batterie !',
        message: 'Vous allez procéder à un archivage.<br>' +
            'Cette procédure peut prendre un certain temps.<br>' +
            'Veuillez garder votre appareil allumé pendant la durée complète du processus.',
        buttons: [
          {
            text: 'Annuler',
            role: 'cancel',
            cssClass: 'alertNeutral',
            handler: () => {
            }
          }, {
            text: 'Confirmer',
            cssClass: 'alertDanger',
            handler: () => {
                this.storage.clear();
                this.closeVisit(idVisit);
            }
          }
        ]
      });
      await alert.present();
  }

  closeVisit(id) {
   const promises = [];
   const arrayIdVisit = [];
   if (typeof id === 'object') {
    id.forEach((value, index) => {
      promises.push(this.visitService.syncVisitMethod(value));
      arrayIdVisit.push(value);
    });
   } else {
     promises.push(this.visitService.syncVisitMethod(id));
     arrayIdVisit.push(id);
   }
   Promise.all(promises).then((res) => {
    res.forEach((val, index) => {
        console.log('Will be send to API', res);
        this.apiService.apiPostRequest([val], this.uri, this.options)
        .then((resp) => {
            console.log('Result from the Api', resp);
        })
        .catch((error) => {
          console.error('Error while trying to sync with API', error);
        });
      });
    });
  }
  async upload(id) {
      const loading = await this.loadFunct('Envois des photos en cours...');
      await loading.present();
      this.photoService.getStorages(id)
          .then(() => {
              loading.dismiss();
              this.toast.logMessage('Envoi des photos réussi.', 'success');
          })
          .catch(() => {
              loading.dismiss();
          });
  }
  async dropAll() {
      const loading = await this.loadFunct('Suppression en cours...');
      await loading.present();
      const result = this.sql.databaseTables.map((table) => {
          return this.sql.dropTable(table.table);
      });
      Promise.all(result)
          .then((response) => {
              loading.dismiss();
              console.log(response);
              this.jwt.logOut();
          })
          .catch(() => {
              loading.dismiss();
          });
  }
  async alertDrop() {
      const alert = await this.alertController.create({
          header: 'Veuillez confirmer',
          subHeader: 'Conseil : Vous allez suprimer toutes les données !',
          message: 'Vous allez supprimez toutes vos visites et photos.<br>' +
              'Vérifiez que toutes vos visites et photos ont était envoyé sur le serveur.<br>',
          buttons: [
              {
                  text: 'Annuler',
                  role: 'cancel',
                  cssClass: 'alertNeutral',
                  handler: () => {
                  }
              }, {
                  text: 'Confirmer',
                  cssClass: 'alertDanger',
                  handler: () => {
                      this.dropAll();
                  }
              }
          ]
      });
      await alert.present();
  }
  async loadFunct(message) {
      return await this.loading.create({
            message,
        });
  }
}
