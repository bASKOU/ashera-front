import { Component, OnInit } from '@angular/core';
import { SqliteService } from '../../services/sqlite/sqlite.service';
import { VisitService } from 'src/app/services/visit/visit.service';
import { ApiService } from 'src/app/services/api/api.service';
import { Storage } from '@ionic/storage';
import { PhotoCommentComponent } from '../../components/modals/photo-coment/photo-comment.component';
import { ModalController } from '@ionic/angular';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
    constructor(
        private sq: SqliteService,
        private visiteService: VisitService,
        private api: ApiService,
        private storage: Storage,
        private modalController: ModalController
    ) { }
  ngOnInit() {
  }
  tryIt() {
    this.sq.createDB()
        .then((result: any) => {
          if (result) {
              this.sq.databaseTables.map((table) => {
                  this.sq.createTable(table)
                      .then((res) => {
                          if (res) {
                              console.log(JSON.stringify(res) + 'création de table');
                          }
                      })
                      .catch((error) => {
                          console.log(JSON.stringify(error) + 'création de table');
                      });
              });
          }
        })
        .catch((error) => {
          console.log(JSON.stringify(error) + 'création BDD');
        });
  }
  insert() {
      this.sq.insertRow({table: 'visite_cas', column: 'cas, notable', value: '("Etat général du site", 1)'})
          .then((res) => {
              console.log(JSON.stringify(res));
          })
          .catch((error) => {
              console.log(JSON.stringify(error));
          });
  }
  selectAll() {
    this.sq.getRows('visite_cas')
        .then((res) => {
            console.log('visite_cas', res);
        })
        .catch((error) => {
            console.log(JSON.stringify(error));
        });
    this.sq.getRows('visite_status')
    .then((res) => {
        console.log('visite_status', res);
    })
    .catch((error) => {
        console.log(JSON.stringify(error));
    });
    this.sq.getRows('visite_note')
    .then((res) => {
        console.log('visite_note', res);
    })
    .catch((error) => {
        console.log(JSON.stringify(error));
    });
    this.sq.getRows('visite')
        .then((res) => {
            console.log('visite', res);
        })
        .catch((error) => {
            console.log(JSON.stringify(error));
        });
    this.sq.getRows('visite_liste_cas')
    .then((res) => {
        console.log('visite_liste_cas', res);
    })
    .catch((error) => {
        console.log(JSON.stringify(error));
    });
  }
  delete() {
      this.sq.deleteRow('1', {table: 'visite', column: '1' })
          .then((res) => {
              console.log(JSON.stringify(res));
          })
          .catch((error) => {
              console.log(JSON.stringify(error));
          });
  }

  fetchListCas() {
    this.api.syncUtilityTables();
  }

  createVisitListCas() {
    this.visiteService.constructVisitListeCas();
  }
    async presentModal(img, storageKey) {
      console.log(img);
      const modal = await this.modalController.create({
            component: PhotoCommentComponent,
            componentProps: {
                picture: img,
                storageKey
            }
        });
      return await modal.present();
    }

    getAllTablesFromDB() {
        this.sq.getRows('sqlite_master', 'name', 'WHERE type=\'table\'')
        .then((res: any) => {
            console.log(res);
            res.forEach(async (value: any) => {
                const result = await this.sq.getRows(value.name)
                .then((resp) => {
                    return resp;
                });
                console.log(' ' + value.name + ' : ', result);
            });
        });
    }
    updateVisit(idVisit: number = 1) {
        let data = {nameAgent: 'NameAgent', date: '2019-11-19', comment: 'BlaBla'};
        this.visiteService.initVisit(1, data)
        .then((res: any) => {
            console.log(res);
        })
        .catch((err: any) => {
            console.log(err);
        });
    }
    getVisitData() {
        this.visiteService.getVisitData();
    }
    joinQuery() {
        this.visiteService.getVisitPoData();
    }
    prepareToSend() {
        this.visiteService.endVisit2();
    }

}
