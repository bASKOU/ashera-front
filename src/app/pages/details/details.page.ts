import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PhotoService } from '../../services/photo/photo.service';
import { ModalController } from '@ionic/angular';
import { PhotoCommentComponent } from '../../components/modals/photo-coment/photo-comment.component';
import { SqliteService } from '../../services/sqlite/sqlite.service';
import { VisitService } from 'src/app/services/visit/visit.service';
import { ToastService } from '../../services/toast/toast.service';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {
  public title: any;
  public comment: any;
  public id: any;
  public type: any;
  public notes: any;
  public casName: any;
  public cas: any;
  public idVisit: any;
  public notable: number;
  private storageName: any;
  public countFailure = 0;

  constructor(private route: ActivatedRoute,
              private photoService: PhotoService,
              private modalController: ModalController,
              private sql: SqliteService,
              private visitService: VisitService,
              private toast: ToastService
  ) {
    this.getURIData()
        .then(() => {
          this.setStorageName()
              .then((response) => {
                this.storageName = response;
                this.photoService.loadStoredImages(this.storageName);
                this.photoService.checkStorage(response, this.idVisit)
                    .then((resp) => {
                      if (resp === true) {
                        this.photoService.insertStorageName(this.storageName, this.idVisit)
                            .then(() => {
                            })
                            .catch(() => {
                            });
                      }
                    });
              });
          this.getCasList(this.type, this.id)
              .then((response) => {
                if (response !== undefined) {
                  this.cas = response[0];
                  this.comment = response[0].commentaire;
                  if (this.type === 'cas') {
                    this.getCas(response[0].id_visite_cas)
                        .then((res) => {
                          this.casName = res[0].cas;
                          this.notable = res[0].notable;
                          this.visitService.getUtilitaryTables()
                              .then((resp) => {
                                this.notes = resp.note;
                              });
                        });
                  }
                }
              });
        });
  }
  ngOnInit() {
  }
  // Method to get datas from URL
  async getURIData() {
    this.id = await this.route.snapshot.paramMap.get('id');
    console.log(this.id);
    this.title = await this.route.snapshot.paramMap.get('title');
    this.type = await this.route.snapshot.paramMap.get('type');
    this.idVisit = await this.route.snapshot.paramMap.get('idVisit');
  }
  // Method to lunch modal to show photo detail and insert comment
  async presentModal(img, pos, type, id) {
    const modal = await this.modalController.create({
      component: PhotoCommentComponent,
      componentProps: {
        picture: img,
        storageKey: this.storageName,
        position: pos,
        type,
        id
      }
    });
    return await modal.present();
  }
  // Method to add photo to favorite
  addFavorite(table, idPhoto) {
    this.photoService.favoritePicture(table, idPhoto);
  }
  // Method to get all the "cas"
  getCasList(type, id) {
    const result = this.getType(type, id);
    const column = '*';
    const options = ' WHERE ' + result.condition.column + ' = ("' + result.condition.value + '")';
    return this.sql.getRows(result.table, column, options);
  }
  // Method to insert comment
  insertComment(entry, type, id) {
    const result = this.getType(type, id);
    const temp = entry.com.value;
    const modified = temp.replace('"', '\'');
    this.photoService.insertComment(modified, result.table, result.condition)
      .then(() => {
        this.toast.logMessage('Commentaire enregistré avec succès.', 'success');
        this.countFailure = 0;
        this.getCasList(type, id)
          .then((response) => {
            this.comment = response[0].commentaire;
          });
        return this.visitService.updateVisiteData(1, result.table, 'status', result.condition.column, result.condition.value);
      })
      .catch(() => {
        if (this.countFailure < 1) { // Condition to check if error happen at the first try or second try
          this.toast.logMessage('Une erreur est survenue. Merci de valider à nouveau.', 'dark');
          this.countFailure++;
        } else {
          this.toast.logMessage('Une erreur est survenue. Merci de vous déconnecter et redémarrer votre application.', 'dark');
        }
        // console.log(error);
      });
  }
  // Method to change data for request for different type of value
  getType(type, id) {
    if (type === 'equip') {
      return { condition: { column: 'id_visite_equipement', value: id }, table: 'visite_equipement' };
    } else if (type === 'cas') {
      return { condition: { column: 'id_visite_liste_cas', value: id }, table: 'visite_liste_cas' };
    }
  }
  // Method to change note of the 'cas'
  onRateChange(event: any, type: string, id: any) {
    const table = 'visite_liste_cas';
    console.log(event);
    this.sql.updateOneColumn({
      table,
      column: 'id_visite_note',
      columnValue: event,
      conditionColumn: 'id_visite_liste_cas',
      conditionValue: id
    })
      .then(() => {
        this.toast.logMessage('Note mise à jour', 'success');
      })
      .catch(() => {
        if (this.countFailure < 1) {
          this.toast.logMessage('Une erreur est survenue. Merci de noter à nouveau.', 'dark');
          this.countFailure++;
        } else {
          this.toast.logMessage('Une erreur est survenue. Merci de vous déconnecter et redémarrer votre application.', 'dark');
        }
      });
  }
  // Method to hydrate select options
  createInterfaceOptions(headerCustom: string, subHeaderCustom: string = '', messageCustom: string = '') {
    return {
      header: headerCustom,
      subHeader: subHeaderCustom,
      message: messageCustom,
      translucent: true
    };
  }
  // Method to get all data for this cas
  getCas(idCas) {
    return this.sql.getRows('visite_cas', '*', ' WHERE id_visite_cas = ' + this.cas.id_visite_cas);
  }
  // Method to create and return a storage name for each cas or equipment
  async setStorageName() {
    return await this.id +
      this.title.split(' ').join('').toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '') +
      this.idVisit +
      this.type;
  }
}
