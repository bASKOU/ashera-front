import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitHomePage } from './visit-home.page';

describe('VisitHomePage', () => {
  let component: VisitHomePage;
  let fixture: ComponentFixture<VisitHomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitHomePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitHomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
