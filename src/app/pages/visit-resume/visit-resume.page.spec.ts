import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitResumePage } from './visit-resume.page';

describe('VisitResumePage', () => {
  let component: VisitResumePage;
  let fixture: ComponentFixture<VisitResumePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitResumePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitResumePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
