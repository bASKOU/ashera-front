import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { VisitService } from 'src/app/services/visit/visit.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-visit-resume',
  templateUrl: './visit-resume.page.html',
  styleUrls: ['./visit-resume.page.scss'],
})
export class VisitResumePage implements OnInit {

  public resumeId: any;
  public resumeTitle: any;
  public partieOuvrage: any[];
  public progressCount: number;
  public progressLength: number;

  constructor(private route: ActivatedRoute,
              private loading: LoadingService,
              private visitService: VisitService,
              private alertController: AlertController,
              private router: Router) {
    this.progressCount = 0;
    this.resumeId = this.route.snapshot.paramMap.get('resumeId');
    this.resumeTitle = this.route.snapshot.paramMap.get('resumeTitle');
    this.visitService.getVisitData(this.resumeId)
    .then((res: any) => {
      this.partieOuvrage = res;
      this.partieOuvrage.forEach((value: any) => {
        if (value.hasOwnProperty('status')) {
          if (value.status > 0) {
            this.progressCount ++;
          }
        }
      });
      this.progressLength = this.partieOuvrage.length;
    })
    .catch(() => {
    });
  }

  ngOnInit() {

  }

  endVisit(idVisit: any) {
    this.visitService.updateVisiteData(3, 'visite', 'id_visite_status', 'id_visite', idVisit).then((res) => {
      this.router.navigateByUrl('/profil');
    });
  }
  async displayAlert(id: any) {
    const alert = await this.alertController.create({
      header: 'Veuillez confirmer',
      message: ' Vous allez clôturer cette visite.<br> Cette opération est <strong>irréversible</strong>',
      buttons: [
        {
          text: 'Annuler',
          role: 'cancel',
          cssClass: 'alertNeutral',
          handler: () => {
          }
        }, {
          text: 'Confirmer',
          cssClass: 'alertDanger',
          handler: () => {
            this.endVisit(id);
          }
        }
      ]
    });
    await alert.present();
  }


  getResumeData(idOuvrage: any) {
    // this.loading.createLoading();
  }
  evalMyVisit(id: number) {
  }
}
