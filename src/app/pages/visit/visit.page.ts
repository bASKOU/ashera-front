import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { InitvisitComponent } from '../../components/modals/initvisit/initvisit.component';
import { VisitService } from 'src/app/services/visit/visit.service';

@Component({
  selector: 'app-visit',
  templateUrl: './visit.page.html',
  styleUrls: ['./visit.page.scss'],
})
export class VisitPage implements OnInit {

  public viewSegment: any;
  public resumeVisit: any;
  public initVisit: any;

  /* NEED TO ADD AN OBSERVABLE FOR INITVISIT ARRAY AND RESUMEVISIT ARRAY CAUSE /
    - Redirect after an init from the modal make the view at it's state n-1
  */
  constructor(private modalController: ModalController, private visiteService: VisitService) {
    this.viewSegment = 'resume';
  }
  ngOnInit() {
    this.initData();
  }
  ionViewWillEnter() {
    this.initData();
  }
  initData() {
    this.visiteService.fetchVisit('resume').then((res: any) => {
      this.resumeVisit = res ;
    });
    this.visiteService.fetchVisit('init').then((res: any) => {
      this.initVisit = res ;
    });
  }

  segmentChanged(segment: any) {
    this.viewSegment = segment;
  }

  async presentModal(data: any) {
    const modal = await this.modalController.create({
      component: InitvisitComponent,
      componentProps: {
        nameOuvrage: data.name,
        idVisit: data.id
      }
    });

    modal.onDidDismiss().then((res: any) => {
      console.log('modal dismissed -→ ', res);
      this.initData();
    });
    return await modal.present();
  }
}
