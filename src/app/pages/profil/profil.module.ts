import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ProfilPage } from './profil.page';
import {ComponentsModule} from '../../components/components.module';

const routes: Routes = [
  {
    path: 'profil',
    component: ProfilPage,
    children: [
      {
        path: 'preparation',
        loadChildren: 'src/app/pages/preparation/preparation.module#PreparationPageModule'
      },
      {
        path: 'send',
        loadChildren: 'src/app/pages/send/send.module#SendPageModule'
      },
      {
        path: 'history',
        loadChildren: 'src/app/pages/history/history.module#HistoryPageModule',
      },
    ]
  },
  {
    path: '',
    redirectTo: 'profil/preparation',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProfilPage],
  exports: [RouterModule]
})
export class ProfilPageModule {}
