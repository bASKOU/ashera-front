import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./pages/home/home.module').then( m => m.HomePageModule)}, // Dev Purposes Only
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
  { path: 'details/:title/:storage/:type/:id/:idVisit', loadChildren: './pages/details/details.module#DetailsPageModule' },
  { path: 'profil', loadChildren: './pages/profil/profil.module#ProfilPageModule' },
  { path: 'history-details', loadChildren: './pages/history-details/history-details.module#HistoryDetailsPageModule' },
  { path: 'preparation', loadChildren: './pages/preparation/preparation.module#PreparationPageModule' },
  { path: 'send', loadChildren: './pages/send/send.module#SendPageModule' },
  { path: 'visit', loadChildren: './pages/visit/visit.module#VisitPageModule' },
  { path: 'visit-resume/:resumeId/:resumeTitle', loadChildren: './pages/visit-resume/visit-resume.module#VisitResumePageModule' },
  { path: 'visit-eval/:partieOuvrageId/:partieOuvrageTitle/:ouvrageName',
    loadChildren: './pages/visit-eval/visit-eval.module#VisitEvalPageModule' },
  { path: 'visit-eval/:partieOuvrageId/:partieOuvrageTitle/:ouvrageName/:idVisit',
    loadChildren: './pages/visit-eval/visit-eval.module#VisitEvalPageModule' },
  { path: 'history', loadChildren: './pages/history/history.module#HistoryPageModule' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
