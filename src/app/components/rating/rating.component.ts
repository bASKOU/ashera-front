import { Component, OnInit, Input, Output, EventEmitter, NgModule } from '@angular/core';

@Component({
  selector: 'app-rating',
  templateUrl: './rating.component.html',
  styleUrls: ['./rating.component.scss'],
})

export class RatingComponent implements OnInit {

  @Input() maxRating: number;
  @Input() rating: number;
  @Input() fontSize: string;

  @Output() ratingChange: EventEmitter<number> = new EventEmitter();

  public readonly: boolean;
  public activeColor: string;
  public defaultColor: string;
  public activeIcon: string;
  public defaultIcon: string;
  public tooltip: any[];
  constructor() {
    this.readonly = false;
    this.activeColor = '#69aec4';
    this.defaultColor = '#f4f4f4';
    this.activeIcon = 'star';
    this.defaultIcon = 'star-outline';
    this.tooltip = [
      'Absent',
      'Mauvais',
      'Moyen',
      'Bon'
    ];
  }

  ngOnInit() {
  }

  rate(value: number) {
    if (this.rating === value && value === 2) {
      this.rating = 1;
    } else {
      this.rating = value;
    }
    this.ratingChange.emit(this.rating);
  }

  colorRating(value: number) {
    return value > this.rating ? this.defaultColor : this.activeColor;
  }

  isEqualRate(value: number) {
    return this.rating === value;
  }

  ratingMax(size: any) {
    if (!size) {
      size = 5;
    }
    // size - 1 to let a '0' value
    return new Array((size - 1)).fill(1).map((value, index) => {
      return index + 2;
    });
  }

}
