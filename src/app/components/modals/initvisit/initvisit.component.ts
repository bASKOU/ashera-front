import { Component, OnInit, Input, NgModule } from '@angular/core';
import { ModalController, NavController} from '@ionic/angular';
import { SqliteService } from 'src/app/services/sqlite/sqlite.service';
import { Router } from '@angular/router';
import { ToastService } from 'src/app/services/toast/toast.service';
import { LoadingService } from 'src/app/services/loading/loading.service';
import { VisitService } from 'src/app/services/visit/visit.service';

@Component({
  selector: 'app-initvisit',
  templateUrl: './initvisit.component.html',
  styleUrls: ['./initvisit.component.scss'],
})


export class InitvisitComponent implements OnInit {

  @Input() nameOuvrage: string;
  @Input() idVisit: number;

  public visitForm: any;

  constructor(
    private router: Router,
    private modal: ModalController,
    private navCtrl: NavController,
    private sqlite: SqliteService,
    private toast: ToastService,
    private loading: LoadingService,
    private visitService: VisitService,
  ) {
    this.visitForm = {
      nameAgent: '',
      date: '',
      comment: ''
    };
  }

  ngOnInit() {}

  initVisit(form: any) {
    // this.loading.createLoading();
    // console.log('id Visit ', this.idVisit);
    const data = form.form.value;
    // console.log('form values: ', form.form.value);
    // let data = {nameAgent: 'NameAgent', date: '2019-11-19', comment: 'BlaBla'};
    this.visitService.initVisit(this.idVisit, data).then((result: any) => {
      // console.log('result of initialisation : ', result);
      this.toast.logMessage('Visite initialisée avec succès !', 'success');
      this.closeModal();
      this.router.navigateByUrl('/visit-resume/' + this.idVisit + '/' + this.nameOuvrage);
    }).catch((error) => {
      // console.error(error);
    });
  }

  closeModal() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modal.dismiss({
      dismissed: true
    });
  }

}
