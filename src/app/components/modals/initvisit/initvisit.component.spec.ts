import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InitvisitComponent } from './initvisit.component';

describe('InitvisitComponent', () => {
  let component: InitvisitComponent;
  let fixture: ComponentFixture<InitvisitComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InitvisitComponent ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InitvisitComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
