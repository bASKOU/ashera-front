import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ToastService } from 'src/app/services/toast/toast.service';
import { LoadingService } from 'src/app/services/loading/loading.service';

@Component({
  selector: 'app-detail-equip-po',
  templateUrl: './detail-equip-po.component.html',
  styleUrls: ['./detail-equip-po.component.scss'],
})
export class DetailEquipPoComponent implements OnInit {
  @Input() name: any;
  @Input() id: any;
  @Input() type: any;

  constructor(
    private router: Router,
    private modal: ModalController,
    private toast: ToastService,
    private loading: LoadingService
  ) { }

  ngOnInit() {}

  closeModal() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modal.dismiss({
      dismissed: true
    });
  }

  confirmUpdate(event: any){
    console.log(event);
  }
}
