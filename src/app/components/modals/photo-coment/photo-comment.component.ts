import { Component, OnInit, Input } from '@angular/core';
import { PhotoService } from '../../../services/photo/photo.service';
import { ModalController } from '@ionic/angular';
/** Import services */
import {SqliteService} from '../../../services/sqlite/sqlite.service';
import {ToastService} from '../../../services/toast/toast.service';

@Component({
  selector: 'app-photo-coment',
  templateUrl: './photo-comment.component.html',
  styleUrls: ['./photo-comment.component.scss'],
})
export class PhotoCommentComponent implements OnInit {
  // Input from modal controller
  @Input() picture: any;
  @Input() storageKey: string;
  @Input() position: number;
  @Input() type: string;
  @Input() id: number;
  // Props
  comment: any;
  countFailure = 0;
  constructor(private photoService: PhotoService,
              private modal: ModalController,
              private sql: SqliteService,
              private toast: ToastService) { }

  ngOnInit() {
    this.getComment()
        .then((response) => {
          this.comment = response[0].commentaire;
        })
        .catch((error) => {
          console.log(error);
        });
  }
  // Method to delete an image
  delete(imgEntry, position, storageKey, id, type) {
    this.photoService.deleteImage(imgEntry, position, storageKey, id, type);
    this.closeModal();
  }
  // Method to close the modal
  closeModal() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modal.dismiss({
      dismissed: true
    });
  }
  // Method to add picture as favorite
  addFavorite(table, idPhoto) {
    this.photoService.favoritePicture(table, idPhoto);
  }
  // Method to get comment from internal BDD
  getComment() {
    const table = 'visite_photo';
    const column = 'commentaire';
    const options = ' WHERE url = ("' + this.picture.name + '")';
    return this.sql.getRows(table, column, options);
  }
  // Method to insert comment
  insertComment(entry) {
    console.log(entry.com.value);
    const table = 'visite_photo';
    const condition = {column: 'id_visite_photo', value: this.picture.id};
    const temp = entry.com.value;
    const modified = temp.replace('"', '\'');
    return this.photoService.insertComment(modified, table, condition)
        .then(() => {
          this.countFailure = 0;
          this.toast.logMessage('Commentaire enregistré avec succes.', 'success');
          this.closeModal();
        })
        .catch(() => {
          if (this.countFailure < 1) {
            this.toast.logMessage('Une erreur est survenue. Merci de valider à nouveau.', 'dark');
            this.countFailure++;
          } else {
            this.toast.logMessage('Une erreur est survenue. Merci de vous déconneter et redémarrer votre application.', 'dark');
          }
        });
  }
}
