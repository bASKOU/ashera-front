import { InitvisitComponent } from './modals/initvisit/initvisit.component';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
/**
 * Here goes all the component you need to use as "views"
 */
import { HeaderComponent } from './header/header.component';
import { RatingComponent } from './rating/rating.component';
import { DetailEquipPoComponent } from './modals/detail-equip-po/detail-equip-po.component';
import { IonicModule } from '@ionic/angular';
import { PhotoCommentComponent } from './modals/photo-coment/photo-comment.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    HeaderComponent,
    InitvisitComponent,
    RatingComponent,
    DetailEquipPoComponent,
    PhotoCommentComponent
  ],
  entryComponents: [
    InitvisitComponent,
    DetailEquipPoComponent,
    PhotoCommentComponent
  ],
  exports: [
    HeaderComponent,
    InitvisitComponent,
    RatingComponent,
    DetailEquipPoComponent,
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ComponentsModule { }
