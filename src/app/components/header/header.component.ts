import { Component, OnInit, Input } from '@angular/core';
import { JwtService } from '../../services/jwt/jwt.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  private _logOut: boolean = false;

  @Input() customTitle: any;
  @Input() customButton: any;
  @Input()
  set logOut(value) {
    this._logOut = value !== 'false';
  }

  constructor(
      private jwt: JwtService,
      ) {
  }

  ngOnInit() {
    /*
    console.log(this.customTitle);
    console.table(this.customButton);
    */
    if (this._logOut) {
      this.customButton = {
        icon: 'log-out',
        function: 'logout'
      };
    }
  }

  hello(event) {
    console.log(event);
    console.log('Hello, this.customButton.function work');
  }

  logout() {
    this.jwt.logOut();
  }
}
