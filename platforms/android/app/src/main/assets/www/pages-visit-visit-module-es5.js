(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-visit-visit-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/visit/visit.page.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/visit/visit.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n\r\n  <app-header [customTitle]=\"'Visites'\"></app-header>\r\n\r\n  <ion-segment [(ngModel)]=\"viewSegment\" value=\"resume\" (ionChange)=\"initData()\">\r\n      <ion-segment-button value=\"resume\">\r\n          <ion-label>Reprendre </ion-label>\r\n      </ion-segment-button> \r\n      <ion-segment-button value=\"init\">\r\n          <ion-label>Initialisation </ion-label>\r\n      </ion-segment-button>\r\n    </ion-segment>\r\n\r\n</ion-header>\r\n\r\n<ion-content>\r\n\r\n<div [ngSwitch]=\"viewSegment\">\r\n\r\n  <ion-list *ngSwitchCase=\"'resume'\">\r\n      <ion-list-header class=\"ion-text-center\">\r\n          <ion-label>Reprendre une visite en cours</ion-label>\r\n      </ion-list-header>\r\n\r\n      <ion-item *ngFor=\"let resume of resumeVisit\">\r\n        <ion-label>{{resume.nom_ouvrage}}</ion-label>\r\n        <ion-button slot=\"end\" color=\"primary\" [routerLink]=\"'/visit-resume/'+resume.id_visite+'/'+resume.nom_ouvrage\" routerDirection=\"forward\">\r\n          Reprendre\r\n          <ion-icon slot=\"end\" name=\"play\"></ion-icon>\r\n        </ion-button>\r\n\r\n      </ion-item>\r\n\r\n  </ion-list>\r\n\r\n  <ion-list *ngSwitchCase=\"'init'\">\r\n\r\n    <ion-list-header class=\"ion-text-center\">\r\n        <ion-label>Initialiser une visite</ion-label>\r\n    </ion-list-header>\r\n\r\n    <ion-item *ngFor=\"let init of initVisit\">\r\n      <ion-label>{{init.nom_ouvrage}}</ion-label>\r\n      <ion-button slot=\"end\" color=\"primary\" (click)=\"presentModal({id: init.id_visite, name: init.nom_ouvrage})\">\r\n        Initialiser \r\n        <ion-icon slot=\"end\" name=\"play\"></ion-icon>\r\n      </ion-button>\r\n    </ion-item>\r\n\r\n  </ion-list>\r\n\r\n</div>\r\n\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/visit/visit.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/visit/visit.module.ts ***!
  \*********************************************/
/*! exports provided: VisitPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitPageModule", function() { return VisitPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _visit_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./visit.page */ "./src/app/pages/visit/visit.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








var routes = [
    {
        path: '',
        component: _visit_page__WEBPACK_IMPORTED_MODULE_6__["VisitPage"]
    }
];
var VisitPageModule = /** @class */ (function () {
    function VisitPageModule() {
    }
    VisitPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_visit_page__WEBPACK_IMPORTED_MODULE_6__["VisitPage"]]
        })
    ], VisitPageModule);
    return VisitPageModule;
}());



/***/ }),

/***/ "./src/app/pages/visit/visit.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/visit/visit.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Zpc2l0L3Zpc2l0LnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/visit/visit.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/visit/visit.page.ts ***!
  \*******************************************/
/*! exports provided: VisitPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitPage", function() { return VisitPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_modals_initvisit_initvisit_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../components/modals/initvisit/initvisit.component */ "./src/app/components/modals/initvisit/initvisit.component.ts");
/* harmony import */ var src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/visit/visit.service */ "./src/app/services/visit/visit.service.ts");





var VisitPage = /** @class */ (function () {
    /* NEED TO ADD AN OBSERVABLE FOR INITVISIT ARRAY AND RESUMEVISIT ARRAY CAUSE /
      - Redirect after an init from the modal make the view at it's state n-1
    */
    function VisitPage(modalController, visiteService) {
        this.modalController = modalController;
        this.visiteService = visiteService;
        this.viewSegment = 'resume';
    }
    VisitPage.prototype.ngOnInit = function () {
        this.initData();
    };
    VisitPage.prototype.ionViewWillEnter = function () {
        this.initData();
    };
    VisitPage.prototype.initData = function () {
        var _this = this;
        this.visiteService.fetchVisit('resume').then(function (res) {
            _this.resumeVisit = res;
        });
        this.visiteService.fetchVisit('init').then(function (res) {
            _this.initVisit = res;
        });
    };
    VisitPage.prototype.segmentChanged = function (segment) {
        this.viewSegment = segment;
    };
    VisitPage.prototype.presentModal = function (data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _components_modals_initvisit_initvisit_component__WEBPACK_IMPORTED_MODULE_3__["InitvisitComponent"],
                            componentProps: {
                                nameOuvrage: data.name,
                                idVisit: data.id
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        modal.onDidDismiss().then(function (res) {
                            console.log('modal dismissed -→ ', res);
                            _this.initData();
                        });
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    VisitPage.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
        { type: src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_4__["VisitService"] }
    ]; };
    VisitPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-visit',
            template: __webpack_require__(/*! raw-loader!./visit.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/visit/visit.page.html"),
            styles: [__webpack_require__(/*! ./visit.page.scss */ "./src/app/pages/visit/visit.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"], src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_4__["VisitService"]])
    ], VisitPage);
    return VisitPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-visit-visit-module-es5.js.map