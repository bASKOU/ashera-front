(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-preparation-preparation-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/preparation/preparation.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/preparation/preparation.page.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\r\n  <ion-list *ngIf=\"visitForm\">\r\n    <ion-list-header>Faites votre choix</ion-list-header>\r\n    <!-- Form Group -->\r\n    <form [formGroup]=\"visitForm\" (ngSubmit)=\"submitForm()\" class=\"ion-text-center\">\r\n      <ion-item *ngFor=\"let formElement of visitForm.controls | keyvalue\">\r\n        <ng-container>\r\n          <ion-label [color]=\"visit[formElement.key] ? '' : 'tertiary'\">{{formTitle[formElement.key]}} :  </ion-label>\r\n          <ion-select \r\n            id=\"{{formElement.key}}\"\r\n            placeholder=\"Selectionnez\"\r\n            okText=\"Valider\"\r\n            cancelText=\"Annuler\"\r\n            [formControlName]=\"formElement.key\"\r\n            (click)=\"setClicked($event)\"\r\n            (ionChange)=\"testInputEvent2($event, formElement.key)\"\r\n            required\r\n            [interfaceOptions] = \"generateInterfaceOptions(formTitle[formElement.key])\"\r\n            [multiple] = \"formElement.key == 'ouvrage' ? true : null\"\r\n            class=\"ion-text-wrap\"\r\n          >\r\n  <!-- \r\n            <ion-select-option \r\n            *ngIf=\"formElement.key == 'ouvrage' && visit[formElement.key]\"\r\n            [ngModel]=\"selectCheckbox\"\r\n            [ngModelOptions]=\"{standalone: true}\"\r\n            >Select All / Deselect All</ion-select-option> -->\r\n            <ion-select-option *ngFor=\"let select of visit[formElement.key]\" value=\"{{select.id}}\">{{select.name}}</ion-select-option>\r\n          </ion-select>\r\n        </ng-container> <!-- \r\n        <ng-template #elseBlock class=\"ion-justify-content-around\">\r\n          <ion-label [color]=\"visit[formElement.key] ? '' : 'tertiary'\" slot=\"start\">{{formTitle[formElement.key]}} :  </ion-label>\r\n          <ion-label item-right class=\"ion-text-end\"><span style=\"vertical-align: middle; color: #a2a4ab\">Selectionnez</span><ion-icon name=\"arrow-dropdown\" style=\"font-size: 19px; vertical-align: middle; color: #a2a4ab\" class=\"ion-align-item-center\"></ion-icon></ion-label>\r\n        </ng-template> -->\r\n        </ion-item>\r\n      \r\n\r\n\r\n\r\n<!-- \r\n    <ion-item *ngIf=\"collectivite\">\r\n      <ion-label>Collectivité </ion-label>\r\n      <ion-select\r\n        (ionChange)=\"testInputEvent($event)\"\r\n        id=\"collectivite\"\r\n        placeholder=\"Selectionnez\"\r\n        okText=\"Valider\"\r\n        cancelText=\"Annuler\"\r\n        [(ngModel)]=\"collectivite.id\"\r\n        name=\"collectivite\"\r\n        required\r\n      >\r\n        <ion-select-option\r\n          *ngFor=\"let select of collectivite\"\r\n          value=\"{{select.id}}\"\r\n          >{{select.name}}</ion-select-option\r\n        >\r\n      </ion-select>\r\n    </ion-item>\r\n\r\n    <ion-item *ngIf=\"entiteGestion\">\r\n      <ion-label>Entité de Gestion </ion-label>\r\n      <ion-select\r\n        (ionChange)=\"testInputEvent($event)\"\r\n        id=\"entiteGestion\"\r\n        placeholder=\"Selectionnez\"\r\n        okText=\"Valider\"\r\n        cancelText=\"Annuler\"\r\n        [(ngModel)]=\"entiteGestion.id\"\r\n        name=\"entiteGestion\"\r\n        required\r\n      >\r\n        <ion-select-option\r\n          *ngFor=\"let select of entiteGestion\"\r\n          value=\"{{ select.id }}\"\r\n          >{{ select.name }}</ion-select-option\r\n        >\r\n      </ion-select>\r\n    </ion-item>\r\n\r\n    <ion-item *ngIf=\"infrastructure\">\r\n      <ion-label>Infrastructure </ion-label>\r\n      <ion-select\r\n        (ionChange)=\"testInputEvent($event)\"\r\n        id=\"infrastructure\"\r\n        placeholder=\"Selectionnez\"\r\n        okText=\"Valider\"\r\n        cancelText=\"Annuler\"\r\n        required\r\n      >\r\n        <ion-select-option\r\n          *ngFor=\"let select of infrastructure\"\r\n          value=\"{{ select.id }}\"\r\n          >{{ select.name }}</ion-select-option\r\n        >\r\n      </ion-select>\r\n    </ion-item>\r\n\r\n    <ion-item *ngIf=\"ouvrage\">\r\n      <ion-label>Ouvrage </ion-label>\r\n      <ion-select\r\n        (ionChange)=\"testInputEvent($event)\"\r\n        id=\"ouvrage\"\r\n        placeholder=\"Selectionnez\"\r\n        okText=\"Valider\"\r\n        cancelText=\"Annuler\"\r\n        required\r\n      >\r\n        <ion-select-option\r\n          *ngFor=\"let select of ouvrage\"\r\n          value=\"{{ select.id }}\"\r\n          >{{ select.name }}</ion-select-option\r\n        >\r\n      </ion-select>\r\n    </ion-item> -->\r\n\r\n    <!-- CREATE VISITE -->\r\n    <div class=\"ion-padding ion-margin\">\r\n      <ion-button *ngIf=\"visitForm.valid\" type=\"submit\" color=\"secondary\" strong>Créer la visite</ion-button>\r\n    </div>\r\n    <div class=\"ion-padding ion-margin\">\r\n      <ion-button (click)=\"submitTest()\" color=\"tertiary\" strong >Test Visit</ion-button>\r\n    </div>\r\n  </form>\r\n  </ion-list>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/preparation/preparation.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/preparation/preparation.module.ts ***!
  \*********************************************************/
/*! exports provided: PreparationPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PreparationPageModule", function() { return PreparationPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _preparation_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./preparation.page */ "./src/app/pages/preparation/preparation.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








const routes = [
    {
        path: '',
        component: _preparation_page__WEBPACK_IMPORTED_MODULE_6__["PreparationPage"]
    }
];
let PreparationPageModule = class PreparationPageModule {
};
PreparationPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_preparation_page__WEBPACK_IMPORTED_MODULE_6__["PreparationPage"]]
    })
], PreparationPageModule);



/***/ }),

/***/ "./src/app/pages/preparation/preparation.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/preparation/preparation.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".no-display {\n  display: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcHJlcGFyYXRpb24vQzpcXFVzZXJzXFxiQVNLT1VcXERlc2t0b3BcXGFzaGVyYV9mcm9udC9zcmNcXGFwcFxccGFnZXNcXHByZXBhcmF0aW9uXFxwcmVwYXJhdGlvbi5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3ByZXBhcmF0aW9uL3ByZXBhcmF0aW9uLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGFBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3ByZXBhcmF0aW9uL3ByZXBhcmF0aW9uLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5uby1kaXNwbGF5e1xyXG4gICAgZGlzcGxheTpub25lO1xyXG59IiwiLm5vLWRpc3BsYXkge1xuICBkaXNwbGF5OiBub25lO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/preparation/preparation.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/preparation/preparation.page.ts ***!
  \*******************************************************/
/*! exports provided: PreparationPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PreparationPage", function() { return PreparationPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/visit/visit.service */ "./src/app/services/visit/visit.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");





let PreparationPage = class PreparationPage {
    constructor(formBuilder, visitService, storage) {
        this.formBuilder = formBuilder;
        this.visitService = visitService;
        this.storage = storage;
        this.selectCheckbox = false;
        this.selectIndeterminate = false;
        this.storage.get('clientId')
            .then((response) => {
            this.idClient = response;
            this.visitService.visitPrepareOption(this.orderForm[0], this.idClient).then((result) => {
                this.visit.collectivite = result;
            });
            /* formAttributes */
            this.visitForm = this.formBuilder.group({
                collectivite: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                entiteGestion: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                infrastructure: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required],
                ouvrage: ['', _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required]
            });
        })
            .catch(() => {
        });
        this.orderForm = [
            'collectivite',
            'entiteGestion',
            'infrastructure',
            'ouvrage'
        ];
        this.formTitle = {
            collectivite: 'Collectivité',
            entiteGestion: 'Entité de Gestion',
            infrastructure: 'Infrastructure',
            ouvrage: 'Ouvrage'
        };
        this.visit = {
            collectivite: '',
            entiteGestion: '',
            infrastructure: '',
            ouvrage: ''
        };
    }
    ngOnInit() {
    }
    getData(dataName, dataId) {
        if (dataId) {
            const indexChoice = this.orderForm.indexOf(dataName);
            /* Condition to test if we are at the end of our form */
            if (indexChoice >= 0 && indexChoice < this.orderForm.length - 1) {
                /* Form ended ? False */
                this.formEnded = false;
                /* Preparation of the next input */
                const nextChoice = this.orderForm[indexChoice + 1];
                /* In case of changing previous input, reinitialise all of them after the one selected */
                for (let i = indexChoice + 1; i < this.orderForm.length; i++) {
                    const propName = this.orderForm[i];
                    this.visit[propName] = null; // reinitialize select option array
                    this.visitForm.controls[propName].reset(); // reinitialize input value;
                }
                /* Call the service to fill the next input */
                this.visitService.visitPrepareOption(nextChoice, dataId).then((result) => {
                    this.visit[nextChoice] = result;
                });
            }
            else {
                console.log('Form ended !');
                this.formEnded = true;
            }
        }
    }
    submitForm() {
        console.log('form', this.visitForm.value);
        const idOuvrage = this.visitForm.value.ouvrage;
        if (idOuvrage) {
            if (typeof idOuvrage === 'object') {
                const promises = [];
                idOuvrage.forEach((val, index) => {
                    promises.push(new Promise((resolve) => {
                        this.visitService.fetchVisitData(val)
                            .then(() => {
                            console.log('Visite crée avec succès !' + (index + 1) + ' / ' + idOuvrage.length);
                            resolve(true);
                        });
                    }));
                    Promise.all(promises).then(() => {
                        console.log('Toutes les visites crées avec succès !');
                    })
                        .catch((err) => {
                        console.error('Erreur  survenue lors de la création multiple de visite →', err);
                    });
                });
            }
            else {
                this.visitService.fetchVisitData(idOuvrage)
                    .then(() => {
                });
            }
        }
    }
    submitTest() {
        this.visitService.fetchVisitData(2930);
    }
    compare(a, b) {
        return (a.name < b.name) ? -1 : 1;
    }
    testInputEvent2(event, input) {
        if (event.target === this.clicked) {
            this.getData(input, event.target.value);
        }
    }
    setClicked(event) {
        this.clicked = event.target;
    }
    generateInterfaceOptions(header) {
        return {
            header,
            translucent: true
        };
    }
};
PreparationPage.ctorParameters = () => [
    { type: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"] },
    { type: src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_3__["VisitService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] }
];
PreparationPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-preparation',
        template: __webpack_require__(/*! raw-loader!./preparation.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/preparation/preparation.page.html"),
        styles: [__webpack_require__(/*! ./preparation.page.scss */ "./src/app/pages/preparation/preparation.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormBuilder"],
        src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_3__["VisitService"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"]])
], PreparationPage);



/***/ })

}]);
//# sourceMappingURL=pages-preparation-preparation-module-es2015.js.map