(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-history-details-history-details-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/history-details/history-details.page.html":
/*!*******************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/history-details/history-details.page.html ***!
  \*******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n    <app-header [customTitle]=\"'Historique'\"></app-header>\r\n    <ion-segment [(ngModel)]=\"viewSegment\">\r\n        <ion-segment-button value=\"equipement\">\r\n            <ion-label>Équipements</ion-label>\r\n        </ion-segment-button>\r\n        <ion-segment-button value=\"listecas\">\r\n            <ion-label>Liste Cas</ion-label>\r\n        </ion-segment-button>\r\n    </ion-segment>\r\n</ion-header>\r\n\r\n\r\n<ion-content [ngSwitch]=\"viewSegment\">\r\n\r\n    <ng-container *ngSwitchCase=\"'equipement'\">\r\n        <ng-container *ngFor=\"let equip of equipment; let i = index\">\r\n            <ion-card class=\"ion-text-center\">\r\n                <img  src=\"{{equip.url_photo_favoris ? 'http://90.113.154.212/ashera/uploads/' + equip.url_photo_favoris : 'assets/img/no-photo-available.png' }}\" />\r\n                <!--          <ion-card-title>Liste Equipement n° {{ equip.id_visite_equipement}}</ion-card-title>-->\r\n                <ion-card-title>{{equip.nom_equipement}}</ion-card-title>\r\n                <ion-card-content class=\"ion-text-start\">Commentaire: {{equip.commentaire}}</ion-card-content>\r\n                <ion-card-content class=\"ion-text-start\">Note: {{equip.note}} </ion-card-content>\r\n            </ion-card>\r\n        </ng-container>\r\n    </ng-container>\r\n\r\n    <ng-container *ngSwitchCase=\"'listecas'\">\r\n        <ng-container *ngFor=\"let listCas of listeCas; let j = index\">\r\n            <ion-card class=\"ion-text-center\">\r\n                 <img  src=\"{{listCas.url_photo_favoris ? 'http://90.113.154.212/ashera/uploads/' + listCas.url_photo_favoris : 'assets/img/no-photo-available.png' }}\" />\r\n                <!-- <ion-card-title>Liste de Cas n° {{ listCas.id_visite_liste_cas}}</ion-card-title>-->\r\n                <ion-card-header>\r\n                    <ion-card-title>{{listCas.cas}}</ion-card-title>\r\n                </ion-card-header>\r\n                <ion-card-content class=\"ion-text-start\">\r\n                    Commentaire: {{listCas.commentaire}}\r\n                </ion-card-content>\r\n                <ion-card-content class=\"ion-text-start\">\r\n                    Note: {{listCas.note}}\r\n                </ion-card-content>\r\n            </ion-card>\r\n        </ng-container>\r\n    </ng-container>\r\n\r\n</ion-content>\r\n\r\n"

/***/ }),

/***/ "./src/app/pages/history-details/history-details.module.ts":
/*!*****************************************************************!*\
  !*** ./src/app/pages/history-details/history-details.module.ts ***!
  \*****************************************************************/
/*! exports provided: HistoryDetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryDetailsPageModule", function() { return HistoryDetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _history_details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./history-details.page */ "./src/app/pages/history-details/history-details.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








const routes = [
    {
        path: '',
        component: _history_details_page__WEBPACK_IMPORTED_MODULE_6__["HistoryDetailsPage"]
    }
];
let HistoryDetailsPageModule = class HistoryDetailsPageModule {
};
HistoryDetailsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["ReactiveFormsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_history_details_page__WEBPACK_IMPORTED_MODULE_6__["HistoryDetailsPage"]]
    })
], HistoryDetailsPageModule);



/***/ }),

/***/ "./src/app/pages/history-details/history-details.page.scss":
/*!*****************************************************************!*\
  !*** ./src/app/pages/history-details/history-details.page.scss ***!
  \*****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2hpc3RvcnktZGV0YWlscy9oaXN0b3J5LWRldGFpbHMucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/history-details/history-details.page.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/history-details/history-details.page.ts ***!
  \***************************************************************/
/*! exports provided: HistoryDetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HistoryDetailsPage", function() { return HistoryDetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_api_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/api/api.service */ "./src/app/services/api/api.service.ts");





let HistoryDetailsPage = class HistoryDetailsPage {
    constructor(storage, api, 
    // tslint:disable-next-line:max-line-length
    route, router) {
        this.storage = storage;
        this.api = api;
        this.route = route;
        this.router = router;
        this.data = this.route.snapshot.params.id;
        this.viewSegment = 'equipement';
        this.route.queryParams.subscribe(params => {
            if (this.router.getCurrentNavigation().extras.state) {
                this.data = this.router.getCurrentNavigation().extras.state.id;
            }
        });
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        this.getDataApi();
    }
    getDataApi() {
        this.api.apiGetRequest('history/history/', this.data)
            .then((res) => {
            for (const prop in res) {
                if (prop === 'liste_equipement') {
                    this.equipment = res[prop];
                }
                else if (prop === 'liste_cas') {
                    this.listeCas = res[prop];
                }
            }
        });
    }
};
HistoryDetailsPage.ctorParameters = () => [
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"] },
    { type: _services_api_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
];
HistoryDetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-history-details',
        template: __webpack_require__(/*! raw-loader!./history-details.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/history-details/history-details.page.html"),
        styles: [__webpack_require__(/*! ./history-details.page.scss */ "./src/app/pages/history-details/history-details.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"],
        _services_api_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
], HistoryDetailsPage);



/***/ })

}]);
//# sourceMappingURL=pages-history-details-history-details-module-es2015.js.map