(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-profil-profil-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/profil/profil.page.html":
/*!*************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/profil/profil.page.html ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-tabs>\r\n  \r\n  <ion-header >\r\n      <app-header customTitle=\"Profil\" logOut></app-header>\r\n    </ion-header>\r\n\r\n  <ion-tab-bar slot=\"top\">\r\n\r\n    <ion-tab-button tab=\"preparation\">\r\n      <ion-icon name=\"download\"></ion-icon>\r\n      <ion-label>Préparation</ion-label>\r\n    </ion-tab-button>\r\n\r\n    <ion-tab-button tab=\"send\">\r\n      <ion-icon name=\"send\"></ion-icon>\r\n      <ion-label>Envoi</ion-label>\r\n    </ion-tab-button>\r\n\r\n    <ion-tab-button tab=\"history\">\r\n        <ion-icon name=\"time\"></ion-icon>\r\n        <ion-label>Historique</ion-label>\r\n    </ion-tab-button>\r\n\r\n  </ion-tab-bar>\r\n\r\n</ion-tabs>\r\n"

/***/ }),

/***/ "./src/app/pages/profil/profil.module.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/profil/profil.module.ts ***!
  \***********************************************/
/*! exports provided: ProfilPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilPageModule", function() { return ProfilPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _profil_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./profil.page */ "./src/app/pages/profil/profil.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");








const routes = [
    {
        path: 'profil',
        component: _profil_page__WEBPACK_IMPORTED_MODULE_6__["ProfilPage"],
        children: [
            {
                path: 'preparation',
                loadChildren: 'src/app/pages/preparation/preparation.module#PreparationPageModule'
            },
            {
                path: 'send',
                loadChildren: 'src/app/pages/send/send.module#SendPageModule'
            },
            {
                path: 'history',
                loadChildren: 'src/app/pages/history/history.module#HistoryPageModule',
            },
        ]
    },
    {
        path: '',
        redirectTo: 'profil/preparation',
        pathMatch: 'full'
    }
];
let ProfilPageModule = class ProfilPageModule {
};
ProfilPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_profil_page__WEBPACK_IMPORTED_MODULE_6__["ProfilPage"]],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"]]
    })
], ProfilPageModule);



/***/ }),

/***/ "./src/app/pages/profil/profil.page.scss":
/*!***********************************************!*\
  !*** ./src/app/pages/profil/profil.page.scss ***!
  \***********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-button {\n  --box-shadow: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvcHJvZmlsL0M6XFxVc2Vyc1xcYkFTS09VXFxEZXNrdG9wXFxhc2hlcmFfZnJvbnQvc3JjXFxhcHBcXHBhZ2VzXFxwcm9maWxcXHByb2ZpbC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3Byb2ZpbC9wcm9maWwucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7QUNDSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Byb2ZpbC9wcm9maWwucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiaW9uLWJ1dHRvbntcclxuICAgIC0tYm94LXNoYWRvdzogbm9uZTtcclxufSIsImlvbi1idXR0b24ge1xuICAtLWJveC1zaGFkb3c6IG5vbmU7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/profil/profil.page.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/profil/profil.page.ts ***!
  \*********************************************/
/*! exports provided: ProfilPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProfilPage", function() { return ProfilPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let ProfilPage = class ProfilPage {
    constructor() { }
    ngOnInit() {
    }
};
ProfilPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-profil',
        template: __webpack_require__(/*! raw-loader!./profil.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/profil/profil.page.html"),
        styles: [__webpack_require__(/*! ./profil.page.scss */ "./src/app/pages/profil/profil.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], ProfilPage);



/***/ })

}]);
//# sourceMappingURL=pages-profil-profil-module-es2015.js.map