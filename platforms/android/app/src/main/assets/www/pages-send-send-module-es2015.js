(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-send-send-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/send/send.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/send/send.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content>\r\n<ion-list>\r\n  <ion-list-header class=\"ion-padding\">\r\n    <ion-label>{{visitClosed.length > 1 ? 'Visites' : 'Visite'}} à envoyer</ion-label>\r\n    <ion-label>Total : {{visitClosed.length}}</ion-label>\r\n  </ion-list-header>\r\n  <ng-container *ngFor=\"let toSend of visitClosed\">\r\n    <ion-grid>\r\n    <ion-row style=\"border-bottom: 1px solid #ddd\">\r\n      <ion-col size=\"6\" class=\"ion-align-self-center\">\r\n        <ion-row>\r\n          <ion-label class=\"ion-text-wrap\">{{toSend.nom_ouvrage}}</ion-label>\r\n        </ion-row>\r\n        <ion-row>\r\n          <ion-label color=\"medium\" lines=\"none\">Commencée le : {{convertDate(toSend.date)}}</ion-label>\r\n        </ion-row>\r\n      </ion-col>\r\n      <ion-col size=\"6\" class=\"ion-align-self-center\">\r\n        <ion-buttons>\r\n          <ion-button color=\"success\" fill=\"solid\" expand=\"block\" (click)=\"displayAlert(toSend.id_visite)\">Envoyer</ion-button>\r\n          <ion-button color=\"success\" fill=\"solid\" expand=\"block\" (click)=\"upload(toSend.id_visite)\">Upload</ion-button>\r\n        </ion-buttons>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-grid>\r\n    \r\n  </ng-container>\r\n</ion-list>\r\n</ion-content>\r\n<ion-footer>\r\n  <ion-toolbar color=\"danger\">\r\n    <ion-button fill=\"clear\" expand=\"full\" color=\"light\" (click)=\"alertDrop()\">\r\n      Vider les visites</ion-button>\r\n  </ion-toolbar>\r\n</ion-footer>\r\n"

/***/ }),

/***/ "./src/app/pages/send/send.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/send/send.module.ts ***!
  \*******************************************/
/*! exports provided: SendPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendPageModule", function() { return SendPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _send_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./send.page */ "./src/app/pages/send/send.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








const routes = [
    {
        path: '',
        component: _send_page__WEBPACK_IMPORTED_MODULE_6__["SendPage"]
    }
];
let SendPageModule = class SendPageModule {
};
SendPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_send_page__WEBPACK_IMPORTED_MODULE_6__["SendPage"]]
    })
], SendPageModule);



/***/ }),

/***/ "./src/app/pages/send/send.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/send/send.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3NlbmQvc2VuZC5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/send/send.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/send/send.page.ts ***!
  \*****************************************/
/*! exports provided: SendPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SendPage", function() { return SendPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/visit/visit.service */ "./src/app/services/visit/visit.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_api_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/api/api.service */ "./src/app/services/api/api.service.ts");
/* harmony import */ var src_app_services_photo_photo_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/photo/photo.service */ "./src/app/services/photo/photo.service.ts");
/* harmony import */ var _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");
/* harmony import */ var _services_jwt_jwt_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/jwt/jwt.service */ "./src/app/services/jwt/jwt.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../../services/toast/toast.service */ "./src/app/services/toast/toast.service.ts");










let SendPage = class SendPage {
    constructor(visitService, alertController, apiService, photoService, sql, jwt, loading, storage, toast) {
        this.visitService = visitService;
        this.alertController = alertController;
        this.apiService = apiService;
        this.photoService = photoService;
        this.sql = sql;
        this.jwt = jwt;
        this.loading = loading;
        this.storage = storage;
        this.toast = toast;
        this.uri = 'cloture/clotureVisite';
        this.options = { 'Access-Control-Allow-Headers': '*', 'Access-Control-Allow-Credentials': 'true', 'Content-Type': 'application/json' };
        this.visitClosed = [];
    }
    ngOnInit() {
    }
    ionViewWillEnter() {
        this.visitService.getClosedVisit().then((res) => {
            this.visitClosed = res;
        });
    }
    convertDate(date) {
        const formatedDate = new Date(date);
        return formatedDate.toLocaleDateString('fr-FR');
    }
    /* Create An Alert when trying to send visit ! */
    displayAlert(idVisit) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Veuillez confirmer',
                subHeader: 'Conseil : Assurez-vous d\'avoir assez de batterie !',
                message: 'Vous allez procéder à un archivage.<br>' +
                    'Cette procédure peut prendre un certain temps.<br>' +
                    'Veuillez garder votre appareil allumé pendant la durée complète du processus.',
                buttons: [
                    {
                        text: 'Annuler',
                        role: 'cancel',
                        cssClass: 'alertNeutral',
                        handler: () => {
                        }
                    }, {
                        text: 'Confirmer',
                        cssClass: 'alertDanger',
                        handler: () => {
                            this.storage.clear();
                            this.closeVisit(idVisit);
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    closeVisit(id) {
        const promises = [];
        const arrayIdVisit = [];
        if (typeof id === 'object') {
            id.forEach((value, index) => {
                promises.push(this.visitService.syncVisitMethod(value));
                arrayIdVisit.push(value);
            });
        }
        else {
            promises.push(this.visitService.syncVisitMethod(id));
            arrayIdVisit.push(id);
        }
        Promise.all(promises).then((res) => {
            res.forEach((val, index) => {
                console.log('Will be send to API', res);
                this.apiService.apiPostRequest([val], this.uri, this.options)
                    .then((resp) => {
                    console.log('Result from the Api', resp);
                })
                    .catch((error) => {
                    console.error('Error while trying to sync with API', error);
                });
            });
        });
    }
    upload(id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadFunct('Envois des photos en cours...');
            yield loading.present();
            this.photoService.getStorages(id)
                .then(() => {
                loading.dismiss();
                this.toast.logMessage('Envoi des photos réussi.', 'success');
            })
                .catch(() => {
                loading.dismiss();
            });
        });
    }
    dropAll() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const loading = yield this.loadFunct('Suppression en cours...');
            yield loading.present();
            const result = this.sql.databaseTables.map((table) => {
                return this.sql.dropTable(table.table);
            });
            Promise.all(result)
                .then((response) => {
                loading.dismiss();
                console.log(response);
                this.jwt.logOut();
            })
                .catch(() => {
                loading.dismiss();
            });
        });
    }
    alertDrop() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const alert = yield this.alertController.create({
                header: 'Veuillez confirmer',
                subHeader: 'Conseil : Vous allez suprimer toutes les données !',
                message: 'Vous allez supprimez toutes vos visites et photos.<br>' +
                    'Vérifiez que toutes vos visites et photos ont était envoyé sur le serveur.<br>',
                buttons: [
                    {
                        text: 'Annuler',
                        role: 'cancel',
                        cssClass: 'alertNeutral',
                        handler: () => {
                        }
                    }, {
                        text: 'Confirmer',
                        cssClass: 'alertDanger',
                        handler: () => {
                            this.dropAll();
                        }
                    }
                ]
            });
            yield alert.present();
        });
    }
    loadFunct(message) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return yield this.loading.create({
                message,
            });
        });
    }
};
SendPage.ctorParameters = () => [
    { type: src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_2__["VisitService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"] },
    { type: src_app_services_api_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"] },
    { type: src_app_services_photo_photo_service__WEBPACK_IMPORTED_MODULE_5__["PhotoService"] },
    { type: _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_6__["SqliteService"] },
    { type: _services_jwt_jwt_service__WEBPACK_IMPORTED_MODULE_7__["JwtService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_8__["Storage"] },
    { type: _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_9__["ToastService"] }
];
SendPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-send',
        template: __webpack_require__(/*! raw-loader!./send.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/send/send.page.html"),
        styles: [__webpack_require__(/*! ./send.page.scss */ "./src/app/pages/send/send.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_2__["VisitService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
        src_app_services_api_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"],
        src_app_services_photo_photo_service__WEBPACK_IMPORTED_MODULE_5__["PhotoService"],
        _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_6__["SqliteService"],
        _services_jwt_jwt_service__WEBPACK_IMPORTED_MODULE_7__["JwtService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["LoadingController"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_8__["Storage"],
        _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_9__["ToastService"]])
], SendPage);



/***/ })

}]);
//# sourceMappingURL=pages-send-send-module-es2015.js.map