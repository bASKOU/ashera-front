(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-details-details-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/details/details.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/details/details.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-header>\r\n    <ion-toolbar>\r\n      <ion-buttons slot=\"start\">\r\n        <ion-back-button  routerDirection=\"backward\"></ion-back-button>\r\n      </ion-buttons>\r\n      <ion-title>{{ title }}</ion-title>\r\n        <ion-icon [name]=\"'camera'\" color=\"primary\" size=\"large\" slot=\"end\" class=\"ion-margin\" (click)=\"photoService.selectImage(storageName, title, id, type)\"></ion-icon>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n</ion-header>\r\n<ion-content [scrollEvents]=\"true\">\r\n    <ion-item-sliding>\r\n      <ion-grid>\r\n        <ion-row>\r\n          <h2 class=\"ion-text-align\" *ngIf=\"!photoService.images[0]\">Prenez une photo</h2>\r\n          <ion-col size=\"6\" style=\"max-height: 150px\" *ngFor=\"let photo of photoService.images; index as pos; let color\">\r\n            <ion-row style=\"margin-bottom: -45px\">\r\n              <ion-col size=\"6\">\r\n                <ion-icon class=\"ion-float-left\" name=\"trash\" style=\"--z-index: 99;\" size=\"large\" color=\"warning\" (click)=\"photoService.deleteImage(photo, pos, storageName, id, type)\"></ion-icon>\r\n              </ion-col>\r\n              <ion-col size=\"6\">\r\n                <ion-icon class=\"ion-float-end\" name=\"heart\" style=\"--z-index: 99;\" size=\"large\" *ngIf=\"(photo.id === photoService.favorite)\" color=\"primary\" (click)=\"addFavorite('visite_photo', photo.id)\"></ion-icon>\r\n                <ion-icon class=\"ion-float-end\" name=\"heart\" style=\"--z-index: 99;\" size=\"large\" *ngIf=\"(photo.id !== photoService.favorite)\" color=\"medium\" (click)=\"addFavorite('visite_photo', photo.id)\"></ion-icon>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-thumbnail style=\"--size: 100%; margin: 0.2em;\">\r\n              <ion-img [src]=\"photo.path\" (click)=\"presentModal(photo, pos, type, id)\"></ion-img>\r\n            </ion-thumbnail>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n    </ion-item-sliding>\r\n</ion-content>\r\n<!--<ion-content>-->\r\n  <ion-content class=\"ion-padding\" style=\"max-height: 35%\">\r\n    <ion-label *ngIf=\"type === 'cas' && cas !== undefined && notable === 1\" position=\"fixed\">Note\r\n      <ion-select *ngIf=\"type === 'cas' && cas !== undefined\" [(ngModel)]=\"cas.id_visite_note\" [interfaceOptions]=\"createInterfaceOptions(casName)\" (ionChange)=\"onRateChange($event.detail.value, type, cas.id_visite_liste_cas )\" >\r\n        <ion-select-option *ngFor=\"let note of notes; let i = index\" [value]=\"note.id_visite_note\">{{note.note}}</ion-select-option>\r\n      </ion-select>\r\n    </ion-label>\r\n      <form #form>\r\n        <ion-label position=\"floating\">Commentaire</ion-label>\r\n        <ion-textarea placeholder=\"Entrez le commentaire...\" *ngIf=\"comment\" name=\"com\" rows=\"4\" cols=\"20\" style=\"border: 1px solid #d7d8da; border-radius:15px; padding:0.5em;margin-top: 1.2em\" value=\"{{comment}}\"></ion-textarea>\r\n        <ion-textarea placeholder=\"Entrez le commentaire...\" *ngIf=\"!comment\" name=\"com\" rows=\"4\" cols=\"20\" style=\"border: 1px solid #d7d8da; border-radius:15px; padding:0.5em;margin-top: 1.2em\" ></ion-textarea>\r\n      </form>\r\n</ion-content>\r\n<ion-footer no-shadow>\r\n  <ion-toolbar position=\"bottom\">\r\n    <ion-buttons slot=\"end\" class=\"ion-padding\">\r\n      <ion-button color=\"primary\" type=\"submit\" (click)=\"insertComment(form, type, id)\">Valider</ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-footer>\r\n"

/***/ }),

/***/ "./src/app/pages/details/details.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/details/details.module.ts ***!
  \*************************************************/
/*! exports provided: DetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailsPageModule", function() { return DetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./details.page */ "./src/app/pages/details/details.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");








const routes = [
    {
        path: '',
        component: _details_page__WEBPACK_IMPORTED_MODULE_6__["DetailsPage"]
    }
];
let DetailsPageModule = class DetailsPageModule {
};
DetailsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_details_page__WEBPACK_IMPORTED_MODULE_6__["DetailsPage"]]
    })
], DetailsPageModule);



/***/ }),

/***/ "./src/app/pages/details/details.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/details/details.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RldGFpbHMvZGV0YWlscy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/details/details.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/details/details.page.ts ***!
  \***********************************************/
/*! exports provided: DetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailsPage", function() { return DetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_photo_photo_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/photo/photo.service */ "./src/app/services/photo/photo.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_modals_photo_coment_photo_comment_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/modals/photo-coment/photo-comment.component */ "./src/app/components/modals/photo-coment/photo-comment.component.ts");
/* harmony import */ var _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");
/* harmony import */ var src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/visit/visit.service */ "./src/app/services/visit/visit.service.ts");
/* harmony import */ var _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/toast/toast.service */ "./src/app/services/toast/toast.service.ts");









let DetailsPage = class DetailsPage {
    constructor(route, photoService, modalController, sql, visitService, toast) {
        this.route = route;
        this.photoService = photoService;
        this.modalController = modalController;
        this.sql = sql;
        this.visitService = visitService;
        this.toast = toast;
        this.countFailure = 0;
        this.getURIData()
            .then(() => {
            this.setStorageName()
                .then((response) => {
                this.storageName = response;
                this.photoService.loadStoredImages(this.storageName);
                this.photoService.checkStorage(response, this.idVisit)
                    .then((resp) => {
                    if (resp === true) {
                        this.photoService.insertStorageName(this.storageName, this.idVisit)
                            .then(() => {
                        })
                            .catch(() => {
                        });
                    }
                });
            });
            this.getCasList(this.type, this.id)
                .then((response) => {
                if (response !== undefined) {
                    this.cas = response[0];
                    this.comment = response[0].commentaire;
                    if (this.type === 'cas') {
                        this.getCas(response[0].id_visite_cas)
                            .then((res) => {
                            this.casName = res[0].cas;
                            this.notable = res[0].notable;
                            this.visitService.getUtilitaryTables()
                                .then((resp) => {
                                this.notes = resp.note;
                            });
                        });
                    }
                }
            });
        });
    }
    ngOnInit() {
    }
    // Method to get datas from URL
    getURIData() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.id = yield this.route.snapshot.paramMap.get('id');
            console.log(this.id);
            this.title = yield this.route.snapshot.paramMap.get('title');
            this.type = yield this.route.snapshot.paramMap.get('type');
            this.idVisit = yield this.route.snapshot.paramMap.get('idVisit');
        });
    }
    // Method to lunch modal to show photo detail and insert comment
    presentModal(img, pos, type, id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const modal = yield this.modalController.create({
                component: _components_modals_photo_coment_photo_comment_component__WEBPACK_IMPORTED_MODULE_5__["PhotoCommentComponent"],
                componentProps: {
                    picture: img,
                    storageKey: this.storageName,
                    position: pos,
                    type,
                    id
                }
            });
            return yield modal.present();
        });
    }
    // Method to add photo to favorite
    addFavorite(table, idPhoto) {
        this.photoService.favoritePicture(table, idPhoto);
    }
    // Method to get all the "cas"
    getCasList(type, id) {
        const result = this.getType(type, id);
        const column = '*';
        const options = ' WHERE ' + result.condition.column + ' = ("' + result.condition.value + '")';
        return this.sql.getRows(result.table, column, options);
    }
    // Method to insert comment
    insertComment(entry, type, id) {
        const result = this.getType(type, id);
        const temp = entry.com.value;
        const modified = temp.replace('"', '\'');
        this.photoService.insertComment(modified, result.table, result.condition)
            .then(() => {
            this.toast.logMessage('Commentaire enregistré avec succès.', 'success');
            this.countFailure = 0;
            this.getCasList(type, id)
                .then((response) => {
                this.comment = response[0].commentaire;
            });
            return this.visitService.updateVisiteData(1, result.table, 'status', result.condition.column, result.condition.value);
        })
            .catch(() => {
            if (this.countFailure < 1) { // Condition to check if error happen at the first try or second try
                this.toast.logMessage('Une erreur est survenue. Merci de valider à nouveau.', 'dark');
                this.countFailure++;
            }
            else {
                this.toast.logMessage('Une erreur est survenue. Merci de vous déconnecter et redémarrer votre application.', 'dark');
            }
            // console.log(error);
        });
    }
    // Method to change data for request for different type of value
    getType(type, id) {
        if (type === 'equip') {
            return { condition: { column: 'id_visite_equipement', value: id }, table: 'visite_equipement' };
        }
        else if (type === 'cas') {
            return { condition: { column: 'id_visite_liste_cas', value: id }, table: 'visite_liste_cas' };
        }
    }
    // Method to change note of the 'cas'
    onRateChange(event, type, id) {
        const table = 'visite_liste_cas';
        console.log(event);
        this.sql.updateOneColumn({
            table,
            column: 'id_visite_note',
            columnValue: event,
            conditionColumn: 'id_visite_liste_cas',
            conditionValue: id
        })
            .then(() => {
            this.toast.logMessage('Note mise à jour', 'success');
        })
            .catch(() => {
            if (this.countFailure < 1) {
                this.toast.logMessage('Une erreur est survenue. Merci de noter à nouveau.', 'dark');
                this.countFailure++;
            }
            else {
                this.toast.logMessage('Une erreur est survenue. Merci de vous déconnecter et redémarrer votre application.', 'dark');
            }
        });
    }
    // Method to hydrate select options
    createInterfaceOptions(headerCustom, subHeaderCustom = '', messageCustom = '') {
        return {
            header: headerCustom,
            subHeader: subHeaderCustom,
            message: messageCustom,
            translucent: true
        };
    }
    // Method to get all data for this cas
    getCas(idCas) {
        return this.sql.getRows('visite_cas', '*', ' WHERE id_visite_cas = ' + this.cas.id_visite_cas);
    }
    // Method to create and return a storage name for each cas or equipment
    setStorageName() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            return (yield this.id) +
                this.title.split(' ').join('').toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '') +
                this.idVisit +
                this.type;
        });
    }
};
DetailsPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _services_photo_photo_service__WEBPACK_IMPORTED_MODULE_3__["PhotoService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
    { type: _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_6__["SqliteService"] },
    { type: src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_7__["VisitService"] },
    { type: _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"] }
];
DetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-details',
        template: __webpack_require__(/*! raw-loader!./details.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/details/details.page.html"),
        styles: [__webpack_require__(/*! ./details.page.scss */ "./src/app/pages/details/details.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
        _services_photo_photo_service__WEBPACK_IMPORTED_MODULE_3__["PhotoService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"],
        _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_6__["SqliteService"],
        src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_7__["VisitService"],
        _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"]])
], DetailsPage);



/***/ })

}]);
//# sourceMappingURL=pages-details-details-module-es2015.js.map