(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/home/home.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/home/home.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <app-header customTitle=\"Accueil\"></app-header>\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n    <ion-list>\r\n    <ion-item>\r\n      <ion-button color=\"primary\" (click)=\"tryIt()\">Test création SQLite</ion-button>\r\n      <ion-button color=\"primary\" (click)=\"insert()\">Insert</ion-button>\r\n      <ion-button color=\"primary\" (click)=\"selectAll()\">Select</ion-button>\r\n      <ion-button color=\"primary\" (click)=\"delete()\">Delete</ion-button>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-button color=\"primary\" (click)=\"fetchListCas()\">Fetch Liste Cas</ion-button>\r\n      <ion-button color=\"tertiary\" (click)=\"createVisitListCas()\">Create Liste Cas for visite</ion-button>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-button color=\"secondary\" (click)=\"getAllTablesFromDB()\">Get All Tables</ion-button>\r\n      <ion-button color=\"secondary\" (click)=\"updateVisit()\">Update Visit</ion-button>\r\n      <ion-button color=\"secondary\" (click)=\"getVisitData()\">Get Visit DATA</ion-button>\r\n    </ion-item>\r\n    <ion-item>\r\n      \r\n      <ion-button color=\"secondary\" (click)=\"joinQuery()\">Join with PO DATA</ion-button>\r\n      <ion-button color=\"secondary\" (click)=\"prepareToSend()\">Prepare to Send</ion-button>\r\n      \r\n    </ion-item>\r\n  </ion-list>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");








let HomePageModule = class HomePageModule {
};
HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                {
                    path: '',
                    component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                }
            ])
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "./src/app/pages/home/home.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-button {\n  --box-shadow: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS9DOlxcVXNlcnNcXGJBU0tPVVxcRGVza3RvcFxcYXNoZXJhX2Zyb250L3NyY1xcYXBwXFxwYWdlc1xcaG9tZVxcaG9tZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2hvbWUvaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBQTtBQ0NGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1idXR0b24ge1xyXG4gIC0tYm94LXNoYWRvdzogbm9uZTtcclxufVxyXG4iLCJpb24tYnV0dG9uIHtcbiAgLS1ib3gtc2hhZG93OiBub25lO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/home/home.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/home/home.page.ts ***!
  \*****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");
/* harmony import */ var src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/visit/visit.service */ "./src/app/services/visit/visit.service.ts");
/* harmony import */ var src_app_services_api_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/api/api.service */ "./src/app/services/api/api.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _components_modals_photo_coment_photo_comment_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/modals/photo-coment/photo-comment.component */ "./src/app/components/modals/photo-coment/photo-comment.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");








let HomePage = class HomePage {
    constructor(sq, visiteService, api, storage, modalController) {
        this.sq = sq;
        this.visiteService = visiteService;
        this.api = api;
        this.storage = storage;
        this.modalController = modalController;
    }
    ngOnInit() {
    }
    tryIt() {
        this.sq.createDB()
            .then((result) => {
            if (result) {
                this.sq.databaseTables.map((table) => {
                    this.sq.createTable(table)
                        .then((res) => {
                        if (res) {
                            console.log(JSON.stringify(res) + 'création de table');
                        }
                    })
                        .catch((error) => {
                        console.log(JSON.stringify(error) + 'création de table');
                    });
                });
            }
        })
            .catch((error) => {
            console.log(JSON.stringify(error) + 'création BDD');
        });
    }
    insert() {
        this.sq.insertRow({ table: 'visite_cas', column: 'cas, notable', value: '("Etat général du site", 1)' })
            .then((res) => {
            console.log(JSON.stringify(res));
        })
            .catch((error) => {
            console.log(JSON.stringify(error));
        });
    }
    selectAll() {
        this.sq.getRows('visite_cas')
            .then((res) => {
            console.log('visite_cas', res);
        })
            .catch((error) => {
            console.log(JSON.stringify(error));
        });
        this.sq.getRows('visite_status')
            .then((res) => {
            console.log('visite_status', res);
        })
            .catch((error) => {
            console.log(JSON.stringify(error));
        });
        this.sq.getRows('visite_note')
            .then((res) => {
            console.log('visite_note', res);
        })
            .catch((error) => {
            console.log(JSON.stringify(error));
        });
        this.sq.getRows('visite')
            .then((res) => {
            console.log('visite', res);
        })
            .catch((error) => {
            console.log(JSON.stringify(error));
        });
        this.sq.getRows('visite_liste_cas')
            .then((res) => {
            console.log('visite_liste_cas', res);
        })
            .catch((error) => {
            console.log(JSON.stringify(error));
        });
    }
    delete() {
        this.sq.deleteRow('1', { table: 'visite', column: '1' })
            .then((res) => {
            console.log(JSON.stringify(res));
        })
            .catch((error) => {
            console.log(JSON.stringify(error));
        });
    }
    fetchListCas() {
        this.api.syncUtilityTables();
    }
    createVisitListCas() {
        this.visiteService.constructVisitListeCas();
    }
    presentModal(img, storageKey) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log(img);
            const modal = yield this.modalController.create({
                component: _components_modals_photo_coment_photo_comment_component__WEBPACK_IMPORTED_MODULE_6__["PhotoCommentComponent"],
                componentProps: {
                    picture: img,
                    storageKey
                }
            });
            return yield modal.present();
        });
    }
    getAllTablesFromDB() {
        this.sq.getRows('sqlite_master', 'name', 'WHERE type=\'table\'')
            .then((res) => {
            console.log(res);
            res.forEach((value) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                const result = yield this.sq.getRows(value.name)
                    .then((resp) => {
                    return resp;
                });
                console.log(' ' + value.name + ' : ', result);
            }));
        });
    }
    updateVisit(idVisit = 1) {
        let data = { nameAgent: 'NameAgent', date: '2019-11-19', comment: 'BlaBla' };
        this.visiteService.initVisit(1, data)
            .then((res) => {
            console.log(res);
        })
            .catch((err) => {
            console.log(err);
        });
    }
    getVisitData() {
        this.visiteService.getVisitData();
    }
    joinQuery() {
        this.visiteService.getVisitPoData();
    }
    prepareToSend() {
        this.visiteService.endVisit2();
    }
};
HomePage.ctorParameters = () => [
    { type: _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_2__["SqliteService"] },
    { type: src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_3__["VisitService"] },
    { type: src_app_services_api_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ModalController"] }
];
HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-home',
        template: __webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/home/home.page.html"),
        styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/pages/home/home.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_2__["SqliteService"],
        src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_3__["VisitService"],
        src_app_services_api_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ModalController"]])
], HomePage);



/***/ })

}]);
//# sourceMappingURL=pages-home-home-module-es2015.js.map