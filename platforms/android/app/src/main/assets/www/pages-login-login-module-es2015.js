(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/login/login.page.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/login/login.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content  color=\"primary\">\r\n  <form  #form=\"ngForm\" (ngSubmit)=\"loginService(form)\">\r\n    <ion-grid>\r\n      <ion-row color=\"primary\" class=\"ion-justify-content-center\" >\r\n        <ion-col class=\"ion-align-self-center\" size-md=\"6\" size-lg=\"5\" size-xs=\"12\">\r\n          <div class=\"ion-text-center\">\r\n            <img src=\"assets/img/espelia_logo.png\">\r\n            <h3>ASHERA</h3>\r\n          </div>\r\n        </ion-col>\r\n      </ion-row>\r\n      <ion-row color=\"primary\" class=\"ion-justify-content-center\" >\r\n        <ion-col class=\"ion-align-self-center\" size-md=\"6\" size-lg=\"5\" size-xs=\"12\">\r\n        \r\n          <ion-card class=\"ion-padding\">\r\n            <!-- CARD HEADER -->\r\n            <ion-card-header class=\"ion-text-center\">\r\n              <ion-card-title size=\"large\" >Bienvenue</ion-card-title>\r\n              <ion-card-subtitle>Veuillez vous connecter en utilisant votre compte GS'O.</ion-card-subtitle>\r\n            </ion-card-header>\r\n\r\n            <!-- CARD BODY -->\r\n            <ion-card-content>\r\n              <ion-item>\r\n                  <ion-label position=\"stacked\" color=\"medium\"> Email </ion-label>\r\n                  <ion-input  name=\"email\" type=\"email\" ngModel=\"\" required></ion-input>\r\n                </ion-item>\r\n                <ion-item>\r\n                  <ion-label position=\"stacked\" color=\"medium\"> Mot de passe </ion-label>\r\n                  <ion-input name=\"password\" type=\"password\" ngModel=\"\" required></ion-input>\r\n                </ion-item>\r\n            </ion-card-content>\r\n\r\n          <!-- SUBMIT BUTTON -->\r\n          <div class=\"ion-padding ion-text-center\">\r\n            <ion-button strong shape=\"round\" color=\"secondary\" type=\"submit\" [disabled]=\"form.invalid\">Se connecter</ion-button>\r\n          </div>\r\n          </ion-card>\r\n        </ion-col>\r\n      </ion-row>\r\n    </ion-grid>\r\n  </form>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/login/login.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
let LoginPageModule = class LoginPageModule {
};
LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
            src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"]
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/pages/login/login.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".button-round {\n  /* box-shadow: 0px 14px 25px rgba(0, 0, 0, 0.59); */\n  --border-radius: 25px;\n  --box-shadow:20px;\n}\n\nion-grid {\n  --ion-grid-padding:20px;\n  margin-top: 20px;\n}\n\nion-card {\n  --background:#fff!important;\n  border-radius: 20px;\n}\n\nion-card ion-card-title {\n  font-size: 1.5rem;\n  margin-bottom: 5px;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbG9naW4vQzpcXFVzZXJzXFxiQVNLT1VcXERlc2t0b3BcXGFzaGVyYV9mcm9udC9zcmNcXGFwcFxccGFnZXNcXGxvZ2luXFxsb2dpbi5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLG1EQUFBO0VBQ0EscUJBQUE7RUFDQSxpQkFBQTtBQ0NKOztBRENBO0VBQ0ksdUJBQUE7RUFDQSxnQkFBQTtBQ0VKOztBREFBO0VBQ0ksMkJBQUE7RUFDQSxtQkFBQTtBQ0dKOztBREZJO0VBQ0ksaUJBQUE7RUFDQSxrQkFBQTtBQ0lSIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmJ1dHRvbi1yb3VuZHtcclxuICAgIC8qIGJveC1zaGFkb3c6IDBweCAxNHB4IDI1cHggcmdiYSgwLCAwLCAwLCAwLjU5KTsgKi9cclxuICAgIC0tYm9yZGVyLXJhZGl1czogMjVweDtcclxuICAgIC0tYm94LXNoYWRvdzoyMHB4O1xyXG59XHJcbmlvbi1ncmlke1xyXG4gICAgLS1pb24tZ3JpZC1wYWRkaW5nOjIwcHg7XHJcbiAgICBtYXJnaW4tdG9wOiAyMHB4O1xyXG59XHJcbmlvbi1jYXJke1xyXG4gICAgLS1iYWNrZ3JvdW5kOiNmZmYhaW1wb3J0YW50O1xyXG4gICAgYm9yZGVyLXJhZGl1czoyMHB4O1xyXG4gICAgaW9uLWNhcmQtdGl0bGV7XHJcbiAgICAgICAgZm9udC1zaXplOjEuNXJlbTtcclxuICAgICAgICBtYXJnaW4tYm90dG9tOjVweDtcclxuICAgIH1cclxufVxyXG4iLCIuYnV0dG9uLXJvdW5kIHtcbiAgLyogYm94LXNoYWRvdzogMHB4IDE0cHggMjVweCByZ2JhKDAsIDAsIDAsIDAuNTkpOyAqL1xuICAtLWJvcmRlci1yYWRpdXM6IDI1cHg7XG4gIC0tYm94LXNoYWRvdzoyMHB4O1xufVxuXG5pb24tZ3JpZCB7XG4gIC0taW9uLWdyaWQtcGFkZGluZzoyMHB4O1xuICBtYXJnaW4tdG9wOiAyMHB4O1xufVxuXG5pb24tY2FyZCB7XG4gIC0tYmFja2dyb3VuZDojZmZmIWltcG9ydGFudDtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbn1cbmlvbi1jYXJkIGlvbi1jYXJkLXRpdGxlIHtcbiAgZm9udC1zaXplOiAxLjVyZW07XG4gIG1hcmdpbi1ib3R0b206IDVweDtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/login/login.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_api_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/api/api.service */ "./src/app/services/api/api.service.ts");
/* harmony import */ var _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../services/sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_jwt_jwt_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/jwt/jwt.service */ "./src/app/services/jwt/jwt.service.ts");
/* harmony import */ var _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../services/toast/toast.service */ "./src/app/services/toast/toast.service.ts");

/** Core importations */


/** Services importations */


/** Components importations */



let LoginPage = class LoginPage {
    constructor(toast, router, apiService, menuCtrl, sql, jwt) {
        this.toast = toast;
        this.router = router;
        this.apiService = apiService;
        this.menuCtrl = menuCtrl;
        this.sql = sql;
        this.jwt = jwt;
        this.initDB();
        this.uri = 'login';
        this.options = { 'Access-Control-Allow-Headers': '*', 'Access-Control-Allow-Credentials': 'true' };
        this.menuCtrl.swipeGesture(false, 'main-content'); // Disable swipe to block menu on login page
    }
    ngOnInit() {
    }
    loginService(data) {
        /* *
         * Call our service with form's data and prop from page environment
         */
        this.apiService.apiPostRequest(data.value, this.uri, this.options)
            .then((result) => {
            if (result.messages) {
                this.toast.logMessage(result.messages.error, 'danger');
            }
            else {
                this.insertToken(result.token)
                    .then(() => {
                    this.jwt.checkValidity(result.token)
                        .then((response) => {
                        if (response === true) {
                            this.jwt.storageClientId()
                                .then(() => {
                                this.toast.logMessage('Connexion réussie !', 'success');
                                this.router.navigateByUrl('/profil');
                                // Sync utility tables when jwt is storred !
                                this.apiService.syncUtilityTables();
                            });
                        }
                        else {
                            this.jwt.logOut();
                        }
                    });
                })
                    .catch(() => {
                    this.toast.logMessage('Merci de vous reconnecter', 'dark');
                });
            }
        })
            .catch(() => {
            this.toast.logMessage('Une erreur est survenue !', 'dark');
        });
    }
    // Method to insert JWT to SQLite table
    insertToken(token) {
        const data = { table: 'visite_token', column: 'token', value: '("' + token + '")' };
        return this.sql.insertRow(data).then((res) => {
            return res;
        })
            .catch((error) => {
            return error;
        });
    }
    initDB() {
        return this.sql.createDB().then(() => {
            this.sql.databaseTables.map((table) => {
                this.sql.createTable(table)
                    .then((resp) => {
                    console.log(resp);
                });
            });
        });
    }
};
LoginPage.ctorParameters = () => [
    { type: _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_7__["ToastService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: src_app_services_api_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"] },
    { type: _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_4__["SqliteService"] },
    { type: _services_jwt_jwt_service__WEBPACK_IMPORTED_MODULE_6__["JwtService"] }
];
LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: __webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/login/login.page.html"),
        styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/pages/login/login.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_toast_toast_service__WEBPACK_IMPORTED_MODULE_7__["ToastService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        src_app_services_api_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["MenuController"],
        _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_4__["SqliteService"],
        _services_jwt_jwt_service__WEBPACK_IMPORTED_MODULE_6__["JwtService"]])
], LoginPage);



/***/ })

}]);
//# sourceMappingURL=pages-login-login-module-es2015.js.map