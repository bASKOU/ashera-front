(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-visit-resume-visit-resume-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/visit-resume/visit-resume.page.html":
/*!*************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/visit-resume/visit-resume.page.html ***!
  \*************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header color=\"primary\">\r\n    <app-header [customTitle]=\"resumeTitle\"></app-header>\r\n    <ion-toolbar>\r\n        <ion-buttons slot=\"start\">\r\n            <ion-back-button defaultHref=\"visit\" text=\"Retour\" icon=\"arrow-back\"></ion-back-button>\r\n        </ion-buttons>\r\n        <ion-button (click)=\"displayAlert(resumeId)\"  slot=\"end\" color=\"secondary\" class=\"ion-margin-end\"><ion-icon name=\"close\" slot=\"end\"></ion-icon>CLOTÛRER</ion-button>\r\n    </ion-toolbar>\r\n</ion-header>\r\n\r\n<ion-content>\r\n    <ion-list>\r\n        <ion-list-header class=\"ion-text-center ion-padding-horizontal\">\r\n            <ion-label item-left>Parties d'ouvrages</ion-label>\r\n            <ion-label item-right>{{progressLength}}</ion-label>\r\n        </ion-list-header>\r\n    \r\n        <ion-item *ngFor=\"let po of partieOuvrage\" lines=\"inset\">\r\n            <ion-label>{{po.nom_partie_ouvrage}}</ion-label>\r\n            <ion-button slot=\"end\" [color]=\"'primary'\" [routerLink]=\"'/visit-eval/' + po.id_visite_partie_ouvrage + '/' + po.nom_partie_ouvrage + '/' + resumeTitle + '/' + resumeId\" routerDirection=\"forward\">\r\n            Détails\r\n            </ion-button>\r\n        </ion-item>\r\n    </ion-list>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/visit-resume/visit-resume.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/visit-resume/visit-resume.module.ts ***!
  \***********************************************************/
/*! exports provided: VisitResumePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitResumePageModule", function() { return VisitResumePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _visit_resume_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./visit-resume.page */ "./src/app/pages/visit-resume/visit-resume.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");








var routes = [
    {
        path: '',
        component: _visit_resume_page__WEBPACK_IMPORTED_MODULE_6__["VisitResumePage"]
    }
];
var VisitResumePageModule = /** @class */ (function () {
    function VisitResumePageModule() {
    }
    VisitResumePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_visit_resume_page__WEBPACK_IMPORTED_MODULE_6__["VisitResumePage"]]
        })
    ], VisitResumePageModule);
    return VisitResumePageModule;
}());



/***/ }),

/***/ "./src/app/pages/visit-resume/visit-resume.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/visit-resume/visit-resume.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-button {\n  --box-shadow: none;\n}\n\n.alertDanger {\n  color: #f04141 !important;\n}\n\n.alertNeutral {\n  color: #989aa2 !important;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdmlzaXQtcmVzdW1lL0M6XFxVc2Vyc1xcYkFTS09VXFxEZXNrdG9wXFxhc2hlcmFfZnJvbnQvc3JjXFxhcHBcXHBhZ2VzXFx2aXNpdC1yZXN1bWVcXHZpc2l0LXJlc3VtZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3Zpc2l0LXJlc3VtZS92aXNpdC1yZXN1bWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0ksa0JBQUE7QUNDSjs7QURFQTtFQUNJLHlCQUFBO0FDQ0o7O0FERUE7RUFDSSx5QkFBQTtBQ0NKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvdmlzaXQtcmVzdW1lL3Zpc2l0LXJlc3VtZS5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tYnV0dG9ue1xyXG4gICAgLS1ib3gtc2hhZG93OiBub25lO1xyXG59XHJcblxyXG4uYWxlcnREYW5nZXJ7XHJcbiAgICBjb2xvcjogI2YwNDE0MSAhaW1wb3J0YW50O1xyXG59XHJcblxyXG4uYWxlcnROZXV0cmFse1xyXG4gICAgY29sb3I6ICM5ODlhYTIgIWltcG9ydGFudDtcclxufSIsImlvbi1idXR0b24ge1xuICAtLWJveC1zaGFkb3c6IG5vbmU7XG59XG5cbi5hbGVydERhbmdlciB7XG4gIGNvbG9yOiAjZjA0MTQxICFpbXBvcnRhbnQ7XG59XG5cbi5hbGVydE5ldXRyYWwge1xuICBjb2xvcjogIzk4OWFhMiAhaW1wb3J0YW50O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/visit-resume/visit-resume.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/visit-resume/visit-resume.page.ts ***!
  \*********************************************************/
/*! exports provided: VisitResumePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitResumePage", function() { return VisitResumePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_loading_loading_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/loading/loading.service */ "./src/app/services/loading/loading.service.ts");
/* harmony import */ var src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/visit/visit.service */ "./src/app/services/visit/visit.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");






var VisitResumePage = /** @class */ (function () {
    function VisitResumePage(route, loading, visitService, alertController, router) {
        var _this = this;
        this.route = route;
        this.loading = loading;
        this.visitService = visitService;
        this.alertController = alertController;
        this.router = router;
        this.progressCount = 0;
        this.resumeId = this.route.snapshot.paramMap.get('resumeId');
        this.resumeTitle = this.route.snapshot.paramMap.get('resumeTitle');
        this.visitService.getVisitData(this.resumeId)
            .then(function (res) {
            _this.partieOuvrage = res;
            _this.partieOuvrage.forEach(function (value) {
                if (value.hasOwnProperty('status')) {
                    if (value.status > 0) {
                        _this.progressCount++;
                    }
                }
            });
            _this.progressLength = _this.partieOuvrage.length;
        })
            .catch(function () {
        });
    }
    VisitResumePage.prototype.ngOnInit = function () {
    };
    VisitResumePage.prototype.endVisit = function (idVisit) {
        var _this = this;
        this.visitService.updateVisiteData(3, 'visite', 'id_visite_status', 'id_visite', idVisit).then(function (res) {
            _this.router.navigateByUrl('/profil');
        });
    };
    VisitResumePage.prototype.displayAlert = function (id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: 'Veuillez confirmer',
                            message: ' Vous allez clôturer cette visite.<br> Cette opération est <strong>irréversible</strong>',
                            buttons: [
                                {
                                    text: 'Annuler',
                                    role: 'cancel',
                                    cssClass: 'alertNeutral',
                                    handler: function () {
                                    }
                                }, {
                                    text: 'Confirmer',
                                    cssClass: 'alertDanger',
                                    handler: function () {
                                        _this.endVisit(id);
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    VisitResumePage.prototype.getResumeData = function (idOuvrage) {
        // this.loading.createLoading();
    };
    VisitResumePage.prototype.evalMyVisit = function (id) {
    };
    VisitResumePage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: src_app_services_loading_loading_service__WEBPACK_IMPORTED_MODULE_3__["LoadingService"] },
        { type: src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_4__["VisitService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] }
    ]; };
    VisitResumePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-visit-resume',
            template: __webpack_require__(/*! raw-loader!./visit-resume.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/visit-resume/visit-resume.page.html"),
            styles: [__webpack_require__(/*! ./visit-resume.page.scss */ "./src/app/pages/visit-resume/visit-resume.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            src_app_services_loading_loading_service__WEBPACK_IMPORTED_MODULE_3__["LoadingService"],
            src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_4__["VisitService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["AlertController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], VisitResumePage);
    return VisitResumePage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-visit-resume-visit-resume-module-es5.js.map