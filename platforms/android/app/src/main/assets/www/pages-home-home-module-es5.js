(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-home-home-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/home/home.page.html":
/*!*********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/home/home.page.html ***!
  \*********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <app-header customTitle=\"Accueil\"></app-header>\r\n</ion-header>\r\n\r\n<ion-content class=\"ion-padding\">\r\n    <ion-list>\r\n    <ion-item>\r\n      <ion-button color=\"primary\" (click)=\"tryIt()\">Test création SQLite</ion-button>\r\n      <ion-button color=\"primary\" (click)=\"insert()\">Insert</ion-button>\r\n      <ion-button color=\"primary\" (click)=\"selectAll()\">Select</ion-button>\r\n      <ion-button color=\"primary\" (click)=\"delete()\">Delete</ion-button>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-button color=\"primary\" (click)=\"fetchListCas()\">Fetch Liste Cas</ion-button>\r\n      <ion-button color=\"tertiary\" (click)=\"createVisitListCas()\">Create Liste Cas for visite</ion-button>\r\n    </ion-item>\r\n    <ion-item>\r\n      <ion-button color=\"secondary\" (click)=\"getAllTablesFromDB()\">Get All Tables</ion-button>\r\n      <ion-button color=\"secondary\" (click)=\"updateVisit()\">Update Visit</ion-button>\r\n      <ion-button color=\"secondary\" (click)=\"getVisitData()\">Get Visit DATA</ion-button>\r\n    </ion-item>\r\n    <ion-item>\r\n      \r\n      <ion-button color=\"secondary\" (click)=\"joinQuery()\">Join with PO DATA</ion-button>\r\n      <ion-button color=\"secondary\" (click)=\"prepareToSend()\">Prepare to Send</ion-button>\r\n      \r\n    </ion-item>\r\n  </ion-list>\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/home/home.module.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.module.ts ***!
  \*******************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home.page */ "./src/app/pages/home/home.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");








var HomePageModule = /** @class */ (function () {
    function HomePageModule() {
    }
    HomePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
                _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_5__["RouterModule"].forChild([
                    {
                        path: '',
                        component: _home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]
                    }
                ])
            ],
            declarations: [_home_page__WEBPACK_IMPORTED_MODULE_6__["HomePage"]]
        })
    ], HomePageModule);
    return HomePageModule;
}());



/***/ }),

/***/ "./src/app/pages/home/home.page.scss":
/*!*******************************************!*\
  !*** ./src/app/pages/home/home.page.scss ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-button {\n  --box-shadow: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvaG9tZS9DOlxcVXNlcnNcXGJBU0tPVVxcRGVza3RvcFxcYXNoZXJhX2Zyb250L3NyY1xcYXBwXFxwYWdlc1xcaG9tZVxcaG9tZS5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL2hvbWUvaG9tZS5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBQTtBQ0NGIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvaG9tZS9ob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbImlvbi1idXR0b24ge1xyXG4gIC0tYm94LXNoYWRvdzogbm9uZTtcclxufVxyXG4iLCJpb24tYnV0dG9uIHtcbiAgLS1ib3gtc2hhZG93OiBub25lO1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/home/home.page.ts":
/*!*****************************************!*\
  !*** ./src/app/pages/home/home.page.ts ***!
  \*****************************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");
/* harmony import */ var src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/visit/visit.service */ "./src/app/services/visit/visit.service.ts");
/* harmony import */ var src_app_services_api_api_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/api/api.service */ "./src/app/services/api/api.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _components_modals_photo_coment_photo_comment_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../components/modals/photo-coment/photo-comment.component */ "./src/app/components/modals/photo-coment/photo-comment.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");








var HomePage = /** @class */ (function () {
    function HomePage(sq, visiteService, api, storage, modalController) {
        this.sq = sq;
        this.visiteService = visiteService;
        this.api = api;
        this.storage = storage;
        this.modalController = modalController;
    }
    HomePage.prototype.ngOnInit = function () {
    };
    HomePage.prototype.tryIt = function () {
        var _this = this;
        this.sq.createDB()
            .then(function (result) {
            if (result) {
                _this.sq.databaseTables.map(function (table) {
                    _this.sq.createTable(table)
                        .then(function (res) {
                        if (res) {
                            console.log(JSON.stringify(res) + 'création de table');
                        }
                    })
                        .catch(function (error) {
                        console.log(JSON.stringify(error) + 'création de table');
                    });
                });
            }
        })
            .catch(function (error) {
            console.log(JSON.stringify(error) + 'création BDD');
        });
    };
    HomePage.prototype.insert = function () {
        this.sq.insertRow({ table: 'visite_cas', column: 'cas, notable', value: '("Etat général du site", 1)' })
            .then(function (res) {
            console.log(JSON.stringify(res));
        })
            .catch(function (error) {
            console.log(JSON.stringify(error));
        });
    };
    HomePage.prototype.selectAll = function () {
        this.sq.getRows('visite_cas')
            .then(function (res) {
            console.log('visite_cas', res);
        })
            .catch(function (error) {
            console.log(JSON.stringify(error));
        });
        this.sq.getRows('visite_status')
            .then(function (res) {
            console.log('visite_status', res);
        })
            .catch(function (error) {
            console.log(JSON.stringify(error));
        });
        this.sq.getRows('visite_note')
            .then(function (res) {
            console.log('visite_note', res);
        })
            .catch(function (error) {
            console.log(JSON.stringify(error));
        });
        this.sq.getRows('visite')
            .then(function (res) {
            console.log('visite', res);
        })
            .catch(function (error) {
            console.log(JSON.stringify(error));
        });
        this.sq.getRows('visite_liste_cas')
            .then(function (res) {
            console.log('visite_liste_cas', res);
        })
            .catch(function (error) {
            console.log(JSON.stringify(error));
        });
    };
    HomePage.prototype.delete = function () {
        this.sq.deleteRow('1', { table: 'visite', column: '1' })
            .then(function (res) {
            console.log(JSON.stringify(res));
        })
            .catch(function (error) {
            console.log(JSON.stringify(error));
        });
    };
    HomePage.prototype.fetchListCas = function () {
        this.api.syncUtilityTables();
    };
    HomePage.prototype.createVisitListCas = function () {
        this.visiteService.constructVisitListeCas();
    };
    HomePage.prototype.presentModal = function (img, storageKey) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(img);
                        return [4 /*yield*/, this.modalController.create({
                                component: _components_modals_photo_coment_photo_comment_component__WEBPACK_IMPORTED_MODULE_6__["PhotoCommentComponent"],
                                componentProps: {
                                    picture: img,
                                    storageKey: storageKey
                                }
                            })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    HomePage.prototype.getAllTablesFromDB = function () {
        var _this = this;
        this.sq.getRows('sqlite_master', 'name', 'WHERE type=\'table\'')
            .then(function (res) {
            console.log(res);
            res.forEach(function (value) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                var result;
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.sq.getRows(value.name)
                                .then(function (resp) {
                                return resp;
                            })];
                        case 1:
                            result = _a.sent();
                            console.log(' ' + value.name + ' : ', result);
                            return [2 /*return*/];
                    }
                });
            }); });
        });
    };
    HomePage.prototype.updateVisit = function (idVisit) {
        if (idVisit === void 0) { idVisit = 1; }
        var data = { nameAgent: 'NameAgent', date: '2019-11-19', comment: 'BlaBla' };
        this.visiteService.initVisit(1, data)
            .then(function (res) {
            console.log(res);
        })
            .catch(function (err) {
            console.log(err);
        });
    };
    HomePage.prototype.getVisitData = function () {
        this.visiteService.getVisitData();
    };
    HomePage.prototype.joinQuery = function () {
        this.visiteService.getVisitPoData();
    };
    HomePage.prototype.prepareToSend = function () {
        this.visiteService.endVisit2();
    };
    HomePage.ctorParameters = function () { return [
        { type: _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_2__["SqliteService"] },
        { type: src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_3__["VisitService"] },
        { type: src_app_services_api_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ModalController"] }
    ]; };
    HomePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! raw-loader!./home.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/home/home.page.html"),
            styles: [__webpack_require__(/*! ./home.page.scss */ "./src/app/pages/home/home.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_2__["SqliteService"],
            src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_3__["VisitService"],
            src_app_services_api_api_service__WEBPACK_IMPORTED_MODULE_4__["ApiService"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["ModalController"]])
    ], HomePage);
    return HomePage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-home-home-module-es5.js.map