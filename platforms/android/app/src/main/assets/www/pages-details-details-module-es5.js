(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-details-details-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/details/details.page.html":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/details/details.page.html ***!
  \***************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\r\n  <ion-header>\r\n    <ion-toolbar>\r\n      <ion-buttons slot=\"start\">\r\n        <ion-back-button  routerDirection=\"backward\"></ion-back-button>\r\n      </ion-buttons>\r\n      <ion-title>{{ title }}</ion-title>\r\n        <ion-icon [name]=\"'camera'\" color=\"primary\" size=\"large\" slot=\"end\" class=\"ion-margin\" (click)=\"photoService.selectImage(storageName, title, id, type)\"></ion-icon>\r\n    </ion-toolbar>\r\n  </ion-header>\r\n</ion-header>\r\n<ion-content [scrollEvents]=\"true\">\r\n    <ion-item-sliding>\r\n      <ion-grid>\r\n        <ion-row>\r\n          <h2 class=\"ion-text-align\" *ngIf=\"!photoService.images[0]\">Prenez une photo</h2>\r\n          <ion-col size=\"6\" style=\"max-height: 150px\" *ngFor=\"let photo of photoService.images; index as pos; let color\">\r\n            <ion-row style=\"margin-bottom: -45px\">\r\n              <ion-col size=\"6\">\r\n                <ion-icon class=\"ion-float-left\" name=\"trash\" style=\"--z-index: 99;\" size=\"large\" color=\"warning\" (click)=\"photoService.deleteImage(photo, pos, storageName, id, type)\"></ion-icon>\r\n              </ion-col>\r\n              <ion-col size=\"6\">\r\n                <ion-icon class=\"ion-float-end\" name=\"heart\" style=\"--z-index: 99;\" size=\"large\" *ngIf=\"(photo.id === photoService.favorite)\" color=\"primary\" (click)=\"addFavorite('visite_photo', photo.id)\"></ion-icon>\r\n                <ion-icon class=\"ion-float-end\" name=\"heart\" style=\"--z-index: 99;\" size=\"large\" *ngIf=\"(photo.id !== photoService.favorite)\" color=\"medium\" (click)=\"addFavorite('visite_photo', photo.id)\"></ion-icon>\r\n              </ion-col>\r\n            </ion-row>\r\n            <ion-thumbnail style=\"--size: 100%; margin: 0.2em;\">\r\n              <ion-img [src]=\"photo.path\" (click)=\"presentModal(photo, pos, type, id)\"></ion-img>\r\n            </ion-thumbnail>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n    </ion-item-sliding>\r\n</ion-content>\r\n<!--<ion-content>-->\r\n  <ion-content class=\"ion-padding\" style=\"max-height: 35%\">\r\n    <ion-label *ngIf=\"type === 'cas' && cas !== undefined && notable === 1\" position=\"fixed\">Note\r\n      <ion-select *ngIf=\"type === 'cas' && cas !== undefined\" [(ngModel)]=\"cas.id_visite_note\" [interfaceOptions]=\"createInterfaceOptions(casName)\" (ionChange)=\"onRateChange($event.detail.value, type, cas.id_visite_liste_cas )\" >\r\n        <ion-select-option *ngFor=\"let note of notes; let i = index\" [value]=\"note.id_visite_note\">{{note.note}}</ion-select-option>\r\n      </ion-select>\r\n    </ion-label>\r\n      <form #form>\r\n        <ion-label position=\"floating\">Commentaire</ion-label>\r\n        <ion-textarea placeholder=\"Entrez le commentaire...\" *ngIf=\"comment\" name=\"com\" rows=\"4\" cols=\"20\" style=\"border: 1px solid #d7d8da; border-radius:15px; padding:0.5em;margin-top: 1.2em\" value=\"{{comment}}\"></ion-textarea>\r\n        <ion-textarea placeholder=\"Entrez le commentaire...\" *ngIf=\"!comment\" name=\"com\" rows=\"4\" cols=\"20\" style=\"border: 1px solid #d7d8da; border-radius:15px; padding:0.5em;margin-top: 1.2em\" ></ion-textarea>\r\n      </form>\r\n</ion-content>\r\n<ion-footer no-shadow>\r\n  <ion-toolbar position=\"bottom\">\r\n    <ion-buttons slot=\"end\" class=\"ion-padding\">\r\n      <ion-button color=\"primary\" type=\"submit\" (click)=\"insertComment(form, type, id)\">Valider</ion-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-footer>\r\n"

/***/ }),

/***/ "./src/app/pages/details/details.module.ts":
/*!*************************************************!*\
  !*** ./src/app/pages/details/details.module.ts ***!
  \*************************************************/
/*! exports provided: DetailsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailsPageModule", function() { return DetailsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _details_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./details.page */ "./src/app/pages/details/details.page.ts");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../components/components.module */ "./src/app/components/components.module.ts");








var routes = [
    {
        path: '',
        component: _details_page__WEBPACK_IMPORTED_MODULE_6__["DetailsPage"]
    }
];
var DetailsPageModule = /** @class */ (function () {
    function DetailsPageModule() {
    }
    DetailsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_details_page__WEBPACK_IMPORTED_MODULE_6__["DetailsPage"]]
        })
    ], DetailsPageModule);
    return DetailsPageModule;
}());



/***/ }),

/***/ "./src/app/pages/details/details.page.scss":
/*!*************************************************!*\
  !*** ./src/app/pages/details/details.page.scss ***!
  \*************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2RldGFpbHMvZGV0YWlscy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/details/details.page.ts":
/*!***********************************************!*\
  !*** ./src/app/pages/details/details.page.ts ***!
  \***********************************************/
/*! exports provided: DetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailsPage", function() { return DetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_photo_photo_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../services/photo/photo.service */ "./src/app/services/photo/photo.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_modals_photo_coment_photo_comment_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/modals/photo-coment/photo-comment.component */ "./src/app/components/modals/photo-coment/photo-comment.component.ts");
/* harmony import */ var _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");
/* harmony import */ var src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/visit/visit.service */ "./src/app/services/visit/visit.service.ts");
/* harmony import */ var _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../services/toast/toast.service */ "./src/app/services/toast/toast.service.ts");









var DetailsPage = /** @class */ (function () {
    function DetailsPage(route, photoService, modalController, sql, visitService, toast) {
        var _this = this;
        this.route = route;
        this.photoService = photoService;
        this.modalController = modalController;
        this.sql = sql;
        this.visitService = visitService;
        this.toast = toast;
        this.countFailure = 0;
        this.getURIData()
            .then(function () {
            _this.setStorageName()
                .then(function (response) {
                _this.storageName = response;
                _this.photoService.loadStoredImages(_this.storageName);
                _this.photoService.checkStorage(response, _this.idVisit)
                    .then(function (resp) {
                    if (resp === true) {
                        _this.photoService.insertStorageName(_this.storageName, _this.idVisit)
                            .then(function () {
                        })
                            .catch(function () {
                        });
                    }
                });
            });
            _this.getCasList(_this.type, _this.id)
                .then(function (response) {
                if (response !== undefined) {
                    _this.cas = response[0];
                    _this.comment = response[0].commentaire;
                    if (_this.type === 'cas') {
                        _this.getCas(response[0].id_visite_cas)
                            .then(function (res) {
                            _this.casName = res[0].cas;
                            _this.notable = res[0].notable;
                            _this.visitService.getUtilitaryTables()
                                .then(function (resp) {
                                _this.notes = resp.note;
                            });
                        });
                    }
                }
            });
        });
    }
    DetailsPage.prototype.ngOnInit = function () {
    };
    // Method to get datas from URL
    DetailsPage.prototype.getURIData = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var _a, _b, _c, _d;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_e) {
                switch (_e.label) {
                    case 0:
                        _a = this;
                        return [4 /*yield*/, this.route.snapshot.paramMap.get('id')];
                    case 1:
                        _a.id = _e.sent();
                        console.log(this.id);
                        _b = this;
                        return [4 /*yield*/, this.route.snapshot.paramMap.get('title')];
                    case 2:
                        _b.title = _e.sent();
                        _c = this;
                        return [4 /*yield*/, this.route.snapshot.paramMap.get('type')];
                    case 3:
                        _c.type = _e.sent();
                        _d = this;
                        return [4 /*yield*/, this.route.snapshot.paramMap.get('idVisit')];
                    case 4:
                        _d.idVisit = _e.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // Method to lunch modal to show photo detail and insert comment
    DetailsPage.prototype.presentModal = function (img, pos, type, id) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modalController.create({
                            component: _components_modals_photo_coment_photo_comment_component__WEBPACK_IMPORTED_MODULE_5__["PhotoCommentComponent"],
                            componentProps: {
                                picture: img,
                                storageKey: this.storageName,
                                position: pos,
                                type: type,
                                id: id
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    // Method to add photo to favorite
    DetailsPage.prototype.addFavorite = function (table, idPhoto) {
        this.photoService.favoritePicture(table, idPhoto);
    };
    // Method to get all the "cas"
    DetailsPage.prototype.getCasList = function (type, id) {
        var result = this.getType(type, id);
        var column = '*';
        var options = ' WHERE ' + result.condition.column + ' = ("' + result.condition.value + '")';
        return this.sql.getRows(result.table, column, options);
    };
    // Method to insert comment
    DetailsPage.prototype.insertComment = function (entry, type, id) {
        var _this = this;
        var result = this.getType(type, id);
        var temp = entry.com.value;
        var modified = temp.replace('"', '\'');
        this.photoService.insertComment(modified, result.table, result.condition)
            .then(function () {
            _this.toast.logMessage('Commentaire enregistré avec succès.', 'success');
            _this.countFailure = 0;
            _this.getCasList(type, id)
                .then(function (response) {
                _this.comment = response[0].commentaire;
            });
            return _this.visitService.updateVisiteData(1, result.table, 'status', result.condition.column, result.condition.value);
        })
            .catch(function () {
            if (_this.countFailure < 1) { // Condition to check if error happen at the first try or second try
                _this.toast.logMessage('Une erreur est survenue. Merci de valider à nouveau.', 'dark');
                _this.countFailure++;
            }
            else {
                _this.toast.logMessage('Une erreur est survenue. Merci de vous déconnecter et redémarrer votre application.', 'dark');
            }
            // console.log(error);
        });
    };
    // Method to change data for request for different type of value
    DetailsPage.prototype.getType = function (type, id) {
        if (type === 'equip') {
            return { condition: { column: 'id_visite_equipement', value: id }, table: 'visite_equipement' };
        }
        else if (type === 'cas') {
            return { condition: { column: 'id_visite_liste_cas', value: id }, table: 'visite_liste_cas' };
        }
    };
    // Method to change note of the 'cas'
    DetailsPage.prototype.onRateChange = function (event, type, id) {
        var _this = this;
        var table = 'visite_liste_cas';
        console.log(event);
        this.sql.updateOneColumn({
            table: table,
            column: 'id_visite_note',
            columnValue: event,
            conditionColumn: 'id_visite_liste_cas',
            conditionValue: id
        })
            .then(function () {
            _this.toast.logMessage('Note mise à jour', 'success');
        })
            .catch(function () {
            if (_this.countFailure < 1) {
                _this.toast.logMessage('Une erreur est survenue. Merci de noter à nouveau.', 'dark');
                _this.countFailure++;
            }
            else {
                _this.toast.logMessage('Une erreur est survenue. Merci de vous déconnecter et redémarrer votre application.', 'dark');
            }
        });
    };
    // Method to hydrate select options
    DetailsPage.prototype.createInterfaceOptions = function (headerCustom, subHeaderCustom, messageCustom) {
        if (subHeaderCustom === void 0) { subHeaderCustom = ''; }
        if (messageCustom === void 0) { messageCustom = ''; }
        return {
            header: headerCustom,
            subHeader: subHeaderCustom,
            message: messageCustom,
            translucent: true
        };
    };
    // Method to get all data for this cas
    DetailsPage.prototype.getCas = function (idCas) {
        return this.sql.getRows('visite_cas', '*', ' WHERE id_visite_cas = ' + this.cas.id_visite_cas);
    };
    // Method to create and return a storage name for each cas or equipment
    DetailsPage.prototype.setStorageName = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.id];
                    case 1: return [2 /*return*/, (_a.sent()) +
                            this.title.split(' ').join('').toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, '') +
                            this.idVisit +
                            this.type];
                }
            });
        });
    };
    DetailsPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: _services_photo_photo_service__WEBPACK_IMPORTED_MODULE_3__["PhotoService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
        { type: _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_6__["SqliteService"] },
        { type: src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_7__["VisitService"] },
        { type: _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"] }
    ]; };
    DetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-details',
            template: __webpack_require__(/*! raw-loader!./details.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/details/details.page.html"),
            styles: [__webpack_require__(/*! ./details.page.scss */ "./src/app/pages/details/details.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            _services_photo_photo_service__WEBPACK_IMPORTED_MODULE_3__["PhotoService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"],
            _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_6__["SqliteService"],
            src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_7__["VisitService"],
            _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"]])
    ], DetailsPage);
    return DetailsPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-details-details-module-es5.js.map