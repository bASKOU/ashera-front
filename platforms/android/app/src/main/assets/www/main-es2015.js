(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./pages/details/details.module": [
		"./src/app/pages/details/details.module.ts",
		"pages-details-details-module"
	],
	"./pages/history-details/history-details.module": [
		"./src/app/pages/history-details/history-details.module.ts",
		"pages-history-details-history-details-module"
	],
	"./pages/history/history.module": [
		"./src/app/pages/history/history.module.ts",
		"pages-history-history-module"
	],
	"./pages/login/login.module": [
		"./src/app/pages/login/login.module.ts",
		"pages-login-login-module"
	],
	"./pages/preparation/preparation.module": [
		"./src/app/pages/preparation/preparation.module.ts",
		"pages-preparation-preparation-module"
	],
	"./pages/profil/profil.module": [
		"./src/app/pages/profil/profil.module.ts",
		"pages-profil-profil-module"
	],
	"./pages/send/send.module": [
		"./src/app/pages/send/send.module.ts",
		"pages-send-send-module"
	],
	"./pages/visit-eval/visit-eval.module": [
		"./src/app/pages/visit-eval/visit-eval.module.ts",
		"pages-visit-eval-visit-eval-module"
	],
	"./pages/visit-resume/visit-resume.module": [
		"./src/app/pages/visit-resume/visit-resume.module.ts",
		"pages-visit-resume-visit-resume-module"
	],
	"./pages/visit/visit.module": [
		"./src/app/pages/visit/visit.module.ts",
		"pages-visit-visit-module"
	],
	"src/app/pages/history/history.module": [
		"./src/app/pages/history/history.module.ts",
		"pages-history-history-module"
	],
	"src/app/pages/preparation/preparation.module": [
		"./src/app/pages/preparation/preparation.module.ts",
		"pages-preparation-preparation-module"
	],
	"src/app/pages/send/send.module": [
		"./src/app/pages/send/send.module.ts",
		"pages-send-send-module"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$":
/*!*****************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \*****************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./ion-action-sheet-controller_8.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-action-sheet-controller_8.entry.js",
		"common",
		0
	],
	"./ion-action-sheet-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-action-sheet-ios.entry.js",
		"common",
		1
	],
	"./ion-action-sheet-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-action-sheet-md.entry.js",
		"common",
		2
	],
	"./ion-alert-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-alert-ios.entry.js",
		"common",
		3
	],
	"./ion-alert-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-alert-md.entry.js",
		"common",
		4
	],
	"./ion-app_8-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-app_8-ios.entry.js",
		"common",
		5
	],
	"./ion-app_8-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-app_8-md.entry.js",
		"common",
		6
	],
	"./ion-avatar_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-avatar_3-ios.entry.js",
		"common",
		7
	],
	"./ion-avatar_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-avatar_3-md.entry.js",
		"common",
		8
	],
	"./ion-back-button-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-back-button-ios.entry.js",
		"common",
		9
	],
	"./ion-back-button-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-back-button-md.entry.js",
		"common",
		10
	],
	"./ion-backdrop-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-backdrop-ios.entry.js",
		11
	],
	"./ion-backdrop-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-backdrop-md.entry.js",
		12
	],
	"./ion-button_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-button_2-ios.entry.js",
		"common",
		13
	],
	"./ion-button_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-button_2-md.entry.js",
		"common",
		14
	],
	"./ion-card_5-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-card_5-ios.entry.js",
		"common",
		15
	],
	"./ion-card_5-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-card_5-md.entry.js",
		"common",
		16
	],
	"./ion-checkbox-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-checkbox-ios.entry.js",
		"common",
		17
	],
	"./ion-checkbox-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-checkbox-md.entry.js",
		"common",
		18
	],
	"./ion-chip-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-chip-ios.entry.js",
		"common",
		19
	],
	"./ion-chip-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-chip-md.entry.js",
		"common",
		20
	],
	"./ion-col_3.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-col_3.entry.js",
		21
	],
	"./ion-datetime_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-datetime_3-ios.entry.js",
		"common",
		22
	],
	"./ion-datetime_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-datetime_3-md.entry.js",
		"common",
		23
	],
	"./ion-fab_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-fab_3-ios.entry.js",
		"common",
		24
	],
	"./ion-fab_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-fab_3-md.entry.js",
		"common",
		25
	],
	"./ion-img.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-img.entry.js",
		26
	],
	"./ion-infinite-scroll_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2-ios.entry.js",
		"common",
		27
	],
	"./ion-infinite-scroll_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-infinite-scroll_2-md.entry.js",
		"common",
		28
	],
	"./ion-input-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-input-ios.entry.js",
		"common",
		29
	],
	"./ion-input-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-input-md.entry.js",
		"common",
		30
	],
	"./ion-item-option_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item-option_3-ios.entry.js",
		"common",
		31
	],
	"./ion-item-option_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item-option_3-md.entry.js",
		"common",
		32
	],
	"./ion-item_8-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item_8-ios.entry.js",
		"common",
		33
	],
	"./ion-item_8-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-item_8-md.entry.js",
		"common",
		34
	],
	"./ion-loading-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-loading-ios.entry.js",
		"common",
		35
	],
	"./ion-loading-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-loading-md.entry.js",
		"common",
		36
	],
	"./ion-menu_4-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-menu_4-ios.entry.js",
		"common",
		37
	],
	"./ion-menu_4-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-menu_4-md.entry.js",
		"common",
		38
	],
	"./ion-modal-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-modal-ios.entry.js",
		"common",
		39
	],
	"./ion-modal-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-modal-md.entry.js",
		"common",
		40
	],
	"./ion-nav_5.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-nav_5.entry.js",
		"common",
		41
	],
	"./ion-popover-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-popover-ios.entry.js",
		"common",
		42
	],
	"./ion-popover-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-popover-md.entry.js",
		"common",
		43
	],
	"./ion-progress-bar-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-progress-bar-ios.entry.js",
		"common",
		44
	],
	"./ion-progress-bar-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-progress-bar-md.entry.js",
		"common",
		45
	],
	"./ion-radio_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-radio_2-ios.entry.js",
		"common",
		46
	],
	"./ion-radio_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-radio_2-md.entry.js",
		"common",
		47
	],
	"./ion-range-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-range-ios.entry.js",
		"common",
		48
	],
	"./ion-range-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-range-md.entry.js",
		"common",
		49
	],
	"./ion-refresher_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-refresher_2-ios.entry.js",
		"common",
		50
	],
	"./ion-refresher_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-refresher_2-md.entry.js",
		"common",
		51
	],
	"./ion-reorder_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-reorder_2-ios.entry.js",
		"common",
		52
	],
	"./ion-reorder_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-reorder_2-md.entry.js",
		"common",
		53
	],
	"./ion-ripple-effect.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-ripple-effect.entry.js",
		54
	],
	"./ion-route_4.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-route_4.entry.js",
		"common",
		55
	],
	"./ion-searchbar-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-searchbar-ios.entry.js",
		"common",
		56
	],
	"./ion-searchbar-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-searchbar-md.entry.js",
		"common",
		57
	],
	"./ion-segment_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-segment_2-ios.entry.js",
		"common",
		58
	],
	"./ion-segment_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-segment_2-md.entry.js",
		"common",
		59
	],
	"./ion-select_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-select_3-ios.entry.js",
		"common",
		60
	],
	"./ion-select_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-select_3-md.entry.js",
		"common",
		61
	],
	"./ion-slide_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-slide_2-ios.entry.js",
		62
	],
	"./ion-slide_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-slide_2-md.entry.js",
		63
	],
	"./ion-spinner.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-spinner.entry.js",
		"common",
		64
	],
	"./ion-split-pane-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-split-pane-ios.entry.js",
		65
	],
	"./ion-split-pane-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-split-pane-md.entry.js",
		66
	],
	"./ion-tab-bar_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-tab-bar_2-ios.entry.js",
		"common",
		67
	],
	"./ion-tab-bar_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-tab-bar_2-md.entry.js",
		"common",
		68
	],
	"./ion-tab_2.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-tab_2.entry.js",
		"common",
		69
	],
	"./ion-text.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-text.entry.js",
		"common",
		70
	],
	"./ion-textarea-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-textarea-ios.entry.js",
		"common",
		71
	],
	"./ion-textarea-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-textarea-md.entry.js",
		"common",
		72
	],
	"./ion-toast-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toast-ios.entry.js",
		"common",
		73
	],
	"./ion-toast-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toast-md.entry.js",
		"common",
		74
	],
	"./ion-toggle-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toggle-ios.entry.js",
		"common",
		75
	],
	"./ion-toggle-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-toggle-md.entry.js",
		"common",
		76
	],
	"./ion-virtual-scroll.entry.js": [
		"./node_modules/@ionic/core/dist/esm/ion-virtual-scroll.entry.js",
		77
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./node_modules/@ionic/core/dist/esm lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-app>\r\n    <ion-split-pane contentId=\"main-content\" >\r\n      <ion-menu contentId=\"main-content\" type=\"overlay\" [swipeGesture]=\"false\">\r\n        <ion-header>\r\n          <ion-toolbar>\r\n            <ion-title class=\"ion-margin\">Ashera</ion-title>\r\n            <ion-title class=\"ion-margin\" size=\"small\">Menu</ion-title>\r\n          </ion-toolbar>\r\n        </ion-header>\r\n        <ion-content >\r\n          <ion-list>\r\n            <ion-menu-toggle auto-hide=\"false\">\r\n              <ng-template ngFor let-p [ngForOf]=\"appPages\">\r\n                <ion-item [routerDirection]=\"'root'\" [routerLink]=\"[p.url]\">\r\n                    <ion-icon slot=\"start\" [name]=\"p.icon\" color=\"primary\"></ion-icon>\r\n                    <ion-label>\r\n                      {{p.title}}\r\n                    </ion-label>\r\n                  </ion-item>\r\n                </ng-template>\r\n              <ion-button [routerDirection]=\"'root'\" (click)=\"logoutJwt()\" color=\"light\" expand=\"block\" class=\"ion-margin\">\r\n                <ion-icon slot=\"start\" name=\"log-out\"></ion-icon>\r\n                Se déconnecter\r\n              </ion-button>\r\n            </ion-menu-toggle>\r\n          </ion-list>\r\n        </ion-content>\r\n      </ion-menu>\r\n      <ion-router-outlet id=\"main-content\"></ion-router-outlet>\r\n    </ion-split-pane>\r\n  </ion-app>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/header/header.component.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/header/header.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-header >\r\n  <ion-toolbar color=\"primary\" class=\"ion-text-center\">\r\n    <ion-buttons slot=\"start\">\r\n        <ion-menu-button></ion-menu-button>\r\n    </ion-buttons>\r\n    <ion-title> {{customTitle}} </ion-title>\r\n    <ion-button slot=\"end\">\r\n        <ion-icon slot=\"icon-only\" name=\"{{customButton.icon}}\" (click)=\"customButton.function\"></ion-icon>\r\n    </ion-button>\r\n  </ion-toolbar>\r\n</ion-header> -->\r\n\r\n<ion-toolbar color=\"primary\" class=\"ion-text-center\">\r\n  <ion-buttons slot=\"start\">\r\n    <ion-menu-button></ion-menu-button>\r\n  </ion-buttons>\r\n  <ion-title>{{customTitle}}</ion-title>\r\n  <ion-button *ngIf=\"customButton\" slot=\"end\" (click)=\"this[customButton.function]($event)\">\r\n    <ion-icon slot=\"icon-only\" name=\"{{customButton.icon}}\" ></ion-icon>\r\n  </ion-button>\r\n</ion-toolbar>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/modals/detail-equip-po/detail-equip-po.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/modals/detail-equip-po/detail-equip-po.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-shadow>\r\n  <ion-toolbar color=\"secondary\" text-center>\r\n      <ion-title>{{name}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n  <ion-list padding>\r\n      <ion-item margin color=\"light\">\r\n          <ion-label position=\"floating\">Nom / Prénom</ion-label>\r\n          <ion-input type=\"text\"></ion-input>\r\n          <ion-icon name=\"person\" slot=\"end\" align-self-center></ion-icon>\r\n      </ion-item>\r\n      <ion-item class=\"ion-margin\" color=\"light\">\r\n          <ion-label position=\"stacked\">Date</ion-label>\r\n          <ion-input type=\"date\"></ion-input>\r\n          <ion-icon name=\"calendar\" slot=\"end\" align-self-center></ion-icon>\r\n      </ion-item>\r\n      <ion-item class=\"ion-margin\" lines=\"none\">\r\n          <ion-label position=\"stacked\">Commentaire</ion-label>\r\n          <ion-textarea rows=\"6\" cols=\"20\" placeholder=\"Une note sur cette visite ?\" style=\"border: 1px solid #d7d8da; border-radius:15px; padding:0.5em;margin-top: 1.2em\"></ion-textarea>\r\n      </ion-item>\r\n      <!-- <ion-item>\r\n          <ion-label position=\"floating\">Password</ion-label>\r\n          <ion-input required></ion-input>\r\n          <ion-icon name='eye' item-right></ion-icon>\r\n      </ion-item> -->\r\n  </ion-list>\r\n</ion-content>\r\n<ion-footer no-shadow>\r\n  <ion-toolbar position=\"bottom\">\r\n      <ion-buttons slot=\"end\" padding>\r\n         <ion-button (click)=\"closeModal()\">Fermer</ion-button>\r\n         <ion-button color=\"secondary\" (click)=\"confirmUpdate($event)\">Valider</ion-button>\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-footer>\r\n\r\n\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/modals/initvisit/initvisit.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/modals/initvisit/initvisit.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = " <ion-header no-shadow>\r\n     <ion-toolbar color=\"primary\">\r\n         <ion-title>Paramètrage de la visite</ion-title>\r\n     </ion-toolbar>\r\n </ion-header>\r\n \r\n    <ion-content>\r\n        <form #form=\"ngForm\">\r\n        <ion-list class=\"ion-padding\">\r\n            <ion-list-header class=\"ion-text-center\">\r\n                <ion-title>{{nameOuvrage}}</ion-title>\r\n            </ion-list-header>\r\n            <ion-item margin color=\"light\">\r\n                <ion-label position=\"floating\">Nom / Prénom</ion-label>\r\n                <ion-input name=\"nameAgent\" type=\"text\" [(ngModel)]=\"visitForm.nameAgent\" required></ion-input>\r\n                <ion-icon name=\"person\" slot=\"end\" align-self-center></ion-icon>\r\n            </ion-item>\r\n            <ion-item class=\"ion-margin\" color=\"light\">\r\n                <ion-label position=\"stacked\">Date</ion-label>\r\n                <ion-input name=\"date\" type=\"date\" [(ngModel)]=\"visitForm.date\" required></ion-input>\r\n                <ion-icon name=\"calendar\" slot=\"end\" align-self-center></ion-icon>\r\n            </ion-item>\r\n            <ion-item class=\"ion-margin\" lines=\"none\">\r\n                <ion-label position=\"stacked\">Commentaire</ion-label>\r\n                <ion-textarea [(ngModel)]=\"visitForm.comment\" name=\"comment\" rows=\"6\" cols=\"20\" placeholder=\"Une note sur cette visite ?\" style=\"border: 1px solid #d7d8da; border-radius:15px; padding:0.5em;margin-top: 1.2em\"></ion-textarea>\r\n            </ion-item>\r\n        </ion-list>\r\n    </form>\r\n    </ion-content>\r\n    <ion-footer no-shadow>\r\n        <ion-toolbar position=\"bottom\">\r\n            <ion-buttons slot=\"end\" class=\"ion-padding\">\r\n                <ion-button (click)=\"closeModal()\">Fermer</ion-button>\r\n                <ion-button color=\"secondary\" (click)=\"initVisit(form)\" [disabled]=\"form.invalid\">Valider</ion-button>\r\n            </ion-buttons>\r\n        </ion-toolbar>\r\n    </ion-footer>\r\n\r\n \r\n \r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/modals/photo-coment/photo-comment.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/modals/photo-coment/photo-comment.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-toolbar>\r\n  <ion-title>{{picture.name}}</ion-title>\r\n</ion-toolbar>\r\n  <ion-content>\r\n    <ion-row>\r\n      <ion-col size=\"12\">\r\n        <ion-row style=\"margin-bottom: -45px\">\r\n          <ion-col size=\"6\">\r\n            <ion-icon class=\"ion-float-left\" name=\"trash\" style=\"--z-index: 99;\" size=\"large\" color=\"warning\" (click)=\"photoService.deleteImage(picture, position, storageKey, id, type)\"></ion-icon>\r\n          </ion-col>\r\n          <ion-col size=\"6\">\r\n            <ion-icon *ngIf=\"picture.id === photoService.favorite\" class=\"ion-float-end\" name=\"heart\" style=\"--z-index: 99;\" size=\"large\" color=\"primary\" (click)=\"addFavorite('visite_photo', picture.id)\"></ion-icon>\r\n            <ion-icon *ngIf=\"picture.id !== photoService.favorite\" class=\"ion-float-end\" name=\"heart\" style=\"--z-index: 99;\" size=\"large\" color=\"medium\" (click)=\"addFavorite('visite_photo', picture.id)\"></ion-icon>\r\n          </ion-col>\r\n        </ion-row>\r\n        <img [src]=\"picture.path\"  alt=\"\"/>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-content>\r\n  <ion-content class=\"ion-padding\" style=\"max-height: 30%\">\r\n    <form #form>\r\n      <ion-label position=\"floating\">Commentaire</ion-label>\r\n      <ion-textarea placeholder=\"Entrez le commentaire...\" *ngIf=\"comment\" name=\"com\" rows=\"4\" cols=\"20\" style=\"border: 1px solid #d7d8da; border-radius:15px; padding:0.5em;margin-top: 1.2em\" value=\"{{comment}}\"></ion-textarea>\r\n      <ion-textarea placeholder=\"Entrez le commentaire...\" *ngIf=\"!comment\" name=\"com\" rows=\"4\" cols=\"20\" style=\"border: 1px solid #d7d8da; border-radius:15px; padding:0.5em;margin-top: 1.2em\"></ion-textarea>\r\n    </form>\r\n  </ion-content>\r\n  <ion-footer no-shadow>\r\n    <ion-toolbar position=\"bottom\">\r\n      <ion-buttons slot=\"end\" class=\"ion-padding\">\r\n        <ion-button color=\"medium\" (click)=\"closeModal()\">Fermer</ion-button>\r\n        <ion-button color=\"primary\" type=\"submit\" (click)=\"insertComment(form)\">Valider</ion-button>\r\n      </ion-buttons>\r\n    </ion-toolbar>\r\n  </ion-footer>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/rating/rating.component.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/rating/rating.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-row>\r\n  <ng-container *ngFor=\"let num of ratingMax(maxRating); let i = index;\">\r\n    <ion-col class=\"ion-text-center ion-no-padding\">\r\n        <ion-icon\r\n        name=\"{{activeIcon}}\"\r\n        (click)=\"rate(num)\"\r\n        [ngStyle] = \"{'color' : colorRating(num), 'font-size': fontSize ? fontSize : '1.4em'}\"\r\n        class=\"ion-no-margin\"\r\n      ></ion-icon>\r\n    </ion-col>\r\n  </ng-container>\r\n</ion-row>\r\n"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



const routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', loadChildren: () => __webpack_require__.e(/*! import() | pages-home-home-module */ "pages-home-home-module").then(__webpack_require__.bind(null, /*! ./pages/home/home.module */ "./src/app/pages/home/home.module.ts")).then(m => m.HomePageModule) },
    { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
    { path: 'details/:title/:storage/:type/:id/:idVisit', loadChildren: './pages/details/details.module#DetailsPageModule' },
    { path: 'profil', loadChildren: './pages/profil/profil.module#ProfilPageModule' },
    { path: 'history-details', loadChildren: './pages/history-details/history-details.module#HistoryDetailsPageModule' },
    { path: 'preparation', loadChildren: './pages/preparation/preparation.module#PreparationPageModule' },
    { path: 'send', loadChildren: './pages/send/send.module#SendPageModule' },
    { path: 'visit', loadChildren: './pages/visit/visit.module#VisitPageModule' },
    { path: 'visit-resume/:resumeId/:resumeTitle', loadChildren: './pages/visit-resume/visit-resume.module#VisitResumePageModule' },
    { path: 'visit-eval/:partieOuvrageId/:partieOuvrageTitle/:ouvrageName',
        loadChildren: './pages/visit-eval/visit-eval.module#VisitEvalPageModule' },
    { path: 'visit-eval/:partieOuvrageId/:partieOuvrageTitle/:ouvrageName/:idVisit',
        loadChildren: './pages/visit-eval/visit-eval.module#VisitEvalPageModule' },
    { path: 'history', loadChildren: './pages/history/history.module#HistoryPageModule' },
];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"] })
        ],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");
/* harmony import */ var _services_jwt_jwt_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./services/jwt/jwt.service */ "./src/app/services/jwt/jwt.service.ts");
/* harmony import */ var _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./services/toast/toast.service */ "./src/app/services/toast/toast.service.ts");






/**
 * Services Import
 */



let AppComponent = class AppComponent {
    constructor(platform, splashScreen, statusBar, router, sql, jwt, toast) {
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.router = router;
        this.sql = sql;
        this.jwt = jwt;
        this.toast = toast;
        // Props of side menu content
        this.test = 'autres';
        this.storage = 'test';
        this.appPages = [
            {
                title: 'Profil',
                url: '/profil',
                icon: 'contact'
            },
            {
                title: 'Visite',
                url: '/visit',
                icon: 'briefcase'
            }
        ];
        this.initializeApp();
        this.allowStatusBar();
        this.initDB()
            .then(() => {
            this.jwt.getToken()
                .then((response) => {
                if (response === undefined) {
                    this.logout();
                }
                else {
                    this.jwt.checkValidity(response[0].token)
                        .then((resp) => {
                        if (resp === true) {
                            this.jwt.storageClientId()
                                .then(() => {
                                this.login();
                            });
                        }
                        else {
                            this.logout();
                        }
                    })
                        .catch(() => {
                        this.toast.logMessage('Une erreur s\'est produite', 'warning')
                            .then(() => {
                            this.logout();
                        });
                    });
                }
            })
                .catch(() => {
                this.toast.logMessage('Une erreur s\'est produite', 'warning')
                    .then(() => {
                    this.logout();
                });
            });
        });
    }
    // Method to init
    initializeApp() {
        this.platform.ready()
            .then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
        });
    }
    allowStatusBar() {
        // set status bar to white
        this.statusBar.backgroundColorByHexString('#69aec4');
        this.statusBar.overlaysWebView(false);
    }
    // Method to redirect to login page if user is not logged
    logout() {
        this.router.navigateByUrl('/login');
    }
    // Method to redirect to home page if user is logged
    login() {
        this.router.navigateByUrl('/profil');
    }
    initDB() {
        return this.sql.createDB().then(() => {
            this.sql.databaseTables.map((table) => {
                this.sql.createTable(table)
                    .then((resp) => {
                    console.log(resp);
                });
            });
        });
    }
    logoutJwt() {
        this.jwt.logOut();
    }
};
AppComponent.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
    { type: _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"] },
    { type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__["StatusBar"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
    { type: _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_6__["SqliteService"] },
    { type: _services_jwt_jwt_service__WEBPACK_IMPORTED_MODULE_7__["JwtService"] },
    { type: _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"] }
];
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
        _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"],
        _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__["StatusBar"],
        _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
        _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_6__["SqliteService"],
        _services_jwt_jwt_service__WEBPACK_IMPORTED_MODULE_7__["JwtService"],
        _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"]])
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/sqlite/ngx */ "./node_modules/@ionic-native/sqlite/ngx/index.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic-native/File/ngx */ "./node_modules/@ionic-native/File/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/ngx/index.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");







/**
 * SQLite
 */











// @ts-ignore
let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        declarations: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]],
        entryComponents: [],
        imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"].forRoot(), _app_routing_module__WEBPACK_IMPORTED_MODULE_9__["AppRoutingModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_10__["HttpClientModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_11__["ComponentsModule"], _ionic_storage__WEBPACK_IMPORTED_MODULE_12__["IonicStorageModule"].forRoot()],
        providers: [
            _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__["StatusBar"],
            _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__["SplashScreen"],
            _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_7__["SQLite"],
            _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_13__["Camera"],
            _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_14__["File"],
            // @ts-ignore
            _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
            _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_17__["Geolocation"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_15__["WebView"],
            _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_16__["FilePath"],
            { provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"], useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicRouteStrategy"] }
        ],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]]
    })
], AppModule);



/***/ }),

/***/ "./src/app/components/components.module.ts":
/*!*************************************************!*\
  !*** ./src/app/components/components.module.ts ***!
  \*************************************************/
/*! exports provided: ComponentsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentsModule", function() { return ComponentsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _modals_initvisit_initvisit_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modals/initvisit/initvisit.component */ "./src/app/components/modals/initvisit/initvisit.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./header/header.component */ "./src/app/components/header/header.component.ts");
/* harmony import */ var _rating_rating_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./rating/rating.component */ "./src/app/components/rating/rating.component.ts");
/* harmony import */ var _modals_detail_equip_po_detail_equip_po_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modals/detail-equip-po/detail-equip-po.component */ "./src/app/components/modals/detail-equip-po/detail-equip-po.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _modals_photo_coment_photo_comment_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./modals/photo-coment/photo-comment.component */ "./src/app/components/modals/photo-coment/photo-comment.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");




/**
 * Here goes all the component you need to use as "views"
 */






let ComponentsModule = class ComponentsModule {
};
ComponentsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _header_header_component__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"],
            _modals_initvisit_initvisit_component__WEBPACK_IMPORTED_MODULE_1__["InitvisitComponent"],
            _rating_rating_component__WEBPACK_IMPORTED_MODULE_5__["RatingComponent"],
            _modals_detail_equip_po_detail_equip_po_component__WEBPACK_IMPORTED_MODULE_6__["DetailEquipPoComponent"],
            _modals_photo_coment_photo_comment_component__WEBPACK_IMPORTED_MODULE_8__["PhotoCommentComponent"]
        ],
        entryComponents: [
            _modals_initvisit_initvisit_component__WEBPACK_IMPORTED_MODULE_1__["InitvisitComponent"],
            _modals_detail_equip_po_detail_equip_po_component__WEBPACK_IMPORTED_MODULE_6__["DetailEquipPoComponent"],
            _modals_photo_coment_photo_comment_component__WEBPACK_IMPORTED_MODULE_8__["PhotoCommentComponent"]
        ],
        exports: [
            _header_header_component__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"],
            _modals_initvisit_initvisit_component__WEBPACK_IMPORTED_MODULE_1__["InitvisitComponent"],
            _rating_rating_component__WEBPACK_IMPORTED_MODULE_5__["RatingComponent"],
            _modals_detail_equip_po_detail_equip_po_component__WEBPACK_IMPORTED_MODULE_6__["DetailEquipPoComponent"],
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonicModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"]
        ],
        schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["CUSTOM_ELEMENTS_SCHEMA"]]
    })
], ComponentsModule);



/***/ }),

/***/ "./src/app/components/header/header.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-button {\n  --box-shadow: none;\n}\n\nion-title {\n  position: absolute;\n  top: 0;\n  left: 0;\n  padding: 0 90px 1px;\n  width: 100%;\n  height: 100%;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9oZWFkZXIvQzpcXFVzZXJzXFxiQVNLT1VcXERlc2t0b3BcXGFzaGVyYV9mcm9udC9zcmNcXGFwcFxcY29tcG9uZW50c1xcaGVhZGVyXFxoZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tYnV0dG9ue1xyXG4gICAgLS1ib3gtc2hhZG93OiBub25lO1xyXG59XHJcblxyXG5pb24tdGl0bGUge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgbGVmdDogMDtcclxuICAgIHBhZGRpbmc6IDAgOTBweCAxcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9IiwiaW9uLWJ1dHRvbiB7XG4gIC0tYm94LXNoYWRvdzogbm9uZTtcbn1cblxuaW9uLXRpdGxlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIHBhZGRpbmc6IDAgOTBweCAxcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/header/header.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/header/header.component.ts ***!
  \*******************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_jwt_jwt_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/jwt/jwt.service */ "./src/app/services/jwt/jwt.service.ts");



let HeaderComponent = class HeaderComponent {
    constructor(jwt) {
        this.jwt = jwt;
        this._logOut = false;
    }
    set logOut(value) {
        this._logOut = value !== 'false';
    }
    ngOnInit() {
        /*
        console.log(this.customTitle);
        console.table(this.customButton);
        */
        if (this._logOut) {
            this.customButton = {
                icon: 'log-out',
                function: 'logout'
            };
        }
    }
    hello(event) {
        console.log(event);
        console.log('Hello, this.customButton.function work');
    }
    logout() {
        this.jwt.logOut();
    }
};
HeaderComponent.ctorParameters = () => [
    { type: _services_jwt_jwt_service__WEBPACK_IMPORTED_MODULE_2__["JwtService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], HeaderComponent.prototype, "customTitle", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], HeaderComponent.prototype, "customButton", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
], HeaderComponent.prototype, "logOut", null);
HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-header',
        template: __webpack_require__(/*! raw-loader!./header.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/header/header.component.html"),
        styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/components/header/header.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_jwt_jwt_service__WEBPACK_IMPORTED_MODULE_2__["JwtService"]])
], HeaderComponent);



/***/ }),

/***/ "./src/app/components/modals/detail-equip-po/detail-equip-po.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/components/modals/detail-equip-po/detail-equip-po.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbW9kYWxzL2RldGFpbC1lcXVpcC1wby9kZXRhaWwtZXF1aXAtcG8uY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/modals/detail-equip-po/detail-equip-po.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/components/modals/detail-equip-po/detail-equip-po.component.ts ***!
  \********************************************************************************/
/*! exports provided: DetailEquipPoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailEquipPoComponent", function() { return DetailEquipPoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_toast_toast_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/toast/toast.service */ "./src/app/services/toast/toast.service.ts");
/* harmony import */ var src_app_services_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/loading/loading.service */ "./src/app/services/loading/loading.service.ts");






let DetailEquipPoComponent = class DetailEquipPoComponent {
    constructor(router, modal, toast, loading) {
        this.router = router;
        this.modal = modal;
        this.toast = toast;
        this.loading = loading;
    }
    ngOnInit() { }
    closeModal() {
        // using the injected ModalController this page
        // can "dismiss" itself and optionally pass back data
        this.modal.dismiss({
            dismissed: true
        });
    }
    confirmUpdate(event) {
        console.log(event);
    }
};
DetailEquipPoComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: src_app_services_toast_toast_service__WEBPACK_IMPORTED_MODULE_4__["ToastService"] },
    { type: src_app_services_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], DetailEquipPoComponent.prototype, "name", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], DetailEquipPoComponent.prototype, "id", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], DetailEquipPoComponent.prototype, "type", void 0);
DetailEquipPoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-detail-equip-po',
        template: __webpack_require__(/*! raw-loader!./detail-equip-po.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/modals/detail-equip-po/detail-equip-po.component.html"),
        styles: [__webpack_require__(/*! ./detail-equip-po.component.scss */ "./src/app/components/modals/detail-equip-po/detail-equip-po.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
        src_app_services_toast_toast_service__WEBPACK_IMPORTED_MODULE_4__["ToastService"],
        src_app_services_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"]])
], DetailEquipPoComponent);



/***/ }),

/***/ "./src/app/components/modals/initvisit/initvisit.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/components/modals/initvisit/initvisit.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbW9kYWxzL2luaXR2aXNpdC9pbml0dmlzaXQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/modals/initvisit/initvisit.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/components/modals/initvisit/initvisit.component.ts ***!
  \********************************************************************/
/*! exports provided: InitvisitComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InitvisitComponent", function() { return InitvisitComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var src_app_services_toast_toast_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/toast/toast.service */ "./src/app/services/toast/toast.service.ts");
/* harmony import */ var src_app_services_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/loading/loading.service */ "./src/app/services/loading/loading.service.ts");
/* harmony import */ var src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/visit/visit.service */ "./src/app/services/visit/visit.service.ts");








let InitvisitComponent = class InitvisitComponent {
    constructor(router, modal, navCtrl, sqlite, toast, loading, visitService) {
        this.router = router;
        this.modal = modal;
        this.navCtrl = navCtrl;
        this.sqlite = sqlite;
        this.toast = toast;
        this.loading = loading;
        this.visitService = visitService;
        this.visitForm = {
            nameAgent: '',
            date: '',
            comment: ''
        };
    }
    ngOnInit() { }
    initVisit(form) {
        // this.loading.createLoading();
        // console.log('id Visit ', this.idVisit);
        const data = form.form.value;
        // console.log('form values: ', form.form.value);
        // let data = {nameAgent: 'NameAgent', date: '2019-11-19', comment: 'BlaBla'};
        this.visitService.initVisit(this.idVisit, data).then((result) => {
            // console.log('result of initialisation : ', result);
            this.toast.logMessage('Visite initialisée avec succès !', 'success');
            this.closeModal();
            this.router.navigateByUrl('/visit-resume/' + this.idVisit + '/' + this.nameOuvrage);
        }).catch((error) => {
            // console.error(error);
        });
    }
    closeModal() {
        // using the injected ModalController this page
        // can "dismiss" itself and optionally pass back data
        this.modal.dismiss({
            dismissed: true
        });
    }
};
InitvisitComponent.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
    { type: src_app_services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_3__["SqliteService"] },
    { type: src_app_services_toast_toast_service__WEBPACK_IMPORTED_MODULE_5__["ToastService"] },
    { type: src_app_services_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"] },
    { type: src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_7__["VisitService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
], InitvisitComponent.prototype, "nameOuvrage", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
], InitvisitComponent.prototype, "idVisit", void 0);
InitvisitComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-initvisit',
        template: __webpack_require__(/*! raw-loader!./initvisit.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/modals/initvisit/initvisit.component.html"),
        styles: [__webpack_require__(/*! ./initvisit.component.scss */ "./src/app/components/modals/initvisit/initvisit.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
        src_app_services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_3__["SqliteService"],
        src_app_services_toast_toast_service__WEBPACK_IMPORTED_MODULE_5__["ToastService"],
        src_app_services_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"],
        src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_7__["VisitService"]])
], InitvisitComponent);



/***/ }),

/***/ "./src/app/components/modals/photo-coment/photo-comment.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/components/modals/photo-coment/photo-comment.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbW9kYWxzL3Bob3RvLWNvbWVudC9waG90by1jb21tZW50LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/modals/photo-coment/photo-comment.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/components/modals/photo-coment/photo-comment.component.ts ***!
  \***************************************************************************/
/*! exports provided: PhotoCommentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhotoCommentComponent", function() { return PhotoCommentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _services_photo_photo_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/photo/photo.service */ "./src/app/services/photo/photo.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");
/* harmony import */ var _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/toast/toast.service */ "./src/app/services/toast/toast.service.ts");




/** Import services */


let PhotoCommentComponent = class PhotoCommentComponent {
    constructor(photoService, modal, sql, toast) {
        this.photoService = photoService;
        this.modal = modal;
        this.sql = sql;
        this.toast = toast;
        this.countFailure = 0;
    }
    ngOnInit() {
        this.getComment()
            .then((response) => {
            this.comment = response[0].commentaire;
        })
            .catch((error) => {
            console.log(error);
        });
    }
    // Method to delete an image
    delete(imgEntry, position, storageKey, id, type) {
        this.photoService.deleteImage(imgEntry, position, storageKey, id, type);
        this.closeModal();
    }
    // Method to close the modal
    closeModal() {
        // using the injected ModalController this page
        // can "dismiss" itself and optionally pass back data
        this.modal.dismiss({
            dismissed: true
        });
    }
    // Method to add picture as favorite
    addFavorite(table, idPhoto) {
        this.photoService.favoritePicture(table, idPhoto);
    }
    // Method to get comment from internal BDD
    getComment() {
        const table = 'visite_photo';
        const column = 'commentaire';
        const options = ' WHERE url = ("' + this.picture.name + '")';
        return this.sql.getRows(table, column, options);
    }
    // Method to insert comment
    insertComment(entry) {
        console.log(entry.com.value);
        const table = 'visite_photo';
        const condition = { column: 'id_visite_photo', value: this.picture.id };
        const temp = entry.com.value;
        const modified = temp.replace('"', '\'');
        return this.photoService.insertComment(modified, table, condition)
            .then(() => {
            this.countFailure = 0;
            this.toast.logMessage('Commentaire enregistré avec succes.', 'success');
            this.closeModal();
        })
            .catch(() => {
            if (this.countFailure < 1) {
                this.toast.logMessage('Une erreur est survenue. Merci de valider à nouveau.', 'dark');
                this.countFailure++;
            }
            else {
                this.toast.logMessage('Une erreur est survenue. Merci de vous déconneter et redémarrer votre application.', 'dark');
            }
        });
    }
};
PhotoCommentComponent.ctorParameters = () => [
    { type: _services_photo_photo_service__WEBPACK_IMPORTED_MODULE_2__["PhotoService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
    { type: _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_4__["SqliteService"] },
    { type: _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_5__["ToastService"] }
];
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
], PhotoCommentComponent.prototype, "picture", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
], PhotoCommentComponent.prototype, "storageKey", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
], PhotoCommentComponent.prototype, "position", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
], PhotoCommentComponent.prototype, "type", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
], PhotoCommentComponent.prototype, "id", void 0);
PhotoCommentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-photo-coment',
        template: __webpack_require__(/*! raw-loader!./photo-comment.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/modals/photo-coment/photo-comment.component.html"),
        styles: [__webpack_require__(/*! ./photo-comment.component.scss */ "./src/app/components/modals/photo-coment/photo-comment.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_photo_photo_service__WEBPACK_IMPORTED_MODULE_2__["PhotoService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
        _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_4__["SqliteService"],
        _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_5__["ToastService"]])
], PhotoCommentComponent);



/***/ }),

/***/ "./src/app/components/rating/rating.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/components/rating/rating.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcmF0aW5nL3JhdGluZy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/rating/rating.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/rating/rating.component.ts ***!
  \*******************************************************/
/*! exports provided: RatingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RatingComponent", function() { return RatingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let RatingComponent = class RatingComponent {
    constructor() {
        this.ratingChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.readonly = false;
        this.activeColor = '#69aec4';
        this.defaultColor = '#f4f4f4';
        this.activeIcon = 'star';
        this.defaultIcon = 'star-outline';
        this.tooltip = [
            'Absent',
            'Mauvais',
            'Moyen',
            'Bon'
        ];
    }
    ngOnInit() {
    }
    rate(value) {
        if (this.rating === value && value === 2) {
            this.rating = 1;
        }
        else {
            this.rating = value;
        }
        this.ratingChange.emit(this.rating);
    }
    colorRating(value) {
        return value > this.rating ? this.defaultColor : this.activeColor;
    }
    isEqualRate(value) {
        return this.rating === value;
    }
    ratingMax(size) {
        if (!size) {
            size = 5;
        }
        // size - 1 to let a '0' value
        return new Array((size - 1)).fill(1).map((value, index) => {
            return index + 2;
        });
    }
};
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
], RatingComponent.prototype, "maxRating", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
], RatingComponent.prototype, "rating", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
], RatingComponent.prototype, "fontSize", void 0);
tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
], RatingComponent.prototype, "ratingChange", void 0);
RatingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-rating',
        template: __webpack_require__(/*! raw-loader!./rating.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/rating/rating.component.html"),
        styles: [__webpack_require__(/*! ./rating.component.scss */ "./src/app/components/rating/rating.component.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], RatingComponent);



/***/ }),

/***/ "./src/app/services/api/api.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/api/api.service.ts ***!
  \*********************************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm2015/http.js");
/* harmony import */ var _jwt_jwt_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../jwt/jwt.service */ "./src/app/services/jwt/jwt.service.ts");
/* harmony import */ var _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");





let ApiService = class ApiService {
    constructor(httpClient, jwt, sqlite) {
        this.httpClient = httpClient;
        this.jwt = jwt;
        this.sqlite = sqlite;
        this.apiUrl = 'http://90.113.154.212/ashera/index.php/';
        this.httpOptions = {
            'Access-Control-Allow-Headers': '*',
            'Access-Control-Allow-Credentials': 'true'
        };
    }
    getHttpOptions(options) {
        // Initialize our headers for our http request
        const header = this.httpOptions;
        // Merge options passed in argument with our base header
        // tslint:disable-next-line:forin
        for (const prop in options) {
            header[prop] = options[prop];
        }
        /**
         *  Merge the httpOptions.headers <Object> with the arguments options <Object>
         */
        return {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"](header)
        };
    }
    /**
     * Service used for post methods
     * @param data // The data you need to send
     * @param uri // The method used by the Api to manage your data
     * @param options // Headers & http options
     * @param dev // Use final api url or custom one in this service properties
     */
    apiPostRequest(data, uri, options) {
        const httpOptions = this.getHttpOptions(options);
        /**
         * Create a promise for the return of our service
         */
        return new Promise((resolve, reject) => {
            const url = this.apiUrl + uri;
            this.httpClient.post(url, data, httpOptions)
                .subscribe((response) => {
                resolve(response);
            }, (error) => {
                reject(error);
            });
        });
    }
    apiGetRequest(url, query) {
        return this.jwt.makeJwtOption().then((result) => {
            if (result) {
                const httpOptions = this.getHttpOptions(result);
                return new Promise((resolve, reject) => {
                    this.httpClient.get(this.apiUrl + url + query, httpOptions)
                        .subscribe((response) => {
                        resolve(response);
                    }, (error) => {
                        reject(error);
                    });
                });
            }
            else {
                throw Error('Can\'t access to the token');
            }
        });
    }
    asyncForEach(array, callback) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            for (let index = 0; index < array.length; index++) {
                yield callback(array[index], index, array);
            }
        });
    }
    syncUtilityTables() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const tables = ['visite_cas', 'visite_status', 'visite_note'];
            let result = {};
            yield this.asyncForEach(tables, (table) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                yield this.fetchUtilityTable(table)
                    .then((utilityTable) => {
                    result[table] = utilityTable;
                    this.dropUtilityTable(table)
                        .then(() => {
                        this.createUtilityTable(table)
                            .then(() => {
                            result[table].forEach((dataObj) => {
                                this.insertFromObj(table, dataObj)
                                    .then(() => {
                                })
                                    .catch(() => {
                                });
                            });
                        });
                    });
                });
            }));
        });
    }
    fetchUtilityTable(tableName) {
        const uriQuery = tableName.split('_')[1];
        return this.apiGetRequest('get/', uriQuery);
    }
    dropUtilityTable(tableName) {
        return this.sqlite.dropTable(tableName);
    }
    createUtilityTable(tableName) {
        const databaseObjArray = [
            'visite_cas',
            'visite_note',
            'visite_status',
        ];
        const index = databaseObjArray.indexOf(tableName);
        return this.sqlite.createTable(this.sqlite.databaseTables[index]);
    }
    insertFromObj(tableName, dataObj) {
        return this.constructInsertQuery(dataObj).then((queryArray) => {
            return this.sqlite.insertRow({ table: tableName, column: queryArray[0], value: queryArray[1] });
        });
    }
    constructInsertQuery(entity) {
        const keys = Object.keys(entity);
        let columns = '';
        let values = keys.length > 1 ? '(' : '';
        return new Promise((resolve, reject) => {
            if (keys.length < 1) {
                reject(new Error('Parameter is not an array/object or it\'s an empty one'));
            }
            else {
                keys.forEach((value, index) => {
                    columns += ` "${value}"`;
                    values += isNaN(entity[value]) ? ` "${entity[value]}"` : ` ${entity[value]}`;
                    if (keys.length - 1 === index) {
                        values += keys.length > 1 ? ')' : '';
                        columns += '';
                        resolve([columns, values]);
                    }
                    else {
                        columns += ',';
                        values += ',';
                    }
                });
            }
        });
    }
};
ApiService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
    { type: _jwt_jwt_service__WEBPACK_IMPORTED_MODULE_3__["JwtService"] },
    { type: _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_4__["SqliteService"] }
];
ApiService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _jwt_jwt_service__WEBPACK_IMPORTED_MODULE_3__["JwtService"], _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_4__["SqliteService"]])
], ApiService);



/***/ }),

/***/ "./src/app/services/entities/visit-cas.ts":
/*!************************************************!*\
  !*** ./src/app/services/entities/visit-cas.ts ***!
  \************************************************/
/*! exports provided: VisitCas */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitCas", function() { return VisitCas; });
/**
 * Utilitary table
 */
class VisitCas {
    constructor() {
        this.tableName = "visite_cas";
        /*
            Array fetched with GSO database
        */
        let allCasesFromService = [
            // Id of each object will be it's index + 1
            { cas: 'État du patrimoine', notable: 0 },
            { cas: 'Sécurité du site et abords extérieurs', notable: 0 },
            { cas: 'Entretien des installations', notable: 0 },
            { cas: 'Renouvellement', notable: 0 },
            { cas: 'État général du site', notable: 1 },
            { cas: 'Propreté du sité', notable: 1 },
            { cas: 'État du génie civil', notable: 1 },
            { cas: 'Clôture', notable: 1 },
            { cas: 'Portail', notable: 1 },
            { cas: 'Espaces verts, Haies', notable: 1 },
            { cas: 'Peinture, enduits, apsects extérieurs', notable: 1 },
            { cas: 'Huisseries', notable: 1 },
            { cas: 'Armoire éléctrique', notable: 1 },
            { cas: 'Équipements électromécaniques', notable: 1 },
            { cas: 'Accessoires hydrauliques', notable: 1 },
            { cas: 'Compteurs', notable: 1 }
        ];
        this.allCases = allCasesFromService;
    }
    ;
    getCas(idCas) {
        return this.allCases[idCas];
    }
    fetchCases() {
        return new Promise((resolve, reject) => {
            setTimeout(() => {
                let cases = this.allCases;
                resolve(cases);
            }, 500);
        });
    }
}


/***/ }),

/***/ "./src/app/services/entities/visit-equipement.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/entities/visit-equipement.ts ***!
  \*******************************************************/
/*! exports provided: VisitEquipement */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitEquipement", function() { return VisitEquipement; });
class VisitEquipement {
    /**
     * Construct for each Equipement, link to VisitPartieOuvrage Object
     * @param idVisitePO
     * @param idEquipement
     * @param nomEquipement
     */
    constructor(idVisitePO, idEquipement, nomEquipement) {
        this.tableName = 'visite_equipement';
        this.id_visite_partie_ouvrage = idVisitePO;
        this.id_equipement = idEquipement;
        this.nom_equipement = nomEquipement;
    }
}
VisitEquipement.count = 1;
VisitEquipement.ctorParameters = () => [
    { type: Number },
    { type: Number },
    { type: String }
];


/***/ }),

/***/ "./src/app/services/entities/visit-partie-ouvrage.ts":
/*!***********************************************************!*\
  !*** ./src/app/services/entities/visit-partie-ouvrage.ts ***!
  \***********************************************************/
/*! exports provided: VisitPartieOuvrage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitPartieOuvrage", function() { return VisitPartieOuvrage; });
class VisitPartieOuvrage {
    /**
     * Construct for each VistPO, linked to Visit Object
     * @param idVisite
     * @param idPO
     * @param namePO
     */
    constructor(idVisite, idPO, namePO) {
        this.tableName = 'visite_partie_ouvrage';
        this.id_visite = idVisite;
        this.id_partie_ouvrage = idPO;
        this.nom_partie_ouvrage = namePO;
    }
}
VisitPartieOuvrage.count = 1;
VisitPartieOuvrage.ctorParameters = () => [
    { type: Number },
    { type: Number },
    { type: String }
];


/***/ }),

/***/ "./src/app/services/entities/visit.ts":
/*!********************************************!*\
  !*** ./src/app/services/entities/visit.ts ***!
  \********************************************/
/*! exports provided: Visit */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Visit", function() { return Visit; });
class Visit {
    /**
     * Construct for object Visit
     * @param idOuvrage the one used for the visit
     * @param idAgent can be get with proper service
     */
    constructor(idOuvrage, idAgent, nameOuvrage) {
        this.tableName = 'visite';
        this.id_ouvrage = idOuvrage;
        this.id_client = idAgent;
        this.nom_ouvrage = nameOuvrage;
    }
}
Visit.count = 1;
Visit.ctorParameters = () => [
    { type: Number },
    { type: Number },
    { type: String }
];


/***/ }),

/***/ "./src/app/services/entities/visite-liste-cas.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/entities/visite-liste-cas.ts ***!
  \*******************************************************/
/*! exports provided: VisiteListeCas */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisiteListeCas", function() { return VisiteListeCas; });
class VisiteListeCas {
    constructor(idCas, idPO) {
        this.tableName = 'visite_liste_cas';
        this.id_visite_cas = idCas;
        this.id_visite_partie_ouvrage = idPO;
    }
}
VisiteListeCas.ctorParameters = () => [
    { type: Number },
    { type: Number }
];


/***/ }),

/***/ "./src/app/services/jwt/jwt.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/jwt/jwt.service.ts ***!
  \*********************************************/
/*! exports provided: JwtService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtService", function() { return JwtService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");
/* harmony import */ var _toast_toast_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../toast/toast.service */ "./src/app/services/toast/toast.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");






let JwtService = class JwtService {
    constructor(sql, storage, toast, router) {
        this.sql = sql;
        this.storage = storage;
        this.toast = toast;
        this.router = router;
    }
    // Method to get token from internal database
    getToken() {
        return this.sql.getRows('visite_token');
    }
    // Method to check validity of the token
    checkValidity(token) {
        return this.splitToken(token)
            .then((response) => {
            this.payload = JSON.parse(response[1]);
            return this.payload.exp > Date.now();
        })
            .catch((error) => {
            return error;
        });
    }
    // Method to split the token and decode it
    splitToken(token) {
        const tab = [];
        return new Promise((resolve, reject) => {
            const temp = token.split('.');
            for (let i = 0; i < temp.length - 1; i++) {
                tab.push(atob(temp[i]));
            }
            if (tab.length > 0) {
                resolve(tab);
            }
            else {
                reject('Pas de token.');
            }
        });
    }
    // Method to get client Id from the storage
    storageClientId() {
        return this.storage.set('clientId', this.payload.data.client_id);
    }
    // Method to make header with the token
    makeJwtOption() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            try {
                const response = yield this.getToken();
                if (response !== false) {
                    return { Authorization: 'Bearer' + response[0].token };
                }
                else {
                    throw Error('No token found !');
                }
            }
            catch (error) {
                return error;
            }
        });
    }
    /**
     * Method to remove the table token the create it again
     * In order to alway keep a clean table
     */
    logOut() {
        this.sql.dropTable('visite_token')
            .then(() => {
            this.toast.logMessage('Vous vous êtes déconnecté!', 'success')
                .then(() => {
                this.sql.createTable({ table: 'visite_token', column: '(id_token INTEGER PRIMARY KEY, token TEXT)' })
                    .then(() => {
                    this.router.navigateByUrl('/login');
                });
            });
        })
            .catch(() => {
            this.toast.logMessage('Erreur', 'black');
        });
    }
};
JwtService.ctorParameters = () => [
    { type: _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_3__["SqliteService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"] },
    { type: _toast_toast_service__WEBPACK_IMPORTED_MODULE_4__["ToastService"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
];
JwtService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_3__["SqliteService"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"],
        _toast_toast_service__WEBPACK_IMPORTED_MODULE_4__["ToastService"],
        _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
], JwtService);



/***/ }),

/***/ "./src/app/services/loading/loading.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/loading/loading.service.ts ***!
  \*****************************************************/
/*! exports provided: LoadingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadingService", function() { return LoadingService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



let LoadingService = class LoadingService {
    constructor(loadingController) {
        this.loadingController = loadingController;
    }
    createLoading(msg = 'Chargement...') {
        this.loadingController.create({
            spinner: 'crescent',
            message: msg
        }).then((res) => {
            res.present();
        });
    }
    closeLoading(id = '') {
        this.loadingController.dismiss(id);
    }
};
LoadingService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
];
LoadingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
], LoadingService);



/***/ }),

/***/ "./src/app/services/photo/photo.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/photo/photo.service.ts ***!
  \*************************************************/
/*! exports provided: PhotoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhotoService", function() { return PhotoService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");
/* harmony import */ var _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/File/ngx */ "./node_modules/@ionic-native/File/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/ngx/index.js");
/* harmony import */ var _toast_toast_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../toast/toast.service */ "./src/app/services/toast/toast.service.ts");
/* harmony import */ var _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _api_api_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../api/api.service */ "./src/app/services/api/api.service.ts");
/* harmony import */ var _jwt_jwt_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../jwt/jwt.service */ "./src/app/services/jwt/jwt.service.ts");













let PhotoService = class PhotoService {
    constructor(camera, file, webview, storage, filePath, plt, sql, toast, geo, api, jwt, action, platform) {
        this.camera = camera;
        this.file = file;
        this.webview = webview;
        this.storage = storage;
        this.filePath = filePath;
        this.plt = plt;
        this.sql = sql;
        this.toast = toast;
        this.geo = geo;
        this.api = api;
        this.jwt = jwt;
        this.action = action;
        this.platform = platform;
        this.images = [];
    }
    ngOnInit() {
        this.images = [];
    }
    // Load images path and name from Ionic storage
    loadStoredImages(storageKey) {
        this.images = []; // Reset value to avoid duplicate from other pages on service load
        this.storage.get(storageKey) // Get storage of the equipment or "partie d'ouvrage"
            .then(images => {
            if (images) {
                const arr = JSON.parse(images);
                this.images = [];
                for (const img of arr) {
                    const filePath = this.file.dataDirectory + img;
                    const resPath = this.pathForImage(filePath);
                    this.sql.getRows('visite_photo', '*', ' WHERE url = ("' + img + '")')
                        .then((response) => {
                        this.images.push({ name: img, path: resPath, filePath, id: response[0].id_visite_photo });
                        if (response[0].favoris === 1) {
                            this.favorite = response[0].id_visite_photo;
                        }
                    })
                        .catch(() => {
                        this.toast.logMessage('Une erreur  est survenue', 'dark');
                    });
                }
            }
        })
            .catch(() => {
            this.toast.logMessage('Une erreur  est survenue', 'dark');
        });
    }
    // Set path for the image
    pathForImage(img) {
        if (img === null) {
            return '';
        }
        else {
            return this.webview.convertFileSrc(img);
        }
    }
    // Method to take picture
    takePicture(storageKey, ouvrageName, id, type, sourceType) {
        // Camera options
        const options = {
            quality: 50,
            sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        // Start camera or convert picture from library
        this.camera.getPicture(options)
            .then(imagePath => {
            const data = { ouvrage: ouvrageName, type: type + id };
            // convert picture from library (It creates a new image)
            if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
                this.filePath.resolveNativePath(imagePath)
                    .then(filePath => {
                    const correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    const currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    this.copyFileToLocalDir(correctPath, currentName, this.createFileName(data), storageKey, type, id);
                });
            }
            else { // Picture taken from the camera
                const currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                const correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                this.copyFileToLocalDir(correctPath, currentName, this.createFileName(data), storageKey, type, id);
            }
        });
    }
    // Method to select to use camera or charge picture from gallery
    selectImage(storageKey, ouvrageName, id, type) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            console.log(id);
            const actionSheet = yield this.action.create({
                header: 'Selection de la source',
                buttons: [{
                        text: 'Charger depuis la gallery',
                        handler: () => {
                            this.takePicture(storageKey, ouvrageName, id, type, this.camera.PictureSourceType.PHOTOLIBRARY);
                        }
                    },
                    {
                        text: 'Appareil photo',
                        handler: () => {
                            this.takePicture(storageKey, ouvrageName, id, type, this.camera.PictureSourceType.CAMERA);
                        }
                    },
                    {
                        text: 'Annuler',
                        role: 'cancel'
                    }
                ]
            });
            yield actionSheet.present();
        });
    }
    // Create name of the picture
    createFileName(data) {
        const a = data.ouvrage.split(' ').join('').toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, ''), b = data.type.replace(' ', ''), c = Date.now(), d = Math.floor(Math.random() * 9999);
        return a + b + c + d + '.jpg';
    }
    // Method to copy the file in the device
    copyFileToLocalDir(namePath, currentName, newFileName, storageKey, type, id) {
        const data = { table: 'visite_photo', column: 'url', value: '("' + newFileName + '")' };
        this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName)
            .then(() => {
            this.sql.insertRow(data)
                .then((response) => {
                this.updateStoredImages(response.insertId, newFileName, storageKey, type, id);
            });
        })
            .catch(() => {
            this.toast.logMessage('Erreur lors de l\'enrgistrememnt de la photo.', 'warning');
        });
    }
    // Method to the pictures in the storage
    updateStoredImages(id, name, storageKey, type, idType) {
        this.storage.get(storageKey)
            .then(images => {
            const arr = JSON.parse(images);
            if (!arr) {
                const newImages = [name];
                this.storage.set(storageKey, JSON.stringify(newImages));
            }
            else {
                arr.push(name);
                this.storage.set(storageKey, JSON.stringify(arr));
            }
            const filePath = this.file.dataDirectory + name;
            const resPath = this.pathForImage(filePath);
            const newEntry = {
                name,
                path: resPath,
                filePath,
                id
            };
            this.images.push(newEntry);
            this.getGeoloc(id);
            this.insertPhotoList(idType, type);
        });
    }
    // Method to stringify the list of pictures
    pushToTab() {
        const arr = [];
        this.images.map((img) => {
            arr.push(img.id);
        });
        return arr.join();
    }
    // Method to insert the list of pictures on a liste_cas or equipement
    insertPhotoList(id, type) {
        console.log(type);
        console.log(id);
        let table;
        let conditionColumn;
        if (type === 'equip') {
            table = 'visite_equipement';
            conditionColumn = 'id_visite_equipement';
        }
        else if (type === 'cas') {
            table = 'visite_liste_cas';
            conditionColumn = 'id_visite_liste_cas';
        }
        const photoList = this.pushToTab();
        const val = {
            table,
            column: 'id_photo',
            columnValue: photoList,
            conditionColumn,
            conditionValue: '(' + id + ')'
        };
        console.log(val);
        this.sql.updateOneColumn(val)
            .then((resp) => {
            console.log(resp);
        })
            .catch((error) => {
            console.log(error);
            this.toast.logMessage('Erreur de sauvegarder de la liste des photos', 'dark');
        });
    }
    // Method to delete an image in the storage and device
    deleteImage(imgEntry, position, storageKey, id, type) {
        this.images.splice(position, 1);
        this.storage.get(storageKey).then(images => {
            const arr = JSON.parse(images);
            const filtered = arr.filter(name => name !== imgEntry.name);
            this.storage.set(storageKey, JSON.stringify(filtered));
            const correctPath = imgEntry.filePath.substr(0, imgEntry.filePath.lastIndexOf('/') + 1);
            this.file.removeFile(correctPath, imgEntry.name)
                .then((res) => {
                const arg = { table: 'visite_photo', column: 'url' };
                this.sql.deleteRow('("' + imgEntry.name + '")', arg)
                    .then(() => {
                    this.toast.logMessage('Photo effacé.', 'success');
                    this.insertPhotoList(id, type);
                })
                    .catch(() => {
                    this.toast.logMessage('Erreur lors de l\'éffacement de la photo.', 'warning');
                });
            })
                .catch((error) => {
                this.toast.logMessage('Erreur lors de l\'éffacement de la photo.', 'warning');
            });
        });
    }
    // Method to get localisation of the picture
    getGeoloc(id) {
        this.geo.getCurrentPosition()
            .then((resp) => {
            const x = { table: 'visite_photo', column: 'x',
                columnValue: resp.coords.longitude,
                conditionColumn: 'id_visite_photo',
                conditionValue: id };
            const y = { table: 'visite_photo', column: 'y',
                columnValue: resp.coords.latitude,
                conditionColumn: 'id_visite_photo',
                conditionValue: id };
            this.sql.updateOneColumn(x)
                .then(() => {
                this.sql.updateOneColumn(y)
                    .then(() => {
                    this.toast.logMessage('Gélocalisation réussie.', 'success');
                })
                    .catch((error) => {
                    this.toast.logMessage('Gélocalisation incomplète.', 'dark');
                });
            })
                .catch(() => {
                this.toast.logMessage('Erreur de géolocalisation, reprenez une photo', 'dark');
            });
        })
            .catch(() => {
            this.toast.logMessage('Echec de gélocalisation, problème de couverture GPS.', 'dark');
        });
    }
    // Method to insert comment into the sqlite table
    insertComment(comment, table, condition) {
        const data = { table, column: 'commentaire',
            columnValue: comment,
            conditionColumn: condition.column,
            conditionValue: condition.value };
        return this.sql.updateOneColumn(data);
    }
    favoritePicture(table, idPhoto) {
        let arr = [];
        const url = 'url';
        arr = this.images.map((img) => {
            return '"' + img.name + '"';
        });
        const joined = arr.join();
        const data = { table, column: 'favoris',
            columnValue: 0,
            conditionColumn: url,
            conditionValue: '(' + joined + ')' };
        this.sql.updateMultiValueOneColumn(data)
            .then(() => {
            data.columnValue = 1;
            data.conditionColumn = 'id_visite_photo';
            data.conditionValue = idPhoto;
            this.sql.updateOneColumn(data)
                .then(() => {
                this.favorite = idPhoto;
                this.toast.logMessage('Photo favorite sélectionné.', 'success');
            })
                .catch((error) => {
                this.toast.logMessage('Erreur d\'ajout aux favoris', 'dark');
            });
        })
            .catch(() => {
            this.toast.logMessage('Erreur d\'ajout aux favoris', 'dark');
        });
    }
    getStorages(idVisit) {
        return new Promise((resolve, reject) => {
            this.sql.getRows('visite_storage_key', 'storage_key', ' WHERE id_visite = (' + idVisit + ')')
                .then((result) => {
                console.log(result);
                this.tabLength = result.length;
                this.getImagesFromStorages(result)
                    .then((res) => {
                    this.filterArray(res)
                        .then((resp) => {
                        console.log(resp);
                        this.startUpload(resp)
                            .then(() => {
                            resolve(resp);
                        })
                            .catch((error) => {
                            this.toast.logMessage('Erreur lors de l\'envoi des photos', 'dark');
                            reject(error);
                        });
                    });
                })
                    .catch((error) => {
                    console.log(error);
                    this.toast.logMessage('Aucune photo trouvé.', 'dark');
                    reject(error);
                });
            })
                .catch((err) => {
                console.log(err);
                this.toast.logMessage('Aucune photo trouvé.', 'dark');
                reject(err);
            });
        });
    }
    getImagesFromStorages(storages) {
        const result = [];
        return new Promise((resolve) => {
            for (let i = 0; i < storages.length; i++) {
                this.storage.get(storages[i].storage_key)
                    .then((images) => {
                    if (images) {
                        const buffer = JSON.parse(images);
                        result.push(buffer);
                    }
                    if (i === storages.length - 1) {
                        resolve(result);
                    }
                });
            }
        });
    }
    filterArray(tab) {
        return new Promise((resolve) => {
            const finalTab = [];
            for (let j = 0; j < tab.length; j++) {
                if (tab[j]) {
                    // tslint:disable-next-line:prefer-for-of
                    for (let i = 0; i < tab[j].length; i++) {
                        const filePath = this.file.dataDirectory + tab[j][i];
                        const resPath = this.pathForImage(filePath);
                        const temp = { name: tab[j][i], path: resPath, filePath };
                        finalTab.push(temp);
                    }
                }
                if (j === tab.length - 1) {
                    resolve(finalTab);
                }
            }
        });
    }
    startUpload(images) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const length = images.length;
            yield images.map((img, i) => {
                this.file.resolveLocalFilesystemUrl(img.filePath)
                    .then((entry) => {
                    entry.file(file => this.readFile(file));
                })
                    .catch((err) => {
                    this.toast.logMessage('Erreur lors de la lecture de la photo.', 'warning');
                });
                if (i === length - 1) {
                    return true;
                    this.toast.logMessage('Toutes les photos ont était envoyé', 'success');
                }
            });
        });
    }
    // Method to get picture
    readFile(file) {
        const reader = new FileReader();
        reader.onload = () => {
            const formData = new FormData();
            const imgBlob = new Blob([reader.result], {
                type: file.type
            });
            formData.append('file', imgBlob, file.name);
            this.uploadImageData(formData);
        };
        // reader.readAsDataURL(file);
        reader.readAsArrayBuffer(file);
    }
    // Method to upload pictures to the api
    uploadImageData(formData) {
        this.jwt.makeJwtOption()
            .then((res) => {
            this.api.apiPostRequest(formData, 'upload/upload', res)
                .catch((error) => {
                this.toast.logMessage('POST: Erreur lors de l\'upload.', 'warning');
            });
        })
            .catch(() => {
            this.toast.logMessage('JWT: Erreur lors de l\'upload.', 'warning');
        });
    }
    // Method to insert new storage name to BDD
    insertStorageName(storageName, idVisit) {
        const data = {
            table: 'visite_storage_key',
            column: 'id_visite, storage_key',
            value: '(' + idVisit + ', "' + storageName + '")'
        };
        return this.sql.insertRow(data);
    }
    /**
     * Method to check if storage name already exist on the BDD or not
     * If exist return true else return false
     */
    checkStorage(storageName, idVisit) {
        return this.sql.getRows('visite_storage_key', 'storage_key', ' WHERE storage_key = ("' + storageName + '")')
            .then((response) => {
            if (response !== undefined && !response.message) {
                return false;
            }
            else {
                return true;
            }
        })
            .catch(() => {
            this.toast.logMessage('Une erreur est survenue, revenez en arrière.', 'dark');
        });
    }
};
PhotoService.ctorParameters = () => [
    { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_2__["Camera"] },
    { type: _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_5__["File"] },
    { type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__["WebView"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
    { type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_7__["FilePath"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
    { type: _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_9__["SqliteService"] },
    { type: _toast_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"] },
    { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__["Geolocation"] },
    { type: _api_api_service__WEBPACK_IMPORTED_MODULE_11__["ApiService"] },
    { type: _jwt_jwt_service__WEBPACK_IMPORTED_MODULE_12__["JwtService"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] }
];
PhotoService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_2__["Camera"],
        _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_5__["File"],
        _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__["WebView"],
        _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"],
        _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_7__["FilePath"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"],
        _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_9__["SqliteService"],
        _toast_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"],
        _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__["Geolocation"],
        _api_api_service__WEBPACK_IMPORTED_MODULE_11__["ApiService"],
        _jwt_jwt_service__WEBPACK_IMPORTED_MODULE_12__["JwtService"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]])
], PhotoService);



/***/ }),

/***/ "./src/app/services/sqlite/sqlite.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/sqlite/sqlite.service.ts ***!
  \***************************************************/
/*! exports provided: SqliteService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SqliteService", function() { return SqliteService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/sqlite/ngx */ "./node_modules/@ionic-native/sqlite/ngx/index.js");


// import SQLite packages

let SqliteService = class SqliteService {
    constructor(sqlite) {
        this.sqlite = sqlite;
        this.databaseName = 'ashera.db'; // BDD name
        // Fixed tables
        this.databaseTables = [
            {
                table: 'visite_cas',
                column: '(id_visite_cas INTEGER PRIMARY KEY, cas TEXT, notable INTEGER) '
            },
            {
                table: 'visite_note',
                column: '(id_visite_note INTEGER PRIMARY KEY, note TEXT)'
            },
            {
                table: 'visite_status',
                column: '(id_visite_status INTEGER PRIMARY KEY, status TEXT)'
            },
            {
                table: 'visite_token',
                column: '(id_token INTEGER PRIMARY KEY, token TEXT)'
            },
            {
                table: 'visite',
                column: '(id_visite INTEGER PRIMARY KEY, ' +
                    'id_ouvrage INTEGER, id_client INTEGER, ' +
                    'nom_client TEXT, ' +
                    'nom_ouvrage TEXT, ' +
                    'date TEXT,' +
                    'commentaire TEXT NULL, ' +
                    'id_visite_status INTEGER, FOREIGN KEY (id_visite_status) REFERENCES visite_status (id_visite_status))'
            },
            {
                table: 'visite_partie_ouvrage',
                column: '(id_visite_partie_ouvrage INTEGER PRIMARY KEY, ' +
                    'id_visite INTEGER, ' +
                    'id_partie_ouvrage INTEGER, ' +
                    'nom_partie_ouvrage TEXT, ' +
                    'status INTEGER DEFAULT 0, ' +
                    'FOREIGN KEY (id_visite) REFERENCES visite (id_visite))'
            },
            {
                table: 'visite_photo',
                column: '(id_visite_photo INTEGER PRIMARY KEY, ' +
                    'url VARCHAR(255), ' +
                    'commentaire TEXT NULL, ' +
                    'favoris INTEGER, ' +
                    'x TEXT NULL, ' +
                    'y TEXT NULL)'
            },
            {
                table: 'visite_liste_cas',
                column: '(id_visite_liste_cas INTEGER PRIMARY KEY, ' +
                    'id_visite_partie_ouvrage INTEGER, ' +
                    'id_visite_note INTEGER DEFAULT 1, ' +
                    'id_photo TEXT, ' +
                    'commentaire TEXT, ' +
                    'status INTEGER DEFAULT 0, ' +
                    'id_visite_cas INTEGER, ' +
                    'FOREIGN KEY (id_visite_partie_ouvrage) REFERENCES visite_partie_ouvrage (id_visite_partie_ouvrage), ' +
                    'FOREIGN KEY (id_visite_note) REFERENCES visite_note (id_visite_note), ' +
                    'FOREIGN KEY (id_visite_cas) REFERENCES visite_cas (id_visite_cas))'
            },
            {
                table: 'visite_equipement',
                column: '(id_visite_equipement INTEGER PRIMARY KEY, ' +
                    'id_visite_partie_ouvrage INTEGER, ' +
                    'id_equipement INTEGER, ' +
                    'id_photo TEXT, ' +
                    'id_visite_note INTEGER DEFAULT 1, ' +
                    'commentaire TEXT, ' +
                    'nom_equipement TEXT, ' +
                    'status INTEGER DEFAULT 0, ' +
                    'FOREIGN KEY (id_visite_note) REFERENCES visite_note (id_visite_note), ' +
                    'FOREIGN KEY (id_visite_partie_ouvrage) REFERENCES visite_partie_ouvrage (id_visite_partie_ouvrage))'
            },
            {
                table: 'visite_storage_key',
                column: '(id_visite_storage_key INTEGER PRIMARY KEY, ' +
                    'id_visite INTEGER, ' +
                    'storage_key TEXT, ' +
                    'FOREIGN KEY (id_visite) REFERENCES visite (id_visite))'
            }
        ];
    }
    /**
     * Method to create de BDD
     * then name is fixed internally on a prop
     */
    createDB() {
        return this.sqlite.create({
            name: 'ashera.db',
            location: 'default'
        })
            .then((db) => {
            this.databaseObj = db;
            return db;
        })
            .catch(error => {
            return error;
        });
    }
    /**
     * Method to create tables
     * The tables are fixed on a prop
     * need to think of a way to make it dynamically
     */
    createTable(tableObject) {
        return this.databaseObj.executeSql('CREATE TABLE IF NOT EXISTS ' + tableObject.table + ' ' + tableObject.column, [])
            .then((response) => {
            return response;
        })
            .catch(e => {
            return e;
        });
    }
    insertOrReplace(data = { table: 'visite_liste_cas', column: ['id_visite_partie_ouvrage'], value: ['4'] }) {
        if (data.column.length !== data.value.length) {
            throw Error('Columns and Values array have a different sizes ! Sql request aborted..');
        }
        else {
            let toConcatenate;
            let whereClause = '';
            const length = data.column.length;
            let valueGroup = '';
            let columnGroup = '';
            let value;
            let column;
            for (let i = 0; i < data.column.length; i++) {
                value = data.value[i];
                column = data.column[i];
                toConcatenate = isNaN(value) ? `"${value}"` : `${value}`;
                if (i === length - 1) {
                    valueGroup += toConcatenate;
                    columnGroup += column;
                    whereClause += `${column} = ${toConcatenate}`;
                }
                else {
                    valueGroup += `${toConcatenate}, `;
                    columnGroup += `${column}, `;
                    whereClause += `${column} = ${toConcatenate} AND `;
                }
            }
            const request = 'INSERT INTO ' + data.table + ' (' + columnGroup + ') SELECT ' + valueGroup +
                ' WHERE NOT EXISTS ( SELECT 1 FROM ' + data.table + ' WHERE ' + whereClause + ')';
            return this.databaseObj.executeSql(request, []);
        }
    }
    /**
     * @param data = {table: 'visite', setStmt: 'date = \'15/10/2018\', nom_agent=\'AgentName\'', conditionStmt: 'WHERE id_visite = 5'}
     */
    updateRequest(data) {
        const updateQuery = `UPDATE ${data.table} SET ${data.setStmt} ${data.conditionStmt}`;
        return this.databaseObj.executeSql(updateQuery, []);
    }
    updateIncrement(data = {
        table: 'visite',
        column: 'id_visite_status',
        incrementValue: 1,
        condition: 'WHERE id_visite = 1 AND id_visite_status = 1'
    }) {
        const updateQuery = `UPDATE ${data.table} SET ${data.column} = ${data.column} + ${data.incrementValue} ${data.condition}`;
        return this.databaseObj.executeSql(updateQuery)
            .then((res) => {
            return res;
        })
            .catch((err) => {
            return err;
        });
    }
    /**
     * Method to insert row in table
     * Object format [table: 'table name', column: 'column(s) name', value: 'value(s) to insert' in respect to SQL syntax
     * @param data // object of data(s) to insert
     */
    insertRow(data = { table: 'visite_note', column: 'note', value: '("mauvais")' }, option = '') {
        return this.databaseObj.executeSql('INSERT INTO ' + data.table + ' (' + data.column + ') VALUES ' + data.value + option, [])
            .then((res) => {
            return res;
        })
            .catch(e => {
            return e;
        });
    }
    customQuery(query) {
        return this.databaseObj.executeSql(query, [])
            .then((res) => {
            if (res.rows.length > 0) {
                const rowData = [];
                for (let i = 0; i < res.rows.length; i++) {
                    rowData.push(res.rows.item(i));
                }
                return rowData;
            }
            else {
                return res;
            }
        })
            .catch(e => {
            return e;
        });
    }
    /**
     * Method to get all rows from a table
     * @param table table name
     * @param columns all columns you want, if multiple join them in parenthesis separeted by commas -> (column1, column2)
     * @param options here goes all sql conditions (WHERE, LIKE etc...)
     */
    getRows(table, columns = '*', options = '') {
        return this.databaseObj.executeSql('SELECT ' + columns + ' FROM ' + table + ' ' + options, [])
            .then((res) => {
            if (res.rows.length > 0) {
                const rowData = [];
                for (let i = 0; i < res.rows.length; i++) {
                    rowData.push(res.rows.item(i));
                }
                return rowData;
            }
        })
            .catch(e => {
            return e;
        });
    }
    /**
     * Method to delete a row
     * @param value // Value to search
     * @param arg // Object containing prop table and column
     */
    deleteRow(value, arg = { table: 'nom de la table', column: 'nom de la column' }) {
        return this.databaseObj.executeSql('DELETE FROM ' + arg.table + ' WHERE ' + arg.column + ' = ' + value, []);
    }
    /**
     * Method to DROP a table
     */
    dropTable(table) {
        return this.databaseObj.executeSql('DROP TABLE IF EXISTS ' + table, []);
    }
    /**
     * Method to update the value in a row
     * @param data // {table: 'table name', column: 'name of the column to change', columnValue: 'value to change or update',
     * conditionColumn: 'name of the column of the condition', conditionValue: 'value of the condition'}
     */
    updateOneColumn(data) {
        return this.databaseObj.executeSql('UPDATE ' + data.table + ' SET ' + data.column +
            ' = ("' + data.columnValue + '") WHERE ' + data.conditionColumn + ' = ' + data.conditionValue, []);
    }
    updateMultiValueOneColumn(data) {
        return this.databaseObj.executeSql('UPDATE ' + data.table + ' SET ' + data.column + ' = (' + data.columnValue + ') WHERE ' +
            data.conditionColumn + ' IN ' + data.conditionValue, []);
    }
};
SqliteService.ctorParameters = () => [
    { type: _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_2__["SQLite"] }
];
SqliteService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_2__["SQLite"]])
], SqliteService);



/***/ }),

/***/ "./src/app/services/toast/toast.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/toast/toast.service.ts ***!
  \*************************************************/
/*! exports provided: ToastService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToastService", function() { return ToastService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



let ToastService = class ToastService {
    constructor(toastController) {
        this.toastController = toastController;
    }
    logMessage(msg, color) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                message: msg,
                duration: 2000,
                color: color,
                buttons: [
                    {
                        side: 'end',
                        role: 'cancel',
                        text: 'Fermer',
                        handler: () => {
                        }
                    }
                ]
            });
            toast.present();
        });
    }
    presentToastWithOptions() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const toast = yield this.toastController.create({
                header: 'Toast header',
                message: 'Click to Close',
                position: 'top',
                buttons: [
                    {
                        side: 'start',
                        icon: 'star',
                        text: 'Favorite',
                        handler: () => {
                        }
                    }, {
                        text: 'Done',
                        role: 'cancel',
                        handler: () => {
                        }
                    }
                ]
            });
            toast.present();
        });
    }
};
ToastService.ctorParameters = () => [
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
];
ToastService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])
], ToastService);



/***/ }),

/***/ "./src/app/services/visit/visit.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/visit/visit.service.ts ***!
  \*************************************************/
/*! exports provided: VisitService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitService", function() { return VisitService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");
/* harmony import */ var _api_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../api/api.service */ "./src/app/services/api/api.service.ts");
/* harmony import */ var _entities_visit__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../entities/visit */ "./src/app/services/entities/visit.ts");
/* harmony import */ var _entities_visit_partie_ouvrage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../entities/visit-partie-ouvrage */ "./src/app/services/entities/visit-partie-ouvrage.ts");
/* harmony import */ var _entities_visit_equipement__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../entities/visit-equipement */ "./src/app/services/entities/visit-equipement.ts");
/* harmony import */ var _toast_toast_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../toast/toast.service */ "./src/app/services/toast/toast.service.ts");
/* harmony import */ var _entities_visit_cas__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../entities/visit-cas */ "./src/app/services/entities/visit-cas.ts");
/* harmony import */ var _jwt_jwt_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../jwt/jwt.service */ "./src/app/services/jwt/jwt.service.ts");
/* harmony import */ var _entities_visite_liste_cas__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../entities/visite-liste-cas */ "./src/app/services/entities/visite-liste-cas.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm2015/ionic-storage.js");




/* All entity class */



/* Extra services to manage error and output data to the view */





let VisitService = class VisitService {
    constructor(sqlite, api, toast, jwt, storage) {
        this.sqlite = sqlite;
        this.api = api;
        this.toast = toast;
        this.jwt = jwt;
        this.storage = storage;
        this.tokenAgent = {};
        this.getAgentId()
            .then((idAgent) => {
            console.log('Trying to get clientId', idAgent);
            this.tokenAgent.id = idAgent;
            console.log(this.tokenAgent.id);
        });
        this.syncStructure = {
            'visite_partie_ouvrage': {
                'visite_liste_cas': {
                    'visite_photo': ''
                },
                'visite_equipement': {
                    'visite_photo': ''
                }
            }
        };
    }
    /* Here are all methods used only for sync with GS'O database ! */
    recursiveObj(obj, structure, id, parentName) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            //console.log('In recursivity, \n - starting obj :', obj);
            //console.log(' - structure : ', structure);
            //console.log(' - parent : ' + parentName + ' with id : ' + id);
            /* Get the object of previous call or instanciate a new one */
            let result = obj || {};
            let promises = [];
            for (let prop in structure) {
                /* Like our structure, try to append a new prop in our previous object filled with result of a custom function */
                let conditionStmt;
                if (prop === 'visite_photo') {
                    conditionStmt = result['id_photo'] ? 'WHERE id_visite_' + 'photo' + ' IN (' + result['id_photo'] + ')' : 'WHERE id_visite_photo = 0';
                    //console.log('Parent prop : photo')
                    delete result['id_' + 'photo'];
                    delete result['id_' + parentName];
                }
                else {
                    conditionStmt = 'WHERE id_' + parentName + ' = ' + id;
                    //console.log('Parent prop : ', parentName)
                    delete result['id_' + parentName];
                }
                //console.log('prop : ', prop);
                //console.log('condition STMT : ', conditionStmt);
                promises.push(this.sqlite.getRows(prop, '*', conditionStmt)
                    .then((res) => {
                    //console.log('result of fillArray for '+ prop + ' with parent '+ parentName + 'and id ' + id, res);
                    result[prop] = res || [];
                    //console.log(prop+' array = ',result[prop]);
                    if (typeof structure[prop] === 'object') {
                        //console.log('Need to recall this recusrive function for ' + prop + ' prop');
                        //console.log('For Prop type object \'' + prop + '\'', parentName)
                        if (result[prop]) {
                            result[prop].map((value, index, array) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                                delete result[prop][index]['id_' + parentName];
                                let idParent = value['id_' + prop];
                                delete result[prop][index]['id_' + prop];
                                let nestedArray = yield this.recursiveObj(result[prop][index], structure[prop], idParent, prop);
                                //console.log('Nested Array for '+ prop + 'and id : '+idParent,nestedArray);
                                return nestedArray;
                            }));
                            //console.log('Result : ', result);
                            return result[prop];
                        }
                        else {
                            return result[prop] = [];
                        }
                    }
                    else {
                        //console.log('Not type object ; ', result[prop]);
                        //console.log('Result : ', result);
                        return result[prop].map((value, index) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                            delete result[prop][index]['id_' + prop];
                        }));
                    }
                }));
            }
            return Promise.all(promises).then((res) => {
                //console.log('Result after all promises resolvation : ', res);
                //console.log('Result var = ', result);
                return result;
            });
        });
    }
    fillArray(id, parentName, childName) {
        return this.sqlite.getRows(childName, '*', 'WHERE id_' + parentName + ' = ' + id);
    }
    syncVisit(idVisit) {
        console.log('Sync launched with id_visite = ' + idVisit);
        return this.sqlite.getRows('visite', '*', 'WHERE id_visite = ' + idVisit)
            .then((visit) => {
            console.log('Visite from table \'visite\' : ', visit);
            return this.recursiveObj(visit[0], this.syncStructure, idVisit, 'visite');
        })
            .then((res) => {
            console.log('Final json to send to the back ! →', res);
            return res;
        })
            .catch((err) => {
            console.error('Error occured while trying to build json for sync ...', err);
            throw Error('Error occured, check log for more informations !');
        });
    }
    syncVisitMethod(idVisit) {
        return new Promise((resolve, reject) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let columns = 'commentaire, date, id_client, id_ouvrage, id_visite_status, nom_client, nom_ouvrage';
            const visit = yield this.sqlite.getRows('visite', columns, 'WHERE id_visite = ' + idVisit);
            console.log('Visit: ', visit);
            this.syncPo(idVisit).then((visitPoArr) => {
                resolve(Object.assign({}, visit[0], { 'visite_partie_ouvrage': visitPoArr }));
            });
        }));
    }
    syncPo(idVisit) {
        return new Promise((resolve, reject) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let column = 'id_partie_ouvrage, nom_partie_ouvrage, status, id_visite_partie_ouvrage';
            const visitPo = yield this.sqlite.getRows('visite_partie_ouvrage', column, 'WHERE id_visite = ' + idVisit);
            console.log('VisitPo: ', visitPo);
            let listeCas = visitPo ? visitPo.map((val, index) => {
                let idPo = visitPo[index]['id_visite_partie_ouvrage'];
                return this.syncVisitCas(idPo)
                    .then((res) => {
                    console.log('Trying to build liste cas array : ', res);
                    return Object.assign({}, visitPo[index], { 'visite_liste_cas': res });
                });
            }) : [];
            Promise.all(listeCas)
                .then((visitPoArray) => {
                let equip = [];
                for (let i = 0; i < visitPoArray.length; i++) {
                    let idPO = visitPoArray[i]['id_visite_partie_ouvrage'];
                    delete visitPoArray[i]['id_visite_partie_ouvrage'];
                    equip.push(this.syncVisitEquip(idPO).then((res) => {
                        console.log('Trying to build equip array : ', res);
                        if (res) {
                            return Object.assign({}, visitPoArray[i], { 'visite_equipement': res });
                        }
                    }));
                }
                Promise.all(equip).then((visitPoArr) => { resolve(visitPoArr); });
            });
        }));
    }
    syncVisitEquip(idPo) {
        return new Promise((resolve, reject) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let columns = 'commentaire, id_equipement, id_photo, id_visite_note, nom_equipement, status';
            const visitEquip = yield this.sqlite.getRows('visite_equipement', columns, 'WHERE id_visite_partie_ouvrage = ' + idPo);
            let picture = visitEquip ? visitEquip.map((val, index) => {
                let idPhoto = visitEquip[index]['id_photo'];
                delete visitEquip[index]['id_photo'];
                let photo_columns = 'commentaire, favoris, url, x, y';
                return this.sqlite.getRows('visite_photo', photo_columns, 'WHERE id_visite_photo IN (' + idPhoto + ')')
                    .then((res) => {
                    console.log('Response from sqlite service : ', res);
                    let photo = res || [];
                    return Object.assign({}, visitEquip[index], { 'visite_photo': photo });
                });
            }) : [];
            Promise.all(picture).then((resArray) => {
                console.log('Resolve of all promises (pictures) for equip : ', resArray);
                resolve(resArray);
            });
        }));
    }
    syncVisitCas(idPo) {
        return new Promise((resolve, reject) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let columns = 'commentaire, id_photo, id_visite_cas, id_visite_note, status';
            const visitCas = (yield this.sqlite.getRows('visite_liste_cas', columns, 'WHERE id_visite_partie_ouvrage = ' + idPo)) || [];
            let picture = visitCas ? visitCas.map((val, index) => {
                let idPhoto = visitCas[index]['id_photo'];
                delete visitCas[index]['id_photo'];
                let photo_columns = 'commentaire, favoris, url, x, y';
                return this.sqlite.getRows('visite_photo', photo_columns, 'WHERE id_visite_photo IN (' + idPhoto + ')')
                    .then((res) => {
                    console.log('Response from sqlite service : ', res);
                    let photo = res || [];
                    return Object.assign({}, visitCas[index], { 'visite_photo': photo });
                });
            }) : [];
            Promise.all(picture).then((resArray) => {
                console.log('Resolve of all promises (pictures) for liste cas : ', resArray);
                resolve(resArray);
            });
        }));
    }
    syncVisitRaw(visitId) {
        let result = {};
        return this.sqlite.getRows('visite', '*', 'WHERE id_visite = ' + visitId)
            .then((visit) => {
            result = visit[0];
            return this.sqlite.getRows('visite_partie_ouvrage', '*', 'WHERE id_visite = ' + result['id_visite']);
        })
            .then((visitePo) => {
            result['visite_partie_ouvrage'] = visitePo || [];
            const promisesEquip = result['visite_partie_ouvrage'].map((val, index) => {
                return this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = ' + val['id_visite_partie_ouvrage'])
                    .then((equipArray) => {
                    let promisePhoto = equipArray.map((val) => {
                        return this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN (' + val['id_photo'] + ')');
                    });
                    return Promise.all(promisePhoto).then((arrayPhotoEquip) => {
                        equipArray.forEach((val, index) => {
                            equipArray[index]['visite_photo'] = arrayPhotoEquip[index];
                        });
                    });
                });
            });
            const promisesListeCas = result['visite_partie_ouvrage'].map((val, index) => {
                return this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = ' + val['id_visite_partie_ouvrage']);
            });
            return [promisesEquip, promisesListeCas];
        })
            .then((arrayPromises) => {
            return [
                Promise.all(arrayPromises[0]).then((arrayEquip) => {
                    //result['visite_partie_ouvrage']['visite_equipement'] = arrayEquip;
                    const promisesPictureEquip = arrayEquip.map((val, index) => {
                        return this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN (' + val['id_photo'] + ')');
                    });
                    return promisesPictureEquip;
                }),
                Promise.all(arrayPromises[1]).then((arrayListeCas) => {
                    result['visite_partie_ouvrage']['visite_liste_cas'] = arrayListeCas;
                    const promisesPictureListeCas = arrayListeCas.map((val, index) => {
                        return this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN (' + val['id_photo'] + ')');
                    });
                    return promisesPictureListeCas;
                })
            ];
        })
            .then((arrayAllPromises) => {
            arrayAllPromises[0];
        });
    }
    asyncForEach(array, callback) {
        return new Promise((resolve) => {
            let result;
            for (let i = 0; i < array.length; i++) {
                if (i === array.length - 1) {
                    resolve(array);
                }
            }
        });
    }
    syncVisitAsync(idVisit) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            var e_1, _a;
            const visit = yield this.sqlite.getRows('visite', '*', 'WHERE id_visite = ' + idVisit);
            let result = visit[0];
            result[0]['visite_partie_ouvrage'] = yield this.sqlite.getRows('visite_partie_ouvrage', '*', 'WHERE id_visite = ' + idVisit);
            let indexEquip, indexListCas = 0;
            try {
                for (var _b = tslib__WEBPACK_IMPORTED_MODULE_0__["__asyncValues"](result[0]['visite_partie_ouvrage']), _c; _c = yield _b.next(), !_c.done;) {
                    let val = _c.value;
                    let visiteEquip = (yield this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = ' + val['id_visite_partie_ouvrage'])) || [];
                    visiteEquip.forEach((value, index) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                        let photo = yield this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN (' + value['id_photo'] + ')');
                        visiteEquip[index]['visite_photo'] = photo || [];
                    }));
                    let listeCas = (yield this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = ' + val['id_visite_partie_ouvrage'])) || [];
                    listeCas.forEach((value, index) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                        let photo = yield this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN (' + value['id_photo'] + ')');
                        listeCas[index]['visite_photo'] = photo || [];
                    }));
                    result[0]['visite_partie_ouvrage'][indexEquip++]['visite_equipement'] = visiteEquip;
                    result[0]['visite_partie_ouvrage'][indexListCas++]['visite_liste_cas'] = listeCas;
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_c && !_c.done && (_a = _b.return)) yield _a.call(_b);
                }
                finally { if (e_1) throw e_1.error; }
            }
            /* result[0]['visite_partie_ouvrage'].forEach(async (val, index) => {
              let visiteEquip = await this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = ' + val['id_visite_partie_ouvrage']) || [];
              visiteEquip.forEach(async (value, index) => {
                let photo = await this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN ('+value['id_photo']+')');
                visiteEquip[index]['visite_photo'] = photo || [];
              })
              let listeCas = await this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = ' + val['id_visite_partie_ouvrage']) || [];
              listeCas.forEach(async (value, index) => {
                let photo = await this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN ('+value['id_photo']+')');
                listeCas[index]['visite_photo'] = photo || [];
              })
              result[0]['visite_partie_ouvrage'][index]['visite_equipement'] = visiteEquip;
              result[0]['visite_partie_ouvrage'][index]['visite_liste_cas'] = listeCas;
            }) */
            return result[0];
        });
    }
    /* -- END :: SYNC METHODS */
    /**
     * Fake Service -- Will be replaced by storage access
     */
    getAgentId() {
        return this.storage.get('clientId');
    }
    getPOData(idPo) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const equip = yield this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = ' + idPo);
            const listCas = yield this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = ' + idPo);
            return {
                equipement: equip,
                listeCas: listCas
            };
        });
    }
    getUtilitaryTables() {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const cases = yield this.sqlite.getRows('visite_cas');
            const rates = yield this.sqlite.getRows('visite_note');
            return {
                cas: cases,
                note: rates
            };
        });
    }
    /* Offlines features */
    updateVisiteData(value, table, column, conditionColumn, conditionValue) {
        // "UPDATE table SET column1 = value1 WHERE id = 1"
        return this.sqlite.updateOneColumn({ table, column, columnValue: value, conditionColumn, conditionValue });
    }
    fetchVisitCas() {
        let visitCas = new _entities_visit_cas__WEBPACK_IMPORTED_MODULE_8__["VisitCas"]();
        visitCas.fetchCases()
            .then((casesArray) => {
            casesArray.forEach((value) => {
                this.constructQueryMultiple(value).then((queryArray) => {
                    let columnQuery = queryArray[0];
                    let valueQuery = queryArray[1];
                    this.sqlite.insertRow({ table: visitCas.tableName, column: columnQuery, value: valueQuery }).then((res) => {
                        console.log("After insertion -> ", res);
                    });
                });
            });
        });
    }
    constructVisitListeCas(idVisitPo = 4) {
        this.sqlite.getRows('visite_cas').then((casesArray) => {
            console.log(casesArray);
            casesArray.forEach((visitCas) => {
                let visitListCas = new _entities_visite_liste_cas__WEBPACK_IMPORTED_MODULE_10__["VisiteListeCas"](visitCas.id_visite_cas, idVisitPo);
                this.constructInsertReplaceQuery(visitListCas)
                    .then((queryArray) => {
                    console.log('queryArray', queryArray);
                    this.sqlite.insertOrReplace({ table: visitListCas.tableName, column: queryArray[0], value: queryArray[1] })
                        .then((res) => {
                        console.log('sql res', res);
                    });
                });
            }); /* END :: FOREACH */
        }); /* END :: PROMISE */
    }
    /* /!\ Should be place in sqlite service ! */
    constructInsertReplaceQuery(entity) {
        let columns = [];
        let values = [];
        let keys = Object.keys(entity);
        return new Promise((resolve, reject) => {
            keys.forEach((value, index) => {
                if (value !== 'tableName') { //Prop in each entity to know where to insert in sqliteDatabase
                    columns.push(value);
                    values.push(entity[value]); //To respect integer syntax
                    if (keys.length - 1 === index) {
                        resolve([columns, values]); // End of our promise
                    }
                }
            });
        });
    }
    /**
     * /!\ Problem list :
     * - Know when a chaining promise IN a foreach are ALL dones (and so for nested promise)
     * - Set an execution time_out that display a toast 'Execution max_time exceeded'
     * -
     */
    getVisitPoData(idPo = 4) {
        // Select all From equip, visite_liste_cas WHERE id_visite_partie_ouvrage = idPO
        // Then : => equip = {id, name, status, id_note} ; liste_cas = {id, id_cas, id_note}
        /**
          SELECT e.* l.* p.*
          FROM visite_partie_ouvrage p
          INNER JOIN visite_equipement e ON e.id_visite_partie_ouvrage = p.id_visite_partie_ouvrage
          INNER JOIN visite_liste_cas l ON l.id_visite_partie_ouvrage = p.id_visite_partie_ouvrage
          WHERE p.id_visite_partie_ouvrage = idPo
        */
        /*
        let query1 = 'SELECT * FROM visite_equipement WHERE id_visite_partie_ouvrage = ' + idPo;
        let query2 = 'SELECT * FROM visite_liste_cas WHERE id_visite_partie_ouvrage = '+ idPo;
        let promises = [];
        promises.push(this.sqlite.getRows('visite_liste_cas','*', 'WHERE id_visite_partie_ouvrage = ' + idPo));
        promises.push(this.sqlite.getRows('visite_equipement','*', 'WHERE id_visite_partie_ouvrage = ' + idPo));
        return Promise.all(promises); */
        let result = [];
        const conditionPo = 'SELECT * FROM visite_liste_cas WHERE id_visite_partie_ouvrage = ' + idPo;
        const conditionEquipement = 'SELECT * FROM visite_equipement WHERE id_visite_partie_ouvrage = ' + idPo;
        return new Promise((resolve, reject) => {
            this.sqlite.customQuery(conditionPo)
                .then((res) => {
                console.log('res', res);
                result['cas'] = [];
                res.map((cas, i) => {
                    console.log(cas);
                    const conditionNotable = 'SELECT * FROM visite_cas WHERE id_visite_cas = (' + cas.id_visite_cas + ')';
                    this.sqlite.customQuery(conditionNotable)
                        .then((response) => {
                        console.log(response);
                        cas.notable = response[0].notable;
                        cas.name = response[0].cas;
                        result['cas'][i] = cas;
                        this.sqlite.customQuery(conditionEquipement)
                            .then((resp) => {
                            result['equipement'] = resp;
                            resolve(result);
                        })
                            .catch((error) => {
                            reject(error);
                        });
                    });
                });
            })
                .catch((err) => {
                reject(err);
            });
        });
    }
    getClosedVisit() {
        return this.sqlite.getRows('visite', '*', 'WHERE id_visite_status = 3');
    }
    getVisitData(idVisit = 1, status = 2) {
        let condition = 'WHERE id_visite IN (SELECT id_visite FROM visite WHERE id_visite = ' + idVisit + ' AND id_visite_status = ' + status + ')';
        return this.sqlite.getRows('visite_partie_ouvrage', '*', condition);
    }
    initVisit(idVisit, data = { nameAgent: 'NameAgent', date: '2019-11-19', comment: '' }) {
        // Update with sqlite service the visit with previous status ++
        // Specifiy in select two clause, id_visit = idVisit AND id_status = visit.status
        let setStatement = 'nom_client = \'' + data.nameAgent + '\',' +
            ' date = \'' + data.date + '\',' + ' commentaire = \'' + data.comment + '\'';
        return this.sqlite.updateRequest({ table: 'visite', setStmt: setStatement, conditionStmt: 'WHERE id_visite = ' +
                idVisit + ' AND id_visite_status = 1' })
            .then((res) => {
            if (res.hasOwnProperty('rowsAffected')) {
                if (res.rowsAffected > 0) {
                    console.log('someRowsWereAffected');
                    console.log('update visite with id = ' + idVisit, res);
                    return this.sqlite.updateIncrement({ table: 'visite',
                        column: 'id_visite_status',
                        incrementValue: 1,
                        condition: 'WHERE id_visite = ' + idVisit + ' AND id_visite_status = 1' })
                        .then((res) => {
                        console.log('changed status visite with id = ' + idVisit, res);
                        return res;
                    })
                        .catch((err) => {
                        console.error(err);
                        throw Error('Error occured : ' + JSON.stringify(err));
                    });
                }
                else {
                    throw Error('Can\'t update visite table before updating status ; Init abort');
                }
            }
            else {
                throw Error('Failing while trying to update visite table at id ' + idVisit + '; No modification were made !');
            }
        }).catch((error) => {
            console.error(error);
            throw Error("Error occured : " + JSON.stringify(error));
        });
        /* return this.sqlite.updateRequest({table: 'visite', setStmt: 'nom_agent = \'' + data.nameAgent +'\', date = \''+ data.date + '\', commentaire = \'' + data.comment, conditionStmt:'\' WHERE id_visite = '+idVisit+' AND id_visite_status = 1'})
        .then((res) => {
          console.log(res);
          if(res.hasOwnProperty('rowsAffected')){
            if(res.rowsAffected != 0){
              console.log('some row are affected');
            }
          }
        }).catch((err) => {
          console.error(err);
        }) */
    }
    fetchVisit(type) {
        // id_visite_status : 1 = créer , 2 = initialiser, 3 = clôturer
        let condition = type === 'init' ? 1 : type === 'resume' ? 2 : null;
        return this.sqlite.getRows('visite', '*', 'WHERE id_visite_status = ' + condition);
    }
    /* Connection required features */
    visitPrepareOption(name, id) {
        const result = [];
        const uri = `preparation/${name}/`;
        // return this.api.apiGetRequest(uri,id);
        const nameRegex = RegExp('nom', 'i');
        const idRegex = RegExp('id', 'i');
        return this.api.apiGetRequest(uri, id)
            .then((apiResponse) => {
            for (const object in apiResponse) {
                const select = {
                    name: '',
                    id: ''
                };
                for (const prop in apiResponse[object]) {
                    if (nameRegex.test(prop)) {
                        select.name = apiResponse[object][prop];
                    }
                    else if (idRegex.test(prop)) {
                        select.id = apiResponse[object][prop];
                    }
                }
                result.push(select);
            }
            return result;
        }).then((toSort) => {
            toSort.sort((a, b) => {
                return a.name > b.name ? 1 : -1;
            });
            return toSort;
        })
            .catch((apiError) => {
            console.error(apiError);
        });
    }
    fetchVisitData(idOuvrage = 2925) {
        /* Call to Api after form submit, passing id Ouvrage */
        return this.api.apiGetRequest('visite/visite/', idOuvrage)
            .then((jsonData) => {
            console.log(jsonData);
            this.storage.get('clientId').then((response) => {
                let idAgent = this.tokenAgent.id || response;
                const visit = new _entities_visit__WEBPACK_IMPORTED_MODULE_4__["Visit"](jsonData.id_ouvrage, idAgent, jsonData.nom_ouvrage);
                console.log('Cette visite object = ', visit);
                const partieOuvrageData = jsonData.hasOwnProperty('visite_partie_ouvrage') ? jsonData.visite_partie_ouvrage : false;
                // Will call the sqlite service and continue to pass our object
                this.constructVisit(visit, partieOuvrageData)
                    .then((data) => {
                    /* Here the visit is created, now create the next table 'visite_partie_ouvrage' */
                    console.log(data[0]);
                    const visit = data[0];
                    const partieOuvrageArray = data[1];
                    partieOuvrageArray.forEach((value) => {
                        /* Here all 'visite_partie_ouvrage' rows are inserted, and will create the next table 'visite_equipement' */
                        // To change for 'nom_partie_ouvrage'
                        const partieOuvrage = new _entities_visit_partie_ouvrage__WEBPACK_IMPORTED_MODULE_5__["VisitPartieOuvrage"](visit.id_visite, value.id_visite_partie_ouvrage, value.nom_ouvrage);
                        const equipementData = value.hasOwnProperty('visite_equipement') ? value.visite_equipement : false;
                        this.constructVisitPO(partieOuvrage, equipementData)
                            .then((data) => {
                            console.log(data[0]);
                            const partieOuvrage = data[0];
                            const visitEquipementArray = data[1];
                            this.constructVisitListeCas(partieOuvrage.id_visite_partie_ouvrage);
                            if (visitEquipementArray !== false) {
                                visitEquipementArray.forEach((value) => {
                                    /* Finally all the equipement */
                                    const equipement = new _entities_visit_equipement__WEBPACK_IMPORTED_MODULE_6__["VisitEquipement"](partieOuvrage.id_visite_partie_ouvrage, value.id_visite_equipement, value.nom_equipement);
                                    this.constructVisitEquipement(equipement)
                                        .then((data) => {
                                        console.log(data);
                                    });
                                });
                            }
                            else {
                                //return visit.id_visite;
                            }
                        });
                    });
                });
            });
        })
            .catch((error) => {
            console.error('', error);
        });
    }
    constructQueryMultiple(entity) {
        let columns = '';
        let values = '(';
        const keys = Object.keys(entity);
        return new Promise((resolve, reject) => {
            keys.forEach((value, index) => {
                if (value !== 'tableName') { // Prop in each entity to know where to insert in sqliteDatabase
                    columns += ' "' + value + '"';
                    values += isNaN(entity[value]) ? ' "' + entity[value] + '"' : ' ' + entity[value]; // To respect integer syntax
                    if (keys.length - 1 === index) {
                        columns += '';
                        values += ')';
                        resolve([columns, values]); // End of our promise
                    }
                    else {
                        columns += ',';
                        values += ',';
                    }
                }
            });
        });
    }
    constructVisit(visitData, partieOuvrageData) {
        visitData.id_visite_status = 1;
        return new Promise((resolve, reject) => {
            this.constructQueryMultiple(visitData)
                .then((data) => {
                let queryColumn = data[0];
                let queryValues = data[1];
                //alert('To insert \ncolumns : '+queryColumn+' ; values : '+queryValues);
                this.sqlite.insertRow({ table: visitData.tableName, column: queryColumn, value: queryValues })
                    .then((response) => {
                    console.log(response);
                    if (response.hasOwnProperty('rowsAffected')) {
                        // alert("Insert successful ! Res -> "+JSON.stringify(response["insertId"]));
                        this.toast.logMessage('Insertion effectuée avec succès !', 'primary');
                        visitData.id_visite = response.insertId;
                        resolve([visitData, partieOuvrageData]);
                    }
                    else {
                        throw ['Error occured while inserting in database', ' Code : ' + response.code + ' , ' + response.message];
                    }
                });
            })
                .catch((errorArray) => {
                // alert("Error : "+error);
                console.log(errorArray[1]);
                this.toast.logMessage(errorArray[0], 'danger');
            });
        });
        /* alert(this.sqlite.insertRow({table: visitData.tableName, })) */
        /* SQL query to make table visit and get back id visite */
        /* return new Promise((resolve, reject)=>{
          setTimeout(()=>{
            visitData.id_visite = Visit.count++;
            resolve([visitData, partieOuvrageData]);
            // Reject will be about the sqlite service
          },500);
        }) */
    }
    vacuumPoData() {
    }
    vacuumPhoto(entity) {
        if (entity.hasOwnProperty('id_photo')) {
            return this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN (' + entity.id_photo + ')')
                .then((photoArray) => {
                return photoArray;
            });
        }
        else {
            return [];
        }
    }
    endVisit2(idVisit = 2) {
        let promisesEquip = [];
        let promisesListeCas = [];
        let promiseVisite = new Promise((resolve, reject) => {
            /* Get the visit */
            this.sqlite.getRows('visite', '*', 'WHERE id_visite = ' + idVisit + '')
                .then((result) => {
                console.log('visite query : ', result);
                resolve(result[0]);
            });
        })
            .then((visit) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            let json = visit;
            const partieOuvrage = yield this.sqlite.getRows('visite_partie_ouvrage', '*', 'WHERE id_visite = ' + idVisit)
                .then((result) => {
                console.log('partie ouvrage query :', result);
                console.log('... and visit param : ', visit);
                return result;
            });
            return Object.assign({}, json, { 'visite_partie_ouvrage': partieOuvrage });
        }))
            .then((result) => {
            console.log('pass json object after visite_partie_ouvrage added :', result);
            let json = result;
            json['visite_partie_ouvrage'] = result['visite_partie_ouvrage'].map((partieOuvrage, i) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                const equip = yield this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = ' + partieOuvrage.id_visite_partie_ouvrage)
                    .then((res) => {
                    if (res) {
                        res.map((equip) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
                            if (equip) {
                            }
                            else {
                            }
                        }));
                    }
                    else {
                        return [];
                    }
                });
                const liste_cas = yield this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = ' + partieOuvrage.id_visite_partie_ouvrage)
                    .then((res) => {
                    return res;
                });
                return Object.assign({}, partieOuvrage, { 'visite_equipement': equip, 'visite_liste_cas': liste_cas });
            }));
            return json;
        })
            .then((result) => {
            console.log('after added equip and liste_cas', result);
        });
    }
    /*
      return this.sqlite.getRows('visite', '*', 'WHERE id_visite = '+idVisit+'')
      .then((visitArray: Visit[]) => {
        console.log('Get visit, Step 1 / ? ', visitArray);
        const visit: Visit = visitArray[0];
        return {
          ...visit,
          'visite_partie_ouvrage':
            this.sqlite.getRows('visite_partie_ouvrage', '*', 'WHERE id_visite = '+visit.id_visite)
            .then((partieOuvrageArray: VisitPartieOuvrage[]) => {
            console.log('Get POs, Step 2 / ? ', partieOuvrageArray);
            if(partieOuvrageArray){
              return partieOuvrageArray.map((partieOuvrage, index) => {
                return {
                  ...partieOuvrage,
                  'visite_equipement':
                    this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = '+partieOuvrage.id_visite_partie_ouvrage)
                    .then((equipementArray: VisitEquipement[]) => {
                    console.log('Get Equips, Step 2.1.'+ (index+1) +' / ?', equipementArray);
                    if(equipementArray){
                      return equipementArray.map((equipement, equipIndex) => {
                        if(equipement.id_photo){
                          this.sqlite.getRows('visite_photo','*', 'WHERE id_visite_photo IN ('+ equipement.id_photo + ')')
                          .then((photoArray: VisitPhoto[]) => {
                            console.log('Get Photos, Step 3.1.'+(equipIndex+1)+'.'+(index+1)+' / ?', photoArray);
                            return {...equipement, 'visite_photo': photoArray };
                          })
                        }else{
                          return {...equipement, 'visite_photo': ['empty photo_array']};
                        }
                      });
                    }else{
                      return ['empty equip_array'];
                    }
                  }),
                  'visite_liste_cas':
                  this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = '+partieOuvrage.id_visite_partie_ouvrage)
                  .then((listeCasArray: VisiteListeCas[]) => {
                    console.log('Get Cases, Step 2.2.'+ (index+1) +' / ?', listeCasArray);
                    if(listeCasArray){
                      return listeCasArray.map((listeCas, casIndex) => {
                        if(listeCas.id_photo){
                          this.sqlite.getRows('visite_photo','*', 'WHERE id_visite_photo IN ('+ listeCas.id_photo + ')')
                          .then((photoArray: VisitPhoto[]) => {
                            console.log('Get Photos, Step 3.2.'+(casIndex+1)+'.'+(index+1)+' / ?', photoArray);
                            return {...listeCas, 'visite_photo': photoArray};
                          })
                        }else{
                          return {...listeCas, 'visite_photo': ['empty photo_array']};
                        }
                      })
                    }else{
                      return ['empty liste_cas_array'];
                    }
                  })
                };
              })
            }else{
              return ['empty partie_ouvrage_array'];
            }
          })
        };
      }) */
    iterateThroughLevel() {
    }
    megaRecursive(idVisit, dataToSend = {}) {
        const tableToSend = {
            'visite_partie_ouvrage': {
                'visite_equipement': {
                    'visite_photo': {}
                },
                'visite_liste_cas': {
                    'visite_photo': {}
                }
            }
        };
        if (Object.keys(dataToSend).length > 0) {
            for (let prop in tableToSend) {
            }
        }
        else {
            this.sqlite.getRows('visite', '*', 'WHERE id_visite = ' + idVisit)
                .then((res) => {
                this.megaRecursive(idVisit, res[0]);
            });
        }
    }
    endVisit(idVisit) {
        const tableToSend = {
            'visite': {
                'visite_partie_ouvrage': {
                    'visite_equipement': {
                        'visite_photo': {}
                    },
                    'visite_liste_cas': {
                        'visite_photo': {}
                    }
                }
            }
        };
        const visit = new _entities_visit__WEBPACK_IMPORTED_MODULE_4__["Visit"](1, 1, 'NAEM');
        return this.sqlite.getRows('visite', '*', 'WHERE id_visite = ' + idVisit + '')
            .then((visitArray) => {
            console.log('Get visit, Step 1 / ? ', visitArray);
            const visit = visitArray[0];
            return Object.assign({}, visit, { 'visite_partie_ouvrage': this.sqlite.getRows('visite_partie_ouvrage', '*', 'WHERE id_visite = ' + visit.id_visite)
                    .then((partieOuvrageArray) => {
                    console.log('Get POs, Step 2 / ? ', partieOuvrageArray);
                    if (partieOuvrageArray) {
                        return partieOuvrageArray.map((partieOuvrage, index) => {
                            return Object.assign({}, partieOuvrage, { 'visite_equipement': 
                                /* Equip for PO */
                                this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = ' + partieOuvrage.id_visite_partie_ouvrage)
                                    .then((equipementArray) => {
                                    console.log('Get Equips, Step 2.1.' + (index + 1) + ' / ?', equipementArray);
                                    if (equipementArray) {
                                        return equipementArray.map((equipement, equipIndex) => {
                                            if (equipement.id_photo) {
                                                this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN (' + equipement.id_photo + ')')
                                                    .then((photoArray) => {
                                                    console.log('Get Photos, Step 3.1.' + (equipIndex + 1) + '.' + (index + 1) + ' / ?', photoArray);
                                                    return Object.assign({}, equipement, { 'visite_photo': photoArray });
                                                });
                                            }
                                            else {
                                                return Object.assign({}, equipement, { 'visite_photo': ['empty photo_array'] });
                                            }
                                        });
                                    }
                                    else {
                                        return ['empty equip_array'];
                                    }
                                }), 'visite_liste_cas': 
                                /* Liste Cas for PO */
                                this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = ' + partieOuvrage.id_visite_partie_ouvrage)
                                    .then((listeCasArray) => {
                                    console.log('Get Cases, Step 2.2.' + (index + 1) + ' / ?', listeCasArray);
                                    if (listeCasArray) {
                                        return listeCasArray.map((listeCas, casIndex) => {
                                            if (listeCas.id_photo) {
                                                this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN (' + listeCas.id_photo + ')')
                                                    .then((photoArray) => {
                                                    console.log('Get Photos, Step 3.2.' + (casIndex + 1) + '.' + (index + 1) + ' / ?', photoArray);
                                                    return Object.assign({}, listeCas, { 'visite_photo': photoArray });
                                                });
                                            }
                                            else {
                                                return Object.assign({}, listeCas, { 'visite_photo': ['empty photo_array'] });
                                            }
                                        });
                                    }
                                    else {
                                        return ['empty liste_cas_array'];
                                    }
                                }) });
                        });
                    }
                    else {
                        return ['empty partie_ouvrage_array'];
                    }
                }) });
        });
        //return 
        /* const visite_partie_ouvrage: VisitPartieOuvrage[] = await this.sqlite.getRows('visite_partie_ouvrage','*','WHERE id_visite = '+visit.id_visite); */
        /* let visite_equipement: VisitEquipement[] = visite_partie_ouvrage.map(async (value, index) => {
          return this.sqlite.getRows('visite_equipement','*', 'WHERE id_visite_partie_ouvrage = '+value.id_visite_partie_ouvrage);
        }) */
        /* const fetchPoData = new Promise((resolve, reject)=>{
          visite_partie_ouvrage.forEach(async (valPo, index) => { */
        /* Object.defineProperties(visite_partie_ouvrage[index], {
          'visite_equipement': {
            valPo: await this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = '+valPo.id_visite_partie_ouvrage),
            writable: true
          },
          'visite_liste_cas': {
            valPo: await this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = '+valPo.id_visite_partie_ouvrage),
            writable: true
          }
        }); */
        /* visite_partie_ouvrage[index].setEquipement(
          await this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = '+valPo.id_visite_partie_ouvrage)
        );
        visite_partie_ouvrage[index].setListeCas(
          await this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = '+valPo.id_visite_partie_ouvrage)
        )
        if(index === visite_partie_ouvrage.length - 1){
          resolve('All data for each partie ouvrage get binded !');
        }
      });
    }); */
        /*
            const fetchPhotoPO = new Promise((resolve) => {
              visite_partie_ouvrage.forEach((valPo, index) => {
                visite_partie_ouvrage[index].getEquipement().map(async (valEquip: VisitEquipement, idx: number) => {
                  visite_partie_ouvrage[index].getEquipement()[idx].setPhoto(
                    await this.sqlite.getRows('visite_photo','*','WHERE id_visite_photo IN (' + valEquip.id_photo + ')')
                  );
                  if(idx === visite_partie_ouvrage[index].getEquipement().length - 1){
                    resolve(true);
                  }
                });
              });
            }); */
        /* const fetchPhotoEquip = new Promise((resolve) => {
          visite_partie_ouvrage.forEach((valPo, index) => {
            visite_partie_ouvrage[index]['visite_liste_cas'].map(async (valCas: VisiteListeCas, indx: number) => {
              visite_partie_ouvrage[index]['visite_liste_cas'][indx]['visite_photo'] = await this.sqlite.getRows('visite_photo','*','WHERE id_visite_photo IN (' + valCasvalEquip.id_photo + ')');
    
              if(indx === visite_partie_ouvrage[index]['visite_liste_cas'].length - 1){
                resolve(true);
              }
            });
          })
        }); */
        /*
        if(resolvePO === true && resolveEquip === true){
          resolve('All photos for each partie ouvrage equip & list cas were binded succesfuly !');
        }else{
          if((resolvePO !== true) && (resolveEquip !== true)){
            throw Error('Error occured while trying to bind, photos with equipment AND listCas !');
          }else if((resolvePO !== true) || (resolveEquip !== true)){
            let customError = resolvePO !== true ? 'listCas' : 'equipment';
            throw Error('Error occured while trying to bind photos with ' + customError + ' ... Aborted !');
          }
        }
        */
        /* return fetchPoData
        .then((res: any) => {
          console.log('Equip & List Cas binded ! Log : ', res);
          fetchPhotoEquip
          .then((res: any)=>{
            console.log('Photos binded ! Log : ', res);
            fetchPhotoPO.then((res: any)=>{
              console.log('Photos binded ! Log : ', res);
              const result = {...visit, visite_partie_ouvrage: visite_partie_ouvrage};
              Object.defineProperty(visit, 'visite_partie_ouvrage',{
                value: visite_partie_ouvrage,
                writable: false
              });
              console.log(result);
              return result
            });
            
          })
        }) */
    }
    uploadVisit() {
    }
    destroyVisit() {
    }
    constructVisitPO(partieOuvrage, equipementData) {
        return new Promise((resolve, reject) => {
            this.constructQueryMultiple(partieOuvrage)
                .then((data) => {
                console.log('query made : ', data);
                let queryColumn = data[0];
                let queryValues = data[1];
                //alert('To insert \ncolumns : '+queryColumn+' ; values : '+queryValues);
                this.sqlite.insertRow({ table: partieOuvrage.tableName, column: queryColumn, value: queryValues })
                    .then((response) => {
                    console.log(response);
                    if (response.hasOwnProperty('rowsAffected')) {
                        // alert("Insert successful ! Res -> "+JSON.stringify(response["insertId"]));
                        this.toast.logMessage('Insertion effectuée avec succès !', 'primary');
                        partieOuvrage.id_visite_partie_ouvrage = response.insertId;
                        resolve([partieOuvrage, equipementData]);
                    }
                    else {
                        throw ['Error occured while inserting in database', ' Code : ' + response.code + ' , ' + response.message];
                    }
                })
                    .catch((errorArray) => {
                    // alert("Error : "+error);
                    console.log(errorArray[1]);
                    this.toast.logMessage(errorArray[0], 'danger');
                });
            });
        });
        /* SQL query to make table visite_partie_ouvrage and get back id or row inserted */
        /* return new Promise((resolve, reject)=>{
          setTimeout(()=>{
            partieOuvrage.id_visite_partie_ouvrage = VisitPartieOuvrage.count++;
            resolve([partieOuvrage, equipementData]); // Can't pass two argument to the resolve callback
          },500);
        }) */
    }
    constructVisitEquipement(equipement) {
        return new Promise((resolve, reject) => {
            this.constructQueryMultiple(equipement)
                .then((data) => {
                let queryColumn = data[0];
                let queryValues = data[1];
                //alert('To insert \ncolumns : '+queryColumn+' ; values : '+queryValues);
                this.sqlite.insertRow({ table: equipement.tableName, column: queryColumn, value: queryValues })
                    .then((response) => {
                    console.log(response);
                    if (response.hasOwnProperty('rowsAffected')) {
                        // alert("Insert successful ! Res -> "+JSON.stringify(response["insertId"]));
                        this.toast.logMessage('Insertion effectuée avec succès !', 'primary');
                        equipement.id_visite_equipement = response.insertId;
                        resolve(equipement);
                    }
                    else {
                        throw ['Error occured while inserting in database', ' Code : ' + response.code + ' , ' + response.message];
                    }
                })
                    .catch((errorArray) => {
                    // alert("Error : "+error);
                    console.log(errorArray[1]);
                    this.toast.logMessage(errorArray[0], 'danger');
                });
            });
        });
        /* return new Promise((resolve, reject)=>{
          setTimeout(()=>{
            equipement.id_visite_equipement = VisitEquipement.count++;
            resolve(equipement);
          },500);
        }) */
    }
    /**
     *
     * @param idOuvrage // 'Ouvrage' to make the visit on
     */
    createVisit(idOuvrage) {
        this.sqlite.insertRow();
    }
};
VisitService.ctorParameters = () => [
    { type: _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_2__["SqliteService"] },
    { type: _api_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"] },
    { type: _toast_toast_service__WEBPACK_IMPORTED_MODULE_7__["ToastService"] },
    { type: _jwt_jwt_service__WEBPACK_IMPORTED_MODULE_9__["JwtService"] },
    { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_11__["Storage"] }
];
VisitService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_2__["SqliteService"], _api_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"], _toast_toast_service__WEBPACK_IMPORTED_MODULE_7__["ToastService"], _jwt_jwt_service__WEBPACK_IMPORTED_MODULE_9__["JwtService"], _ionic_storage__WEBPACK_IMPORTED_MODULE_11__["Storage"]])
], VisitService);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.log(err));


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\bASKOU\Desktop\ashera_front\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map