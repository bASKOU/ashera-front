(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-visit-eval-visit-eval-module"],{

/***/ "./node_modules/ionic-rating/fesm5/ionic-rating.js":
/*!*********************************************************!*\
  !*** ./node_modules/ionic-rating/fesm5/ionic-rating.js ***!
  \*********************************************************/
/*! exports provided: IonRatingComponent, IonicRatingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonRatingComponent", function() { return IonRatingComponent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IonicRatingModule", function() { return IonicRatingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");





/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var IonRatingComponent = /** @class */ (function () {
    function IonRatingComponent(cd) {
        this.cd = cd;
        this.hover = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.leave = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.rateChange = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    IonRatingComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes.rate) {
            this.update(this.rate);
        }
    };
    /**
     * @private
     * @param {?} value
     * @param {?=} internalChange
     * @return {?}
     */
    IonRatingComponent.prototype.update = /**
     * @private
     * @param {?} value
     * @param {?=} internalChange
     * @return {?}
     */
    function (value, internalChange) {
        if (internalChange === void 0) { internalChange = true; }
        if (!(this.readonly || this.disabled || this.rate === value)) {
            this.rate = value;
            this.rateChange.emit(this.rate);
        }
        if (internalChange) {
            if (this.onChange) {
                this.onChange(this.rate);
            }
            if (this.onTouched) {
                this.onTouched();
            }
        }
    };
    /**
     * @param {?} rate
     * @return {?}
     */
    IonRatingComponent.prototype.onClick = /**
     * @param {?} rate
     * @return {?}
     */
    function (rate) {
        this.update(this.resettable && this.rate === rate ? 0 : rate);
    };
    /**
     * @param {?} value
     * @return {?}
     */
    IonRatingComponent.prototype.onMouseEnter = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        if (!(this.disabled || this.readonly)) {
            this.hoverRate = value;
        }
        this.hover.emit(value);
    };
    /**
     * @return {?}
     */
    IonRatingComponent.prototype.onMouseLeave = /**
     * @return {?}
     */
    function () {
        this.leave.emit(this.hoverRate);
        this.hoverRate = 0;
    };
    /**
     * @return {?}
     */
    IonRatingComponent.prototype.onBlur = /**
     * @return {?}
     */
    function () {
        if (this.onTouched) {
            this.onTouched();
        }
    };
    /**
     * @param {?} value
     * @return {?}
     */
    IonRatingComponent.prototype.writeValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.update(value, false);
        this.cd.markForCheck();
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    IonRatingComponent.prototype.registerOnChange = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onChange = fn;
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    IonRatingComponent.prototype.registerOnTouched = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this.onTouched = fn;
    };
    /**
     * @param {?} isDisabled
     * @return {?}
     */
    IonRatingComponent.prototype.setDisabledState = /**
     * @param {?} isDisabled
     * @return {?}
     */
    function (isDisabled) {
        this.disabled = isDisabled;
    };
    IonRatingComponent.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"], args: [{
                    selector: 'ion-rating',
                    template: "<ion-button *ngFor=\"let i of [1, 2, 3, 4, 5]\" type=\"button\" fill=\"clear\" [disabled]=\"disabled || readonly\"\n  (mouseenter)=\"onMouseEnter(i)\" (click)=\"onClick(i)\" [class.filled]=\"i <= hoverRate || (!hoverRate && i <= rate)\"\n  [class.readonly]=\"readonly\">\n  <ion-icon slot=\"icon-only\" name=\"star\" [size]=\"size\">\n  </ion-icon>\n</ion-button>",
                    changeDetection: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectionStrategy"].OnPush,
                    providers: [
                        {
                            provide: _angular_forms__WEBPACK_IMPORTED_MODULE_1__["NG_VALUE_ACCESSOR"],
                            useExisting: Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["forwardRef"])((/**
                             * @return {?}
                             */
                            function () { return IonRatingComponent; })),
                            multi: true
                        }
                    ],
                    styles: [":host{--star-color:gray;--star-color-filled:orange;display:inline-block}ion-button{--padding-start:0;--padding-end:0;--color:var(--star-color);--color-focused:var(--star-color);--color-activated:var(--star-color)}ion-button.filled{--color:var(--star-color-filled);--color-focused:var(--star-color-filled);--color-activated:var(--star-color-filled)}ion-button.readonly{--opacity:1}"]
                }] }
    ];
    /** @nocollapse */
    IonRatingComponent.ctorParameters = function () { return [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"] }
    ]; };
    IonRatingComponent.propDecorators = {
        rate: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        readonly: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        resettable: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        size: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"] }],
        hover: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
        leave: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
        rateChange: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"] }],
        onMouseLeave: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ['mouseleave', [],] }],
        onBlur: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"], args: ['blur', [],] }]
    };
    return IonRatingComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var IonicRatingModule = /** @class */ (function () {
    function IonicRatingModule() {
    }
    IonicRatingModule.decorators = [
        { type: _angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"], args: [{
                    imports: [_angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"], _angular_forms__WEBPACK_IMPORTED_MODULE_1__["FormsModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"]],
                    exports: [IonRatingComponent],
                    declarations: [IonRatingComponent]
                },] }
    ];
    return IonicRatingModule;
}());


//# sourceMappingURL=ionic-rating.js.map


/***/ }),

/***/ "./node_modules/ionic4-star-rating/dist/components/ionic4-star-rating-component.js":
/*!*****************************************************************************************!*\
  !*** ./node_modules/ionic4-star-rating/dist/components/ionic4-star-rating-component.js ***!
  \*****************************************************************************************/
/*! exports provided: StarRating */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StarRating", function() { return StarRating; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var HTML_TEMPLATE = "\n<div class=\"ionic4-star-rating\">\n  <button [ngStyle]=\"{'width' : fontSize, 'height' : fontSize}\" *ngFor=\"let index of iconsArray\" id=\"{{index}}\" type=\"button\" ion-button icon-only (click)=\"changeRating($event)\">\n    <ion-icon [ngStyle]=\"{'color':index < this.Math.round(this.parseFloat(rating)) ? activeColor : defaultColor, 'font-size' : fontSize }\" name=\"{{(halfStar ==='true' && (rating - index > 0) && (rating - index <= 0.5)) ? halfIcon : (index < this.Math.round(this.parseFloat(rating))) ? activeIcon : defaultIcon}}\"></ion-icon>\n  </button>\n</div>\n";
var CSS_STYLE = "\n    .ionic4-star-rating button {\n        background: none;\n        box-shadow: none;\n        -webkit-box-shadow: none;\n        padding : 0px;\n    }\n";
var StarRating = /** @class */ (function () {
    function StarRating(events) {
        this.events = events;
        this.eventInfo = (function () {
            var id = new Date().getTime();
            var topic = "star-rating:" + id + ":changed";
            return {
                topic: topic
            };
        })();
        this.ratingChanged = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
        this.readonly = "false";
        this.activeColor = '#488aff';
        this.defaultColor = '#aaaaaa';
        this.activeIcon = 'ios-star';
        this.defaultIcon = 'ios-star-outline';
        this.halfIcon = 'ios-star-half';
        this.halfStar = "false";
        this.maxRating = 5;
        this.fontSize = '28px';
        this.iconsArray = [];
        this.Math = Math;
        this.parseFloat = parseFloat;
    }
    StarRating_1 = StarRating;
    StarRating.prototype.ngOnInit = function () {
        this.rating = this.rating || 3; //default after input`s initialization
        for (var i = 0; i < this.maxRating; i++) {
            this.iconsArray.push(i);
        }
    };
    StarRating.prototype.writeValue = function (obj) {
        this.rating = obj;
    };
    StarRating.prototype.registerOnChange = function (fn) {
        this.onChange = fn;
    };
    StarRating.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    StarRating.prototype.setDisabledState = function (isDisabled) {
        this.readonly = isDisabled ? "true" : "false";
    };
    Object.defineProperty(StarRating.prototype, "rating", {
        get: function () {
            return this._rating;
        },
        set: function (val) {
            this._rating = val;
            // for form
            if (this.onChange) {
                this.onChange(val);
            }
        },
        enumerable: true,
        configurable: true
    });
    StarRating.prototype.changeRating = function (event) {
        if (this.readonly && this.readonly === "true")
            return;
        // event is different for firefox and chrome
        var id = event.target.id ? parseInt(event.target.id) : parseInt(event.target.parentElement.id);
        if (this.halfStar && this.halfStar === "true") {
            this.rating = ((this.rating - id > 0) && (this.rating - id <= 0.5)) ? id + 1 : id + .5;
        }
        else {
            this.rating = id + 1;
        }
        // subscribe this event to get the changed value in your parent compoanent 
        this.events.publish("star-rating:changed", this.rating); //common event for all instances included for backwards compatibility
        this.events.publish(this.eventInfo.topic, this.rating); //common event for all instances
        // unique event
        this.ratingChanged.emit(this.rating);
    };
    var StarRating_1;
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number),
        __metadata("design:paramtypes", [Number])
    ], StarRating.prototype, "rating", null);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Output"])(),
        __metadata("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"])
    ], StarRating.prototype, "ratingChanged", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StarRating.prototype, "readonly", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StarRating.prototype, "activeColor", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StarRating.prototype, "defaultColor", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StarRating.prototype, "activeIcon", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StarRating.prototype, "defaultIcon", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StarRating.prototype, "halfIcon", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StarRating.prototype, "halfStar", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], StarRating.prototype, "maxRating", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], StarRating.prototype, "fontSize", void 0);
    StarRating = StarRating_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'ionic4-star-rating',
            template: HTML_TEMPLATE,
            styles: [CSS_STYLE],
            providers: [
                {
                    provide: _angular_forms__WEBPACK_IMPORTED_MODULE_2__["NG_VALUE_ACCESSOR"],
                    useExisting: StarRating_1,
                    multi: true
                }
            ]
        }),
        __metadata("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_1__["Events"]])
    ], StarRating);
    return StarRating;
}());

//# sourceMappingURL=ionic4-star-rating-component.js.map

/***/ }),

/***/ "./node_modules/ionic4-star-rating/dist/index.js":
/*!*******************************************************!*\
  !*** ./node_modules/ionic4-star-rating/dist/index.js ***!
  \*******************************************************/
/*! exports provided: StarRatingModule, StarRating */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _ionic4_star_rating_module__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ionic4-star-rating.module */ "./node_modules/ionic4-star-rating/dist/ionic4-star-rating.module.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "StarRatingModule", function() { return _ionic4_star_rating_module__WEBPACK_IMPORTED_MODULE_0__["StarRatingModule"]; });

/* harmony import */ var _components_ionic4_star_rating_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/ionic4-star-rating-component */ "./node_modules/ionic4-star-rating/dist/components/ionic4-star-rating-component.js");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "StarRating", function() { return _components_ionic4_star_rating_component__WEBPACK_IMPORTED_MODULE_1__["StarRating"]; });



//# sourceMappingURL=index.js.map

/***/ }),

/***/ "./node_modules/ionic4-star-rating/dist/ionic4-star-rating.module.js":
/*!***************************************************************************!*\
  !*** ./node_modules/ionic4-star-rating/dist/ionic4-star-rating.module.js ***!
  \***************************************************************************/
/*! exports provided: StarRatingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StarRatingModule", function() { return StarRatingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _components_ionic4_star_rating_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./components/ionic4-star-rating-component */ "./node_modules/ionic4-star-rating/dist/components/ionic4-star-rating-component.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var StarRatingModule = /** @class */ (function () {
    function StarRatingModule() {
    }
    StarRatingModule_1 = StarRatingModule;
    StarRatingModule.forRoot = function () {
        return {
            ngModule: StarRatingModule_1,
        };
    };
    var StarRatingModule_1;
    StarRatingModule = StarRatingModule_1 = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [
                _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["IonicModule"],
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"]
            ],
            declarations: [
                _components_ionic4_star_rating_component__WEBPACK_IMPORTED_MODULE_1__["StarRating"]
            ],
            exports: [
                _components_ionic4_star_rating_component__WEBPACK_IMPORTED_MODULE_1__["StarRating"]
            ]
        })
    ], StarRatingModule);
    return StarRatingModule;
}());

//# sourceMappingURL=ionic4-star-rating.module.js.map

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/visit-eval/visit-eval.page.html":
/*!*********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/visit-eval/visit-eval.page.html ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n<ion-header color=\"primary\">\r\n  <app-header [customTitle]=\"partieOuvrageTitle\"></app-header>\r\n  \r\n  <ion-toolbar>\r\n    <ion-buttons slot=\"start\">\r\n        <ion-back-button defaultHref=\"visit\" text=\"Retour\" icon=\"arrow-back\"></ion-back-button>\r\n    </ion-buttons>\r\n  </ion-toolbar>\r\n\r\n  <ion-segment [(ngModel)]=\"viewSegment\">\r\n      <ion-segment-button value=\"partieOuvrage\">\r\n          <ion-label>Partie d'Ouvrage</ion-label>\r\n      </ion-segment-button> \r\n      <ion-segment-button value=\"equip\">\r\n          <ion-label>Équipements</ion-label>\r\n      </ion-segment-button>\r\n  </ion-segment>\r\n</ion-header>\r\n\r\n<ion-content [ngSwitch]=\"viewSegment\">\r\n  <!-- \r\n    PARTIE OUVRAGE CASE \r\n  -->\r\n  <ng-container *ngSwitchCase=\"'partieOuvrage'\"> \r\n    <!-- PROGRESS BAR -->\r\n    <ion-list-header class=\"ion-padding\">\r\n      <ion-label>Progression</ion-label>\r\n      <ion-label>{{progressPO.count}} / {{progressPO.total}}</ion-label>\r\n    </ion-list-header>\r\n    \r\n    <!-- \r\n      FIRST LIST, for cas (notable == true) \r\n    -->\r\n    <ion-list>\r\n      <!-- LIST-HEADER -->\r\n      <ion-item lines=\"full\">\r\n        <ion-label class=\"list-title\" lines=\"full\">Partie d'ouvrage</ion-label>\r\n      </ion-item>\r\n      \r\n      <ion-grid *ngFor=\"let notable of casNotable; let k = index\">\r\n        <ion-row style=\"border-bottom: 1px solid #ddd\">\r\n          <ion-col size=\"1\" class=\"ion-align-self-center\">\r\n              <ion-icon [color]=\"notable.status === 1 ? 'success' : 'warning'\" [name]=\" notable.status === 1 ? 'checkmark' : 'close'\"></ion-icon>\r\n          </ion-col>\r\n          <ion-col size=\"4\" class=\"ion-align-self-center\">\r\n            <ion-label class=\"ion-text-wrap\">{{getCas(notable.id_visite_cas)}}</ion-label>\r\n          </ion-col>\r\n          <ion-col size=\"6\" class=\"ion-align-self-center\">\r\n              <app-rating [maxRating]=\"(notes.length)\" [(rating)]=\"rate[notable.id_visite_cas]\" (ratingChange)=\"onRateChange($event, 'cas', k, notable.id_visite_liste_cas)\"></app-rating>\r\n          </ion-col>\r\n          <ion-col size=\"1\" class=\"ion-align-self-center ion-text-center\">\r\n            <ion-buttons>\r\n              <ion-icon class=\"ion-float-right\" color=\"primary\" size=\"large\" name=\"md-add-circle\" [routerLink]=\"'/details/' + getCas(notable.id_visite_cas) + '/' + ouvrageName + '/' + 'cas' + '/' + notable.id_visite_liste_cas + '/' + idVisit\" routerDirection=\"forward\" fill=\"solid\" expand=\"block\"></ion-icon>\r\n            </ion-buttons>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n      <!-- END :: LIST-BODY -->\r\n    </ion-list>\r\n\r\n    <!-- \r\n      SECOND LIST, for cas (nottable == false) \r\n    -->\r\n    <ion-list>\r\n      <!-- LIST-HEADER -->\r\n      <ion-item lines=\"full\">\r\n        <ion-label class=\"list-title\" lines=\"full\">Description général</ion-label>\r\n      </ion-item>\r\n      <!-- LIST-BODY -->\r\n      <!-- <ion-grid *ngFor=\"let not of listeCas; let p=index\">\r\n        <ion-row style=\"border-bottom: 1px solid #ddd\" *ngIf=\"!isNotable(not.id_visite_cas)\"> -->\r\n      <ion-grid *ngFor=\"let other of casOther; let p=index\">\r\n        <ion-row style=\"border-bottom: 1px solid #ddd\">\r\n          <ion-col size=\"1\" class=\"ion-align-self-center\">\r\n            <ion-icon [color]=\"other.status ? 'success' : 'warning'\" [name]=\"other.status ? 'checkmark' : 'close'\"></ion-icon>\r\n          </ion-col>\r\n          <ion-col size=\"7\" class=\"ion-align-self-center\">\r\n            <ion-label class=\"ion-text-wrap\">{{getCas(other.id_visite_cas)}}</ion-label>\r\n            <ion-row>\r\n              <ion-label color=\"medium\">{{ sliceText(other.commentaire) }}</ion-label>\r\n            </ion-row>\r\n          </ion-col>\r\n\r\n          <ion-col size=\"4\" class=\"ion-align-self-center\" color=\"danger\">\r\n            <ion-buttons class=\"ion-padding\">\r\n                <ion-button color=\"light\" [routerLink]=\"'/details/' + getCas(other.id_visite_cas) + '/' + ouvrageName + '/' + 'cas' + '/' + other.id_visite_liste_cas + '/' + idVisit\" routerDirection=\"forward\" fill=\"solid\" expand=\"block\">Détail</ion-button>\r\n            </ion-buttons>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n      <!-- END :: LIST-BODY -->\r\n    </ion-list>\r\n  \r\n  </ng-container> \r\n  <!-- \r\n    END :: PARTIE OUVRAGE CASE \r\n  -->\r\n\r\n  <!-- \r\n    EQUIPEMENT CASE \r\n  -->\r\n  <ng-container *ngSwitchCase=\"'equip'\">\r\n    <!-- PROGRESS BAR -->\r\n    <ion-list-header class=\"ion-padding\">\r\n      <ion-label>Progression</ion-label>\r\n      <ion-label>{{progressEquip.count}} / {{progressEquip.total}}</ion-label>\r\n    </ion-list-header>\r\n\r\n    <!-- LIST OF EQUIP -->\r\n    <ion-list>\r\n      <!-- LIST-HEADER -->\r\n      <ion-item lines=\"full\">\r\n        <ion-label class=\"list-title\" lines=\"full\">Liste des équipements</ion-label>\r\n      </ion-item>\r\n      <!-- LIST-BODY -->\r\n      <ion-grid *ngFor=\"let equip of equipement; let j = index\">\r\n        <ion-row style=\"border-bottom: 1px solid #ddd\">\r\n          <ion-col size=\"1\" class=\"ion-align-self-center\">\r\n            <ion-icon [color]=\"equip.status ? 'success' : 'warning'\" [name]=\"equip.status ? 'checkmark' : 'close'\"></ion-icon>\r\n          </ion-col>\r\n          <ion-col size=\"4\" class=\"ion-align-self-center\">\r\n            <ion-label class=\"ion-text-wrap\">{{equip.nom_equipement}}</ion-label>\r\n          </ion-col>\r\n          <ion-col size=\"4\" class=\"ion-align-self-center\" color=\"danger\">\r\n            <ion-select placeholder=\"Évaluation\" interface=\"alert\" [(ngModel)]=\"equip.id_visite_note\" [interfaceOptions]=\"createInterfaceOptions(equip.nom_equipement)\" (ionChange)=\"onRateChange($event.detail.value, 'equip', j, equip.id_visite_equipement)\">\r\n              <ion-select-option *ngFor=\"let note of notes; let i = index\" [value]=\"note.id_visite_note\">{{note.note}}</ion-select-option>\r\n            </ion-select>\r\n          </ion-col>\r\n          <ion-col size=\"3\" class=\"ion-align-self-center ion-text-center\">\r\n            <ion-buttons>\r\n              <ion-button class=\"ion-margin-left\" color=\"light\" [routerLink]=\"'/details/' + equip.nom_equipement + '/' + ouvrageName + '/' + 'equip' + '/' + equip.id_visite_equipement + '/' + idVisit\" routerDirection=\"forward\" fill=\"solid\" expand=\"block\">Détail</ion-button>\r\n            </ion-buttons>\r\n          </ion-col>\r\n        </ion-row>\r\n      </ion-grid>\r\n      <!-- END :: LIST-BODY -->\r\n    </ion-list>\r\n\r\n  </ng-container>\r\n  <!-- \r\n    END :: EQUIPEMENT CASE \r\n  -->\r\n</ion-content>\r\n"

/***/ }),

/***/ "./src/app/pages/visit-eval/visit-eval.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/visit-eval/visit-eval.module.ts ***!
  \*******************************************************/
/*! exports provided: VisitEvalPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitEvalPageModule", function() { return VisitEvalPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _visit_eval_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./visit-eval.page */ "./src/app/pages/visit-eval/visit-eval.page.ts");
/* harmony import */ var src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var ionic4_star_rating__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ionic4-star-rating */ "./node_modules/ionic4-star-rating/dist/index.js");
/* harmony import */ var ionic_rating__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ionic-rating */ "./node_modules/ionic-rating/fesm5/ionic-rating.js");








//import { IonicRatingModule } from 'ionic4-rating';


var routes = [
    {
        path: '',
        component: _visit_eval_page__WEBPACK_IMPORTED_MODULE_6__["VisitEvalPage"]
    }
];
var VisitEvalPageModule = /** @class */ (function () {
    function VisitEvalPageModule() {
    }
    VisitEvalPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                src_app_components_components_module__WEBPACK_IMPORTED_MODULE_7__["ComponentsModule"],
                ionic_rating__WEBPACK_IMPORTED_MODULE_9__["IonicRatingModule"],
                ionic4_star_rating__WEBPACK_IMPORTED_MODULE_8__["StarRatingModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_visit_eval_page__WEBPACK_IMPORTED_MODULE_6__["VisitEvalPage"]]
        })
    ], VisitEvalPageModule);
    return VisitEvalPageModule;
}());



/***/ }),

/***/ "./src/app/pages/visit-eval/visit-eval.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/visit-eval/visit-eval.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".light-text {\n  --font-weight: 100;\n}\n\n.list-title {\n  font-weight: bold;\n  font-size: 1.1em;\n}\n\n.detail-button {\n  --box-shadow: none;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvdmlzaXQtZXZhbC9DOlxcVXNlcnNcXGJBU0tPVVxcRGVza3RvcFxcYXNoZXJhX2Zyb250L3NyY1xcYXBwXFxwYWdlc1xcdmlzaXQtZXZhbFxcdmlzaXQtZXZhbC5wYWdlLnNjc3MiLCJzcmMvYXBwL3BhZ2VzL3Zpc2l0LWV2YWwvdmlzaXQtZXZhbC5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxrQkFBQTtBQ0NKOztBRENBO0VBQ0ksaUJBQUE7RUFDQSxnQkFBQTtBQ0VKOztBRENBO0VBQ0ksa0JBQUE7QUNFSiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3Zpc2l0LWV2YWwvdmlzaXQtZXZhbC5wYWdlLnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIubGlnaHQtdGV4dHtcclxuICAgIC0tZm9udC13ZWlnaHQ6IDEwMDtcclxufVxyXG4ubGlzdC10aXRsZXtcclxuICAgIGZvbnQtd2VpZ2h0OiBib2xkO1xyXG4gICAgZm9udC1zaXplOiAxLjFlbTtcclxufVxyXG5cclxuLmRldGFpbC1idXR0b257XHJcbiAgICAtLWJveC1zaGFkb3c6IG5vbmU7XHJcbn0iLCIubGlnaHQtdGV4dCB7XG4gIC0tZm9udC13ZWlnaHQ6IDEwMDtcbn1cblxuLmxpc3QtdGl0bGUge1xuICBmb250LXdlaWdodDogYm9sZDtcbiAgZm9udC1zaXplOiAxLjFlbTtcbn1cblxuLmRldGFpbC1idXR0b24ge1xuICAtLWJveC1zaGFkb3c6IG5vbmU7XG59Il19 */"

/***/ }),

/***/ "./src/app/pages/visit-eval/visit-eval.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/visit-eval/visit-eval.page.ts ***!
  \*****************************************************/
/*! exports provided: VisitEvalPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitEvalPage", function() { return VisitEvalPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_loading_loading_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/loading/loading.service */ "./src/app/services/loading/loading.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_components_modals_detail_equip_po_detail_equip_po_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/components/modals/detail-equip-po/detail-equip-po.component */ "./src/app/components/modals/detail-equip-po/detail-equip-po.component.ts");
/* harmony import */ var src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/visit/visit.service */ "./src/app/services/visit/visit.service.ts");







var VisitEvalPage = /** @class */ (function () {
    function VisitEvalPage(router, loading, modal, visitService) {
        this.router = router;
        this.loading = loading;
        this.modal = modal;
        this.visitService = visitService;
        /* Initializing variables */
        this.viewSegment = 'partieOuvrage';
        console.log(this.router.snapshot.paramMap.get('partieOuvrageId'));
        console.log('ouvrageName : ', this.router.snapshot.paramMap.get('ouvrageName'));
        console.log(this.router.snapshot.paramMap.get('partieOuvrageTitle'));
        this.searchToggled = false;
        this.casNotable = [];
        this.casOther = [];
        this.progressPO = {
            count: 0,
            total: 0
        };
        this.progressEquip = {
            count: 0,
            total: 0
        };
        this.rate = [];
        this.partieOuvrage = this.router.snapshot.paramMap.get('partieOuvrageId');
        this.partieOuvrageTitle = this.router.snapshot.paramMap.get('partieOuvrageTitle');
        this.ouvrageName = this.router.snapshot.paramMap.get('ouvrageName');
        this.idVisit = this.router.snapshot.paramMap.get('idVisit');
    }
    VisitEvalPage.prototype.ngOnInit = function () {
    };
    VisitEvalPage.prototype.ionViewWillEnter = function () {
        /* Fetching data with the help of our services */
        this.initEvaluation(this.partieOuvrage);
    };
    VisitEvalPage.prototype.initEvaluation = function (idVisitePo) {
        var _this = this;
        var promises = [];
        promises.push(this.visitService.getPOData(idVisitePo));
        promises.push(this.visitService.getUtilitaryTables());
        Promise.all(promises)
            .then(function (res) {
            _this.listeCas = res[0]['listeCas'] || [];
            _this.equipement = res[0]['equipement'] || [];
            _this.cas = res[1]['cas'] || [];
            _this.notes = res[1]['note'] || [];
            return new Promise(function (resolve, reject) {
                _this.casNotable = [];
                _this.casOther = [];
                _this.listeCas.forEach(function (value) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                    var column, cas;
                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                        switch (_a.label) {
                            case 0:
                                column = 'id_visite_cas';
                                return [4 /*yield*/, this.isInArray(this.cas, value[column], column)];
                            case 1:
                                cas = _a.sent();
                                if (cas['notable'] === 1) {
                                    this.casNotable.push(value);
                                    this.rate[value.id_visite_cas] = value.id_visite_note || false;
                                }
                                else if (cas['notable'] === 0) {
                                    this.casOther.push(value);
                                }
                                return [2 /*return*/];
                        }
                    });
                }); });
                _this.updateProgress();
            });
        });
    };
    VisitEvalPage.prototype.isInArray = function (array, idToFind, propName) {
        return new Promise(function (resolve, reject) {
            for (var i = 0; i < array.length; i++) {
                if (array[i][propName] === idToFind) {
                    resolve(array[i]);
                }
            }
            reject('No item found !');
        });
    };
    VisitEvalPage.prototype.getCas = function (idCas) {
        /**
         * Trying to optimise the execution time (length - 1) * length loop less
         */
        if (this.cas[(idCas - 1)]['id_visite_cas'] === idCas) {
            return this.cas[(idCas - 1)].cas;
        }
        else {
            // tslint:disable-next-line:prefer-for-of
            for (var i = 0; i < this.cas.length; i++) {
                if (idCas === this.cas[i].id_visite_cas) {
                    return this.cas[i].cas;
                }
            }
            return false;
        }
    };
    VisitEvalPage.prototype.updateProgress = function (whichOne) {
        var _this = this;
        if (whichOne === void 0) { whichOne = ''; }
        if (whichOne === 'equipement') {
            this.progressEquip.count = 0;
            this.progressEquip.total = this.equipement.length;
            this.equipement.forEach(function (value) {
                if (value.status) {
                    _this.progressEquip.count++;
                }
            });
        }
        else if (whichOne === 'liste_cas') {
            this.progressPO.count = 0;
            this.progressPO.total = this.listeCas.length;
            this.listeCas.forEach(function (value) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                    if (value.status) {
                        this.progressPO.count++;
                    }
                    return [2 /*return*/];
                });
            }); });
        }
        else {
            this.updateProgress('liste_cas');
            this.updateProgress('equipement');
        }
    };
    VisitEvalPage.prototype.sliceText = function (arg, maxSize) {
        if (maxSize === void 0) { maxSize = 50; }
        if (arg) {
            return arg.length > maxSize + 1 ? arg.slice(0, maxSize) + '..' : arg;
        }
        else {
            return '';
        }
    };
    VisitEvalPage.prototype.onRateChange = function (event, type, id, idType) {
        var _this = this;
        var table = type === 'equip' ? 'visite_equipement' : type === 'cas' ? 'visite_liste_cas' : '';
        this.visitService.updateVisiteData(event, table, 'id_visite_note', 'id_' + table, idType)
            .then(function (res) {
            // Need to add extra condition to avoid reupdate where status = 1 already !
            return _this.visitService.updateVisiteData(1, table, 'status', 'id_' + table, idType);
        })
            .then(function (res) {
            if (res.hasOwnProperty('rowsAffected')) {
                if (res.rowsAffected > 0) {
                    var prop = type === 'equip' ? 'equipement' : type === 'cas' ? 'casNotable' : '';
                    if (_this[prop][id]['id_' + table] === idType) {
                        _this[prop][id]['status'] = 1;
                    }
                    else {
                        // tslint:disable-next-line:prefer-for-of
                        for (var i = 0; i < _this[prop].length; i++) {
                            if (_this[prop][i]['id_' + table] === idType) {
                                _this[prop][i]['status'] = 1;
                            }
                        }
                    }
                    _this.updateProgress(prop);
                }
            }
        });
    };
    /**
     * Method used for creating a custom interface of the select option
     * (as defined in the component 'interfaceOptions' attribute for 'interface' attribute)
     */
    VisitEvalPage.prototype.createInterfaceOptions = function (headerCustom, subHeaderCustom, messageCustom) {
        if (subHeaderCustom === void 0) { subHeaderCustom = ''; }
        if (messageCustom === void 0) { messageCustom = ''; }
        return {
            header: headerCustom,
            subHeader: subHeaderCustom,
            message: messageCustom,
            translucent: true
        };
    };
    VisitEvalPage.prototype.detailModal = function (data) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var modal;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.modal.create({
                            component: src_app_components_modals_detail_equip_po_detail_equip_po_component__WEBPACK_IMPORTED_MODULE_5__["DetailEquipPoComponent"],
                            componentProps: {
                                name: data.name,
                                id: data.id,
                                type: data.type
                            }
                        })];
                    case 1:
                        modal = _a.sent();
                        return [4 /*yield*/, modal.present()];
                    case 2: return [2 /*return*/, _a.sent()];
                }
            });
        });
    };
    VisitEvalPage.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
        { type: src_app_services_loading_loading_service__WEBPACK_IMPORTED_MODULE_3__["LoadingService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"] },
        { type: src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_6__["VisitService"] }
    ]; };
    VisitEvalPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-visit-eval',
            template: __webpack_require__(/*! raw-loader!./visit-eval.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/visit-eval/visit-eval.page.html"),
            styles: [__webpack_require__(/*! ./visit-eval.page.scss */ "./src/app/pages/visit-eval/visit-eval.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"],
            src_app_services_loading_loading_service__WEBPACK_IMPORTED_MODULE_3__["LoadingService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ModalController"],
            src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_6__["VisitService"]])
    ], VisitEvalPage);
    return VisitEvalPage;
}());



/***/ })

}]);
//# sourceMappingURL=pages-visit-eval-visit-eval-module-es5.js.map