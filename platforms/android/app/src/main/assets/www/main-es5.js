(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./pages/details/details.module": [
		"./src/app/pages/details/details.module.ts",
		"pages-details-details-module"
	],
	"./pages/history-details/history-details.module": [
		"./src/app/pages/history-details/history-details.module.ts",
		"pages-history-details-history-details-module"
	],
	"./pages/history/history.module": [
		"./src/app/pages/history/history.module.ts",
		"pages-history-history-module"
	],
	"./pages/login/login.module": [
		"./src/app/pages/login/login.module.ts",
		"pages-login-login-module"
	],
	"./pages/preparation/preparation.module": [
		"./src/app/pages/preparation/preparation.module.ts",
		"pages-preparation-preparation-module"
	],
	"./pages/profil/profil.module": [
		"./src/app/pages/profil/profil.module.ts",
		"pages-profil-profil-module"
	],
	"./pages/send/send.module": [
		"./src/app/pages/send/send.module.ts",
		"pages-send-send-module"
	],
	"./pages/visit-eval/visit-eval.module": [
		"./src/app/pages/visit-eval/visit-eval.module.ts",
		"pages-visit-eval-visit-eval-module"
	],
	"./pages/visit-resume/visit-resume.module": [
		"./src/app/pages/visit-resume/visit-resume.module.ts",
		"pages-visit-resume-visit-resume-module"
	],
	"./pages/visit/visit.module": [
		"./src/app/pages/visit/visit.module.ts",
		"pages-visit-visit-module"
	],
	"src/app/pages/history/history.module": [
		"./src/app/pages/history/history.module.ts",
		"pages-history-history-module"
	],
	"src/app/pages/preparation/preparation.module": [
		"./src/app/pages/preparation/preparation.module.ts",
		"pages-preparation-preparation-module"
	],
	"src/app/pages/send/send.module": [
		"./src/app/pages/send/send.module.ts",
		"pages-send-send-module"
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./$$_lazy_route_resource lazy recursive";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/@ionic/core/dist/esm-es5 lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$":
/*!*********************************************************************************************************************************************!*\
  !*** ./node_modules/@ionic/core/dist/esm-es5 lazy ^\.\/.*\.entry\.js$ include: \.entry\.js$ exclude: \.system\.entry\.js$ namespace object ***!
  \*********************************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./ion-action-sheet-controller_8.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-action-sheet-controller_8.entry.js",
		"common",
		2
	],
	"./ion-action-sheet-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-action-sheet-ios.entry.js",
		"common",
		3
	],
	"./ion-action-sheet-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-action-sheet-md.entry.js",
		"common",
		4
	],
	"./ion-alert-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-alert-ios.entry.js",
		"common",
		5
	],
	"./ion-alert-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-alert-md.entry.js",
		"common",
		6
	],
	"./ion-app_8-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-app_8-ios.entry.js",
		0,
		"common",
		7
	],
	"./ion-app_8-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-app_8-md.entry.js",
		0,
		"common",
		8
	],
	"./ion-avatar_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-avatar_3-ios.entry.js",
		"common",
		9
	],
	"./ion-avatar_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-avatar_3-md.entry.js",
		"common",
		10
	],
	"./ion-back-button-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-back-button-ios.entry.js",
		"common",
		11
	],
	"./ion-back-button-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-back-button-md.entry.js",
		"common",
		12
	],
	"./ion-backdrop-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-backdrop-ios.entry.js",
		13
	],
	"./ion-backdrop-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-backdrop-md.entry.js",
		14
	],
	"./ion-button_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-button_2-ios.entry.js",
		"common",
		15
	],
	"./ion-button_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-button_2-md.entry.js",
		"common",
		16
	],
	"./ion-card_5-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-card_5-ios.entry.js",
		"common",
		17
	],
	"./ion-card_5-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-card_5-md.entry.js",
		"common",
		18
	],
	"./ion-checkbox-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-checkbox-ios.entry.js",
		"common",
		19
	],
	"./ion-checkbox-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-checkbox-md.entry.js",
		"common",
		20
	],
	"./ion-chip-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-chip-ios.entry.js",
		"common",
		21
	],
	"./ion-chip-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-chip-md.entry.js",
		"common",
		22
	],
	"./ion-col_3.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-col_3.entry.js",
		23
	],
	"./ion-datetime_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-datetime_3-ios.entry.js",
		"common",
		24
	],
	"./ion-datetime_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-datetime_3-md.entry.js",
		"common",
		25
	],
	"./ion-fab_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-fab_3-ios.entry.js",
		"common",
		26
	],
	"./ion-fab_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-fab_3-md.entry.js",
		"common",
		27
	],
	"./ion-img.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-img.entry.js",
		28
	],
	"./ion-infinite-scroll_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-infinite-scroll_2-ios.entry.js",
		"common",
		29
	],
	"./ion-infinite-scroll_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-infinite-scroll_2-md.entry.js",
		"common",
		30
	],
	"./ion-input-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-input-ios.entry.js",
		"common",
		31
	],
	"./ion-input-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-input-md.entry.js",
		"common",
		32
	],
	"./ion-item-option_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-item-option_3-ios.entry.js",
		"common",
		33
	],
	"./ion-item-option_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-item-option_3-md.entry.js",
		"common",
		34
	],
	"./ion-item_8-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-item_8-ios.entry.js",
		"common",
		35
	],
	"./ion-item_8-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-item_8-md.entry.js",
		"common",
		36
	],
	"./ion-loading-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-loading-ios.entry.js",
		"common",
		37
	],
	"./ion-loading-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-loading-md.entry.js",
		"common",
		38
	],
	"./ion-menu_4-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-menu_4-ios.entry.js",
		"common",
		39
	],
	"./ion-menu_4-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-menu_4-md.entry.js",
		"common",
		40
	],
	"./ion-modal-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-modal-ios.entry.js",
		0,
		"common",
		41
	],
	"./ion-modal-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-modal-md.entry.js",
		0,
		"common",
		42
	],
	"./ion-nav_5.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-nav_5.entry.js",
		0,
		"common",
		43
	],
	"./ion-popover-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-popover-ios.entry.js",
		0,
		"common",
		44
	],
	"./ion-popover-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-popover-md.entry.js",
		0,
		"common",
		45
	],
	"./ion-progress-bar-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-progress-bar-ios.entry.js",
		"common",
		46
	],
	"./ion-progress-bar-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-progress-bar-md.entry.js",
		"common",
		47
	],
	"./ion-radio_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-radio_2-ios.entry.js",
		"common",
		48
	],
	"./ion-radio_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-radio_2-md.entry.js",
		"common",
		49
	],
	"./ion-range-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-range-ios.entry.js",
		"common",
		50
	],
	"./ion-range-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-range-md.entry.js",
		"common",
		51
	],
	"./ion-refresher_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-refresher_2-ios.entry.js",
		"common",
		52
	],
	"./ion-refresher_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-refresher_2-md.entry.js",
		"common",
		53
	],
	"./ion-reorder_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-reorder_2-ios.entry.js",
		"common",
		54
	],
	"./ion-reorder_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-reorder_2-md.entry.js",
		"common",
		55
	],
	"./ion-ripple-effect.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-ripple-effect.entry.js",
		56
	],
	"./ion-route_4.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-route_4.entry.js",
		"common",
		57
	],
	"./ion-searchbar-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-searchbar-ios.entry.js",
		"common",
		58
	],
	"./ion-searchbar-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-searchbar-md.entry.js",
		"common",
		59
	],
	"./ion-segment_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-segment_2-ios.entry.js",
		"common",
		60
	],
	"./ion-segment_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-segment_2-md.entry.js",
		"common",
		61
	],
	"./ion-select_3-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-select_3-ios.entry.js",
		"common",
		62
	],
	"./ion-select_3-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-select_3-md.entry.js",
		"common",
		63
	],
	"./ion-slide_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-slide_2-ios.entry.js",
		64
	],
	"./ion-slide_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-slide_2-md.entry.js",
		65
	],
	"./ion-spinner.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-spinner.entry.js",
		"common",
		66
	],
	"./ion-split-pane-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-split-pane-ios.entry.js",
		67
	],
	"./ion-split-pane-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-split-pane-md.entry.js",
		68
	],
	"./ion-tab-bar_2-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-tab-bar_2-ios.entry.js",
		"common",
		69
	],
	"./ion-tab-bar_2-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-tab-bar_2-md.entry.js",
		"common",
		70
	],
	"./ion-tab_2.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-tab_2.entry.js",
		1
	],
	"./ion-text.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-text.entry.js",
		"common",
		71
	],
	"./ion-textarea-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-textarea-ios.entry.js",
		"common",
		72
	],
	"./ion-textarea-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-textarea-md.entry.js",
		"common",
		73
	],
	"./ion-toast-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-toast-ios.entry.js",
		"common",
		74
	],
	"./ion-toast-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-toast-md.entry.js",
		"common",
		75
	],
	"./ion-toggle-ios.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-toggle-ios.entry.js",
		"common",
		76
	],
	"./ion-toggle-md.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-toggle-md.entry.js",
		"common",
		77
	],
	"./ion-virtual-scroll.entry.js": [
		"./node_modules/@ionic/core/dist/esm-es5/ion-virtual-scroll.entry.js",
		78
	]
};
function webpackAsyncContext(req) {
	if(!__webpack_require__.o(map, req)) {
		return Promise.resolve().then(function() {
			var e = new Error("Cannot find module '" + req + "'");
			e.code = 'MODULE_NOT_FOUND';
			throw e;
		});
	}

	var ids = map[req], id = ids[0];
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(id);
	});
}
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = "./node_modules/@ionic/core/dist/esm-es5 lazy recursive ^\\.\\/.*\\.entry\\.js$ include: \\.entry\\.js$ exclude: \\.system\\.entry\\.js$";
module.exports = webpackAsyncContext;

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-app>\r\n    <ion-split-pane contentId=\"main-content\" >\r\n      <ion-menu contentId=\"main-content\" type=\"overlay\" [swipeGesture]=\"false\">\r\n        <ion-header>\r\n          <ion-toolbar>\r\n            <ion-title class=\"ion-margin\">Ashera</ion-title>\r\n            <ion-title class=\"ion-margin\" size=\"small\">Menu</ion-title>\r\n          </ion-toolbar>\r\n        </ion-header>\r\n        <ion-content >\r\n          <ion-list>\r\n            <ion-menu-toggle auto-hide=\"false\">\r\n              <ng-template ngFor let-p [ngForOf]=\"appPages\">\r\n                <ion-item [routerDirection]=\"'root'\" [routerLink]=\"[p.url]\">\r\n                    <ion-icon slot=\"start\" [name]=\"p.icon\" color=\"primary\"></ion-icon>\r\n                    <ion-label>\r\n                      {{p.title}}\r\n                    </ion-label>\r\n                  </ion-item>\r\n                </ng-template>\r\n              <ion-button [routerDirection]=\"'root'\" (click)=\"logoutJwt()\" color=\"light\" expand=\"block\" class=\"ion-margin\">\r\n                <ion-icon slot=\"start\" name=\"log-out\"></ion-icon>\r\n                Se déconnecter\r\n              </ion-button>\r\n            </ion-menu-toggle>\r\n          </ion-list>\r\n        </ion-content>\r\n      </ion-menu>\r\n      <ion-router-outlet id=\"main-content\"></ion-router-outlet>\r\n    </ion-split-pane>\r\n  </ion-app>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/header/header.component.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/header/header.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!-- <ion-header >\r\n  <ion-toolbar color=\"primary\" class=\"ion-text-center\">\r\n    <ion-buttons slot=\"start\">\r\n        <ion-menu-button></ion-menu-button>\r\n    </ion-buttons>\r\n    <ion-title> {{customTitle}} </ion-title>\r\n    <ion-button slot=\"end\">\r\n        <ion-icon slot=\"icon-only\" name=\"{{customButton.icon}}\" (click)=\"customButton.function\"></ion-icon>\r\n    </ion-button>\r\n  </ion-toolbar>\r\n</ion-header> -->\r\n\r\n<ion-toolbar color=\"primary\" class=\"ion-text-center\">\r\n  <ion-buttons slot=\"start\">\r\n    <ion-menu-button></ion-menu-button>\r\n  </ion-buttons>\r\n  <ion-title>{{customTitle}}</ion-title>\r\n  <ion-button *ngIf=\"customButton\" slot=\"end\" (click)=\"this[customButton.function]($event)\">\r\n    <ion-icon slot=\"icon-only\" name=\"{{customButton.icon}}\" ></ion-icon>\r\n  </ion-button>\r\n</ion-toolbar>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/modals/detail-equip-po/detail-equip-po.component.html":
/*!************************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/modals/detail-equip-po/detail-equip-po.component.html ***!
  \************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header no-shadow>\r\n  <ion-toolbar color=\"secondary\" text-center>\r\n      <ion-title>{{name}}</ion-title>\r\n  </ion-toolbar>\r\n</ion-header>\r\n<ion-content>\r\n  <ion-list padding>\r\n      <ion-item margin color=\"light\">\r\n          <ion-label position=\"floating\">Nom / Prénom</ion-label>\r\n          <ion-input type=\"text\"></ion-input>\r\n          <ion-icon name=\"person\" slot=\"end\" align-self-center></ion-icon>\r\n      </ion-item>\r\n      <ion-item class=\"ion-margin\" color=\"light\">\r\n          <ion-label position=\"stacked\">Date</ion-label>\r\n          <ion-input type=\"date\"></ion-input>\r\n          <ion-icon name=\"calendar\" slot=\"end\" align-self-center></ion-icon>\r\n      </ion-item>\r\n      <ion-item class=\"ion-margin\" lines=\"none\">\r\n          <ion-label position=\"stacked\">Commentaire</ion-label>\r\n          <ion-textarea rows=\"6\" cols=\"20\" placeholder=\"Une note sur cette visite ?\" style=\"border: 1px solid #d7d8da; border-radius:15px; padding:0.5em;margin-top: 1.2em\"></ion-textarea>\r\n      </ion-item>\r\n      <!-- <ion-item>\r\n          <ion-label position=\"floating\">Password</ion-label>\r\n          <ion-input required></ion-input>\r\n          <ion-icon name='eye' item-right></ion-icon>\r\n      </ion-item> -->\r\n  </ion-list>\r\n</ion-content>\r\n<ion-footer no-shadow>\r\n  <ion-toolbar position=\"bottom\">\r\n      <ion-buttons slot=\"end\" padding>\r\n         <ion-button (click)=\"closeModal()\">Fermer</ion-button>\r\n         <ion-button color=\"secondary\" (click)=\"confirmUpdate($event)\">Valider</ion-button>\r\n      </ion-buttons>\r\n  </ion-toolbar>\r\n</ion-footer>\r\n\r\n\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/modals/initvisit/initvisit.component.html":
/*!************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/modals/initvisit/initvisit.component.html ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = " <ion-header no-shadow>\r\n     <ion-toolbar color=\"primary\">\r\n         <ion-title>Paramètrage de la visite</ion-title>\r\n     </ion-toolbar>\r\n </ion-header>\r\n \r\n    <ion-content>\r\n        <form #form=\"ngForm\">\r\n        <ion-list class=\"ion-padding\">\r\n            <ion-list-header class=\"ion-text-center\">\r\n                <ion-title>{{nameOuvrage}}</ion-title>\r\n            </ion-list-header>\r\n            <ion-item margin color=\"light\">\r\n                <ion-label position=\"floating\">Nom / Prénom</ion-label>\r\n                <ion-input name=\"nameAgent\" type=\"text\" [(ngModel)]=\"visitForm.nameAgent\" required></ion-input>\r\n                <ion-icon name=\"person\" slot=\"end\" align-self-center></ion-icon>\r\n            </ion-item>\r\n            <ion-item class=\"ion-margin\" color=\"light\">\r\n                <ion-label position=\"stacked\">Date</ion-label>\r\n                <ion-input name=\"date\" type=\"date\" [(ngModel)]=\"visitForm.date\" required></ion-input>\r\n                <ion-icon name=\"calendar\" slot=\"end\" align-self-center></ion-icon>\r\n            </ion-item>\r\n            <ion-item class=\"ion-margin\" lines=\"none\">\r\n                <ion-label position=\"stacked\">Commentaire</ion-label>\r\n                <ion-textarea [(ngModel)]=\"visitForm.comment\" name=\"comment\" rows=\"6\" cols=\"20\" placeholder=\"Une note sur cette visite ?\" style=\"border: 1px solid #d7d8da; border-radius:15px; padding:0.5em;margin-top: 1.2em\"></ion-textarea>\r\n            </ion-item>\r\n        </ion-list>\r\n    </form>\r\n    </ion-content>\r\n    <ion-footer no-shadow>\r\n        <ion-toolbar position=\"bottom\">\r\n            <ion-buttons slot=\"end\" class=\"ion-padding\">\r\n                <ion-button (click)=\"closeModal()\">Fermer</ion-button>\r\n                <ion-button color=\"secondary\" (click)=\"initVisit(form)\" [disabled]=\"form.invalid\">Valider</ion-button>\r\n            </ion-buttons>\r\n        </ion-toolbar>\r\n    </ion-footer>\r\n\r\n \r\n \r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/modals/photo-coment/photo-comment.component.html":
/*!*******************************************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/modals/photo-coment/photo-comment.component.html ***!
  \*******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-toolbar>\r\n  <ion-title>{{picture.name}}</ion-title>\r\n</ion-toolbar>\r\n  <ion-content>\r\n    <ion-row>\r\n      <ion-col size=\"12\">\r\n        <ion-row style=\"margin-bottom: -45px\">\r\n          <ion-col size=\"6\">\r\n            <ion-icon class=\"ion-float-left\" name=\"trash\" style=\"--z-index: 99;\" size=\"large\" color=\"warning\" (click)=\"photoService.deleteImage(picture, position, storageKey, id, type)\"></ion-icon>\r\n          </ion-col>\r\n          <ion-col size=\"6\">\r\n            <ion-icon *ngIf=\"picture.id === photoService.favorite\" class=\"ion-float-end\" name=\"heart\" style=\"--z-index: 99;\" size=\"large\" color=\"primary\" (click)=\"addFavorite('visite_photo', picture.id)\"></ion-icon>\r\n            <ion-icon *ngIf=\"picture.id !== photoService.favorite\" class=\"ion-float-end\" name=\"heart\" style=\"--z-index: 99;\" size=\"large\" color=\"medium\" (click)=\"addFavorite('visite_photo', picture.id)\"></ion-icon>\r\n          </ion-col>\r\n        </ion-row>\r\n        <img [src]=\"picture.path\"  alt=\"\"/>\r\n      </ion-col>\r\n    </ion-row>\r\n  </ion-content>\r\n  <ion-content class=\"ion-padding\" style=\"max-height: 30%\">\r\n    <form #form>\r\n      <ion-label position=\"floating\">Commentaire</ion-label>\r\n      <ion-textarea placeholder=\"Entrez le commentaire...\" *ngIf=\"comment\" name=\"com\" rows=\"4\" cols=\"20\" style=\"border: 1px solid #d7d8da; border-radius:15px; padding:0.5em;margin-top: 1.2em\" value=\"{{comment}}\"></ion-textarea>\r\n      <ion-textarea placeholder=\"Entrez le commentaire...\" *ngIf=\"!comment\" name=\"com\" rows=\"4\" cols=\"20\" style=\"border: 1px solid #d7d8da; border-radius:15px; padding:0.5em;margin-top: 1.2em\"></ion-textarea>\r\n    </form>\r\n  </ion-content>\r\n  <ion-footer no-shadow>\r\n    <ion-toolbar position=\"bottom\">\r\n      <ion-buttons slot=\"end\" class=\"ion-padding\">\r\n        <ion-button color=\"medium\" (click)=\"closeModal()\">Fermer</ion-button>\r\n        <ion-button color=\"primary\" type=\"submit\" (click)=\"insertComment(form)\">Valider</ion-button>\r\n      </ion-buttons>\r\n    </ion-toolbar>\r\n  </ion-footer>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/components/rating/rating.component.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/components/rating/rating.component.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-row>\r\n  <ng-container *ngFor=\"let num of ratingMax(maxRating); let i = index;\">\r\n    <ion-col class=\"ion-text-center ion-no-padding\">\r\n        <ion-icon\r\n        name=\"{{activeIcon}}\"\r\n        (click)=\"rate(num)\"\r\n        [ngStyle] = \"{'color' : colorRating(num), 'font-size': fontSize ? fontSize : '1.4em'}\"\r\n        class=\"ion-no-margin\"\r\n      ></ion-icon>\r\n    </ion-col>\r\n  </ng-container>\r\n</ion-row>\r\n"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    { path: 'home', loadChildren: function () { return __webpack_require__.e(/*! import() | pages-home-home-module */ "pages-home-home-module").then(__webpack_require__.bind(null, /*! ./pages/home/home.module */ "./src/app/pages/home/home.module.ts")).then(function (m) { return m.HomePageModule; }); } },
    { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule' },
    { path: 'details/:title/:storage/:type/:id/:idVisit', loadChildren: './pages/details/details.module#DetailsPageModule' },
    { path: 'profil', loadChildren: './pages/profil/profil.module#ProfilPageModule' },
    { path: 'history-details', loadChildren: './pages/history-details/history-details.module#HistoryDetailsPageModule' },
    { path: 'preparation', loadChildren: './pages/preparation/preparation.module#PreparationPageModule' },
    { path: 'send', loadChildren: './pages/send/send.module#SendPageModule' },
    { path: 'visit', loadChildren: './pages/visit/visit.module#VisitPageModule' },
    { path: 'visit-resume/:resumeId/:resumeTitle', loadChildren: './pages/visit-resume/visit-resume.module#VisitResumePageModule' },
    { path: 'visit-eval/:partieOuvrageId/:partieOuvrageTitle/:ouvrageName',
        loadChildren: './pages/visit-eval/visit-eval.module#VisitEvalPageModule' },
    { path: 'visit-eval/:partieOuvrageId/:partieOuvrageTitle/:ouvrageName/:idVisit',
        loadChildren: './pages/visit-eval/visit-eval.module#VisitEvalPageModule' },
    { path: 'history', loadChildren: './pages/history/history.module#HistoryPageModule' },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes, { preloadingStrategy: _angular_router__WEBPACK_IMPORTED_MODULE_2__["PreloadAllModules"] })
            ],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.scss":
/*!************************************!*\
  !*** ./src/app/app.component.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./services/sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");
/* harmony import */ var _services_jwt_jwt_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./services/jwt/jwt.service */ "./src/app/services/jwt/jwt.service.ts");
/* harmony import */ var _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./services/toast/toast.service */ "./src/app/services/toast/toast.service.ts");






/**
 * Services Import
 */



var AppComponent = /** @class */ (function () {
    function AppComponent(platform, splashScreen, statusBar, router, sql, jwt, toast) {
        var _this = this;
        this.platform = platform;
        this.splashScreen = splashScreen;
        this.statusBar = statusBar;
        this.router = router;
        this.sql = sql;
        this.jwt = jwt;
        this.toast = toast;
        // Props of side menu content
        this.test = 'autres';
        this.storage = 'test';
        this.appPages = [
            {
                title: 'Profil',
                url: '/profil',
                icon: 'contact'
            },
            {
                title: 'Visite',
                url: '/visit',
                icon: 'briefcase'
            }
        ];
        this.initializeApp();
        this.allowStatusBar();
        this.initDB()
            .then(function () {
            _this.jwt.getToken()
                .then(function (response) {
                if (response === undefined) {
                    _this.logout();
                }
                else {
                    _this.jwt.checkValidity(response[0].token)
                        .then(function (resp) {
                        if (resp === true) {
                            _this.jwt.storageClientId()
                                .then(function () {
                                _this.login();
                            });
                        }
                        else {
                            _this.logout();
                        }
                    })
                        .catch(function () {
                        _this.toast.logMessage('Une erreur s\'est produite', 'warning')
                            .then(function () {
                            _this.logout();
                        });
                    });
                }
            })
                .catch(function () {
                _this.toast.logMessage('Une erreur s\'est produite', 'warning')
                    .then(function () {
                    _this.logout();
                });
            });
        });
    }
    // Method to init
    AppComponent.prototype.initializeApp = function () {
        var _this = this;
        this.platform.ready()
            .then(function () {
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
        });
    };
    AppComponent.prototype.allowStatusBar = function () {
        // set status bar to white
        this.statusBar.backgroundColorByHexString('#69aec4');
        this.statusBar.overlaysWebView(false);
    };
    // Method to redirect to login page if user is not logged
    AppComponent.prototype.logout = function () {
        this.router.navigateByUrl('/login');
    };
    // Method to redirect to home page if user is logged
    AppComponent.prototype.login = function () {
        this.router.navigateByUrl('/profil');
    };
    AppComponent.prototype.initDB = function () {
        var _this = this;
        return this.sql.createDB().then(function () {
            _this.sql.databaseTables.map(function (table) {
                _this.sql.createTable(table)
                    .then(function (resp) {
                    console.log(resp);
                });
            });
        });
    };
    AppComponent.prototype.logoutJwt = function () {
        this.jwt.logOut();
    };
    AppComponent.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"] },
        { type: _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"] },
        { type: _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__["StatusBar"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] },
        { type: _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_6__["SqliteService"] },
        { type: _services_jwt_jwt_service__WEBPACK_IMPORTED_MODULE_7__["JwtService"] },
        { type: _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"] }
    ]; };
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.scss */ "./src/app/app.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["Platform"],
            _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_3__["SplashScreen"],
            _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_4__["StatusBar"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"],
            _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_6__["SqliteService"],
            _services_jwt_jwt_service__WEBPACK_IMPORTED_MODULE_7__["JwtService"],
            _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"]])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/splash-screen/ngx */ "./node_modules/@ionic-native/splash-screen/ngx/index.js");
/* harmony import */ var _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/status-bar/ngx */ "./node_modules/@ionic-native/status-bar/ngx/index.js");
/* harmony import */ var _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/sqlite/ngx */ "./node_modules/@ionic-native/sqlite/ngx/index.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _components_components_module__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/components.module */ "./src/app/components/components.module.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! @ionic-native/File/ngx */ "./node_modules/@ionic-native/File/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/ngx/index.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");







/**
 * SQLite
 */











// @ts-ignore
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]],
            entryComponents: [],
            imports: [_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["BrowserModule"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"].forRoot(), _app_routing_module__WEBPACK_IMPORTED_MODULE_9__["AppRoutingModule"], _angular_common_http__WEBPACK_IMPORTED_MODULE_10__["HttpClientModule"], _components_components_module__WEBPACK_IMPORTED_MODULE_11__["ComponentsModule"], _ionic_storage__WEBPACK_IMPORTED_MODULE_12__["IonicStorageModule"].forRoot()],
            providers: [
                _ionic_native_status_bar_ngx__WEBPACK_IMPORTED_MODULE_6__["StatusBar"],
                _ionic_native_splash_screen_ngx__WEBPACK_IMPORTED_MODULE_5__["SplashScreen"],
                _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_7__["SQLite"],
                _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_13__["Camera"],
                _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_14__["File"],
                // @ts-ignore
                _angular_core__WEBPACK_IMPORTED_MODULE_1__["ChangeDetectorRef"],
                _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_17__["Geolocation"],
                _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_15__["WebView"],
                _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_16__["FilePath"],
                { provide: _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouteReuseStrategy"], useClass: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicRouteStrategy"] }
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_8__["AppComponent"]],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_1__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/components.module.ts":
/*!*************************************************!*\
  !*** ./src/app/components/components.module.ts ***!
  \*************************************************/
/*! exports provided: ComponentsModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ComponentsModule", function() { return ComponentsModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _modals_initvisit_initvisit_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./modals/initvisit/initvisit.component */ "./src/app/components/modals/initvisit/initvisit.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _header_header_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./header/header.component */ "./src/app/components/header/header.component.ts");
/* harmony import */ var _rating_rating_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./rating/rating.component */ "./src/app/components/rating/rating.component.ts");
/* harmony import */ var _modals_detail_equip_po_detail_equip_po_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./modals/detail-equip-po/detail-equip-po.component */ "./src/app/components/modals/detail-equip-po/detail-equip-po.component.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _modals_photo_coment_photo_comment_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./modals/photo-coment/photo-comment.component */ "./src/app/components/modals/photo-coment/photo-comment.component.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");




/**
 * Here goes all the component you need to use as "views"
 */






var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _header_header_component__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"],
                _modals_initvisit_initvisit_component__WEBPACK_IMPORTED_MODULE_1__["InitvisitComponent"],
                _rating_rating_component__WEBPACK_IMPORTED_MODULE_5__["RatingComponent"],
                _modals_detail_equip_po_detail_equip_po_component__WEBPACK_IMPORTED_MODULE_6__["DetailEquipPoComponent"],
                _modals_photo_coment_photo_comment_component__WEBPACK_IMPORTED_MODULE_8__["PhotoCommentComponent"]
            ],
            entryComponents: [
                _modals_initvisit_initvisit_component__WEBPACK_IMPORTED_MODULE_1__["InitvisitComponent"],
                _modals_detail_equip_po_detail_equip_po_component__WEBPACK_IMPORTED_MODULE_6__["DetailEquipPoComponent"],
                _modals_photo_coment_photo_comment_component__WEBPACK_IMPORTED_MODULE_8__["PhotoCommentComponent"]
            ],
            exports: [
                _header_header_component__WEBPACK_IMPORTED_MODULE_4__["HeaderComponent"],
                _modals_initvisit_initvisit_component__WEBPACK_IMPORTED_MODULE_1__["InitvisitComponent"],
                _rating_rating_component__WEBPACK_IMPORTED_MODULE_5__["RatingComponent"],
                _modals_detail_equip_po_detail_equip_po_component__WEBPACK_IMPORTED_MODULE_6__["DetailEquipPoComponent"],
            ],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_3__["CommonModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_7__["IonicModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"]
            ],
            schemas: [_angular_core__WEBPACK_IMPORTED_MODULE_2__["CUSTOM_ELEMENTS_SCHEMA"]]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());



/***/ }),

/***/ "./src/app/components/header/header.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/components/header/header.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "ion-button {\n  --box-shadow: none;\n}\n\nion-title {\n  position: absolute;\n  top: 0;\n  left: 0;\n  padding: 0 90px 1px;\n  width: 100%;\n  height: 100%;\n  text-align: center;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9oZWFkZXIvQzpcXFVzZXJzXFxiQVNLT1VcXERlc2t0b3BcXGFzaGVyYV9mcm9udC9zcmNcXGFwcFxcY29tcG9uZW50c1xcaGVhZGVyXFxoZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzcmMvYXBwL2NvbXBvbmVudHMvaGVhZGVyL2hlYWRlci5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNJLGtCQUFBO0FDQ0o7O0FERUE7RUFDSSxrQkFBQTtFQUNBLE1BQUE7RUFDQSxPQUFBO0VBQ0EsbUJBQUE7RUFDQSxXQUFBO0VBQ0EsWUFBQTtFQUNBLGtCQUFBO0FDQ0oiLCJmaWxlIjoic3JjL2FwcC9jb21wb25lbnRzL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJpb24tYnV0dG9ue1xyXG4gICAgLS1ib3gtc2hhZG93OiBub25lO1xyXG59XHJcblxyXG5pb24tdGl0bGUge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgdG9wOiAwO1xyXG4gICAgbGVmdDogMDtcclxuICAgIHBhZGRpbmc6IDAgOTBweCAxcHg7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGhlaWdodDogMTAwJTtcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxuICB9IiwiaW9uLWJ1dHRvbiB7XG4gIC0tYm94LXNoYWRvdzogbm9uZTtcbn1cblxuaW9uLXRpdGxlIHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB0b3A6IDA7XG4gIGxlZnQ6IDA7XG4gIHBhZGRpbmc6IDAgOTBweCAxcHg7XG4gIHdpZHRoOiAxMDAlO1xuICBoZWlnaHQ6IDEwMCU7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/components/header/header.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/header/header.component.ts ***!
  \*******************************************************/
/*! exports provided: HeaderComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HeaderComponent", function() { return HeaderComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_jwt_jwt_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/jwt/jwt.service */ "./src/app/services/jwt/jwt.service.ts");



var HeaderComponent = /** @class */ (function () {
    function HeaderComponent(jwt) {
        this.jwt = jwt;
        this._logOut = false;
    }
    Object.defineProperty(HeaderComponent.prototype, "logOut", {
        set: function (value) {
            this._logOut = value !== 'false';
        },
        enumerable: true,
        configurable: true
    });
    HeaderComponent.prototype.ngOnInit = function () {
        /*
        console.log(this.customTitle);
        console.table(this.customButton);
        */
        if (this._logOut) {
            this.customButton = {
                icon: 'log-out',
                function: 'logout'
            };
        }
    };
    HeaderComponent.prototype.hello = function (event) {
        console.log(event);
        console.log('Hello, this.customButton.function work');
    };
    HeaderComponent.prototype.logout = function () {
        this.jwt.logOut();
    };
    HeaderComponent.ctorParameters = function () { return [
        { type: _services_jwt_jwt_service__WEBPACK_IMPORTED_MODULE_2__["JwtService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], HeaderComponent.prototype, "customTitle", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], HeaderComponent.prototype, "customButton", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [Object])
    ], HeaderComponent.prototype, "logOut", null);
    HeaderComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-header',
            template: __webpack_require__(/*! raw-loader!./header.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/header/header.component.html"),
            styles: [__webpack_require__(/*! ./header.component.scss */ "./src/app/components/header/header.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_jwt_jwt_service__WEBPACK_IMPORTED_MODULE_2__["JwtService"]])
    ], HeaderComponent);
    return HeaderComponent;
}());



/***/ }),

/***/ "./src/app/components/modals/detail-equip-po/detail-equip-po.component.scss":
/*!**********************************************************************************!*\
  !*** ./src/app/components/modals/detail-equip-po/detail-equip-po.component.scss ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbW9kYWxzL2RldGFpbC1lcXVpcC1wby9kZXRhaWwtZXF1aXAtcG8uY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/modals/detail-equip-po/detail-equip-po.component.ts":
/*!********************************************************************************!*\
  !*** ./src/app/components/modals/detail-equip-po/detail-equip-po.component.ts ***!
  \********************************************************************************/
/*! exports provided: DetailEquipPoComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailEquipPoComponent", function() { return DetailEquipPoComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_toast_toast_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/app/services/toast/toast.service */ "./src/app/services/toast/toast.service.ts");
/* harmony import */ var src_app_services_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/loading/loading.service */ "./src/app/services/loading/loading.service.ts");






var DetailEquipPoComponent = /** @class */ (function () {
    function DetailEquipPoComponent(router, modal, toast, loading) {
        this.router = router;
        this.modal = modal;
        this.toast = toast;
        this.loading = loading;
    }
    DetailEquipPoComponent.prototype.ngOnInit = function () { };
    DetailEquipPoComponent.prototype.closeModal = function () {
        // using the injected ModalController this page
        // can "dismiss" itself and optionally pass back data
        this.modal.dismiss({
            dismissed: true
        });
    };
    DetailEquipPoComponent.prototype.confirmUpdate = function (event) {
        console.log(event);
    };
    DetailEquipPoComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
        { type: src_app_services_toast_toast_service__WEBPACK_IMPORTED_MODULE_4__["ToastService"] },
        { type: src_app_services_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DetailEquipPoComponent.prototype, "name", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DetailEquipPoComponent.prototype, "id", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], DetailEquipPoComponent.prototype, "type", void 0);
    DetailEquipPoComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-detail-equip-po',
            template: __webpack_require__(/*! raw-loader!./detail-equip-po.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/modals/detail-equip-po/detail-equip-po.component.html"),
            styles: [__webpack_require__(/*! ./detail-equip-po.component.scss */ "./src/app/components/modals/detail-equip-po/detail-equip-po.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
            src_app_services_toast_toast_service__WEBPACK_IMPORTED_MODULE_4__["ToastService"],
            src_app_services_loading_loading_service__WEBPACK_IMPORTED_MODULE_5__["LoadingService"]])
    ], DetailEquipPoComponent);
    return DetailEquipPoComponent;
}());



/***/ }),

/***/ "./src/app/components/modals/initvisit/initvisit.component.scss":
/*!**********************************************************************!*\
  !*** ./src/app/components/modals/initvisit/initvisit.component.scss ***!
  \**********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbW9kYWxzL2luaXR2aXNpdC9pbml0dmlzaXQuY29tcG9uZW50LnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/components/modals/initvisit/initvisit.component.ts":
/*!********************************************************************!*\
  !*** ./src/app/components/modals/initvisit/initvisit.component.ts ***!
  \********************************************************************/
/*! exports provided: InitvisitComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InitvisitComponent", function() { return InitvisitComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_toast_toast_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/toast/toast.service */ "./src/app/services/toast/toast.service.ts");
/* harmony import */ var src_app_services_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/loading/loading.service */ "./src/app/services/loading/loading.service.ts");
/* harmony import */ var src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! src/app/services/visit/visit.service */ "./src/app/services/visit/visit.service.ts");








var InitvisitComponent = /** @class */ (function () {
    function InitvisitComponent(router, modal, navCtrl, sqlite, toast, loading, visitService) {
        this.router = router;
        this.modal = modal;
        this.navCtrl = navCtrl;
        this.sqlite = sqlite;
        this.toast = toast;
        this.loading = loading;
        this.visitService = visitService;
        this.visitForm = {
            nameAgent: '',
            date: '',
            comment: ''
        };
    }
    InitvisitComponent.prototype.ngOnInit = function () { };
    InitvisitComponent.prototype.initVisit = function (form) {
        var _this = this;
        // this.loading.createLoading();
        // console.log('id Visit ', this.idVisit);
        var data = form.form.value;
        // console.log('form values: ', form.form.value);
        // let data = {nameAgent: 'NameAgent', date: '2019-11-19', comment: 'BlaBla'};
        this.visitService.initVisit(this.idVisit, data).then(function (result) {
            // console.log('result of initialisation : ', result);
            _this.toast.logMessage('Visite initialisée avec succès !', 'success');
            _this.closeModal();
            _this.router.navigateByUrl('/visit-resume/' + _this.idVisit + '/' + _this.nameOuvrage);
        }).catch(function (error) {
            // console.error(error);
        });
    };
    InitvisitComponent.prototype.closeModal = function () {
        // using the injected ModalController this page
        // can "dismiss" itself and optionally pass back data
        this.modal.dismiss({
            dismissed: true
        });
    };
    InitvisitComponent.ctorParameters = function () { return [
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"] },
        { type: src_app_services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_3__["SqliteService"] },
        { type: src_app_services_toast_toast_service__WEBPACK_IMPORTED_MODULE_5__["ToastService"] },
        { type: src_app_services_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"] },
        { type: src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_7__["VisitService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], InitvisitComponent.prototype, "nameOuvrage", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], InitvisitComponent.prototype, "idVisit", void 0);
    InitvisitComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-initvisit',
            template: __webpack_require__(/*! raw-loader!./initvisit.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/modals/initvisit/initvisit.component.html"),
            styles: [__webpack_require__(/*! ./initvisit.component.scss */ "./src/app/components/modals/initvisit/initvisit.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ModalController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["NavController"],
            src_app_services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_3__["SqliteService"],
            src_app_services_toast_toast_service__WEBPACK_IMPORTED_MODULE_5__["ToastService"],
            src_app_services_loading_loading_service__WEBPACK_IMPORTED_MODULE_6__["LoadingService"],
            src_app_services_visit_visit_service__WEBPACK_IMPORTED_MODULE_7__["VisitService"]])
    ], InitvisitComponent);
    return InitvisitComponent;
}());



/***/ }),

/***/ "./src/app/components/modals/photo-coment/photo-comment.component.scss":
/*!*****************************************************************************!*\
  !*** ./src/app/components/modals/photo-coment/photo-comment.component.scss ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvbW9kYWxzL3Bob3RvLWNvbWVudC9waG90by1jb21tZW50LmNvbXBvbmVudC5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/components/modals/photo-coment/photo-comment.component.ts":
/*!***************************************************************************!*\
  !*** ./src/app/components/modals/photo-coment/photo-comment.component.ts ***!
  \***************************************************************************/
/*! exports provided: PhotoCommentComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhotoCommentComponent", function() { return PhotoCommentComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_photo_photo_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../../services/photo/photo.service */ "./src/app/services/photo/photo.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../../../services/sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");
/* harmony import */ var _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../services/toast/toast.service */ "./src/app/services/toast/toast.service.ts");




/** Import services */


var PhotoCommentComponent = /** @class */ (function () {
    function PhotoCommentComponent(photoService, modal, sql, toast) {
        this.photoService = photoService;
        this.modal = modal;
        this.sql = sql;
        this.toast = toast;
        this.countFailure = 0;
    }
    PhotoCommentComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.getComment()
            .then(function (response) {
            _this.comment = response[0].commentaire;
        })
            .catch(function (error) {
            console.log(error);
        });
    };
    // Method to delete an image
    PhotoCommentComponent.prototype.delete = function (imgEntry, position, storageKey, id, type) {
        this.photoService.deleteImage(imgEntry, position, storageKey, id, type);
        this.closeModal();
    };
    // Method to close the modal
    PhotoCommentComponent.prototype.closeModal = function () {
        // using the injected ModalController this page
        // can "dismiss" itself and optionally pass back data
        this.modal.dismiss({
            dismissed: true
        });
    };
    // Method to add picture as favorite
    PhotoCommentComponent.prototype.addFavorite = function (table, idPhoto) {
        this.photoService.favoritePicture(table, idPhoto);
    };
    // Method to get comment from internal BDD
    PhotoCommentComponent.prototype.getComment = function () {
        var table = 'visite_photo';
        var column = 'commentaire';
        var options = ' WHERE url = ("' + this.picture.name + '")';
        return this.sql.getRows(table, column, options);
    };
    // Method to insert comment
    PhotoCommentComponent.prototype.insertComment = function (entry) {
        var _this = this;
        console.log(entry.com.value);
        var table = 'visite_photo';
        var condition = { column: 'id_visite_photo', value: this.picture.id };
        var temp = entry.com.value;
        var modified = temp.replace('"', '\'');
        return this.photoService.insertComment(modified, table, condition)
            .then(function () {
            _this.countFailure = 0;
            _this.toast.logMessage('Commentaire enregistré avec succes.', 'success');
            _this.closeModal();
        })
            .catch(function () {
            if (_this.countFailure < 1) {
                _this.toast.logMessage('Une erreur est survenue. Merci de valider à nouveau.', 'dark');
                _this.countFailure++;
            }
            else {
                _this.toast.logMessage('Une erreur est survenue. Merci de vous déconneter et redémarrer votre application.', 'dark');
            }
        });
    };
    PhotoCommentComponent.ctorParameters = function () { return [
        { type: _services_photo_photo_service__WEBPACK_IMPORTED_MODULE_2__["PhotoService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"] },
        { type: _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_4__["SqliteService"] },
        { type: _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_5__["ToastService"] }
    ]; };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], PhotoCommentComponent.prototype, "picture", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], PhotoCommentComponent.prototype, "storageKey", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], PhotoCommentComponent.prototype, "position", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], PhotoCommentComponent.prototype, "type", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], PhotoCommentComponent.prototype, "id", void 0);
    PhotoCommentComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-photo-coment',
            template: __webpack_require__(/*! raw-loader!./photo-comment.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/modals/photo-coment/photo-comment.component.html"),
            styles: [__webpack_require__(/*! ./photo-comment.component.scss */ "./src/app/components/modals/photo-coment/photo-comment.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_photo_photo_service__WEBPACK_IMPORTED_MODULE_2__["PhotoService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ModalController"],
            _services_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_4__["SqliteService"],
            _services_toast_toast_service__WEBPACK_IMPORTED_MODULE_5__["ToastService"]])
    ], PhotoCommentComponent);
    return PhotoCommentComponent;
}());



/***/ }),

/***/ "./src/app/components/rating/rating.component.scss":
/*!*********************************************************!*\
  !*** ./src/app/components/rating/rating.component.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2NvbXBvbmVudHMvcmF0aW5nL3JhdGluZy5jb21wb25lbnQuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/components/rating/rating.component.ts":
/*!*******************************************************!*\
  !*** ./src/app/components/rating/rating.component.ts ***!
  \*******************************************************/
/*! exports provided: RatingComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RatingComponent", function() { return RatingComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var RatingComponent = /** @class */ (function () {
    function RatingComponent() {
        this.ratingChange = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.readonly = false;
        this.activeColor = '#69aec4';
        this.defaultColor = '#f4f4f4';
        this.activeIcon = 'star';
        this.defaultIcon = 'star-outline';
        this.tooltip = [
            'Absent',
            'Mauvais',
            'Moyen',
            'Bon'
        ];
    }
    RatingComponent.prototype.ngOnInit = function () {
    };
    RatingComponent.prototype.rate = function (value) {
        if (this.rating === value && value === 2) {
            this.rating = 1;
        }
        else {
            this.rating = value;
        }
        this.ratingChange.emit(this.rating);
    };
    RatingComponent.prototype.colorRating = function (value) {
        return value > this.rating ? this.defaultColor : this.activeColor;
    };
    RatingComponent.prototype.isEqualRate = function (value) {
        return this.rating === value;
    };
    RatingComponent.prototype.ratingMax = function (size) {
        if (!size) {
            size = 5;
        }
        // size - 1 to let a '0' value
        return new Array((size - 1)).fill(1).map(function (value, index) {
            return index + 2;
        });
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], RatingComponent.prototype, "maxRating", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Number)
    ], RatingComponent.prototype, "rating", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], RatingComponent.prototype, "fontSize", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"])
    ], RatingComponent.prototype, "ratingChange", void 0);
    RatingComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-rating',
            template: __webpack_require__(/*! raw-loader!./rating.component.html */ "./node_modules/raw-loader/index.js!./src/app/components/rating/rating.component.html"),
            styles: [__webpack_require__(/*! ./rating.component.scss */ "./src/app/components/rating/rating.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], RatingComponent);
    return RatingComponent;
}());



/***/ }),

/***/ "./src/app/services/api/api.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/api/api.service.ts ***!
  \*********************************************/
/*! exports provided: ApiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ApiService", function() { return ApiService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _jwt_jwt_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../jwt/jwt.service */ "./src/app/services/jwt/jwt.service.ts");
/* harmony import */ var _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");





var ApiService = /** @class */ (function () {
    function ApiService(httpClient, jwt, sqlite) {
        this.httpClient = httpClient;
        this.jwt = jwt;
        this.sqlite = sqlite;
        this.apiUrl = 'http://90.113.154.212/ashera/index.php/';
        this.httpOptions = {
            'Access-Control-Allow-Headers': '*',
            'Access-Control-Allow-Credentials': 'true'
        };
    }
    ApiService.prototype.getHttpOptions = function (options) {
        // Initialize our headers for our http request
        var header = this.httpOptions;
        // Merge options passed in argument with our base header
        // tslint:disable-next-line:forin
        for (var prop in options) {
            header[prop] = options[prop];
        }
        /**
         *  Merge the httpOptions.headers <Object> with the arguments options <Object>
         */
        return {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"](header)
        };
    };
    /**
     * Service used for post methods
     * @param data // The data you need to send
     * @param uri // The method used by the Api to manage your data
     * @param options // Headers & http options
     * @param dev // Use final api url or custom one in this service properties
     */
    ApiService.prototype.apiPostRequest = function (data, uri, options) {
        var _this = this;
        var httpOptions = this.getHttpOptions(options);
        /**
         * Create a promise for the return of our service
         */
        return new Promise(function (resolve, reject) {
            var url = _this.apiUrl + uri;
            _this.httpClient.post(url, data, httpOptions)
                .subscribe(function (response) {
                resolve(response);
            }, function (error) {
                reject(error);
            });
        });
    };
    ApiService.prototype.apiGetRequest = function (url, query) {
        var _this = this;
        return this.jwt.makeJwtOption().then(function (result) {
            if (result) {
                var httpOptions_1 = _this.getHttpOptions(result);
                return new Promise(function (resolve, reject) {
                    _this.httpClient.get(_this.apiUrl + url + query, httpOptions_1)
                        .subscribe(function (response) {
                        resolve(response);
                    }, function (error) {
                        reject(error);
                    });
                });
            }
            else {
                throw Error('Can\'t access to the token');
            }
        });
    };
    ApiService.prototype.asyncForEach = function (array, callback) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var index;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        index = 0;
                        _a.label = 1;
                    case 1:
                        if (!(index < array.length)) return [3 /*break*/, 4];
                        return [4 /*yield*/, callback(array[index], index, array)];
                    case 2:
                        _a.sent();
                        _a.label = 3;
                    case 3:
                        index++;
                        return [3 /*break*/, 1];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ApiService.prototype.syncUtilityTables = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var tables, result;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        tables = ['visite_cas', 'visite_status', 'visite_note'];
                        result = {};
                        return [4 /*yield*/, this.asyncForEach(tables, function (table) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                                var _this = this;
                                return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                                    switch (_a.label) {
                                        case 0: return [4 /*yield*/, this.fetchUtilityTable(table)
                                                .then(function (utilityTable) {
                                                result[table] = utilityTable;
                                                _this.dropUtilityTable(table)
                                                    .then(function () {
                                                    _this.createUtilityTable(table)
                                                        .then(function () {
                                                        result[table].forEach(function (dataObj) {
                                                            _this.insertFromObj(table, dataObj)
                                                                .then(function () {
                                                            })
                                                                .catch(function () {
                                                            });
                                                        });
                                                    });
                                                });
                                            })];
                                        case 1:
                                            _a.sent();
                                            return [2 /*return*/];
                                    }
                                });
                            }); })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    ApiService.prototype.fetchUtilityTable = function (tableName) {
        var uriQuery = tableName.split('_')[1];
        return this.apiGetRequest('get/', uriQuery);
    };
    ApiService.prototype.dropUtilityTable = function (tableName) {
        return this.sqlite.dropTable(tableName);
    };
    ApiService.prototype.createUtilityTable = function (tableName) {
        var databaseObjArray = [
            'visite_cas',
            'visite_note',
            'visite_status',
        ];
        var index = databaseObjArray.indexOf(tableName);
        return this.sqlite.createTable(this.sqlite.databaseTables[index]);
    };
    ApiService.prototype.insertFromObj = function (tableName, dataObj) {
        var _this = this;
        return this.constructInsertQuery(dataObj).then(function (queryArray) {
            return _this.sqlite.insertRow({ table: tableName, column: queryArray[0], value: queryArray[1] });
        });
    };
    ApiService.prototype.constructInsertQuery = function (entity) {
        var keys = Object.keys(entity);
        var columns = '';
        var values = keys.length > 1 ? '(' : '';
        return new Promise(function (resolve, reject) {
            if (keys.length < 1) {
                reject(new Error('Parameter is not an array/object or it\'s an empty one'));
            }
            else {
                keys.forEach(function (value, index) {
                    columns += " \"" + value + "\"";
                    values += isNaN(entity[value]) ? " \"" + entity[value] + "\"" : " " + entity[value];
                    if (keys.length - 1 === index) {
                        values += keys.length > 1 ? ')' : '';
                        columns += '';
                        resolve([columns, values]);
                    }
                    else {
                        columns += ',';
                        values += ',';
                    }
                });
            }
        });
    };
    ApiService.ctorParameters = function () { return [
        { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] },
        { type: _jwt_jwt_service__WEBPACK_IMPORTED_MODULE_3__["JwtService"] },
        { type: _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_4__["SqliteService"] }
    ]; };
    ApiService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _jwt_jwt_service__WEBPACK_IMPORTED_MODULE_3__["JwtService"], _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_4__["SqliteService"]])
    ], ApiService);
    return ApiService;
}());



/***/ }),

/***/ "./src/app/services/entities/visit-cas.ts":
/*!************************************************!*\
  !*** ./src/app/services/entities/visit-cas.ts ***!
  \************************************************/
/*! exports provided: VisitCas */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitCas", function() { return VisitCas; });
/**
 * Utilitary table
 */
var VisitCas = /** @class */ (function () {
    function VisitCas() {
        this.tableName = "visite_cas";
        /*
            Array fetched with GSO database
        */
        var allCasesFromService = [
            // Id of each object will be it's index + 1
            { cas: 'État du patrimoine', notable: 0 },
            { cas: 'Sécurité du site et abords extérieurs', notable: 0 },
            { cas: 'Entretien des installations', notable: 0 },
            { cas: 'Renouvellement', notable: 0 },
            { cas: 'État général du site', notable: 1 },
            { cas: 'Propreté du sité', notable: 1 },
            { cas: 'État du génie civil', notable: 1 },
            { cas: 'Clôture', notable: 1 },
            { cas: 'Portail', notable: 1 },
            { cas: 'Espaces verts, Haies', notable: 1 },
            { cas: 'Peinture, enduits, apsects extérieurs', notable: 1 },
            { cas: 'Huisseries', notable: 1 },
            { cas: 'Armoire éléctrique', notable: 1 },
            { cas: 'Équipements électromécaniques', notable: 1 },
            { cas: 'Accessoires hydrauliques', notable: 1 },
            { cas: 'Compteurs', notable: 1 }
        ];
        this.allCases = allCasesFromService;
    }
    ;
    VisitCas.prototype.getCas = function (idCas) {
        return this.allCases[idCas];
    };
    VisitCas.prototype.fetchCases = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            setTimeout(function () {
                var cases = _this.allCases;
                resolve(cases);
            }, 500);
        });
    };
    return VisitCas;
}());



/***/ }),

/***/ "./src/app/services/entities/visit-equipement.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/entities/visit-equipement.ts ***!
  \*******************************************************/
/*! exports provided: VisitEquipement */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitEquipement", function() { return VisitEquipement; });
var VisitEquipement = /** @class */ (function () {
    /**
     * Construct for each Equipement, link to VisitPartieOuvrage Object
     * @param idVisitePO
     * @param idEquipement
     * @param nomEquipement
     */
    function VisitEquipement(idVisitePO, idEquipement, nomEquipement) {
        this.tableName = 'visite_equipement';
        this.id_visite_partie_ouvrage = idVisitePO;
        this.id_equipement = idEquipement;
        this.nom_equipement = nomEquipement;
    }
    VisitEquipement.count = 1;
    VisitEquipement.ctorParameters = function () { return [
        { type: Number },
        { type: Number },
        { type: String }
    ]; };
    return VisitEquipement;
}());



/***/ }),

/***/ "./src/app/services/entities/visit-partie-ouvrage.ts":
/*!***********************************************************!*\
  !*** ./src/app/services/entities/visit-partie-ouvrage.ts ***!
  \***********************************************************/
/*! exports provided: VisitPartieOuvrage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitPartieOuvrage", function() { return VisitPartieOuvrage; });
var VisitPartieOuvrage = /** @class */ (function () {
    /**
     * Construct for each VistPO, linked to Visit Object
     * @param idVisite
     * @param idPO
     * @param namePO
     */
    function VisitPartieOuvrage(idVisite, idPO, namePO) {
        this.tableName = 'visite_partie_ouvrage';
        this.id_visite = idVisite;
        this.id_partie_ouvrage = idPO;
        this.nom_partie_ouvrage = namePO;
    }
    VisitPartieOuvrage.count = 1;
    VisitPartieOuvrage.ctorParameters = function () { return [
        { type: Number },
        { type: Number },
        { type: String }
    ]; };
    return VisitPartieOuvrage;
}());



/***/ }),

/***/ "./src/app/services/entities/visit.ts":
/*!********************************************!*\
  !*** ./src/app/services/entities/visit.ts ***!
  \********************************************/
/*! exports provided: Visit */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Visit", function() { return Visit; });
var Visit = /** @class */ (function () {
    /**
     * Construct for object Visit
     * @param idOuvrage the one used for the visit
     * @param idAgent can be get with proper service
     */
    function Visit(idOuvrage, idAgent, nameOuvrage) {
        this.tableName = 'visite';
        this.id_ouvrage = idOuvrage;
        this.id_client = idAgent;
        this.nom_ouvrage = nameOuvrage;
    }
    Visit.count = 1;
    Visit.ctorParameters = function () { return [
        { type: Number },
        { type: Number },
        { type: String }
    ]; };
    return Visit;
}());



/***/ }),

/***/ "./src/app/services/entities/visite-liste-cas.ts":
/*!*******************************************************!*\
  !*** ./src/app/services/entities/visite-liste-cas.ts ***!
  \*******************************************************/
/*! exports provided: VisiteListeCas */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisiteListeCas", function() { return VisiteListeCas; });
var VisiteListeCas = /** @class */ (function () {
    function VisiteListeCas(idCas, idPO) {
        this.tableName = 'visite_liste_cas';
        this.id_visite_cas = idCas;
        this.id_visite_partie_ouvrage = idPO;
    }
    VisiteListeCas.ctorParameters = function () { return [
        { type: Number },
        { type: Number }
    ]; };
    return VisiteListeCas;
}());



/***/ }),

/***/ "./src/app/services/jwt/jwt.service.ts":
/*!*********************************************!*\
  !*** ./src/app/services/jwt/jwt.service.ts ***!
  \*********************************************/
/*! exports provided: JwtService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "JwtService", function() { return JwtService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");
/* harmony import */ var _toast_toast_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../toast/toast.service */ "./src/app/services/toast/toast.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");






var JwtService = /** @class */ (function () {
    function JwtService(sql, storage, toast, router) {
        this.sql = sql;
        this.storage = storage;
        this.toast = toast;
        this.router = router;
    }
    // Method to get token from internal database
    JwtService.prototype.getToken = function () {
        return this.sql.getRows('visite_token');
    };
    // Method to check validity of the token
    JwtService.prototype.checkValidity = function (token) {
        var _this = this;
        return this.splitToken(token)
            .then(function (response) {
            _this.payload = JSON.parse(response[1]);
            return _this.payload.exp > Date.now();
        })
            .catch(function (error) {
            return error;
        });
    };
    // Method to split the token and decode it
    JwtService.prototype.splitToken = function (token) {
        var tab = [];
        return new Promise(function (resolve, reject) {
            var temp = token.split('.');
            for (var i = 0; i < temp.length - 1; i++) {
                tab.push(atob(temp[i]));
            }
            if (tab.length > 0) {
                resolve(tab);
            }
            else {
                reject('Pas de token.');
            }
        });
    };
    // Method to get client Id from the storage
    JwtService.prototype.storageClientId = function () {
        return this.storage.set('clientId', this.payload.data.client_id);
    };
    // Method to make header with the token
    JwtService.prototype.makeJwtOption = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var response, error_1;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.getToken()];
                    case 1:
                        response = _a.sent();
                        if (response !== false) {
                            return [2 /*return*/, { Authorization: 'Bearer' + response[0].token }];
                        }
                        else {
                            throw Error('No token found !');
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        return [2 /*return*/, error_1];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * Method to remove the table token the create it again
     * In order to alway keep a clean table
     */
    JwtService.prototype.logOut = function () {
        var _this = this;
        this.sql.dropTable('visite_token')
            .then(function () {
            _this.toast.logMessage('Vous vous êtes déconnecté!', 'success')
                .then(function () {
                _this.sql.createTable({ table: 'visite_token', column: '(id_token INTEGER PRIMARY KEY, token TEXT)' })
                    .then(function () {
                    _this.router.navigateByUrl('/login');
                });
            });
        })
            .catch(function () {
            _this.toast.logMessage('Erreur', 'black');
        });
    };
    JwtService.ctorParameters = function () { return [
        { type: _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_3__["SqliteService"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"] },
        { type: _toast_toast_service__WEBPACK_IMPORTED_MODULE_4__["ToastService"] },
        { type: _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"] }
    ]; };
    JwtService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_3__["SqliteService"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_2__["Storage"],
            _toast_toast_service__WEBPACK_IMPORTED_MODULE_4__["ToastService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_5__["Router"]])
    ], JwtService);
    return JwtService;
}());



/***/ }),

/***/ "./src/app/services/loading/loading.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/services/loading/loading.service.ts ***!
  \*****************************************************/
/*! exports provided: LoadingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadingService", function() { return LoadingService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var LoadingService = /** @class */ (function () {
    function LoadingService(loadingController) {
        this.loadingController = loadingController;
    }
    LoadingService.prototype.createLoading = function (msg) {
        if (msg === void 0) { msg = 'Chargement...'; }
        this.loadingController.create({
            spinner: 'crescent',
            message: msg
        }).then(function (res) {
            res.present();
        });
    };
    LoadingService.prototype.closeLoading = function (id) {
        if (id === void 0) { id = ''; }
        this.loadingController.dismiss(id);
    };
    LoadingService.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"] }
    ]; };
    LoadingService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["LoadingController"]])
    ], LoadingService);
    return LoadingService;
}());



/***/ }),

/***/ "./src/app/services/photo/photo.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/photo/photo.service.ts ***!
  \*************************************************/
/*! exports provided: PhotoService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PhotoService", function() { return PhotoService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/camera/ngx */ "./node_modules/@ionic-native/camera/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic-native/File/ngx */ "./node_modules/@ionic-native/File/ngx/index.js");
/* harmony import */ var _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/ionic-webview/ngx */ "./node_modules/@ionic-native/ionic-webview/ngx/index.js");
/* harmony import */ var _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ionic-native/file-path/ngx */ "./node_modules/@ionic-native/file-path/ngx/index.js");
/* harmony import */ var _toast_toast_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../toast/toast.service */ "./src/app/services/toast/toast.service.ts");
/* harmony import */ var _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _api_api_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ../api/api.service */ "./src/app/services/api/api.service.ts");
/* harmony import */ var _jwt_jwt_service__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ../jwt/jwt.service */ "./src/app/services/jwt/jwt.service.ts");













var PhotoService = /** @class */ (function () {
    function PhotoService(camera, file, webview, storage, filePath, plt, sql, toast, geo, api, jwt, action, platform) {
        this.camera = camera;
        this.file = file;
        this.webview = webview;
        this.storage = storage;
        this.filePath = filePath;
        this.plt = plt;
        this.sql = sql;
        this.toast = toast;
        this.geo = geo;
        this.api = api;
        this.jwt = jwt;
        this.action = action;
        this.platform = platform;
        this.images = [];
    }
    PhotoService.prototype.ngOnInit = function () {
        this.images = [];
    };
    // Load images path and name from Ionic storage
    PhotoService.prototype.loadStoredImages = function (storageKey) {
        var _this = this;
        this.images = []; // Reset value to avoid duplicate from other pages on service load
        this.storage.get(storageKey) // Get storage of the equipment or "partie d'ouvrage"
            .then(function (images) {
            if (images) {
                var arr = JSON.parse(images);
                _this.images = [];
                var _loop_1 = function (img) {
                    var filePath = _this.file.dataDirectory + img;
                    var resPath = _this.pathForImage(filePath);
                    _this.sql.getRows('visite_photo', '*', ' WHERE url = ("' + img + '")')
                        .then(function (response) {
                        _this.images.push({ name: img, path: resPath, filePath: filePath, id: response[0].id_visite_photo });
                        if (response[0].favoris === 1) {
                            _this.favorite = response[0].id_visite_photo;
                        }
                    })
                        .catch(function () {
                        _this.toast.logMessage('Une erreur  est survenue', 'dark');
                    });
                };
                for (var _i = 0, arr_1 = arr; _i < arr_1.length; _i++) {
                    var img = arr_1[_i];
                    _loop_1(img);
                }
            }
        })
            .catch(function () {
            _this.toast.logMessage('Une erreur  est survenue', 'dark');
        });
    };
    // Set path for the image
    PhotoService.prototype.pathForImage = function (img) {
        if (img === null) {
            return '';
        }
        else {
            return this.webview.convertFileSrc(img);
        }
    };
    // Method to take picture
    PhotoService.prototype.takePicture = function (storageKey, ouvrageName, id, type, sourceType) {
        var _this = this;
        // Camera options
        var options = {
            quality: 50,
            sourceType: sourceType,
            saveToPhotoAlbum: false,
            correctOrientation: true
        };
        // Start camera or convert picture from library
        this.camera.getPicture(options)
            .then(function (imagePath) {
            var data = { ouvrage: ouvrageName, type: type + id };
            // convert picture from library (It creates a new image)
            if (_this.platform.is('android') && sourceType === _this.camera.PictureSourceType.PHOTOLIBRARY) {
                _this.filePath.resolveNativePath(imagePath)
                    .then(function (filePath) {
                    var correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                    var currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                    _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName(data), storageKey, type, id);
                });
            }
            else { // Picture taken from the camera
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                _this.copyFileToLocalDir(correctPath, currentName, _this.createFileName(data), storageKey, type, id);
            }
        });
    };
    // Method to select to use camera or charge picture from gallery
    PhotoService.prototype.selectImage = function (storageKey, ouvrageName, id, type) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var actionSheet;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        console.log(id);
                        return [4 /*yield*/, this.action.create({
                                header: 'Selection de la source',
                                buttons: [{
                                        text: 'Charger depuis la gallery',
                                        handler: function () {
                                            _this.takePicture(storageKey, ouvrageName, id, type, _this.camera.PictureSourceType.PHOTOLIBRARY);
                                        }
                                    },
                                    {
                                        text: 'Appareil photo',
                                        handler: function () {
                                            _this.takePicture(storageKey, ouvrageName, id, type, _this.camera.PictureSourceType.CAMERA);
                                        }
                                    },
                                    {
                                        text: 'Annuler',
                                        role: 'cancel'
                                    }
                                ]
                            })];
                    case 1:
                        actionSheet = _a.sent();
                        return [4 /*yield*/, actionSheet.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // Create name of the picture
    PhotoService.prototype.createFileName = function (data) {
        var a = data.ouvrage.split(' ').join('').toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, ''), b = data.type.replace(' ', ''), c = Date.now(), d = Math.floor(Math.random() * 9999);
        return a + b + c + d + '.jpg';
    };
    // Method to copy the file in the device
    PhotoService.prototype.copyFileToLocalDir = function (namePath, currentName, newFileName, storageKey, type, id) {
        var _this = this;
        var data = { table: 'visite_photo', column: 'url', value: '("' + newFileName + '")' };
        this.file.copyFile(namePath, currentName, this.file.dataDirectory, newFileName)
            .then(function () {
            _this.sql.insertRow(data)
                .then(function (response) {
                _this.updateStoredImages(response.insertId, newFileName, storageKey, type, id);
            });
        })
            .catch(function () {
            _this.toast.logMessage('Erreur lors de l\'enrgistrememnt de la photo.', 'warning');
        });
    };
    // Method to the pictures in the storage
    PhotoService.prototype.updateStoredImages = function (id, name, storageKey, type, idType) {
        var _this = this;
        this.storage.get(storageKey)
            .then(function (images) {
            var arr = JSON.parse(images);
            if (!arr) {
                var newImages = [name];
                _this.storage.set(storageKey, JSON.stringify(newImages));
            }
            else {
                arr.push(name);
                _this.storage.set(storageKey, JSON.stringify(arr));
            }
            var filePath = _this.file.dataDirectory + name;
            var resPath = _this.pathForImage(filePath);
            var newEntry = {
                name: name,
                path: resPath,
                filePath: filePath,
                id: id
            };
            _this.images.push(newEntry);
            _this.getGeoloc(id);
            _this.insertPhotoList(idType, type);
        });
    };
    // Method to stringify the list of pictures
    PhotoService.prototype.pushToTab = function () {
        var arr = [];
        this.images.map(function (img) {
            arr.push(img.id);
        });
        return arr.join();
    };
    // Method to insert the list of pictures on a liste_cas or equipement
    PhotoService.prototype.insertPhotoList = function (id, type) {
        var _this = this;
        console.log(type);
        console.log(id);
        var table;
        var conditionColumn;
        if (type === 'equip') {
            table = 'visite_equipement';
            conditionColumn = 'id_visite_equipement';
        }
        else if (type === 'cas') {
            table = 'visite_liste_cas';
            conditionColumn = 'id_visite_liste_cas';
        }
        var photoList = this.pushToTab();
        var val = {
            table: table,
            column: 'id_photo',
            columnValue: photoList,
            conditionColumn: conditionColumn,
            conditionValue: '(' + id + ')'
        };
        console.log(val);
        this.sql.updateOneColumn(val)
            .then(function (resp) {
            console.log(resp);
        })
            .catch(function (error) {
            console.log(error);
            _this.toast.logMessage('Erreur de sauvegarder de la liste des photos', 'dark');
        });
    };
    // Method to delete an image in the storage and device
    PhotoService.prototype.deleteImage = function (imgEntry, position, storageKey, id, type) {
        var _this = this;
        this.images.splice(position, 1);
        this.storage.get(storageKey).then(function (images) {
            var arr = JSON.parse(images);
            var filtered = arr.filter(function (name) { return name !== imgEntry.name; });
            _this.storage.set(storageKey, JSON.stringify(filtered));
            var correctPath = imgEntry.filePath.substr(0, imgEntry.filePath.lastIndexOf('/') + 1);
            _this.file.removeFile(correctPath, imgEntry.name)
                .then(function (res) {
                var arg = { table: 'visite_photo', column: 'url' };
                _this.sql.deleteRow('("' + imgEntry.name + '")', arg)
                    .then(function () {
                    _this.toast.logMessage('Photo effacé.', 'success');
                    _this.insertPhotoList(id, type);
                })
                    .catch(function () {
                    _this.toast.logMessage('Erreur lors de l\'éffacement de la photo.', 'warning');
                });
            })
                .catch(function (error) {
                _this.toast.logMessage('Erreur lors de l\'éffacement de la photo.', 'warning');
            });
        });
    };
    // Method to get localisation of the picture
    PhotoService.prototype.getGeoloc = function (id) {
        var _this = this;
        this.geo.getCurrentPosition()
            .then(function (resp) {
            var x = { table: 'visite_photo', column: 'x',
                columnValue: resp.coords.longitude,
                conditionColumn: 'id_visite_photo',
                conditionValue: id };
            var y = { table: 'visite_photo', column: 'y',
                columnValue: resp.coords.latitude,
                conditionColumn: 'id_visite_photo',
                conditionValue: id };
            _this.sql.updateOneColumn(x)
                .then(function () {
                _this.sql.updateOneColumn(y)
                    .then(function () {
                    _this.toast.logMessage('Gélocalisation réussie.', 'success');
                })
                    .catch(function (error) {
                    _this.toast.logMessage('Gélocalisation incomplète.', 'dark');
                });
            })
                .catch(function () {
                _this.toast.logMessage('Erreur de géolocalisation, reprenez une photo', 'dark');
            });
        })
            .catch(function () {
            _this.toast.logMessage('Echec de gélocalisation, problème de couverture GPS.', 'dark');
        });
    };
    // Method to insert comment into the sqlite table
    PhotoService.prototype.insertComment = function (comment, table, condition) {
        var data = { table: table, column: 'commentaire',
            columnValue: comment,
            conditionColumn: condition.column,
            conditionValue: condition.value };
        return this.sql.updateOneColumn(data);
    };
    PhotoService.prototype.favoritePicture = function (table, idPhoto) {
        var _this = this;
        var arr = [];
        var url = 'url';
        arr = this.images.map(function (img) {
            return '"' + img.name + '"';
        });
        var joined = arr.join();
        var data = { table: table, column: 'favoris',
            columnValue: 0,
            conditionColumn: url,
            conditionValue: '(' + joined + ')' };
        this.sql.updateMultiValueOneColumn(data)
            .then(function () {
            data.columnValue = 1;
            data.conditionColumn = 'id_visite_photo';
            data.conditionValue = idPhoto;
            _this.sql.updateOneColumn(data)
                .then(function () {
                _this.favorite = idPhoto;
                _this.toast.logMessage('Photo favorite sélectionné.', 'success');
            })
                .catch(function (error) {
                _this.toast.logMessage('Erreur d\'ajout aux favoris', 'dark');
            });
        })
            .catch(function () {
            _this.toast.logMessage('Erreur d\'ajout aux favoris', 'dark');
        });
    };
    PhotoService.prototype.getStorages = function (idVisit) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.sql.getRows('visite_storage_key', 'storage_key', ' WHERE id_visite = (' + idVisit + ')')
                .then(function (result) {
                console.log(result);
                _this.tabLength = result.length;
                _this.getImagesFromStorages(result)
                    .then(function (res) {
                    _this.filterArray(res)
                        .then(function (resp) {
                        console.log(resp);
                        _this.startUpload(resp)
                            .then(function () {
                            resolve(resp);
                        })
                            .catch(function (error) {
                            _this.toast.logMessage('Erreur lors de l\'envoi des photos', 'dark');
                            reject(error);
                        });
                    });
                })
                    .catch(function (error) {
                    console.log(error);
                    _this.toast.logMessage('Aucune photo trouvé.', 'dark');
                    reject(error);
                });
            })
                .catch(function (err) {
                console.log(err);
                _this.toast.logMessage('Aucune photo trouvé.', 'dark');
                reject(err);
            });
        });
    };
    PhotoService.prototype.getImagesFromStorages = function (storages) {
        var _this = this;
        var result = [];
        return new Promise(function (resolve) {
            var _loop_2 = function (i) {
                _this.storage.get(storages[i].storage_key)
                    .then(function (images) {
                    if (images) {
                        var buffer = JSON.parse(images);
                        result.push(buffer);
                    }
                    if (i === storages.length - 1) {
                        resolve(result);
                    }
                });
            };
            for (var i = 0; i < storages.length; i++) {
                _loop_2(i);
            }
        });
    };
    PhotoService.prototype.filterArray = function (tab) {
        var _this = this;
        return new Promise(function (resolve) {
            var finalTab = [];
            for (var j = 0; j < tab.length; j++) {
                if (tab[j]) {
                    // tslint:disable-next-line:prefer-for-of
                    for (var i = 0; i < tab[j].length; i++) {
                        var filePath = _this.file.dataDirectory + tab[j][i];
                        var resPath = _this.pathForImage(filePath);
                        var temp = { name: tab[j][i], path: resPath, filePath: filePath };
                        finalTab.push(temp);
                    }
                }
                if (j === tab.length - 1) {
                    resolve(finalTab);
                }
            }
        });
    };
    PhotoService.prototype.startUpload = function (images) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var length;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        length = images.length;
                        return [4 /*yield*/, images.map(function (img, i) {
                                _this.file.resolveLocalFilesystemUrl(img.filePath)
                                    .then(function (entry) {
                                    entry.file(function (file) { return _this.readFile(file); });
                                })
                                    .catch(function (err) {
                                    _this.toast.logMessage('Erreur lors de la lecture de la photo.', 'warning');
                                });
                                if (i === length - 1) {
                                    return true;
                                    _this.toast.logMessage('Toutes les photos ont était envoyé', 'success');
                                }
                            })];
                    case 1:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    // Method to get picture
    PhotoService.prototype.readFile = function (file) {
        var _this = this;
        var reader = new FileReader();
        reader.onload = function () {
            var formData = new FormData();
            var imgBlob = new Blob([reader.result], {
                type: file.type
            });
            formData.append('file', imgBlob, file.name);
            _this.uploadImageData(formData);
        };
        // reader.readAsDataURL(file);
        reader.readAsArrayBuffer(file);
    };
    // Method to upload pictures to the api
    PhotoService.prototype.uploadImageData = function (formData) {
        var _this = this;
        this.jwt.makeJwtOption()
            .then(function (res) {
            _this.api.apiPostRequest(formData, 'upload/upload', res)
                .catch(function (error) {
                _this.toast.logMessage('POST: Erreur lors de l\'upload.', 'warning');
            });
        })
            .catch(function () {
            _this.toast.logMessage('JWT: Erreur lors de l\'upload.', 'warning');
        });
    };
    // Method to insert new storage name to BDD
    PhotoService.prototype.insertStorageName = function (storageName, idVisit) {
        var data = {
            table: 'visite_storage_key',
            column: 'id_visite, storage_key',
            value: '(' + idVisit + ', "' + storageName + '")'
        };
        return this.sql.insertRow(data);
    };
    /**
     * Method to check if storage name already exist on the BDD or not
     * If exist return true else return false
     */
    PhotoService.prototype.checkStorage = function (storageName, idVisit) {
        var _this = this;
        return this.sql.getRows('visite_storage_key', 'storage_key', ' WHERE storage_key = ("' + storageName + '")')
            .then(function (response) {
            if (response !== undefined && !response.message) {
                return false;
            }
            else {
                return true;
            }
        })
            .catch(function () {
            _this.toast.logMessage('Une erreur est survenue, revenez en arrière.', 'dark');
        });
    };
    PhotoService.ctorParameters = function () { return [
        { type: _ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_2__["Camera"] },
        { type: _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_5__["File"] },
        { type: _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__["WebView"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"] },
        { type: _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_7__["FilePath"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] },
        { type: _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_9__["SqliteService"] },
        { type: _toast_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"] },
        { type: _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__["Geolocation"] },
        { type: _api_api_service__WEBPACK_IMPORTED_MODULE_11__["ApiService"] },
        { type: _jwt_jwt_service__WEBPACK_IMPORTED_MODULE_12__["JwtService"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"] },
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"] }
    ]; };
    PhotoService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_camera_ngx__WEBPACK_IMPORTED_MODULE_2__["Camera"],
            _ionic_native_File_ngx__WEBPACK_IMPORTED_MODULE_5__["File"],
            _ionic_native_ionic_webview_ngx__WEBPACK_IMPORTED_MODULE_6__["WebView"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_4__["Storage"],
            _ionic_native_file_path_ngx__WEBPACK_IMPORTED_MODULE_7__["FilePath"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"],
            _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_9__["SqliteService"],
            _toast_toast_service__WEBPACK_IMPORTED_MODULE_8__["ToastService"],
            _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_10__["Geolocation"],
            _api_api_service__WEBPACK_IMPORTED_MODULE_11__["ApiService"],
            _jwt_jwt_service__WEBPACK_IMPORTED_MODULE_12__["JwtService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ActionSheetController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["Platform"]])
    ], PhotoService);
    return PhotoService;
}());



/***/ }),

/***/ "./src/app/services/sqlite/sqlite.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/sqlite/sqlite.service.ts ***!
  \***************************************************/
/*! exports provided: SqliteService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SqliteService", function() { return SqliteService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic-native/sqlite/ngx */ "./node_modules/@ionic-native/sqlite/ngx/index.js");


// import SQLite packages

var SqliteService = /** @class */ (function () {
    function SqliteService(sqlite) {
        this.sqlite = sqlite;
        this.databaseName = 'ashera.db'; // BDD name
        // Fixed tables
        this.databaseTables = [
            {
                table: 'visite_cas',
                column: '(id_visite_cas INTEGER PRIMARY KEY, cas TEXT, notable INTEGER) '
            },
            {
                table: 'visite_note',
                column: '(id_visite_note INTEGER PRIMARY KEY, note TEXT)'
            },
            {
                table: 'visite_status',
                column: '(id_visite_status INTEGER PRIMARY KEY, status TEXT)'
            },
            {
                table: 'visite_token',
                column: '(id_token INTEGER PRIMARY KEY, token TEXT)'
            },
            {
                table: 'visite',
                column: '(id_visite INTEGER PRIMARY KEY, ' +
                    'id_ouvrage INTEGER, id_client INTEGER, ' +
                    'nom_client TEXT, ' +
                    'nom_ouvrage TEXT, ' +
                    'date TEXT,' +
                    'commentaire TEXT NULL, ' +
                    'id_visite_status INTEGER, FOREIGN KEY (id_visite_status) REFERENCES visite_status (id_visite_status))'
            },
            {
                table: 'visite_partie_ouvrage',
                column: '(id_visite_partie_ouvrage INTEGER PRIMARY KEY, ' +
                    'id_visite INTEGER, ' +
                    'id_partie_ouvrage INTEGER, ' +
                    'nom_partie_ouvrage TEXT, ' +
                    'status INTEGER DEFAULT 0, ' +
                    'FOREIGN KEY (id_visite) REFERENCES visite (id_visite))'
            },
            {
                table: 'visite_photo',
                column: '(id_visite_photo INTEGER PRIMARY KEY, ' +
                    'url VARCHAR(255), ' +
                    'commentaire TEXT NULL, ' +
                    'favoris INTEGER, ' +
                    'x TEXT NULL, ' +
                    'y TEXT NULL)'
            },
            {
                table: 'visite_liste_cas',
                column: '(id_visite_liste_cas INTEGER PRIMARY KEY, ' +
                    'id_visite_partie_ouvrage INTEGER, ' +
                    'id_visite_note INTEGER DEFAULT 1, ' +
                    'id_photo TEXT, ' +
                    'commentaire TEXT, ' +
                    'status INTEGER DEFAULT 0, ' +
                    'id_visite_cas INTEGER, ' +
                    'FOREIGN KEY (id_visite_partie_ouvrage) REFERENCES visite_partie_ouvrage (id_visite_partie_ouvrage), ' +
                    'FOREIGN KEY (id_visite_note) REFERENCES visite_note (id_visite_note), ' +
                    'FOREIGN KEY (id_visite_cas) REFERENCES visite_cas (id_visite_cas))'
            },
            {
                table: 'visite_equipement',
                column: '(id_visite_equipement INTEGER PRIMARY KEY, ' +
                    'id_visite_partie_ouvrage INTEGER, ' +
                    'id_equipement INTEGER, ' +
                    'id_photo TEXT, ' +
                    'id_visite_note INTEGER DEFAULT 1, ' +
                    'commentaire TEXT, ' +
                    'nom_equipement TEXT, ' +
                    'status INTEGER DEFAULT 0, ' +
                    'FOREIGN KEY (id_visite_note) REFERENCES visite_note (id_visite_note), ' +
                    'FOREIGN KEY (id_visite_partie_ouvrage) REFERENCES visite_partie_ouvrage (id_visite_partie_ouvrage))'
            },
            {
                table: 'visite_storage_key',
                column: '(id_visite_storage_key INTEGER PRIMARY KEY, ' +
                    'id_visite INTEGER, ' +
                    'storage_key TEXT, ' +
                    'FOREIGN KEY (id_visite) REFERENCES visite (id_visite))'
            }
        ];
    }
    /**
     * Method to create de BDD
     * then name is fixed internally on a prop
     */
    SqliteService.prototype.createDB = function () {
        var _this = this;
        return this.sqlite.create({
            name: 'ashera.db',
            location: 'default'
        })
            .then(function (db) {
            _this.databaseObj = db;
            return db;
        })
            .catch(function (error) {
            return error;
        });
    };
    /**
     * Method to create tables
     * The tables are fixed on a prop
     * need to think of a way to make it dynamically
     */
    SqliteService.prototype.createTable = function (tableObject) {
        return this.databaseObj.executeSql('CREATE TABLE IF NOT EXISTS ' + tableObject.table + ' ' + tableObject.column, [])
            .then(function (response) {
            return response;
        })
            .catch(function (e) {
            return e;
        });
    };
    SqliteService.prototype.insertOrReplace = function (data) {
        if (data === void 0) { data = { table: 'visite_liste_cas', column: ['id_visite_partie_ouvrage'], value: ['4'] }; }
        if (data.column.length !== data.value.length) {
            throw Error('Columns and Values array have a different sizes ! Sql request aborted..');
        }
        else {
            var toConcatenate = void 0;
            var whereClause = '';
            var length_1 = data.column.length;
            var valueGroup = '';
            var columnGroup = '';
            var value = void 0;
            var column = void 0;
            for (var i = 0; i < data.column.length; i++) {
                value = data.value[i];
                column = data.column[i];
                toConcatenate = isNaN(value) ? "\"" + value + "\"" : "" + value;
                if (i === length_1 - 1) {
                    valueGroup += toConcatenate;
                    columnGroup += column;
                    whereClause += column + " = " + toConcatenate;
                }
                else {
                    valueGroup += toConcatenate + ", ";
                    columnGroup += column + ", ";
                    whereClause += column + " = " + toConcatenate + " AND ";
                }
            }
            var request = 'INSERT INTO ' + data.table + ' (' + columnGroup + ') SELECT ' + valueGroup +
                ' WHERE NOT EXISTS ( SELECT 1 FROM ' + data.table + ' WHERE ' + whereClause + ')';
            return this.databaseObj.executeSql(request, []);
        }
    };
    /**
     * @param data = {table: 'visite', setStmt: 'date = \'15/10/2018\', nom_agent=\'AgentName\'', conditionStmt: 'WHERE id_visite = 5'}
     */
    SqliteService.prototype.updateRequest = function (data) {
        var updateQuery = "UPDATE " + data.table + " SET " + data.setStmt + " " + data.conditionStmt;
        return this.databaseObj.executeSql(updateQuery, []);
    };
    SqliteService.prototype.updateIncrement = function (data) {
        if (data === void 0) { data = {
            table: 'visite',
            column: 'id_visite_status',
            incrementValue: 1,
            condition: 'WHERE id_visite = 1 AND id_visite_status = 1'
        }; }
        var updateQuery = "UPDATE " + data.table + " SET " + data.column + " = " + data.column + " + " + data.incrementValue + " " + data.condition;
        return this.databaseObj.executeSql(updateQuery)
            .then(function (res) {
            return res;
        })
            .catch(function (err) {
            return err;
        });
    };
    /**
     * Method to insert row in table
     * Object format [table: 'table name', column: 'column(s) name', value: 'value(s) to insert' in respect to SQL syntax
     * @param data // object of data(s) to insert
     */
    SqliteService.prototype.insertRow = function (data, option) {
        if (data === void 0) { data = { table: 'visite_note', column: 'note', value: '("mauvais")' }; }
        if (option === void 0) { option = ''; }
        return this.databaseObj.executeSql('INSERT INTO ' + data.table + ' (' + data.column + ') VALUES ' + data.value + option, [])
            .then(function (res) {
            return res;
        })
            .catch(function (e) {
            return e;
        });
    };
    SqliteService.prototype.customQuery = function (query) {
        return this.databaseObj.executeSql(query, [])
            .then(function (res) {
            if (res.rows.length > 0) {
                var rowData = [];
                for (var i = 0; i < res.rows.length; i++) {
                    rowData.push(res.rows.item(i));
                }
                return rowData;
            }
            else {
                return res;
            }
        })
            .catch(function (e) {
            return e;
        });
    };
    /**
     * Method to get all rows from a table
     * @param table table name
     * @param columns all columns you want, if multiple join them in parenthesis separeted by commas -> (column1, column2)
     * @param options here goes all sql conditions (WHERE, LIKE etc...)
     */
    SqliteService.prototype.getRows = function (table, columns, options) {
        if (columns === void 0) { columns = '*'; }
        if (options === void 0) { options = ''; }
        return this.databaseObj.executeSql('SELECT ' + columns + ' FROM ' + table + ' ' + options, [])
            .then(function (res) {
            if (res.rows.length > 0) {
                var rowData = [];
                for (var i = 0; i < res.rows.length; i++) {
                    rowData.push(res.rows.item(i));
                }
                return rowData;
            }
        })
            .catch(function (e) {
            return e;
        });
    };
    /**
     * Method to delete a row
     * @param value // Value to search
     * @param arg // Object containing prop table and column
     */
    SqliteService.prototype.deleteRow = function (value, arg) {
        if (arg === void 0) { arg = { table: 'nom de la table', column: 'nom de la column' }; }
        return this.databaseObj.executeSql('DELETE FROM ' + arg.table + ' WHERE ' + arg.column + ' = ' + value, []);
    };
    /**
     * Method to DROP a table
     */
    SqliteService.prototype.dropTable = function (table) {
        return this.databaseObj.executeSql('DROP TABLE IF EXISTS ' + table, []);
    };
    /**
     * Method to update the value in a row
     * @param data // {table: 'table name', column: 'name of the column to change', columnValue: 'value to change or update',
     * conditionColumn: 'name of the column of the condition', conditionValue: 'value of the condition'}
     */
    SqliteService.prototype.updateOneColumn = function (data) {
        return this.databaseObj.executeSql('UPDATE ' + data.table + ' SET ' + data.column +
            ' = ("' + data.columnValue + '") WHERE ' + data.conditionColumn + ' = ' + data.conditionValue, []);
    };
    SqliteService.prototype.updateMultiValueOneColumn = function (data) {
        return this.databaseObj.executeSql('UPDATE ' + data.table + ' SET ' + data.column + ' = (' + data.columnValue + ') WHERE ' +
            data.conditionColumn + ' IN ' + data.conditionValue, []);
    };
    SqliteService.ctorParameters = function () { return [
        { type: _ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_2__["SQLite"] }
    ]; };
    SqliteService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_native_sqlite_ngx__WEBPACK_IMPORTED_MODULE_2__["SQLite"]])
    ], SqliteService);
    return SqliteService;
}());



/***/ }),

/***/ "./src/app/services/toast/toast.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/toast/toast.service.ts ***!
  \*************************************************/
/*! exports provided: ToastService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ToastService", function() { return ToastService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");



var ToastService = /** @class */ (function () {
    function ToastService(toastController) {
        this.toastController = toastController;
    }
    ToastService.prototype.logMessage = function (msg, color) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: msg,
                            duration: 2000,
                            color: color,
                            buttons: [
                                {
                                    side: 'end',
                                    role: 'cancel',
                                    text: 'Fermer',
                                    handler: function () {
                                    }
                                }
                            ]
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ToastService.prototype.presentToastWithOptions = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            header: 'Toast header',
                            message: 'Click to Close',
                            position: 'top',
                            buttons: [
                                {
                                    side: 'start',
                                    icon: 'star',
                                    text: 'Favorite',
                                    handler: function () {
                                    }
                                }, {
                                    text: 'Done',
                                    role: 'cancel',
                                    handler: function () {
                                    }
                                }
                            ]
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    ToastService.ctorParameters = function () { return [
        { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"] }
    ]; };
    ToastService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_ionic_angular__WEBPACK_IMPORTED_MODULE_2__["ToastController"]])
    ], ToastService);
    return ToastService;
}());



/***/ }),

/***/ "./src/app/services/visit/visit.service.ts":
/*!*************************************************!*\
  !*** ./src/app/services/visit/visit.service.ts ***!
  \*************************************************/
/*! exports provided: VisitService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VisitService", function() { return VisitService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../sqlite/sqlite.service */ "./src/app/services/sqlite/sqlite.service.ts");
/* harmony import */ var _api_api_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../api/api.service */ "./src/app/services/api/api.service.ts");
/* harmony import */ var _entities_visit__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../entities/visit */ "./src/app/services/entities/visit.ts");
/* harmony import */ var _entities_visit_partie_ouvrage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../entities/visit-partie-ouvrage */ "./src/app/services/entities/visit-partie-ouvrage.ts");
/* harmony import */ var _entities_visit_equipement__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../entities/visit-equipement */ "./src/app/services/entities/visit-equipement.ts");
/* harmony import */ var _toast_toast_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../toast/toast.service */ "./src/app/services/toast/toast.service.ts");
/* harmony import */ var _entities_visit_cas__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../entities/visit-cas */ "./src/app/services/entities/visit-cas.ts");
/* harmony import */ var _jwt_jwt_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../jwt/jwt.service */ "./src/app/services/jwt/jwt.service.ts");
/* harmony import */ var _entities_visite_liste_cas__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../entities/visite-liste-cas */ "./src/app/services/entities/visite-liste-cas.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");




/* All entity class */



/* Extra services to manage error and output data to the view */





var VisitService = /** @class */ (function () {
    function VisitService(sqlite, api, toast, jwt, storage) {
        var _this = this;
        this.sqlite = sqlite;
        this.api = api;
        this.toast = toast;
        this.jwt = jwt;
        this.storage = storage;
        this.tokenAgent = {};
        this.getAgentId()
            .then(function (idAgent) {
            console.log('Trying to get clientId', idAgent);
            _this.tokenAgent.id = idAgent;
            console.log(_this.tokenAgent.id);
        });
        this.syncStructure = {
            'visite_partie_ouvrage': {
                'visite_liste_cas': {
                    'visite_photo': ''
                },
                'visite_equipement': {
                    'visite_photo': ''
                }
            }
        };
    }
    /* Here are all methods used only for sync with GS'O database ! */
    VisitService.prototype.recursiveObj = function (obj, structure, id, parentName) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var result, promises, _loop_1, this_1, prop;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                result = obj || {};
                promises = [];
                _loop_1 = function (prop) {
                    /* Like our structure, try to append a new prop in our previous object filled with result of a custom function */
                    var conditionStmt = void 0;
                    if (prop === 'visite_photo') {
                        conditionStmt = result['id_photo'] ? 'WHERE id_visite_' + 'photo' + ' IN (' + result['id_photo'] + ')' : 'WHERE id_visite_photo = 0';
                        //console.log('Parent prop : photo')
                        delete result['id_' + 'photo'];
                        delete result['id_' + parentName];
                    }
                    else {
                        conditionStmt = 'WHERE id_' + parentName + ' = ' + id;
                        //console.log('Parent prop : ', parentName)
                        delete result['id_' + parentName];
                    }
                    //console.log('prop : ', prop);
                    //console.log('condition STMT : ', conditionStmt);
                    promises.push(this_1.sqlite.getRows(prop, '*', conditionStmt)
                        .then(function (res) {
                        //console.log('result of fillArray for '+ prop + ' with parent '+ parentName + 'and id ' + id, res);
                        result[prop] = res || [];
                        //console.log(prop+' array = ',result[prop]);
                        if (typeof structure[prop] === 'object') {
                            //console.log('Need to recall this recusrive function for ' + prop + ' prop');
                            //console.log('For Prop type object \'' + prop + '\'', parentName)
                            if (result[prop]) {
                                result[prop].map(function (value, index, array) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                                    var idParent, nestedArray;
                                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                                        switch (_a.label) {
                                            case 0:
                                                delete result[prop][index]['id_' + parentName];
                                                idParent = value['id_' + prop];
                                                delete result[prop][index]['id_' + prop];
                                                return [4 /*yield*/, this.recursiveObj(result[prop][index], structure[prop], idParent, prop)];
                                            case 1:
                                                nestedArray = _a.sent();
                                                //console.log('Nested Array for '+ prop + 'and id : '+idParent,nestedArray);
                                                return [2 /*return*/, nestedArray];
                                        }
                                    });
                                }); });
                                //console.log('Result : ', result);
                                return result[prop];
                            }
                            else {
                                return result[prop] = [];
                            }
                        }
                        else {
                            //console.log('Not type object ; ', result[prop]);
                            //console.log('Result : ', result);
                            return result[prop].map(function (value, index) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                                return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                                    delete result[prop][index]['id_' + prop];
                                    return [2 /*return*/];
                                });
                            }); });
                        }
                    }));
                };
                this_1 = this;
                for (prop in structure) {
                    _loop_1(prop);
                }
                return [2 /*return*/, Promise.all(promises).then(function (res) {
                        //console.log('Result after all promises resolvation : ', res);
                        //console.log('Result var = ', result);
                        return result;
                    })];
            });
        });
    };
    VisitService.prototype.fillArray = function (id, parentName, childName) {
        return this.sqlite.getRows(childName, '*', 'WHERE id_' + parentName + ' = ' + id);
    };
    VisitService.prototype.syncVisit = function (idVisit) {
        var _this = this;
        console.log('Sync launched with id_visite = ' + idVisit);
        return this.sqlite.getRows('visite', '*', 'WHERE id_visite = ' + idVisit)
            .then(function (visit) {
            console.log('Visite from table \'visite\' : ', visit);
            return _this.recursiveObj(visit[0], _this.syncStructure, idVisit, 'visite');
        })
            .then(function (res) {
            console.log('Final json to send to the back ! →', res);
            return res;
        })
            .catch(function (err) {
            console.error('Error occured while trying to build json for sync ...', err);
            throw Error('Error occured, check log for more informations !');
        });
    };
    VisitService.prototype.syncVisitMethod = function (idVisit) {
        var _this = this;
        return new Promise(function (resolve, reject) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var columns, visit;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        columns = 'commentaire, date, id_client, id_ouvrage, id_visite_status, nom_client, nom_ouvrage';
                        return [4 /*yield*/, this.sqlite.getRows('visite', columns, 'WHERE id_visite = ' + idVisit)];
                    case 1:
                        visit = _a.sent();
                        console.log('Visit: ', visit);
                        this.syncPo(idVisit).then(function (visitPoArr) {
                            resolve(tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, visit[0], { 'visite_partie_ouvrage': visitPoArr }));
                        });
                        return [2 /*return*/];
                }
            });
        }); });
    };
    VisitService.prototype.syncPo = function (idVisit) {
        var _this = this;
        return new Promise(function (resolve, reject) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var column, visitPo, listeCas;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        column = 'id_partie_ouvrage, nom_partie_ouvrage, status, id_visite_partie_ouvrage';
                        return [4 /*yield*/, this.sqlite.getRows('visite_partie_ouvrage', column, 'WHERE id_visite = ' + idVisit)];
                    case 1:
                        visitPo = _a.sent();
                        console.log('VisitPo: ', visitPo);
                        listeCas = visitPo ? visitPo.map(function (val, index) {
                            var idPo = visitPo[index]['id_visite_partie_ouvrage'];
                            return _this.syncVisitCas(idPo)
                                .then(function (res) {
                                console.log('Trying to build liste cas array : ', res);
                                return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, visitPo[index], { 'visite_liste_cas': res });
                            });
                        }) : [];
                        Promise.all(listeCas)
                            .then(function (visitPoArray) {
                            var equip = [];
                            var _loop_2 = function (i) {
                                var idPO = visitPoArray[i]['id_visite_partie_ouvrage'];
                                delete visitPoArray[i]['id_visite_partie_ouvrage'];
                                equip.push(_this.syncVisitEquip(idPO).then(function (res) {
                                    console.log('Trying to build equip array : ', res);
                                    if (res) {
                                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, visitPoArray[i], { 'visite_equipement': res });
                                    }
                                }));
                            };
                            for (var i = 0; i < visitPoArray.length; i++) {
                                _loop_2(i);
                            }
                            Promise.all(equip).then(function (visitPoArr) { resolve(visitPoArr); });
                        });
                        return [2 /*return*/];
                }
            });
        }); });
    };
    VisitService.prototype.syncVisitEquip = function (idPo) {
        var _this = this;
        return new Promise(function (resolve, reject) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var columns, visitEquip, picture;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        columns = 'commentaire, id_equipement, id_photo, id_visite_note, nom_equipement, status';
                        return [4 /*yield*/, this.sqlite.getRows('visite_equipement', columns, 'WHERE id_visite_partie_ouvrage = ' + idPo)];
                    case 1:
                        visitEquip = _a.sent();
                        picture = visitEquip ? visitEquip.map(function (val, index) {
                            var idPhoto = visitEquip[index]['id_photo'];
                            delete visitEquip[index]['id_photo'];
                            var photo_columns = 'commentaire, favoris, url, x, y';
                            return _this.sqlite.getRows('visite_photo', photo_columns, 'WHERE id_visite_photo IN (' + idPhoto + ')')
                                .then(function (res) {
                                console.log('Response from sqlite service : ', res);
                                var photo = res || [];
                                return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, visitEquip[index], { 'visite_photo': photo });
                            });
                        }) : [];
                        Promise.all(picture).then(function (resArray) {
                            console.log('Resolve of all promises (pictures) for equip : ', resArray);
                            resolve(resArray);
                        });
                        return [2 /*return*/];
                }
            });
        }); });
    };
    VisitService.prototype.syncVisitCas = function (idPo) {
        var _this = this;
        return new Promise(function (resolve, reject) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var columns, visitCas, picture;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        columns = 'commentaire, id_photo, id_visite_cas, id_visite_note, status';
                        return [4 /*yield*/, this.sqlite.getRows('visite_liste_cas', columns, 'WHERE id_visite_partie_ouvrage = ' + idPo)];
                    case 1:
                        visitCas = (_a.sent()) || [];
                        picture = visitCas ? visitCas.map(function (val, index) {
                            var idPhoto = visitCas[index]['id_photo'];
                            delete visitCas[index]['id_photo'];
                            var photo_columns = 'commentaire, favoris, url, x, y';
                            return _this.sqlite.getRows('visite_photo', photo_columns, 'WHERE id_visite_photo IN (' + idPhoto + ')')
                                .then(function (res) {
                                console.log('Response from sqlite service : ', res);
                                var photo = res || [];
                                return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, visitCas[index], { 'visite_photo': photo });
                            });
                        }) : [];
                        Promise.all(picture).then(function (resArray) {
                            console.log('Resolve of all promises (pictures) for liste cas : ', resArray);
                            resolve(resArray);
                        });
                        return [2 /*return*/];
                }
            });
        }); });
    };
    VisitService.prototype.syncVisitRaw = function (visitId) {
        var _this = this;
        var result = {};
        return this.sqlite.getRows('visite', '*', 'WHERE id_visite = ' + visitId)
            .then(function (visit) {
            result = visit[0];
            return _this.sqlite.getRows('visite_partie_ouvrage', '*', 'WHERE id_visite = ' + result['id_visite']);
        })
            .then(function (visitePo) {
            result['visite_partie_ouvrage'] = visitePo || [];
            var promisesEquip = result['visite_partie_ouvrage'].map(function (val, index) {
                return _this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = ' + val['id_visite_partie_ouvrage'])
                    .then(function (equipArray) {
                    var promisePhoto = equipArray.map(function (val) {
                        return _this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN (' + val['id_photo'] + ')');
                    });
                    return Promise.all(promisePhoto).then(function (arrayPhotoEquip) {
                        equipArray.forEach(function (val, index) {
                            equipArray[index]['visite_photo'] = arrayPhotoEquip[index];
                        });
                    });
                });
            });
            var promisesListeCas = result['visite_partie_ouvrage'].map(function (val, index) {
                return _this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = ' + val['id_visite_partie_ouvrage']);
            });
            return [promisesEquip, promisesListeCas];
        })
            .then(function (arrayPromises) {
            return [
                Promise.all(arrayPromises[0]).then(function (arrayEquip) {
                    //result['visite_partie_ouvrage']['visite_equipement'] = arrayEquip;
                    var promisesPictureEquip = arrayEquip.map(function (val, index) {
                        return _this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN (' + val['id_photo'] + ')');
                    });
                    return promisesPictureEquip;
                }),
                Promise.all(arrayPromises[1]).then(function (arrayListeCas) {
                    result['visite_partie_ouvrage']['visite_liste_cas'] = arrayListeCas;
                    var promisesPictureListeCas = arrayListeCas.map(function (val, index) {
                        return _this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN (' + val['id_photo'] + ')');
                    });
                    return promisesPictureListeCas;
                })
            ];
        })
            .then(function (arrayAllPromises) {
            arrayAllPromises[0];
        });
    };
    VisitService.prototype.asyncForEach = function (array, callback) {
        return new Promise(function (resolve) {
            var result;
            for (var i = 0; i < array.length; i++) {
                if (i === array.length - 1) {
                    resolve(array);
                }
            }
        });
    };
    VisitService.prototype.syncVisitAsync = function (idVisit) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var e_1, _a, visit, result, _b, _c, indexEquip, indexListCas, _d, _e, val, visiteEquip, listeCas, e_1_1;
            var _this = this;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_f) {
                switch (_f.label) {
                    case 0: return [4 /*yield*/, this.sqlite.getRows('visite', '*', 'WHERE id_visite = ' + idVisit)];
                    case 1:
                        visit = _f.sent();
                        result = visit[0];
                        _b = result[0];
                        _c = 'visite_partie_ouvrage';
                        return [4 /*yield*/, this.sqlite.getRows('visite_partie_ouvrage', '*', 'WHERE id_visite = ' + idVisit)];
                    case 2:
                        _b[_c] = _f.sent();
                        indexListCas = 0;
                        _f.label = 3;
                    case 3:
                        _f.trys.push([3, 10, 11, 16]);
                        _d = tslib__WEBPACK_IMPORTED_MODULE_0__["__asyncValues"](result[0]['visite_partie_ouvrage']);
                        _f.label = 4;
                    case 4: return [4 /*yield*/, _d.next()];
                    case 5:
                        if (!(_e = _f.sent(), !_e.done)) return [3 /*break*/, 9];
                        val = _e.value;
                        return [4 /*yield*/, this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = ' + val['id_visite_partie_ouvrage'])];
                    case 6:
                        visiteEquip = (_f.sent()) || [];
                        visiteEquip.forEach(function (value, index) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                            var photo;
                            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN (' + value['id_photo'] + ')')];
                                    case 1:
                                        photo = _a.sent();
                                        visiteEquip[index]['visite_photo'] = photo || [];
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        return [4 /*yield*/, this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = ' + val['id_visite_partie_ouvrage'])];
                    case 7:
                        listeCas = (_f.sent()) || [];
                        listeCas.forEach(function (value, index) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                            var photo;
                            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                                switch (_a.label) {
                                    case 0: return [4 /*yield*/, this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN (' + value['id_photo'] + ')')];
                                    case 1:
                                        photo = _a.sent();
                                        listeCas[index]['visite_photo'] = photo || [];
                                        return [2 /*return*/];
                                }
                            });
                        }); });
                        result[0]['visite_partie_ouvrage'][indexEquip++]['visite_equipement'] = visiteEquip;
                        result[0]['visite_partie_ouvrage'][indexListCas++]['visite_liste_cas'] = listeCas;
                        _f.label = 8;
                    case 8: return [3 /*break*/, 4];
                    case 9: return [3 /*break*/, 16];
                    case 10:
                        e_1_1 = _f.sent();
                        e_1 = { error: e_1_1 };
                        return [3 /*break*/, 16];
                    case 11:
                        _f.trys.push([11, , 14, 15]);
                        if (!(_e && !_e.done && (_a = _d.return))) return [3 /*break*/, 13];
                        return [4 /*yield*/, _a.call(_d)];
                    case 12:
                        _f.sent();
                        _f.label = 13;
                    case 13: return [3 /*break*/, 15];
                    case 14:
                        if (e_1) throw e_1.error;
                        return [7 /*endfinally*/];
                    case 15: return [7 /*endfinally*/];
                    case 16: 
                    /* result[0]['visite_partie_ouvrage'].forEach(async (val, index) => {
                      let visiteEquip = await this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = ' + val['id_visite_partie_ouvrage']) || [];
                      visiteEquip.forEach(async (value, index) => {
                        let photo = await this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN ('+value['id_photo']+')');
                        visiteEquip[index]['visite_photo'] = photo || [];
                      })
                      let listeCas = await this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = ' + val['id_visite_partie_ouvrage']) || [];
                      listeCas.forEach(async (value, index) => {
                        let photo = await this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN ('+value['id_photo']+')');
                        listeCas[index]['visite_photo'] = photo || [];
                      })
                      result[0]['visite_partie_ouvrage'][index]['visite_equipement'] = visiteEquip;
                      result[0]['visite_partie_ouvrage'][index]['visite_liste_cas'] = listeCas;
                    }) */
                    return [2 /*return*/, result[0]];
                }
            });
        });
    };
    /* -- END :: SYNC METHODS */
    /**
     * Fake Service -- Will be replaced by storage access
     */
    VisitService.prototype.getAgentId = function () {
        return this.storage.get('clientId');
    };
    VisitService.prototype.getPOData = function (idPo) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var equip, listCas;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = ' + idPo)];
                    case 1:
                        equip = _a.sent();
                        return [4 /*yield*/, this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = ' + idPo)];
                    case 2:
                        listCas = _a.sent();
                        return [2 /*return*/, {
                                equipement: equip,
                                listeCas: listCas
                            }];
                }
            });
        });
    };
    VisitService.prototype.getUtilitaryTables = function () {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var cases, rates;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.sqlite.getRows('visite_cas')];
                    case 1:
                        cases = _a.sent();
                        return [4 /*yield*/, this.sqlite.getRows('visite_note')];
                    case 2:
                        rates = _a.sent();
                        return [2 /*return*/, {
                                cas: cases,
                                note: rates
                            }];
                }
            });
        });
    };
    /* Offlines features */
    VisitService.prototype.updateVisiteData = function (value, table, column, conditionColumn, conditionValue) {
        // "UPDATE table SET column1 = value1 WHERE id = 1"
        return this.sqlite.updateOneColumn({ table: table, column: column, columnValue: value, conditionColumn: conditionColumn, conditionValue: conditionValue });
    };
    VisitService.prototype.fetchVisitCas = function () {
        var _this = this;
        var visitCas = new _entities_visit_cas__WEBPACK_IMPORTED_MODULE_8__["VisitCas"]();
        visitCas.fetchCases()
            .then(function (casesArray) {
            casesArray.forEach(function (value) {
                _this.constructQueryMultiple(value).then(function (queryArray) {
                    var columnQuery = queryArray[0];
                    var valueQuery = queryArray[1];
                    _this.sqlite.insertRow({ table: visitCas.tableName, column: columnQuery, value: valueQuery }).then(function (res) {
                        console.log("After insertion -> ", res);
                    });
                });
            });
        });
    };
    VisitService.prototype.constructVisitListeCas = function (idVisitPo) {
        var _this = this;
        if (idVisitPo === void 0) { idVisitPo = 4; }
        this.sqlite.getRows('visite_cas').then(function (casesArray) {
            console.log(casesArray);
            casesArray.forEach(function (visitCas) {
                var visitListCas = new _entities_visite_liste_cas__WEBPACK_IMPORTED_MODULE_10__["VisiteListeCas"](visitCas.id_visite_cas, idVisitPo);
                _this.constructInsertReplaceQuery(visitListCas)
                    .then(function (queryArray) {
                    console.log('queryArray', queryArray);
                    _this.sqlite.insertOrReplace({ table: visitListCas.tableName, column: queryArray[0], value: queryArray[1] })
                        .then(function (res) {
                        console.log('sql res', res);
                    });
                });
            }); /* END :: FOREACH */
        }); /* END :: PROMISE */
    };
    /* /!\ Should be place in sqlite service ! */
    VisitService.prototype.constructInsertReplaceQuery = function (entity) {
        var columns = [];
        var values = [];
        var keys = Object.keys(entity);
        return new Promise(function (resolve, reject) {
            keys.forEach(function (value, index) {
                if (value !== 'tableName') { //Prop in each entity to know where to insert in sqliteDatabase
                    columns.push(value);
                    values.push(entity[value]); //To respect integer syntax
                    if (keys.length - 1 === index) {
                        resolve([columns, values]); // End of our promise
                    }
                }
            });
        });
    };
    /**
     * /!\ Problem list :
     * - Know when a chaining promise IN a foreach are ALL dones (and so for nested promise)
     * - Set an execution time_out that display a toast 'Execution max_time exceeded'
     * -
     */
    VisitService.prototype.getVisitPoData = function (idPo) {
        // Select all From equip, visite_liste_cas WHERE id_visite_partie_ouvrage = idPO
        // Then : => equip = {id, name, status, id_note} ; liste_cas = {id, id_cas, id_note}
        /**
          SELECT e.* l.* p.*
          FROM visite_partie_ouvrage p
          INNER JOIN visite_equipement e ON e.id_visite_partie_ouvrage = p.id_visite_partie_ouvrage
          INNER JOIN visite_liste_cas l ON l.id_visite_partie_ouvrage = p.id_visite_partie_ouvrage
          WHERE p.id_visite_partie_ouvrage = idPo
        */
        /*
        let query1 = 'SELECT * FROM visite_equipement WHERE id_visite_partie_ouvrage = ' + idPo;
        let query2 = 'SELECT * FROM visite_liste_cas WHERE id_visite_partie_ouvrage = '+ idPo;
        let promises = [];
        promises.push(this.sqlite.getRows('visite_liste_cas','*', 'WHERE id_visite_partie_ouvrage = ' + idPo));
        promises.push(this.sqlite.getRows('visite_equipement','*', 'WHERE id_visite_partie_ouvrage = ' + idPo));
        return Promise.all(promises); */
        var _this = this;
        if (idPo === void 0) { idPo = 4; }
        var result = [];
        var conditionPo = 'SELECT * FROM visite_liste_cas WHERE id_visite_partie_ouvrage = ' + idPo;
        var conditionEquipement = 'SELECT * FROM visite_equipement WHERE id_visite_partie_ouvrage = ' + idPo;
        return new Promise(function (resolve, reject) {
            _this.sqlite.customQuery(conditionPo)
                .then(function (res) {
                console.log('res', res);
                result['cas'] = [];
                res.map(function (cas, i) {
                    console.log(cas);
                    var conditionNotable = 'SELECT * FROM visite_cas WHERE id_visite_cas = (' + cas.id_visite_cas + ')';
                    _this.sqlite.customQuery(conditionNotable)
                        .then(function (response) {
                        console.log(response);
                        cas.notable = response[0].notable;
                        cas.name = response[0].cas;
                        result['cas'][i] = cas;
                        _this.sqlite.customQuery(conditionEquipement)
                            .then(function (resp) {
                            result['equipement'] = resp;
                            resolve(result);
                        })
                            .catch(function (error) {
                            reject(error);
                        });
                    });
                });
            })
                .catch(function (err) {
                reject(err);
            });
        });
    };
    VisitService.prototype.getClosedVisit = function () {
        return this.sqlite.getRows('visite', '*', 'WHERE id_visite_status = 3');
    };
    VisitService.prototype.getVisitData = function (idVisit, status) {
        if (idVisit === void 0) { idVisit = 1; }
        if (status === void 0) { status = 2; }
        var condition = 'WHERE id_visite IN (SELECT id_visite FROM visite WHERE id_visite = ' + idVisit + ' AND id_visite_status = ' + status + ')';
        return this.sqlite.getRows('visite_partie_ouvrage', '*', condition);
    };
    VisitService.prototype.initVisit = function (idVisit, data) {
        var _this = this;
        if (data === void 0) { data = { nameAgent: 'NameAgent', date: '2019-11-19', comment: '' }; }
        // Update with sqlite service the visit with previous status ++
        // Specifiy in select two clause, id_visit = idVisit AND id_status = visit.status
        var setStatement = 'nom_client = \'' + data.nameAgent + '\',' +
            ' date = \'' + data.date + '\',' + ' commentaire = \'' + data.comment + '\'';
        return this.sqlite.updateRequest({ table: 'visite', setStmt: setStatement, conditionStmt: 'WHERE id_visite = ' +
                idVisit + ' AND id_visite_status = 1' })
            .then(function (res) {
            if (res.hasOwnProperty('rowsAffected')) {
                if (res.rowsAffected > 0) {
                    console.log('someRowsWereAffected');
                    console.log('update visite with id = ' + idVisit, res);
                    return _this.sqlite.updateIncrement({ table: 'visite',
                        column: 'id_visite_status',
                        incrementValue: 1,
                        condition: 'WHERE id_visite = ' + idVisit + ' AND id_visite_status = 1' })
                        .then(function (res) {
                        console.log('changed status visite with id = ' + idVisit, res);
                        return res;
                    })
                        .catch(function (err) {
                        console.error(err);
                        throw Error('Error occured : ' + JSON.stringify(err));
                    });
                }
                else {
                    throw Error('Can\'t update visite table before updating status ; Init abort');
                }
            }
            else {
                throw Error('Failing while trying to update visite table at id ' + idVisit + '; No modification were made !');
            }
        }).catch(function (error) {
            console.error(error);
            throw Error("Error occured : " + JSON.stringify(error));
        });
        /* return this.sqlite.updateRequest({table: 'visite', setStmt: 'nom_agent = \'' + data.nameAgent +'\', date = \''+ data.date + '\', commentaire = \'' + data.comment, conditionStmt:'\' WHERE id_visite = '+idVisit+' AND id_visite_status = 1'})
        .then((res) => {
          console.log(res);
          if(res.hasOwnProperty('rowsAffected')){
            if(res.rowsAffected != 0){
              console.log('some row are affected');
            }
          }
        }).catch((err) => {
          console.error(err);
        }) */
    };
    VisitService.prototype.fetchVisit = function (type) {
        // id_visite_status : 1 = créer , 2 = initialiser, 3 = clôturer
        var condition = type === 'init' ? 1 : type === 'resume' ? 2 : null;
        return this.sqlite.getRows('visite', '*', 'WHERE id_visite_status = ' + condition);
    };
    /* Connection required features */
    VisitService.prototype.visitPrepareOption = function (name, id) {
        var result = [];
        var uri = "preparation/" + name + "/";
        // return this.api.apiGetRequest(uri,id);
        var nameRegex = RegExp('nom', 'i');
        var idRegex = RegExp('id', 'i');
        return this.api.apiGetRequest(uri, id)
            .then(function (apiResponse) {
            for (var object in apiResponse) {
                var select = {
                    name: '',
                    id: ''
                };
                for (var prop in apiResponse[object]) {
                    if (nameRegex.test(prop)) {
                        select.name = apiResponse[object][prop];
                    }
                    else if (idRegex.test(prop)) {
                        select.id = apiResponse[object][prop];
                    }
                }
                result.push(select);
            }
            return result;
        }).then(function (toSort) {
            toSort.sort(function (a, b) {
                return a.name > b.name ? 1 : -1;
            });
            return toSort;
        })
            .catch(function (apiError) {
            console.error(apiError);
        });
    };
    VisitService.prototype.fetchVisitData = function (idOuvrage) {
        var _this = this;
        if (idOuvrage === void 0) { idOuvrage = 2925; }
        /* Call to Api after form submit, passing id Ouvrage */
        return this.api.apiGetRequest('visite/visite/', idOuvrage)
            .then(function (jsonData) {
            console.log(jsonData);
            _this.storage.get('clientId').then(function (response) {
                var idAgent = _this.tokenAgent.id || response;
                var visit = new _entities_visit__WEBPACK_IMPORTED_MODULE_4__["Visit"](jsonData.id_ouvrage, idAgent, jsonData.nom_ouvrage);
                console.log('Cette visite object = ', visit);
                var partieOuvrageData = jsonData.hasOwnProperty('visite_partie_ouvrage') ? jsonData.visite_partie_ouvrage : false;
                // Will call the sqlite service and continue to pass our object
                _this.constructVisit(visit, partieOuvrageData)
                    .then(function (data) {
                    /* Here the visit is created, now create the next table 'visite_partie_ouvrage' */
                    console.log(data[0]);
                    var visit = data[0];
                    var partieOuvrageArray = data[1];
                    partieOuvrageArray.forEach(function (value) {
                        /* Here all 'visite_partie_ouvrage' rows are inserted, and will create the next table 'visite_equipement' */
                        // To change for 'nom_partie_ouvrage'
                        var partieOuvrage = new _entities_visit_partie_ouvrage__WEBPACK_IMPORTED_MODULE_5__["VisitPartieOuvrage"](visit.id_visite, value.id_visite_partie_ouvrage, value.nom_ouvrage);
                        var equipementData = value.hasOwnProperty('visite_equipement') ? value.visite_equipement : false;
                        _this.constructVisitPO(partieOuvrage, equipementData)
                            .then(function (data) {
                            console.log(data[0]);
                            var partieOuvrage = data[0];
                            var visitEquipementArray = data[1];
                            _this.constructVisitListeCas(partieOuvrage.id_visite_partie_ouvrage);
                            if (visitEquipementArray !== false) {
                                visitEquipementArray.forEach(function (value) {
                                    /* Finally all the equipement */
                                    var equipement = new _entities_visit_equipement__WEBPACK_IMPORTED_MODULE_6__["VisitEquipement"](partieOuvrage.id_visite_partie_ouvrage, value.id_visite_equipement, value.nom_equipement);
                                    _this.constructVisitEquipement(equipement)
                                        .then(function (data) {
                                        console.log(data);
                                    });
                                });
                            }
                            else {
                                //return visit.id_visite;
                            }
                        });
                    });
                });
            });
        })
            .catch(function (error) {
            console.error('', error);
        });
    };
    VisitService.prototype.constructQueryMultiple = function (entity) {
        var columns = '';
        var values = '(';
        var keys = Object.keys(entity);
        return new Promise(function (resolve, reject) {
            keys.forEach(function (value, index) {
                if (value !== 'tableName') { // Prop in each entity to know where to insert in sqliteDatabase
                    columns += ' "' + value + '"';
                    values += isNaN(entity[value]) ? ' "' + entity[value] + '"' : ' ' + entity[value]; // To respect integer syntax
                    if (keys.length - 1 === index) {
                        columns += '';
                        values += ')';
                        resolve([columns, values]); // End of our promise
                    }
                    else {
                        columns += ',';
                        values += ',';
                    }
                }
            });
        });
    };
    VisitService.prototype.constructVisit = function (visitData, partieOuvrageData) {
        var _this = this;
        visitData.id_visite_status = 1;
        return new Promise(function (resolve, reject) {
            _this.constructQueryMultiple(visitData)
                .then(function (data) {
                var queryColumn = data[0];
                var queryValues = data[1];
                //alert('To insert \ncolumns : '+queryColumn+' ; values : '+queryValues);
                _this.sqlite.insertRow({ table: visitData.tableName, column: queryColumn, value: queryValues })
                    .then(function (response) {
                    console.log(response);
                    if (response.hasOwnProperty('rowsAffected')) {
                        // alert("Insert successful ! Res -> "+JSON.stringify(response["insertId"]));
                        _this.toast.logMessage('Insertion effectuée avec succès !', 'primary');
                        visitData.id_visite = response.insertId;
                        resolve([visitData, partieOuvrageData]);
                    }
                    else {
                        throw ['Error occured while inserting in database', ' Code : ' + response.code + ' , ' + response.message];
                    }
                });
            })
                .catch(function (errorArray) {
                // alert("Error : "+error);
                console.log(errorArray[1]);
                _this.toast.logMessage(errorArray[0], 'danger');
            });
        });
        /* alert(this.sqlite.insertRow({table: visitData.tableName, })) */
        /* SQL query to make table visit and get back id visite */
        /* return new Promise((resolve, reject)=>{
          setTimeout(()=>{
            visitData.id_visite = Visit.count++;
            resolve([visitData, partieOuvrageData]);
            // Reject will be about the sqlite service
          },500);
        }) */
    };
    VisitService.prototype.vacuumPoData = function () {
    };
    VisitService.prototype.vacuumPhoto = function (entity) {
        if (entity.hasOwnProperty('id_photo')) {
            return this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN (' + entity.id_photo + ')')
                .then(function (photoArray) {
                return photoArray;
            });
        }
        else {
            return [];
        }
    };
    VisitService.prototype.endVisit2 = function (idVisit) {
        var _this = this;
        if (idVisit === void 0) { idVisit = 2; }
        var promisesEquip = [];
        var promisesListeCas = [];
        var promiseVisite = new Promise(function (resolve, reject) {
            /* Get the visit */
            _this.sqlite.getRows('visite', '*', 'WHERE id_visite = ' + idVisit + '')
                .then(function (result) {
                console.log('visite query : ', result);
                resolve(result[0]);
            });
        })
            .then(function (visit) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
            var json, partieOuvrage;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0:
                        json = visit;
                        return [4 /*yield*/, this.sqlite.getRows('visite_partie_ouvrage', '*', 'WHERE id_visite = ' + idVisit)
                                .then(function (result) {
                                console.log('partie ouvrage query :', result);
                                console.log('... and visit param : ', visit);
                                return result;
                            })];
                    case 1:
                        partieOuvrage = _a.sent();
                        return [2 /*return*/, tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, json, { 'visite_partie_ouvrage': partieOuvrage })];
                }
            });
        }); })
            .then(function (result) {
            console.log('pass json object after visite_partie_ouvrage added :', result);
            var json = result;
            json['visite_partie_ouvrage'] = result['visite_partie_ouvrage'].map(function (partieOuvrage, i) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                var equip, liste_cas;
                var _this = this;
                return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = ' + partieOuvrage.id_visite_partie_ouvrage)
                                .then(function (res) {
                                if (res) {
                                    res.map(function (equip) { return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](_this, void 0, void 0, function () {
                                        return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                                            if (equip) {
                                            }
                                            else {
                                            }
                                            return [2 /*return*/];
                                        });
                                    }); });
                                }
                                else {
                                    return [];
                                }
                            })];
                        case 1:
                            equip = _a.sent();
                            return [4 /*yield*/, this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = ' + partieOuvrage.id_visite_partie_ouvrage)
                                    .then(function (res) {
                                    return res;
                                })];
                        case 2:
                            liste_cas = _a.sent();
                            return [2 /*return*/, tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, partieOuvrage, { 'visite_equipement': equip, 'visite_liste_cas': liste_cas })];
                    }
                });
            }); });
            return json;
        })
            .then(function (result) {
            console.log('after added equip and liste_cas', result);
        });
    };
    /*
      return this.sqlite.getRows('visite', '*', 'WHERE id_visite = '+idVisit+'')
      .then((visitArray: Visit[]) => {
        console.log('Get visit, Step 1 / ? ', visitArray);
        const visit: Visit = visitArray[0];
        return {
          ...visit,
          'visite_partie_ouvrage':
            this.sqlite.getRows('visite_partie_ouvrage', '*', 'WHERE id_visite = '+visit.id_visite)
            .then((partieOuvrageArray: VisitPartieOuvrage[]) => {
            console.log('Get POs, Step 2 / ? ', partieOuvrageArray);
            if(partieOuvrageArray){
              return partieOuvrageArray.map((partieOuvrage, index) => {
                return {
                  ...partieOuvrage,
                  'visite_equipement':
                    this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = '+partieOuvrage.id_visite_partie_ouvrage)
                    .then((equipementArray: VisitEquipement[]) => {
                    console.log('Get Equips, Step 2.1.'+ (index+1) +' / ?', equipementArray);
                    if(equipementArray){
                      return equipementArray.map((equipement, equipIndex) => {
                        if(equipement.id_photo){
                          this.sqlite.getRows('visite_photo','*', 'WHERE id_visite_photo IN ('+ equipement.id_photo + ')')
                          .then((photoArray: VisitPhoto[]) => {
                            console.log('Get Photos, Step 3.1.'+(equipIndex+1)+'.'+(index+1)+' / ?', photoArray);
                            return {...equipement, 'visite_photo': photoArray };
                          })
                        }else{
                          return {...equipement, 'visite_photo': ['empty photo_array']};
                        }
                      });
                    }else{
                      return ['empty equip_array'];
                    }
                  }),
                  'visite_liste_cas':
                  this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = '+partieOuvrage.id_visite_partie_ouvrage)
                  .then((listeCasArray: VisiteListeCas[]) => {
                    console.log('Get Cases, Step 2.2.'+ (index+1) +' / ?', listeCasArray);
                    if(listeCasArray){
                      return listeCasArray.map((listeCas, casIndex) => {
                        if(listeCas.id_photo){
                          this.sqlite.getRows('visite_photo','*', 'WHERE id_visite_photo IN ('+ listeCas.id_photo + ')')
                          .then((photoArray: VisitPhoto[]) => {
                            console.log('Get Photos, Step 3.2.'+(casIndex+1)+'.'+(index+1)+' / ?', photoArray);
                            return {...listeCas, 'visite_photo': photoArray};
                          })
                        }else{
                          return {...listeCas, 'visite_photo': ['empty photo_array']};
                        }
                      })
                    }else{
                      return ['empty liste_cas_array'];
                    }
                  })
                };
              })
            }else{
              return ['empty partie_ouvrage_array'];
            }
          })
        };
      }) */
    VisitService.prototype.iterateThroughLevel = function () {
    };
    VisitService.prototype.megaRecursive = function (idVisit, dataToSend) {
        var _this = this;
        if (dataToSend === void 0) { dataToSend = {}; }
        var tableToSend = {
            'visite_partie_ouvrage': {
                'visite_equipement': {
                    'visite_photo': {}
                },
                'visite_liste_cas': {
                    'visite_photo': {}
                }
            }
        };
        if (Object.keys(dataToSend).length > 0) {
            for (var prop in tableToSend) {
            }
        }
        else {
            this.sqlite.getRows('visite', '*', 'WHERE id_visite = ' + idVisit)
                .then(function (res) {
                _this.megaRecursive(idVisit, res[0]);
            });
        }
    };
    VisitService.prototype.endVisit = function (idVisit) {
        var _this = this;
        var tableToSend = {
            'visite': {
                'visite_partie_ouvrage': {
                    'visite_equipement': {
                        'visite_photo': {}
                    },
                    'visite_liste_cas': {
                        'visite_photo': {}
                    }
                }
            }
        };
        var visit = new _entities_visit__WEBPACK_IMPORTED_MODULE_4__["Visit"](1, 1, 'NAEM');
        return this.sqlite.getRows('visite', '*', 'WHERE id_visite = ' + idVisit + '')
            .then(function (visitArray) {
            console.log('Get visit, Step 1 / ? ', visitArray);
            var visit = visitArray[0];
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, visit, { 'visite_partie_ouvrage': _this.sqlite.getRows('visite_partie_ouvrage', '*', 'WHERE id_visite = ' + visit.id_visite)
                    .then(function (partieOuvrageArray) {
                    console.log('Get POs, Step 2 / ? ', partieOuvrageArray);
                    if (partieOuvrageArray) {
                        return partieOuvrageArray.map(function (partieOuvrage, index) {
                            return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, partieOuvrage, { 'visite_equipement': 
                                /* Equip for PO */
                                _this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = ' + partieOuvrage.id_visite_partie_ouvrage)
                                    .then(function (equipementArray) {
                                    console.log('Get Equips, Step 2.1.' + (index + 1) + ' / ?', equipementArray);
                                    if (equipementArray) {
                                        return equipementArray.map(function (equipement, equipIndex) {
                                            if (equipement.id_photo) {
                                                _this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN (' + equipement.id_photo + ')')
                                                    .then(function (photoArray) {
                                                    console.log('Get Photos, Step 3.1.' + (equipIndex + 1) + '.' + (index + 1) + ' / ?', photoArray);
                                                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, equipement, { 'visite_photo': photoArray });
                                                });
                                            }
                                            else {
                                                return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, equipement, { 'visite_photo': ['empty photo_array'] });
                                            }
                                        });
                                    }
                                    else {
                                        return ['empty equip_array'];
                                    }
                                }), 'visite_liste_cas': 
                                /* Liste Cas for PO */
                                _this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = ' + partieOuvrage.id_visite_partie_ouvrage)
                                    .then(function (listeCasArray) {
                                    console.log('Get Cases, Step 2.2.' + (index + 1) + ' / ?', listeCasArray);
                                    if (listeCasArray) {
                                        return listeCasArray.map(function (listeCas, casIndex) {
                                            if (listeCas.id_photo) {
                                                _this.sqlite.getRows('visite_photo', '*', 'WHERE id_visite_photo IN (' + listeCas.id_photo + ')')
                                                    .then(function (photoArray) {
                                                    console.log('Get Photos, Step 3.2.' + (casIndex + 1) + '.' + (index + 1) + ' / ?', photoArray);
                                                    return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, listeCas, { 'visite_photo': photoArray });
                                                });
                                            }
                                            else {
                                                return tslib__WEBPACK_IMPORTED_MODULE_0__["__assign"]({}, listeCas, { 'visite_photo': ['empty photo_array'] });
                                            }
                                        });
                                    }
                                    else {
                                        return ['empty liste_cas_array'];
                                    }
                                }) });
                        });
                    }
                    else {
                        return ['empty partie_ouvrage_array'];
                    }
                }) });
        });
        //return 
        /* const visite_partie_ouvrage: VisitPartieOuvrage[] = await this.sqlite.getRows('visite_partie_ouvrage','*','WHERE id_visite = '+visit.id_visite); */
        /* let visite_equipement: VisitEquipement[] = visite_partie_ouvrage.map(async (value, index) => {
          return this.sqlite.getRows('visite_equipement','*', 'WHERE id_visite_partie_ouvrage = '+value.id_visite_partie_ouvrage);
        }) */
        /* const fetchPoData = new Promise((resolve, reject)=>{
          visite_partie_ouvrage.forEach(async (valPo, index) => { */
        /* Object.defineProperties(visite_partie_ouvrage[index], {
          'visite_equipement': {
            valPo: await this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = '+valPo.id_visite_partie_ouvrage),
            writable: true
          },
          'visite_liste_cas': {
            valPo: await this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = '+valPo.id_visite_partie_ouvrage),
            writable: true
          }
        }); */
        /* visite_partie_ouvrage[index].setEquipement(
          await this.sqlite.getRows('visite_equipement', '*', 'WHERE id_visite_partie_ouvrage = '+valPo.id_visite_partie_ouvrage)
        );
        visite_partie_ouvrage[index].setListeCas(
          await this.sqlite.getRows('visite_liste_cas', '*', 'WHERE id_visite_partie_ouvrage = '+valPo.id_visite_partie_ouvrage)
        )
        if(index === visite_partie_ouvrage.length - 1){
          resolve('All data for each partie ouvrage get binded !');
        }
      });
    }); */
        /*
            const fetchPhotoPO = new Promise((resolve) => {
              visite_partie_ouvrage.forEach((valPo, index) => {
                visite_partie_ouvrage[index].getEquipement().map(async (valEquip: VisitEquipement, idx: number) => {
                  visite_partie_ouvrage[index].getEquipement()[idx].setPhoto(
                    await this.sqlite.getRows('visite_photo','*','WHERE id_visite_photo IN (' + valEquip.id_photo + ')')
                  );
                  if(idx === visite_partie_ouvrage[index].getEquipement().length - 1){
                    resolve(true);
                  }
                });
              });
            }); */
        /* const fetchPhotoEquip = new Promise((resolve) => {
          visite_partie_ouvrage.forEach((valPo, index) => {
            visite_partie_ouvrage[index]['visite_liste_cas'].map(async (valCas: VisiteListeCas, indx: number) => {
              visite_partie_ouvrage[index]['visite_liste_cas'][indx]['visite_photo'] = await this.sqlite.getRows('visite_photo','*','WHERE id_visite_photo IN (' + valCasvalEquip.id_photo + ')');
    
              if(indx === visite_partie_ouvrage[index]['visite_liste_cas'].length - 1){
                resolve(true);
              }
            });
          })
        }); */
        /*
        if(resolvePO === true && resolveEquip === true){
          resolve('All photos for each partie ouvrage equip & list cas were binded succesfuly !');
        }else{
          if((resolvePO !== true) && (resolveEquip !== true)){
            throw Error('Error occured while trying to bind, photos with equipment AND listCas !');
          }else if((resolvePO !== true) || (resolveEquip !== true)){
            let customError = resolvePO !== true ? 'listCas' : 'equipment';
            throw Error('Error occured while trying to bind photos with ' + customError + ' ... Aborted !');
          }
        }
        */
        /* return fetchPoData
        .then((res: any) => {
          console.log('Equip & List Cas binded ! Log : ', res);
          fetchPhotoEquip
          .then((res: any)=>{
            console.log('Photos binded ! Log : ', res);
            fetchPhotoPO.then((res: any)=>{
              console.log('Photos binded ! Log : ', res);
              const result = {...visit, visite_partie_ouvrage: visite_partie_ouvrage};
              Object.defineProperty(visit, 'visite_partie_ouvrage',{
                value: visite_partie_ouvrage,
                writable: false
              });
              console.log(result);
              return result
            });
            
          })
        }) */
    };
    VisitService.prototype.uploadVisit = function () {
    };
    VisitService.prototype.destroyVisit = function () {
    };
    VisitService.prototype.constructVisitPO = function (partieOuvrage, equipementData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.constructQueryMultiple(partieOuvrage)
                .then(function (data) {
                console.log('query made : ', data);
                var queryColumn = data[0];
                var queryValues = data[1];
                //alert('To insert \ncolumns : '+queryColumn+' ; values : '+queryValues);
                _this.sqlite.insertRow({ table: partieOuvrage.tableName, column: queryColumn, value: queryValues })
                    .then(function (response) {
                    console.log(response);
                    if (response.hasOwnProperty('rowsAffected')) {
                        // alert("Insert successful ! Res -> "+JSON.stringify(response["insertId"]));
                        _this.toast.logMessage('Insertion effectuée avec succès !', 'primary');
                        partieOuvrage.id_visite_partie_ouvrage = response.insertId;
                        resolve([partieOuvrage, equipementData]);
                    }
                    else {
                        throw ['Error occured while inserting in database', ' Code : ' + response.code + ' , ' + response.message];
                    }
                })
                    .catch(function (errorArray) {
                    // alert("Error : "+error);
                    console.log(errorArray[1]);
                    _this.toast.logMessage(errorArray[0], 'danger');
                });
            });
        });
        /* SQL query to make table visite_partie_ouvrage and get back id or row inserted */
        /* return new Promise((resolve, reject)=>{
          setTimeout(()=>{
            partieOuvrage.id_visite_partie_ouvrage = VisitPartieOuvrage.count++;
            resolve([partieOuvrage, equipementData]); // Can't pass two argument to the resolve callback
          },500);
        }) */
    };
    VisitService.prototype.constructVisitEquipement = function (equipement) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.constructQueryMultiple(equipement)
                .then(function (data) {
                var queryColumn = data[0];
                var queryValues = data[1];
                //alert('To insert \ncolumns : '+queryColumn+' ; values : '+queryValues);
                _this.sqlite.insertRow({ table: equipement.tableName, column: queryColumn, value: queryValues })
                    .then(function (response) {
                    console.log(response);
                    if (response.hasOwnProperty('rowsAffected')) {
                        // alert("Insert successful ! Res -> "+JSON.stringify(response["insertId"]));
                        _this.toast.logMessage('Insertion effectuée avec succès !', 'primary');
                        equipement.id_visite_equipement = response.insertId;
                        resolve(equipement);
                    }
                    else {
                        throw ['Error occured while inserting in database', ' Code : ' + response.code + ' , ' + response.message];
                    }
                })
                    .catch(function (errorArray) {
                    // alert("Error : "+error);
                    console.log(errorArray[1]);
                    _this.toast.logMessage(errorArray[0], 'danger');
                });
            });
        });
        /* return new Promise((resolve, reject)=>{
          setTimeout(()=>{
            equipement.id_visite_equipement = VisitEquipement.count++;
            resolve(equipement);
          },500);
        }) */
    };
    /**
     *
     * @param idOuvrage // 'Ouvrage' to make the visit on
     */
    VisitService.prototype.createVisit = function (idOuvrage) {
        this.sqlite.insertRow();
    };
    VisitService.ctorParameters = function () { return [
        { type: _sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_2__["SqliteService"] },
        { type: _api_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"] },
        { type: _toast_toast_service__WEBPACK_IMPORTED_MODULE_7__["ToastService"] },
        { type: _jwt_jwt_service__WEBPACK_IMPORTED_MODULE_9__["JwtService"] },
        { type: _ionic_storage__WEBPACK_IMPORTED_MODULE_11__["Storage"] }
    ]; };
    VisitService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_sqlite_sqlite_service__WEBPACK_IMPORTED_MODULE_2__["SqliteService"], _api_api_service__WEBPACK_IMPORTED_MODULE_3__["ApiService"], _toast_toast_service__WEBPACK_IMPORTED_MODULE_7__["ToastService"], _jwt_jwt_service__WEBPACK_IMPORTED_MODULE_9__["JwtService"], _ionic_storage__WEBPACK_IMPORTED_MODULE_11__["Storage"]])
    ], VisitService);
    return VisitService;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\bASKOU\Desktop\ashera_front\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es5.js.map